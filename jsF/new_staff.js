<!-- Include AJAX Framework -->
/* ---------------------------- */
/* XMLHTTPRequest Enable */
/* ---------------------------- */
function createObject() {
var request_type;
var browser = navigator.appName;
if(browser == "Microsoft Internet Explorer"){
request_type = new ActiveXObject("Microsoft.XMLHTTP");
}else{
request_type = new XMLHttpRequest();
}
return request_type;
}

var http = createObject();
/* -------------------------- */
/* INSERT */
/* -------------------------- */
/* Required: var nocache is a random number to add to request. This value solve an Internet Explorer cache issue */
var nocache = 0;
function insert() {
// Optional: Show a waiting message in the layer with ID login_response
document.getElementById('insert_response').innerHTML = "Just a second..."
// Required: verify that all fileds is not empty. Use encodeURI() to solve some issues about character encoding.
var name= encodeURI(document.getElementById('name').value);
var email = encodeURI(document.getElementById('email').value);
var phone = encodeURI(document.getElementById('phone').value);
var department = encodeURI(document.getElementById('department').value);
var pass = encodeURI(document.getElementById('pass').value);
/*var email = encodeURI(document.getElementById('email').value);
var tele = encodeURI(document.getElementById('tele').value);
var type_of_isue = encodeURI(document.getElementById('type_of_isue').value);
*/
// Set te random number to add to URL request'&email=' +email+'&tele='+tele+'&type_of_isue=' +type_of_isue+'&occ_date='+occ_date+'&filterContainer='+filterContainer+'&caller='+caller+'&email_no='+email_no+'&tick_src='+tick_src+'&contact='+contact+'&summary='+summary+'&department='+department+'&tat='+tat+
nocache = Math.random();
// Pass the login variables like URL variable
http.open('get', 'new_staff_add.php?name='+name+'&email=' +email+'&phone=' +phone+'&department='+department+'&pass='+pass+'&nocache = '+nocache);
http.onreadystatechange = insertReply;
http.send(null);
}
function insertReply() {
if(http.readyState == 4){
var response = http.responseText;
// else if login is ok show a message: "Site added+ site URL".
document.getElementById('insert_response').innerHTML = 'New staff add successful:'+response;
location.reload();
}
}