<?php
include("../include/config.php");

session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../login.php");
exit();

}

	if(isset($_GET['int_id']))
	{
  $int_id = $_GET['int_id'];
	}
if(isset($_POST['action']) && $_POST['action'] == 'submitform')
{
	
	
	$q1=$_POST['q1'];
	$int_id=$_POST['int_id'];
	$lid=$_POST['lid'];
	
	$sql="UPDATE qc_data SET Q1=:val WHERE id =:id";
$stmt = dbConnect()->prepare($sql);                                 
$stmt->bindParam(':val',$q1, PDO::PARAM_STR);
$stmt->bindParam(':id',$int_id, PDO::PARAM_STR);  
$stmt->execute(); 
	$msg="Question 1 Information Saved Successfully!";


if($q1==1)
{
	header("location:q4.php?int_id=$int_id");
}

if($q1==3)
{
	header("location:end.php?int_id=$int_id");
}
}

$query_getbank = dbConnect()->prepare("SELECT qc_data.id,qc_data.interviewer,qc_data.phone,qc_data.lid FROM  qc_data WHERE  qc_data.id = '".$int_id."'");
$query_getbank->execute();
$row_getbank=$query_getbank->fetch();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- CSS main application styling. -->
    <link rel="icon" type="image/ico" href="../uploadedfiles/school_logo/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
    <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
    <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
    <link rel="stylesheet" type="text/css" href="../css/formelements.css" />
    <link rel="stylesheet" href="../css1/coda-slider-2.0.css" type="text/css" media="screen" />  
     <link rel="stylesheet" href="css/BeatPicker.min.css"/>
 
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/BeatPicker.min.js"></script>
     <script type="text/javascript" src="../../js/js/jquery-1.7.1.min.js"></script>
      <script type="text/javascript" src="../../js/js/chart/highcharts.js"></script>
    <script type="text/javascript" src="../../js/js/custom-form-elements.js"></script>   
   </script>
    <script type="text/javascript" src="../../js/js/jquery-ui.min.js"></script>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <script>
	$(document).ready(function() {
	$("#lodrop").click(function(){
	
            	if ($("#account_drop").is(':hidden')){
                	$("#account_drop").show();
				}
            	else{
                	$("#account_drop").hide();
            	}
            return false;
       			 });
				  $('#account_drop').click(function(e) {
            		e.stopPropagation();
        			});
        		$(document).click(function() {
					if (!$("#account_drop").is(':hidden')){
            		$('#account_drop').hide();
					}
        			});	
                
});
</script>

<script type="text/javascript">
$(document).ready(function()
{
$(".reporting_manager").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;

$.ajax
({
type: "POST",
url: "ajax_city.php",
data: dataString,
cache: false,
success: function(html)
{
$(".reporting_lead").html(html);
} 
});

});
});
</script>


<script>
$(document).ready(function() {
  $(".nav_drop_but").click(function() {
  $(".navigationbtm_wrapper_outer").slideToggle();
	});
});
</script>

<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<link type="text/css" href="../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
  <script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		$('#exdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>

    
</head>
<title>::Rapid Smart qc_data on male and female farmers’ satisfaction with plant clinic visits ::</title>
<body>
<div class="wrapper">


    <div class="header">
     
   <div class="lo_drop" id="account_drop">
     <div class="lo_drop_hov"></div> 
     	<div class="lo_name">
        <?php ?><?php ?>
 <span> <?php echo $_SESSION['name']; ?> </span>
            <div class="clear"></div>
        </div>
    <ul>
        	<li><a href="profile.php"><?php echo 'My Account';?></li>
            <li><a href="settings.php"><?php echo 'Settings';?></a></li>
            <li> <a href="../logout.php"><?php echo 'Logout';?></a></li>
        </ul>
     </div>
     
     
   
	
    
        	<div class="logo">
            <a href="index.php"><img src="../images/logo-plantwise.png" alt=""  border="0" />		</a> </div>
            
			
			 <div class="">
            
<?php include('app_nav.php');?>
               
            </div>
    
    
      </div>
     
    
     
    <div class="midnav">
    
   
        <a class="first-letter"> Home</a>
		 <span>Leads Management</span>
		   <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
     </div>

     
     <div class="container">
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
  <?php include('../left_side.php');?>
    
    </td>
    <td valign="top">
	 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top" width="75%"><div style="padding-left:20px; padding-right:10px;">


<h3></h3>


<div class="formCon" >

<div class="">


 <form name="frmRespondent" action="q3.php" enctype="multipart/form-data" method="post" >  
     
            <table width="708" align="center">
              <tr>
                <!--<td colspan="2" align="right"><strong>QN:
                  <?php $sno = str_pad($row_getbank['id'],6,"0",STR_PAD_LEFT); echo $sno; ?>            
                  <input type="hidden" name="qsno" value="<?php echo $row_getbank['id']; ?>" />
                </strong></td>-->
              </tr>
              <tr>
                <td colspan="2" align="right"><div align="left"><strong>
				
			Q2.Are you willing to conduct the interview if we call you back at a date & time of your convenience?</a>
	 </strong>
</tr>
<br/><br/>
             <tr>
                <td width="263" align="left">
                  <label>
                    <input type="radio" onclick = "showTable(1)" name="q2" value="1" id="intro_0" required />
                 Yes</label>
                  
                </td>
                <td width="433" >&nbsp;</td>
              </tr>
			  
			  <tr style="display:none" id='p1'>
                <td colspan="2" align="right"><div align="left"><strong>Q3.Please indicate a convenient date & Time to call you back</strong></div></td>
              </tr>
			   <tr style="display:none;text-align:center"  id='p3'>
                <td>
                  <label>
                    <input type="radio"  name="q3" value="1" id="op1" required />
                 Yes</label>
                </td>
              </tr>
              <tr style="display:none;text-align:right"  id='p2'>
                  <td><label>
                Convenient Date/Time  </label>
            <label for="textfield"></label>
            <input type="text" name="callback" id="datepicker" size=""/></td>
			<td><select name="time" id="time">
              <option value="">Choose</option>
              <option value="7:00am">7:00am</option>
              <option value="8:00am">8:00am</option>
              <option value="9:00am">9:00am</option>
              <option value="10:00am">10:00am</option>
              <option value="11:00am">11:00am</option>
              <option value="12:00noon">12:00noon</option>
              <option value="01:00pm">01:00pm</option>
              <option value="02:00pm">02:00pm</option>
              <option value="03:00pm">03:00pm</option>
              <option value="04:00pm">04:00pm</option>
              <option value="05:00pm">05:00pm</option>
              <option value="06:00pm">06:00pm</option>
              <option value="07:00pm">07:00pm</option>
              <option value="08:00pm">08:00pm</option>
              <option value="09:00pm">09:00pm</option>
            </select></td>
		
                 
                  <br/>
                </p></td>
                <td >&nbsp;</td>
              </tr>
			  
			    <tr style="display:none;text-align:center"  id='p4'>
                <td>
                  <label>
                    <input type="radio"  name="q3" value="2" id="op2" required />
              I don`t Know</label>
                </td>
              </tr>
			  
			  
			  
              <tr>
			   <td width="263" align="left">
                <label><input type="radio" name="q2" value="2" onclick ="showTable(2)" id="intro_1"  required />
               No</label>
                </td>
              </tr>
			    <tr>
			   <td width="263" align="left">
                <label><input type="radio" name="q2" value="3" onclick ="showTable(2)" id="intro_1"  required />
               I don't Know</label>
                </td>
              </tr>
            
			
            </table>
          
        <p align="center">
          <input type="hidden" id="action" name="action" value="submitform" />
          <input type="hidden" id="int_id" name="int_id" value="<?php echo $row_getbank['id']; ?>" />
		     <input type="hidden" id="int_id" name="lid" value="<?php echo $row_getbank['lid']; ?>" />
         
          <input type="submit" name="submit" 
		  style=" padding:0px 20px;
	background:url(../img/fbut-bg.png) repeat-x;
	height:30px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:13px;
	cursor:pointer;
	"
		  value="Save & Continue" />
          </p>
       
</form>
    
  

<tr>
                                 
</div>
</div>


            
</td>
        
      </tr>
	  
    </table>
	
	  <form name="frmTerminate" action="end.php" enctype="multipart/form-data" method="post">
		<p align="right">
		<input type="hidden" name="pageid" value="<?php $currentFile = $_SERVER["PHP_SELF"];$parts = Explode('/', $currentFile);echo $parts[count($parts) - 1];?>" />
        <input type="hidden" id="stop_time" name="stop_time" value="<?php echo $row_getbank['date']; ?>" />
        <input type="hidden" id="action" name="action" value="submitform" />
        <input type="hidden" id="int_id" name="int_id" value="<?php echo $int_id; ?>" />
        <input type="submit" name="submit" 
		style=" padding:0px 20px;
	background:red;
	height:30px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#FCFCFC;
	font-size:13px;
	cursor:pointer;
	"
		value="Click Here To Terminate" onclick="return confirm('Are you sure you want to TERMINATE? This operation cannot be undone!');" />
        </p>
  	</form>
    </td>
  </tr>
  
</table>
    </div>
 <div class="midfooter">
    
   
        <a class="first-letter"> &copy <?php echo date('Y');?> Developed and Designed by Techno Brain BPO/ITES</a>
		
     </div>
	 
	 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
 <script type = "text/javascript">
function showTable(which) {
if (which==1) {
document.getElementById("p1").style.display="table-row";
document.getElementById("p2").style.display ="table-row";
document.getElementById("p3").style.display="table-row";
document.getElementById("p4").style.display ="table-row";
document.getElementById("op1").checked="true";
}

else if(which==2)
{
document.getElementById("datepicker").value ="";
document.getElementById("time").value="";
document.getElementById("p1").style.display="none";
document.getElementById("p2").style.display ="none";
document.getElementById("p3").style.display="none";
document.getElementById("p4").style.display ="none";
document.getElementById("op1").checked="false";
document.getElementById("op2").checked="false";

}



}
  </script>
  <script>
$(function() {
$( "#datepicker,#datepicker2" ).datepicker({
dateFormat: 'yy-mm-dd' });
});

</script>
</body>
</html>