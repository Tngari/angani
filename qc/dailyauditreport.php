<?php
include("../include/config.php");

session_start();
 error_reporting(0);
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../login.php");
exit();

}

if (isset($_GET['transactiondate'])) {
  $colname_dailyrep = $_GET['transactiondate'];
}
//mysql_select_db($database_tblcms, $tblcms);
$query_dailyrep = sprintf("SELECT * FROM qcfiles WHERE date(asof) = %s", GetSQLValueString($colname_dailyrep, "date"));
//$dailyrep = mysql_query($query_dailyrep, $tblcms) or die(mysql_error());
$row_dailyrep = mysql_fetch_assoc($dailyrep);
$totalRows_dailyrep = mysql_num_rows($dailyrep);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>::Ilogix QA Parameters SYSTEM::-Audit Reports </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	
    <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
    <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
    <link rel="stylesheet" type="text/css" href="../css/formelements.css" />

<script src="http://code.jquery.com/jquery-latest.js"></script>
<style>
.tblBorder {
		border:#232b81 solid 1px;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	.tblBorder a {
	text-decoration:none;
	}
	.tblBorder a:link {
	text-decoration:none;
	}
	.tblBorder a:visited {
	text-decoration:none;
	}
	.tblBorder a:hover, a:active, a:focus {
	text-decoration:underline;
	}
	
	.tblBorder2 {
		border:#e67817 solid 1px;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblBorder2a {
		border:#e67817 solid 1px;
		text-align:left;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
		background-color:#f4cca8;
	}
	
	.tblBorder3 {
		border:#232b81 solid 1px;
		text-align:left;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblBorder3b {
		border:#232b81 solid 1px;
		background-color:#fdf3cb;
		color:#ff0000;
		text-align:left;
		font-size:14px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
		padding-bottom:5px;
		padding-top:5px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblShowing {
		text-align:center;
		font-size:15px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
	}
	
	.tblQO {
		background-color:#4d485c;
		color:#FFFFFF;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblQO2 {
		background-color:#e67817;
		color:#FFFFFF;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tbl1a {
		background-color:#fac090;
		font-weight:bold;
		text-align:right;
	}
	
	.tbl1aa {
		background-color:#fac090;
		font-weight:bold;
		text-align:center;
		padding:5px;
	}
	
	.tbl1b {
		background-color:#a15b21;
		color:#FFFFFF;
		font-weight:bold;
		text-align:right;
	}
	
	.tbl1bb {
		background-color:#a15b21;
		color:#FFFFFF;
		font-weight:bold;
		text-align:center;
		padding:5px;
	}
	
	.tbl2b {
		background-color:#f05454;
		text-align:left;
		color:#000000;
		font-weight:bold;
	}
	.tbl2a {
		background-color:#c42727;
		color:#FFFFFF;
		font-weight:bold;
		text-align:left;
	}
	
	.tbl3a {
		background-color:#ff9136;
		color:#000000;
		text-align:left;
		font-weight:bold;
	}
	.tbl3b {
		background-color:#cc5c00;
		color:#FFFFFF;
		text-align:left;
	}
	
	.tbl4b {
		background-color:#3e7804;
		color:#FFFFFF;
		text-align:center;
	}
	
	.tbl5b {
		background-color:#ff9136;
		color:#000000;
		text-align:center;
	}
</style>
</head>
<body>
<div class="wrapper">
 <div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/logo-plantwise.png" alt=""  border="0" />		</a> </div>
            
            
              <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav">
    
   
        <a class="first-letter"> Home</a>
		 <span>QA</span>
		   <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
     </div>
	 
<div class="container formCon" >
 
<table id="content">
<tr><td>
<form id="frmStock" name="frmStock" method="get" action="dailyauditreport.php">
  <table cellpadding="5" align="center">
  <tr valign="middle">
  	<td valign="middle" class="h3_refresh">DAILY AUDIT REPORT</td>
    <td valign="middle"><label for="fromdt">Select Date:</label></td>
    <td valign="middle"><input type="text" name="transactiondate" id="fromdt" /></td>
    <td valign="middle"><input type="submit" name="search" id="search" value="SEARCH" /></td>
  </tr>
  </table>
</form>
<p>&nbsp;</p>

<?php if ($totalRows_dailyrep == 0) { // Show if recordset empty ?>
  <p align="center">There are no QA Records for the Date Selected. Kindly Select Another Day.</p>
  <?php } // Show if recordset empty ?>
  
<?php if ($totalRows_dailyrep > 0) { // Show if recordset not empty ?>
<table align="center" width="100%">
    <tr>
      <td class="tblBorder"><strong>Transaction Date</strong></td>
      <td class="tblBorder2"><strong>QA Date</strong></td>
      <td class="tblBorder"><strong>No of QA Files</strong></td>
      <td class="tblBorder2"><strong>Average Score</strong></td>
      <td class="tblBorder"><strong>Supervisor</strong></td>
	</tr>
    <tr>
      <td class="tblBorder2"><?php echo date("jS M Y", strtotime($row_dailyrep['transactiondate'])); ?></td>
      <td class="tblBorder"><?php echo date("jS M Y", strtotime($row_dailyrep['qadate'])); ?></td>
      <td class="tblBorder2">
	  <?php
			$received = $_GET['transactiondate'];
			mysql_select_db($database_tblcms, $tblcms);
			$count = mysql_query("SELECT count(*) as nofiles from qcfiles WHERE date(asof) = '".$received."'");
			$num = mysql_fetch_array($count);
			$nofile = $num['nofiles'];
			echo $nofile;
			?>
      </td>
      <td class="tblBorder">
	  <?php
	  	$received = $_GET['transactiondate'];
		mysql_select_db($database_tblcms, $tblcms);
		$query_qin = "SELECT SUM(qamarks) AS 'qamarks' FROM qcfiles WHERE date(asof) = '".$received."'";
		$qin = mysql_query($query_qin, $tblcms) or die(mysql_error());
		$row_qin = mysql_fetch_assoc($qin);
		$totalRows_qin = mysql_num_rows($qin);
	  ?>
      <?php $totsum = $row_qin['qamarks']; ?>
      <?php
	  $totals = ($totsum/($nofile * 2300))*100;
	  echo round($totals, 0);
	  ?>%
      </td>
      <td class="tblBorder2"><?php echo $row_dailyrep['supervisor']; ?></td>
	</tr>
</table>

<p align="right">
<form action="getCSVAudit.php" method ="post" > 
<input type="hidden" name="csv_text" id="csv_text">
<input type="submit" alt="Submit Form" value="Export To Excel" onclick="getCSVData()" />
</form>

<script>
function getCSVData(){
 var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 $("#csv_text").val(csv_value);
}
</script>
</p>

<table align="center" width="100%" id="csvdownload">
    <tr>
      <td class="tbl1aa">SERIAL</td>
      <td class="tbl1bb">Q NO</td>
      <td class="tbl1aa">TRANSACTION DATE</td>
      <td class="tbl1bb">AUDIT DATE</td>
      <td class="tbl1aa">CUSTOMER NAME</td>
      <td class="tbl1bb">PHONE NO</td>
   	  <td class="tbl1aa">PROCESSOR</td>
      <td class="tbl1bb">AUDITOR</td>
      <td class="tbl1aa">OVERALL</td>
      <td class="tbl1bb">PERCENTAGE</td>
      <td class="tbl1aa">COMMENTS</td>
    </tr>

  	<?php do { ?>
  	  <tr>
        <td class="tblBorder"><a href="dailyreps.php?id=<?php echo $row_dailyrep['id']; ?>"><?php echo str_pad($row_dailyrep['id'],7,"0",STR_PAD_LEFT); ?></a></td>
  	    <td class="tblBorder2"><?php echo $row_dailyrep['samaid']; ?></td>
  	    <td class="tblBorder"><?php echo $row_dailyrep['transactiondate']; ?></td>
        <td class="tblBorder2"><?php echo date("Y-m-d", strtotime($row_dailyrep['asof'])); ?></td>
        <td class="tblBorder"><?php echo $row_dailyrep['customername']; ?></td>
  	    <td class="tblBorder2"><?php echo $row_dailyrep['phoneno']; ?></td>
  	    <td class="tblBorder"><?php echo $row_dailyrep['processor']; ?></td>
  	    <td class="tblBorder2"><?php echo $row_dailyrep['auditor']; ?></td>
        <td class="tblBorder2"><?php echo $row_dailyrep['qamarks']; ?></td>
  	    <td class="tblBorder"><?php
			$zote = $row_dailyrep['qamarks'];
			$perc = ($zote/2300) * 100;
			$percentage = round($perc, 0);
		?><?php echo $percentage; ?> %</td>
        <td class="tblBorder2"><?php echo $row_dailyrep['comments']; ?></td>
      </tr>
  	  <?php } while ($row_dailyrep = mysql_fetch_assoc($dailyrep)); ?>
  </table>
<?php } // Show if recordset not empty ?>
</td></tr></table>
</div>
</body>
</html>
<?php
mysql_free_result($dailyrep);
?>
