 <?php
include('include/config.php');
session_start(); // Starting Session
$error = ''; // Variable To Store Error Message
if (isset($_POST['submit'])) {
    if (empty($_POST['username']) || empty($_POST['password'])) {
        $error = "Username or Password is invalid";
    } else {
// Define $username and $password

        $username = $_POST['username'];
        $password = $_POST['password'];


// Establishing Connection with Server by passing server_name, user_id and password as a parameter
//$connection = mysql_connect("localhost", "gatool", "zuku");
// Selecting Database
//$db = mysql_select_db("qatool", $connection);
// SQL query to fetch information of registerd users and finds user match.
        $query = dbConnect()->prepare("SELECT * FROM users WHERE username=:username AND password=:password");
        $query->bindParam(':username', $username);
        $query->bindParam(':password', $password);
        $query->execute();
        if ($row = $query->fetch()) {
            $_SESSION['username'] = $row['username'];
            $_SESSION['name'] = $row['user'];
            $_SESSION['level'] = $row['level'];
            if ($row['level'] == 'Admin' || $row['level'] == 'Supervisor') {
                header("Location:front/leads.php");
            } else {
                header("Location:front/leads.php");
            }
        } else {
            header("location:index.php?msg=Wrong%20Username%20/%20Password or%20Unauthorized Access Control&type=error");
        }
        // Closing Connection
    }
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="css/login.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <script src="js/capslock.js"></script>

        <script type="text/javascript">

            function clearText(field)
            {
                if (field.defaultValue == field.value)
                    field.value = '';

                else if (field.value == '')
                    field.value = field.defaultValue;
            }

        </script> 


        <style>

            .show-password-link {
                display: block;
                position: absolute;
                z-index: 11;
                background:url(images/psswrd_shwhide_icon.png) no-repeat;
                width:18px;
                height:12px;
                left: 212px !important;
            }
            .password-showing {
                position: absolute;
                z-index: 10;
            }
        </style> 
        <title>::ANGANI::</title>
    </head>    

    <!--<div class="loginimg"></div>-->
    <div class="loginboxWrapper"  style="font-family:Arial, Helvetica, sans-serif; font-size:12px; background:url(images/back.png) no-repeat right bottom">
        <div class="lw_left">
            
            <div class="lw_logo" style="text-align:center; " ><img src="images/logo_angani.png" width="99" height="94" />
<h1 style="text-align:center; margin-top:20px;"><?php echo "ANGANI"; ?></h1>
                <td style="text-align:center;"><?php echo "CRM Powered By" ?></td>

                <td style="text-align:center; "> <img src="images/logo.png" width="106" height="27"></td><p style="text-align:center;">
                   
                &copy; <?php
$copyYear = 2016; // Set your website start date
$curYear = date('Y'); // Keeps the second year updated
echo $copyYear . (($copyYear != $curYear) ? '-' . $curYear : '');
?> Copyright.
            </div>
      </div>
        <div class="lw_right">
            <h1><?php echo 'Login'; ?></h1>


            <p><?php echo 'Please fill out the following form with your login credentials:'; ?></p>

            <div class="form">

                <form  name="form1" action="" enctype="multipart/form-data" method="POST" >
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php
if (isset($_REQUEST['msg']) && isset($_REQUEST['type'])) {

    if ($_REQUEST['type'] == "error")
        $msg = "<div class='errorSummary'>" . $_REQUEST['msg'] . "</div>";


    echo $msg;
}
?>

                        <tr>
                            <td>
                                <input name="username" type="text" id="username" placeholder="Enter Username" style="padding:5px 5px 5px 5px; border:1px solid #FC0; border-radius:4px;" required/></td>
                        </tr>
                        <tr>
                            <td> <input name="password" type="password" id="password" placeholder="Password" style="padding:5px 5px 5px 5px; border:1px solid #FC0; border-radius:4px;" required/></td>
                        </tr>

                        <tr>
                            <td style="padding:0px;">
                                <table width="60%" border="0" cellspacing="0" cellpadding="0">

                                </table>


                            </td>
                        </tr>

                        <tr>
                            <td>
                                <table width="60%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><input name="submit" type="submit" id="submit" value="Login" style=" padding:0px 20px;
                                                   background:#2C433B;
                                                   height:37px;
                                                   -webkit-border-radius: 4px;
                                                   -moz-border-radius: 4px;
                                                   border-radius: 4px;
                                                   border:1px #b58530 solid;
                                                   color:#FFF;
                                                   font-size:15px;
                                                   cursor:pointer;
                                                   font-weight:bold;" /></td>
                                        <td><input type="reset" name="reset" value="Reset" style=" padding:0px 20px;
                                                   background:#2C433B;
                                                   height:37px;
                                                   -webkit-border-radius: 4px;
                                                   -moz-border-radius: 4px;
                                                   border-radius: 4px;
                                                   border:1px #b58530 solid;
                                                   color:#FFF;
                                                   font-size:15px;
                                                   cursor:pointer;
                                                   font-weight:bold;" /></td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>


                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</body>
</html>


<script type="text/javascript">
    $(document).ready(function () {

        var options = {
            caps_lock_on: function () {
                $('#pid').css({"display": "block"});
                $('#pid').html("Caps lock is on");
            },
            caps_lock_off: function () {
                $('#pid').css({"display": "none"});
            },

        };

        $("input[type='password']").capslock(options);

    });
</script>