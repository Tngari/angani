<?php
include("../include/config.php");

session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../login.php");
    exit();
}

$int_id = $_GET['int_id'];

$query_getbank = dbConnect()->prepare("SELECT  survey.id,  survey.interviewer,survey.phone,survey.farmer,survey.id_number,survey.gender,survey.clinic_visited,survey.date_visited, survey.lid FROM  survey WHERE  survey.id = '" . $int_id . "'");
$query_getbank->execute();
$row_getbank = $query_getbank->fetch();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <!-- CSS main application styling. -->
        <link rel="icon" type="image/ico" href="../uploadedfiles/school_logo/favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
        <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
        <link rel="stylesheet" type="text/css" href="../css/formelements.css" />
        <link rel="stylesheet" href="../css1/coda-slider-2.0.css" type="text/css" media="screen" />  
        <link rel="stylesheet" href="css/BeatPicker.min.css"/>

        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/BeatPicker.min.js"></script>
        <script type="text/javascript" src="../../js/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="../../js/js/chart/highcharts.js"></script>
        <script type="text/javascript" src="../../js/js/custom-form-elements.js"></script>   
        </script>
        <script type="text/javascript" src="../../js/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

        <script>
            $(document).ready(function () {
                $("#lodrop").click(function () {

                    if ($("#account_drop").is(':hidden')) {
                        $("#account_drop").show();
                    } else {
                        $("#account_drop").hide();
                    }
                    return false;
                });
                $('#account_drop').click(function (e) {
                    e.stopPropagation();
                });
                $(document).click(function () {
                    if (!$("#account_drop").is(':hidden')) {
                        $('#account_drop').hide();
                    }
                });

            });
        </script>

        <script type="text/javascript">
            $(document).ready(function ()
            {
                $(".reporting_manager").change(function ()
                {
                    var id = $(this).val();
                    var dataString = 'id=' + id;

                    $.ajax
                            ({
                                type: "POST",
                                url: "ajax_city.php",
                                data: dataString,
                                cache: false,
                                success: function (html)
                                {
                                    $(".reporting_lead").html(html);
                                }
                            });

                });
            });
        </script>


        <script>
            $(document).ready(function () {
                $(".nav_drop_but").click(function () {
                    $(".navigationbtm_wrapper_outer").slideToggle();
                });
            });
        </script>

        <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="../js/table2CSV.js" ></script>
        <link type="text/css" href="../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
        <script type="text/javascript">
            $(function () {
                $('#fromdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('#todt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
                $('#exdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });
        </script>


    </head>
    <title>::Ilogix Questionnaire ::</title>
    <body>
        <div class="wrapper">


            <div class="header">

                <div class="lo_drop" id="account_drop">
                    <div class="lo_drop_hov"></div> 
                    <div class="lo_name">
<?php ?><?php ?>
                        <span> <?php echo $_SESSION['name']; ?> </span>
                        <div class="clear"></div>
                    </div>
                    <ul>
                        <li><a href="profile.php"><?php echo 'My Account'; ?></li>
                        <li><a href="settings.php"><?php echo 'Settings'; ?></a></li>
                        <li> <a href="../logout.php"><?php echo 'Logout'; ?></a></li>
                    </ul>
                </div>





                <div class="logo">
                    <a href="index.php"><img src="../images/logo-plantwise.png" alt=""  border="0" />		</a> </div>


                <div class="">

<?php include('app_nav.php'); ?>

                </div>


            </div>



            <div class="midnav">


                <a class="first-letter"> Home</a>
                <span>Leads Management</span>
                <span style="float:right"><a href="../logout.php"> Logout</a></span>
                <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>
            </div>


            <div class="container">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="247" valign="top">

<?php include('../left_side.php'); ?>

                        </td>
                        <td valign="top">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="top" width="75%"><div style="padding-left:20px; padding-right:10px;">


                                            <h3></h3>


                                            <div class="formCon" >

                                                <div class="">


                                                    <form name="frmRespondent" action="q2.php" enctype="multipart/form-data" method="post" >  

                                                        <table width="708" align="center">
                                                            <tr>
                                                              <!--<td colspan="2" align="right"><strong>QN:
<?php $sno = str_pad($row_getbank['id'], 6, "0", STR_PAD_LEFT);
echo $sno; ?>            
                                                                <input type="hidden" name="qsno" value="<?php echo $row_getbank['id']; ?>" />
                                                              </strong></td>-->
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="right"><div align="left"><strong>

                                                                            We are calling from [.....] working with the [ECONET] tasked to assess ....., are you willing to answer a few questions on this subject?
                                                                            <br><br/>S1. Kindly confirm Gender
                                                                        </strong>
                                                                        </tr>
                                                                        <br/><br/>
                                                                        <tr>



                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S1" value="Female" id="intro_0" required  />
                                                                                    Male</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S1" value="Male" id="intro_1"  required />
                                                                                    Female</label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                                  <td colspan="2" align="right"><div align="left"><strong>

                                                                            <br><br/>S2. Kindly confirm your Age
                                                                        </strong>
                                                                        </tr>
                                                                        <br/><br/>
                                                                        <tr>



                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S2" value="18-24" id="intro_0" required  />
                                                                                    18-24</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S2" value="25-29" id="intro_1"  required />
                                                                                    25-29</label>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                        <tr>

                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S2" value="30-39" id="intro_0" required  />
                                                                                    30-39</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S2" value="40-49" id="intro_1"  required />
                                                                                    40-49</label>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                         <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S2" value="Over 49" id="intro_1"  required />
                                                                                    Over 49</label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                         <td colspan="2" align="right"><div align="left"><strong>

                                                                            <br><br/>S3. Region of current residence
                                                                        </strong>
                                                                        </tr>
                                                                     
                                                                        <tr>



                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S3" value="Harare" id="intro_0" required  />
                                                                                    Harare</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S3" value="Bulawayo" id="intro_1"  required />
                                                                                    Bulawayo</label>
                                                                            </td>
                                                                        </tr>
                                                                      
                                                                        <tr>

                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S3" value="Mutare" id="intro_0" required  />
                                                                                    Mutare</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S3" value="Masvingo" id="intro_1"  required />
                                                                                    Masvingo</label>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                         <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S3" value="Gweru" id="intro_1"  required />
                                                                                    Gweru</label>
                                                                            </td>
                                                                        </tr>
                                                             
                                                                         	   <tr >
			   <td width="263" align="left">
                <label><input type="radio" id="checkbox1"  name="S3" value="Other"  required />
              Other <input type="text" name="other" id="textbox" disabled="disabled"></label>
                </td>
              </tr>
                                                                        
                                                                        <tr>
                                                                         <td colspan="2" align="right"><div align="left"><strong>

                                                                            <br><br/>S4. Which of the following best describes your work status?
                                                                        </strong>
                                                                        </tr>
                                                                     
                                                                        <tr>



                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S4" value="Formal employment" id="intro_0" required  />
                                                                                    Formal employment</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S4" value="Self employed" id="intro_1"  required />
                                                                                    Self employed</label>
                                                                            </td>
                                                                        </tr>
                                                                      
                                                                        <tr>

                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S4" value="Unemployed" id="intro_2" required  />
                                                                                    Unemployed</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S4" value="Student" id="intro_3"  required />
                                                                                    Student</label>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                         <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S4" value="Retired" id="intro_4"  required />
                                                                                    Retired</label>
                                                                            </td>
                                                                        </tr>
                                                              <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S4" value="Housewife" id="intro_5"  required />
                                                                                    Housewife</label>
                                                                            </td>
                                                                        </tr>
                                                                         	   <tr >
			   <td width="263" align="left">
                <label><input type="radio" id="checkbox1"  name="S4" value="Other"  required />
              Other <input type="text" name="other" id="textbox" disabled="disabled"></label>
                </td>
              </tr>
                                                                        
                                                                        
                                                                        <tr>
                                                                         <td colspan="2" align="right"><div align="left"><strong>

                                                                            <br><br/>S6. Please tell me who pays the phone costs i.e. airtime for this line we have called you on?
                                                                        </strong>
                                                                        </tr>
                                                                     
                                                                        <tr>



                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S6" value="Myself" id="intro_0" required  />
                                                                                    
                                                                                 
                                                                                   Myself</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S6" value="Myself, via my phone allowance/benefit" id="intro_1"  required />
                                                                                    Myself, via my phone allowance/benefit</label>
                                                                            </td>
                                                                        </tr>
                                                                      
                                                                        <tr>

                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S6" value="My employer/Company pays" id="intro_2" required  />
                                                                                    My employer/Company pays</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                 

                                                                                <label><input type="radio" name="S6" value="My spouse" id="intro_3"  required />
                                                                                    My spouse</label>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                         <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S6" value="My parent" id="intro_4"  required />
                                                                                    My parent</label>
                                                                            </td>
                                                                        </tr>
                                                              <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S6" value="My daughter/Son" id="intro_5"  required />
                                                                                    My daughter/Son</label>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                        
                                                                         	   <tr >
			   <td width="263" align="left">
                <label><input type="radio" id="checkbox1"  name="S6" value="Other"  required />
              Other <input type="text" name="other" id="textbox" disabled="disabled"></label>
                </td>
              </tr>
                                                                        <tr>
                                                                <td colspan="2" align="right"><div align="left"><strong>

                                                                        <br><br/>S7.Do you use your phone mostly for business or personal use?
                                                                        </strong>
                                                                        </tr>
                                                                       
                                                                        <tr>



                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S7" value="Business" id="intro_0" required  />
                                                                                    Business</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S7" value="Personal" id="intro_1"  required />
                                                                                    Personal</label>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                                                                     <tr>
                                                                                  <td colspan="2" align="right"><div align="left"><strong>

                                                                            <br><br/>S5. Please tell me your Monthly personal income
                                                                        </strong>
                                                                        </tr>
                                                                     
                                                                        <tr>



                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S5" value="Less that USD 80" id="intro_0" required  />
                                                                                    Less that USD 80</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S5" value="USD 81 to 160" id="intro_1"  required />
                                                                                    USD 81 to 160</label>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                        <tr>

                                                                            <td width="263" align="left">
                                                                                <label>
                                                                                    <input type="radio" name="S5" value="USD 161 to 450" id="intro_2" required  />
                                                                                    USD 161 to 450</label>

                                                                            </td>
                                                                            <td width="433" >&nbsp;</td>
                                                                        </tr>
                                                                        <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S5" value="USD 451 to 650" id="intro_3"  required />
                                                                                    USD 451 to 650</label>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                         <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S5" value="USD 651 to 850" id="intro_4"  required />
                                                                                    USD 651 to 850</label>
                                                                            </td>
                                                                        </tr>
                                                                          <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S5" value="USD 851 to 1700" id="intro_5"  required />
                                                                                    USD 851 to 1700</label>
                                                                            </td>
                                                                        </tr>
                                                                          <tr >
                                                                            <td width="263" align="left">
                                                                                <label><input type="radio" name="S5" value="Over USD 1700" id="intro_6"  required />
                                                                                    Over USD 1700</label>
                                                                            </td>
                                                                        </tr>
                                                                        </table>

                                                                        <p align="center">
                                                                            <input type="hidden" id="action" name="action" value="submitform" />
                                                                            <input type="hidden" id="int_id" name="int_id" value="<?php echo $row_getbank['id']; ?>" />
                                                                            <input type="hidden" id="int_id" name="lid" value="<?php echo $row_getbank['lid']; ?>" />

                                                                            <input type="submit" name="submit" 
                                                                                   style=" padding:0px 20px;
                                                                                   background:url(../img/fbut-bg.png) repeat-x;
                                                                                   height:30px;
                                                                                   -webkit-border-radius: 4px;
                                                                                   -moz-border-radius: 4px;
                                                                                   border-radius: 4px;
                                                                                   border:1px #b58530 solid;
                                                                                   color:#633c15;
                                                                                   font-size:13px;
                                                                                   cursor:pointer;
                                                                                   "
                                                                                   value="Save & Continue" />
                                                                        </p>

                                                                        </form>



                                                                        <tr>

                                                                    </div>
                                                                    </div>



                                                                </td>

                                                            </tr>

                                                        </table>

                                                        <!--<form name="frmTerminate" action="end.php" enctype="multipart/form-data" method="post">
                                                              <p align="right">
                                                              <input type="hidden" name="pageid" value="<?php $currentFile = $_SERVER["PHP_SELF"];
                                                                $parts = Explode('/', $currentFile);
                                                                echo $parts[count($parts) - 1]; ?>" />
                                                      <input type="hidden" id="stop_time" name="stop_time" value="<?php echo $row_getbank['date']; ?>" />
                                                      <input type="hidden" id="action" name="action" value="submitform" />
                                                      <input type="hidden" id="int_id" name="int_id" value="<?php echo $int_id; ?>" />
                                                              <input type="hidden" id="" name="terminate" value="Yes" />
                                                      <input type="submit" name="submit" 
                                                              style=" padding:0px 20px;
                                                      background:red;
                                                      height:30px;
                                                      -webkit-border-radius: 4px;
                                                      -moz-border-radius: 4px;
                                                      border-radius: 4px;
                                                      border:1px #b58530 solid;
                                                      color:#FCFCFC;
                                                      font-size:13px;
                                                      cursor:pointer;
                                                      "
                                                              value="Click Here To Terminate" onclick="return confirm('Are you sure you want to TERMINATE? This operation cannot be undone!');" />
                                                      </p>
                                                      </form>-->
                                                        </td>
                                                        </tr>

                                                        </table>
                                                </div>
                                                <div class="midfooter">


                                                    <a class="first-letter"> &copy <?php echo date('Y'); ?> Developed and Designed by Techno Brain BPO/ITES</a>

                                                </div>
                                                </body>
                                                </html>