<?php
include("../include/config.php");
error_reporting(false);
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../login.php");
    exit();
}
if (isset($_GET['int_id'])) {
    $int_id = $_GET['int_id'];
}

if (isset($_POST['action']) && $_POST['action'] == 'submitform') {
	
 
   $method = $_POST['method'];

   $other_method = $_POST['other_method'];
    $appointment = $_POST['appointment'];
	$time = $_POST['time'];
     $q7 = $_POST['q7'];
     $q8 = $_POST['q8'];
     $q9 = $_POST['q9'];
    $time = $_POST['time'];

     $email = $_POST['email'];
    $int_id = $_POST['int_id'];
    $lid = $_POST['lid'];
 
    $int_id = $_POST['int_id'];
    $lid = $_POST['lid'];
	//	
	//echo "<pre>";
    //var_dump($_POST);
    //echo "</pre>";
    //die();
   
	try{
	  $sql2 = "UPDATE survey SET method=:method, other_method=:other_method, q7=:q7, q8=:q8, q9=:q9,appointment=:appointment, email=:email,time=:time WHERE id =:id";
            $stmt2 = dbConnect()->prepare($sql2);
            $stmt2->bindParam(':method',  $method, PDO::PARAM_STR);
             $stmt2->bindParam(':other_method', $other_method, PDO::PARAM_STR);
              $stmt2->bindParam(':q7', $q7, PDO::PARAM_STR);
               $stmt2->bindParam(':q8', $q8, PDO::PARAM_STR);
                $stmt2->bindParam(':q9', $q9, PDO::PARAM_STR);
            $stmt2->bindParam(':appointment',  $appointment, PDO::PARAM_STR);
			$stmt2->bindParam(':time',  $time, PDO::PARAM_STR);
            $stmt2->bindParam(':email',  $email, PDO::PARAM_STR);
            $stmt2->bindParam(':id', $int_id, PDO::PARAM_STR);
            $stmt2->execute();
            $stmt2->execute();		
	}catch(PDOException $e){
		echo $e->getMessage();
	}	
}


$query_getbank = dbConnect()->prepare("SELECT  survey.id,survey.interviewer,survey.phone,survey.lid FROM  survey WHERE  survey.id = '" . $int_id . "'");
$query_getbank->execute();
$row_getbank = $query_getbank->fetch();
//$query_getbank = dbConnect()->prepare("SELECT  survey.id,survey.interviewer,survey.phone,survey.lid,survey.Q3_Other, survey.Q1_other FROM  survey WHERE  survey.id = '".$int_id."'");
//$query_getbank->execute();
//$row_getbank=$query_getbank->fetch();
//$list=$row_getbank['Q3_Other'];
//$list2=$row_getbank['Q1_other'];
//if($list !=NULL)
//{
$id2=$row_getbank['lid'];

$query_getbank2 = dbConnect()->prepare("SELECT  * FROM  leads WHERE  leads.id = '" . $id2 . "'");
$query_getbank2->execute();
$row_getbank2 = $query_getbank2->fetch();


date_default_timezone_set("Africa/Nairobi");


if (isset($_GET['int_id'])) {
    $int_id = $_GET['int_id'];
} else {
    $int_id = $_POST['int_id'];
}

if (isset($_POST['stop_time'])) {
    $stop_time = $_POST['stop_time'];
} else {
    $stop_time = date("Y-m-d H:i:s");
}

if (isset($_POST['pageid'])) {
    $last_page = $_POST['pageid'];
} else {
    $last_page = 'end.php';
}


$sql = "UPDATE survey SET stop_time=:val WHERE id =:id";
$stmt = dbConnect()->prepare($sql);
$stmt->bindParam(':val', $stop_time, PDO::PARAM_STR);
$stmt->bindParam(':id', $int_id, PDO::PARAM_STR);
$stmt->execute();

$query_getbank = dbConnect()->prepare("SELECT  survey.id FROM  survey WHERE  survey.id = '" . $int_id . "'");
$query_getbank->execute();
$row_end = $query_getbank->fetch();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <!-- CSS main application styling. -->
        <link rel="icon" type="image/ico" href="../uploadedfiles/school_logo/favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
        <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
        <link rel="stylesheet" type="text/css" href="../css/formelements.css" />
        <link rel="stylesheet" href="../css1/coda-slider-2.0.css" type="text/css" media="screen" />  
        <link rel="stylesheet" href="css/BeatPicker.min.css"/>

        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/BeatPicker.min.js"></script>
        <script type="text/javascript" src="../../js/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="../../js/js/chart/highcharts.js"></script>
        <script type="text/javascript" src="../../js/js/custom-form-elements.js"></script>   
        </script>
        <script type="text/javascript" src="../../js/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

        <script>
            $(document).ready(function () {
                $("#lodrop").click(function () {

                    if ($("#account_drop").is(':hidden')) {
                        $("#account_drop").show();
                    } else {
                        $("#account_drop").hide();
                    }
                    return false;
                });
                $('#account_drop').click(function (e) {
                    e.stopPropagation();
                });
                $(document).click(function () {
                    if (!$("#account_drop").is(':hidden')) {
                        $('#account_drop').hide();
                    }
                });

            });
        </script>

        <script type="text/javascript">
            $(document).ready(function ()
            {
                $(".reporting_manager").change(function ()
                {
                    var id = $(this).val();
                    var dataString = 'id=' + id;

                    $.ajax
                            ({
                                type: "POST",
                                url: "ajax_city.php",
                                data: dataString,
                                cache: false,
                                success: function (html)
                                {
                                    $(".reporting_lead").html(html);
                                }
                            });

                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                    $("#checkbox1").click(function () {
                        if ($(this).is(":checked")) {
                            $("#textbox").removeAttr("disabled");
                            $("#textbox").attr("required", "required");
                            $("#textbox").focus();
                        } else {
                            $("#textbox").attr("disabled", "disabled");
                            $("#textbox").removeAttr("required");
                            document.getElementById("textbox").value = "";

                        }
                    });
                });
 $(function () {
                    $("#1,#2,#3,#4,#5,#6,#7,#8,#9,#10").click(function () {
                        if ($(this).is(":checked")) {
                            $("#textbox").removeAttr("disabled");
                           
                             $("#textbox").attr("disabled", "disabled");
                              document.getElementById("textbox").value = "";
                        } else {
                            $("#textbox").attr("disabled", "disabled");
                            $("#textbox").removeAttr("required");
                            document.getElementById("textbox").value = "";

                        }
                    });
                });

            </script>

        <script>
            $(document).ready(function () {
                $(".nav_drop_but").click(function () {
                    $(".navigationbtm_wrapper_outer").slideToggle();
                });
            });
        </script>

        <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="../js/table2CSV.js" ></script>
        <link type="text/css" href="../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
        <script type="text/javascript">
            $(function () {
                $('#fromdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('#todt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
                $('#exdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });
        </script>


    </head>
    <title>::ANAGANI::</title>
    <body>
        <div class="wrapper">


            <div class="header">

                <div class="lo_drop" id="account_drop">
                    <div class="lo_drop_hov"></div> 
                    <div class="lo_name">
                        <?php ?><?php ?>
                        <span> <?php echo $_SESSION['name']; ?> </span>
                        <div class="clear"></div>
                    </div>
                    <ul>
                        <li><a href="profile.php"><?php echo 'My Account'; ?></li>
                        <li><a href="settings.php"><?php echo 'Settings'; ?></a></li>
                        <li> <a href="../logout.php"><?php echo 'Logout'; ?></a></li>
                    </ul>
                </div>





                <div class="logo">
                    <a href="index.php"><img src="../images/logo.png" alt="" height="67" border="0" />		</a> </div>


                <div class="">

                    <?php include('app_nav.php'); ?>

                </div>


            </div>



            <div class="midnav">


                <a class="first-letter"> Home</a>
                <span>Leads Management</span>
                <span style="float:right"><a href="../logout.php"> Logout</a></span>
                <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>
            </div>


            <div class="container">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="247" valign="top">

                            <?php include('../left_side.php'); ?>

                        </td>
                        <td valign="top">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="top" width="75%"><div style="padding-left:20px; padding-right:10px;">
                                            <h3 align="center">THANK RESPONDENT AND CLOSE INTERVIEW</h3>

                                            <div class="formCon2" >

                                                <div class="">
                                                   
                                                    <!-- <font color="blue" size="3"><center><strong>Fine, Thank you for your time. Have a nice day!</strong></center></font> -->

                                                    <form name="frmPriority" action="end_final.php" enctype="multipart/form-data" method="post">

  <br><br>
   <p><strong>Q 10. </strong>Is there any question you would like me to address with regards to our Services / or any clarification on the information I have given you?</p>

                                                            <p align="center"><textarea cols="40" rows="3" name="q12" id="q12"></textarea></p>
 
                                                        

                                                              
                                                                   <p>Well <span style="color:#06F"><?php echo $row_getbank2['name']; ?></span>, thank you for your time, it's been a pleasure talking to you.
If you have any further clarifications , feel free to contact our website <strong>www.angani.co</strong> or phone number 020 – 5230028.

Have a good day. Good bye. </p>

           <tr><td>
                                                                                                        
                                                                                                          
                                                                                                        
                                                                                                    </td></tr>
                                                            </table>


                                                            <p></p>

                          <p align="center">
                                                            <input name="complete_Survey" type="checkbox" value="complete survey" required="required" />
                                                            <strong>complete Survey
                          </strong></p>
                                                            <p align="center">
                                                                <input type="hidden" id="action" name="action" value="submitform" />
                                                                <input type="hidden" id="int_id" name="int_id" value="<?php echo $row_end['id']; ?>" />
                                                                <input type="hidden" id="" name="page_id" value="<?php echo $last_page; ?>" />
                                                                <input type="hidden" id="lid" name="lid" value="<?php echo $row_end['lid']; ?>" />
                                                                <input type="submit" name="submit" 
                                                                       style=" padding:0px 20px;
                                                                       background:red;
                                                                       height:30px;
                                                                       -webkit-border-radius: 4px;
                                                                       -moz-border-radius: 4px;
                                                                       border-radius: 4px;
                                                                       border:1px #b58530 solid;
                                                                       color:#FCFCFC;
                                                                       font-size:13px;
                                                                       cursor:pointer;
                                                                       "
                                                                       value="Close Call" />
                                                            </p>
                                                    </form>



                                                    <tr>

                                                </div>
                                            </div>


                                            </form>
                                    </td>

                                </tr>

                            </table></form>
                        </td>
                    </tr>

                </table>
            </div>
            <div class="midfooter">


                <a class="first-letter"> &copy <?php echo date('Y'); ?> Developed and Designed by Techno Brain BPO/ITES</a>

            </div>

            <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
                <script src="//code.jquery.com/jquery-1.9.1.js"></script>
                <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
                <script>
                                                                    $(function () {
                                                                        $("#datepicker,#datepicker2,#datepicker3").datepicker({
                                                                            dateFormat: 'yy-mm-dd'});
                                                                    });
                                                                    function showTable(which) {
                                                                        if (which == 1) {
                                                                            //document.getElementById("p1").style.display="table-row";
                                                                            document.getElementById("p2").style.display = "table-row";
                                                                            //document.getElementById("p3").style.display="table-row";
                                                                            //document.getElementById("p4").style.display ="table-row";
                                                                            //document.getElementById("op1").checked="true";
                                                                        } else if (which == 2)
                                                                        {
                                                                            //document.getElementById("p2").style.display="none";
                                                                            //document.getElementById("tablecallB").style.display="none";
                                                                            document.getElementById("datepicker").value = "";
                                                                            document.getElementById("time").value = "";
                                                                            document.getElementById("p2").style.display = "none";

                                                                            //document.getElementById("tablecallC").style.display="none";
                                                                            //document.getElementById("tablecallD").style.display="none";
                                                                        } else if (which == 3)
                                                                        {
                                                                            document.getElementById("tablecallB").style.display = "none";
                                                                            document.getElementById("datepicker").value = "";
                                                                            document.getElementById("time").value = "";
                                                                            document.getElementById("tablecallC").style.display = "table-row";
                                                                            document.getElementById("tablecallD").style.display = "none";
                                                                        } else if (which == 4)
                                                                        {
                                                                            document.getElementById("tablecallB").style.display = "none";
                                                                            document.getElementById("datepicker").value = "";
                                                                            document.getElementById("time").value = "";
                                                                            document.getElementById("tablecallC").style.display = "none";
                                                                            document.getElementById("tablecallD").style.display = "table-row";
                                                                        }
                                                                    }
                </script>
                </body>
                </html>
