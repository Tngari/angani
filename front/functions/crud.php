<?php
error_reporting(0);
include "../config.php";

function packageStatus($newpackage,$oldpackage)
{
	global $db;
	
	//oldpackage
	$query = "SELECT position FROM zuku_pckg WHERE package='$oldpackage'";
	
	$result = $db->query($query) or die($db->error.__LINE__);

	
	$row = $result->fetch_assoc();
	
	//new package
	$query2 = "SELECT position FROM zuku_pckg WHERE package='$newpackage'";
	
	$result2 = $db->query($query2) or die($db->error.__LINE__);
	
	
	$row2 = $result2->fetch_assoc();
	
	$old=$row['position'];
	$new=$row2['position'];
	
	if($old==$new)
	{
		//same
		echo '<div class="radio clip-radio radio-warning radio-inline">
														<input type="radio" id="radio9" name="upgrade" value="Same" checked>
														<label for="radio9" style="color:#51375A;font-size:12px; font-weight:bold">
															Same
														</label>
													</div>';
	}
	
	if($old>$new)
	{
		//same
		echo '<div class="radio clip-radio radio-warning radio-inline">
														<input type="radio" id="radio8" name="upgrade" value="Degrade" checked>
														<label for="radio8" style="color:#51375A;font-size:12px; font-weight:bold">
															Downgrade
														</label>
													</div>';
	}
	
	if($old<$new)
	{
		//same
		echo '<div class="radio clip-radio radio-success radio-inline">
														<input type="radio" id="radio7" name="upgrade" value="Upgrade" checked>
														<label for="radio7" style="color:#51375A;font-size:12px; font-weight:bold">
															Upgrade
														</label>
													</div>';
	}
	
}

function createRecord()
{
	global $db;
	$maindisposation=$_POST['maindisposation'];
	$disposation=$_POST['disposation'];
        $undisposation=$_POST['undisposation'];
$subdispo=$_POST['subdispo'];
$inline=$_POST['inline'];
$package=$_POST['package'];
$comments=$_POST['comments'];
$pid=$_POST['pid'];
$ip=$_SERVER['REMOTE_ADDR'];
$actionby=$_SESSION['byte_user'];
$upgrade=$_POST['upgrade'];
$date_done=date('Y-m-d H:i:s');
$expectedpaydate=$_POST['expectedpaydate'];
$sugprice=$_POST['sugprice'];
$warrantstatus=$_POST['warrantstatus'];
$competitor=$_POST['competitors'];
$travel_otherreason=$_POST['travel_otherreason'];
$travel_duration=$_POST['travel_duration'];
$oldpackage=$_POST['oldpackage'];
$transfer_name=$_POST['transfer_name'];
$transfer_phone=$_POST['transfer_phone'];
$actionplan=$_POST['actionplan'];
$myesc=$_POST['myesc'];
$escalation=$_POST['escalation'];

//ADDITIONAL FIELDS FOR DATA SUBMISSION
$area=$_POST['area'];
$region=$_POST['region'];
$disconnectiondate=$_POST['disconnectiondate'];
$nonrfsarea=$_POST['nonrfsarea'];



if($escalation!='')
{
	$escalation=$_POST['escalation'];
}
else 
{
	$escalation='';
}
if($transfer_name!='')
{
	$transfer_name=$_POST['transfer_name'];
}
else {
$transfer_name='';
}


if($transfer_phone!='')
{
	$transfer_phone=$_POST['transfer_phone'];
}
else {
$transfer_phone='';
}

if($travel_otherreason!='')
{
	$travel_otherreason=$_POST['travel_otherreason'];
}
else {
	$travel_otherreason='';
}
if($travel_duration!='')
{
	$travel_duration=$_POST['travel_duration'];
}
else {
	$travel_duration='';
}
if($sugprice!='')
{
	$sugprice=$_POST['sugprice'];
}
else{
	$sugprice='';
}
$duplicateaccount=$_POST['duplicateaccount'];

if($duplicateaccount!='')
{
	$duplicateaccount=$_POST['duplicateaccount'];
}
else
{
	$duplicateaccount="";
}

//multipleaccount accounts

$multipleaccount=$_POST['multipleaccount'];

if($multipleaccount!='')
{
	$multipleaccount=$_POST['multipleaccount'];
}
else
{
	$multipleaccount="";
}

$status=1;
	$query2 ="UPDATE leads SET status=? WHERE id=?";
$statement2 = $db->prepare($query2);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement2->bind_param('ss',$status,$pid); 

$statement2->execute();
$statement2->close();

if(isset($_POST['create']))
{
	
	$stmt = $db->prepare("SELECT * FROM tbl_clients WHERE lead_id=?");

$stmt->bind_param('i',$pid);

$result = $stmt->execute();

$stmt->store_result();
$count=$stmt->num_rows;

//end avilability

if($count==0)
{
$query ="INSERT INTO tbl_clients(main_dispo,diposation,sub_disposation,activated,activatedpackage,comments,
		agent,ip,lead_id,date_done,expectedpaydate,sugprice,warrantstatus,competitor,duplicateaccount,travel_otherreason,
		travel_duration,transfer_phone,transfer_name,oldpackage,actionplan,multipleaccount,
		myesc,escalation,area,region,disconnection_date,non_rfs) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";//,?,?,?,?,?,?,?
$statement = $db->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('ssssssssssssssssssssssssssss',$maindisposation,$disposation,$subdispo,
		$inline,$package,$comments,$actionby,$ip,$pid,$date_done,
		$expectedpaydate,$sugprice,$warrantstatus,$competitor,
		$duplicateaccount,$travel_otherreason,$travel_duration,$transfer_phone,
		$transfer_name,$oldpackage,$actionplan,$multipleaccount,$myesc,$escalation,
                $area,$region,$disconnectiondate,$nonrfsarea); //,$email,$accno,$altemail,$mobileno,$package,$actionby,$ip,$pid,$date_done

		//send email
		
		

if($statement->execute()){
	
?>
<h3>Add Successful</h3>
<?php
   
}else{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();
}
else
{
		
		$queryUpdate="UPDATE tbl_clients SET main_dispo=?, 
		diposation=?, 
		sub_disposation=?,
		activated=?, 
		activatedpackage=?,
		comments=?,
		expectedpaydate=?,
		sugprice=?,
		warrantstatus=?,
		competitor=?,
		duplicateaccount=?,
		travel_otherreason=?,
		travel_duration=?,
		transfer_phone=?,
		transfer_name=?,
		oldpackage=?,multipleaccount=?,actionplan=?  WHERE lead_id=?"; //,multipleaccount=? //,actionplan=? 
$statement = $db->prepare($queryUpdate);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('ssssssssssssssssssi',$maindisposation,$disposation,$subdispo,$inline,$package,$comments,$expectedpaydate,$sugprice,$warrantstatus,$competitor,$duplicateaccount,$travel_otherreason,$travel_duration,$transfer_phone,$transfer_name,$oldpackage,$multipleaccount,$actionplan,$pid); //,$email,$accno,$altemail,$mobileno,$package,$actionby,$ip,$pid,$date_done


if($statement->execute()){
	
	echo "Update Successful";

}	
else{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();
	}
}


if(isset($_POST['terminate']))
{
        $area1=$_POST['area1'];
        $region1=$_POST['region1'];
	$dispo='Terminate';
	$oldpackage=$_POST['oldpackage'];
$query ="INSERT INTO tbl_clients(main_dispo,diposation,agent,ip,lead_id,date_done,dispo,oldpackage,actionplan,area,region) VALUES(?,?,?,?,?,?,?,?,?,?,?)";//,?,?,?,?,?,?,?
$statement = $db->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('sssssssssss',$maindisposation,$undisposation,$actionby,$ip,$pid,$date_done,$dispo,$oldpackage,$actionplan,$area1,$region1); //,$email,$accno,$altemail,$mobileno,$package,$actionby,$ip,$pid,$date_done


if($statement->execute()){
	
?>
<h3>Add Successful</h3>
<?php
   
}
else
{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();
?>
<script>
 window.location="?rd=o&fm=/";
 </script>
<?php
	
}

if(isset($_POST['cback']))
{
$maindisposation=$_POST['maindisposation'];
$disposation=$_POST['disposation'];
$subdispo=$_POST['subdispo'];
$inline=$_POST['inline'];
$package=$_POST['package'];
$oldpackage=$_POST['oldpackage'];
$actionplan=$_POST['actionplan'];
$comments=$_POST['comments'];
$pid=$_POST['pid'];
$ip=$_SERVER['REMOTE_ADDR'];
$actionby=$_SESSION['byte_user'];

$myesc=$_POST['myesc'];
$escalation=$_POST['escalation'];


//ADDITIONAL REQUIREMENTS
$area=$_POST['area'];
$region=$_POST['region'];
$disconnectiondate=$_POST['disconnectiondate'];
$nonrfsarea=$_POST['nonrfsarea'];

if($escalation!='')
{
	$escalation=$_POST['escalation'];
}
else
{
	$escalation='';
}

$date_done=date('Y-m-d H:i:s');
$query ="INSERT INTO tbl_clients(main_dispo,diposation,sub_disposation,activated,comments,agent,ip,lead_id,date_done,oldpackage,actionplan,myesc,escalation, area,region,disconnection_date,non_rfs) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";//,?,?,?,?,?,?,?
$statement = $db->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('sssssssssssssssss',$maindisposation,$disposation,$subdispo,$inline,$comments,$actionby,$ip,$pid,$date_done,$oldpackage,$actionplan,$myesc,$escalation,$area,$region,$disconnectiondate,$nonrfsarea); //,$email,$accno,$altemail,$mobileno,$package,$actionby,$ip,$pid,$date_done


if($statement->execute()){
	
?>
<h3>Add Successful</h3>
<?php
   
}else{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();
?>
<script>
 window.location="?rd=z&fm=&d=<?php echo $pid;?>&/";
 </script>
<?php
}

}


function updateRecord()

{
	global $db;
	
$name=$_POST['name'];
$accnonew=$_POST['accno'];
$mobileno=$_POST['mobileno'];
$email=$_POST['email'];
$altemail=$_POST['altemail'];
$accno=$_POST['accnoold'];
$mobilenoold=$_POST['mobilenoold'];
$isp=$_POST['isp'];
	$pid=$_POST['pid'];
	if(isset($_POST['create']))
	{
	$dispo="Complete";
$query ="UPDATE tbl_clients SET name=? ,email=?,accnonew=?,altemail=?,mobileno=?, dispo=?,isp=?,accno=?,mobilenoold=? WHERE lead_id=?";
$statement = $db->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('ssssssssss',$name,$email,$accnonew,$altemail,$mobilenoold,$dispo,$isp,$accno,$mobileno,$pid); 


if($statement->execute()){
	
?>
 <script>
 window.location="?rd=o&fm=/";
 </script>
<?php
   
}else{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();
	}
	
	if(isset($_POST['cbk']))
	{
		$callback="Call Back";
		$time=$_POST['time'];
		$dpicker=$_POST['datepicker'].' '.$time;
		
	$query ="UPDATE tbl_clients SET dispo=? ,callbackdate=? WHERE lead_id=?";//,?,?,?,?,?,?,?
$statement = $db->prepare($query);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('sss',$callback,$dpicker,$pid); //,$email,$accno,$altemail,$mobileno,$package,$actionby,$ip,$pid,$date_done


if($statement->execute()){
	
?>
 <script>
 window.location="?rd=o&fm=/";
 </script>
<?php
   
}else{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();
	}	

}


//Reminder 2px


function createReminder1()
{
	global $db;
	$maindisposation=$_POST['maindisposation'];
$inline=$_POST['inline'];
$package=$_POST['package'];
$comments=$_POST['comments'];
$pid=$_POST['pid'];
$date_done=date('Y-m-d H:i:s');
$expectedpaydate=$_POST['expectedpaydate'];
	$value=1;
$payd=$_POST['payd'];
if(isset($_POST['create']))
{


		$queryUpdate="UPDATE tbl_clients SET main_dispo_rem=?,activated_rem=?, activatedpackage_rem=?,comments_rem=?,expectedpaydate_rem=?,reminder1=?,reminder1date=?,pay_status=? WHERE lead_id=?";
$statement = $db->prepare($queryUpdate);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('ssssssssi',$maindisposation,$inline,$package,$comments,$expectedpaydate,$value,$date_done,$payd,$pid); 


if($statement->execute()){
	
	echo "Update Successful";

}	
else{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();
?>
<script>
 window.location="?rd=r&fm=/";
 </script>
<?php
	
}


//Start Terminate Disposatiosn

if(isset($_POST['terminate']))
{

$queryUpdate="UPDATE tbl_clients SET main_dispo_rem=?,reminder1=?,reminder1date=? WHERE lead_id=?";
$statement = $db->prepare($queryUpdate);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('sssi',$maindisposation,$value,$date_done,$pid); 


if($statement->execute()){
	
?>
<div class="alert alert-success">Update successfull</div>

<?php
   
}else{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();

?>
<script>
 window.location="?rd=r&fm=/";
 </script>
<?php
	
}


}

//Reminder 2


function createReminder2()
{
	global $db;
	$maindisposation=$_POST['maindisposation'];
$inline=$_POST['inline'];
$package=$_POST['package'];
$comments=$_POST['comments'];
$pid=$_POST['pid'];
$date_done=date('Y-m-d H:i:s');
$expectedpaydate=$_POST['expectedpaydate'];
	$value=1;

if(isset($_POST['create']))
{


		$queryUpdate="UPDATE tbl_clients SET main_dispo_rem=?,activated_rem=?, activatedpackage_rem=?,comments_rem=?,expectedpaydate_rem=?,reminder2=?,reminder2date=? WHERE lead_id=?";
$statement = $db->prepare($queryUpdate);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('sssssssi',$maindisposation,$inline,$package,$comments,$expectedpaydate,$value,$date_done,$pid); 


if($statement->execute()){
	
	echo "Update Successful";

}	
else{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();
?>
<script>
 window.location="?rd=r&fm=/";
 </script>
<?php
	
}


if(isset($_POST['terminate']))
{
	$queryUpdate="UPDATE tbl_clients SET main_dispo_rem=?,reminder2=?,reminder2date=? WHERE lead_id=?";
$statement = $db->prepare($queryUpdate);

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
$statement->bind_param('sssi',$maindisposation,$value,$date_done,$pid); 


if($statement->execute()){
	
?>
<div class="alert alert-success">Update successfull</div>

<?php
   
}else{
    die('<div id="flash"  style="color:#F60;font-weight:bold; font-size:14px; margin-left:5%">Error : ('. $db->errno .') '. $db->error).'</div>';
}
$statement->close();
?>
<script>
 window.location="?rd=r&fm=/";
 </script>
<?php
	
}


}