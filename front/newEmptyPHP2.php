<?php
include("../include/config.php");

session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../login.php");
exit();

}
	date_default_timezone_set("Africa/Nairobi");


	if(isset($_GET['int_id']))
	{
   $int_id = $_GET['int_id'];
	}
	else
	{
	$int_id = $_POST['int_id'];
	}
	
	if(isset($_POST['stop_time']))
	{
   $stop_time = $_POST['stop_time'];
	}
	else
	{
	 $stop_time = date("Y-m-d H:i:s");
	}
	
	if(isset($_POST['pageid']))
	{
   $last_page = $_POST['pageid'];
	}
	else
	{
	 $last_page = 'end.php';
	}
	
	
	$sql="UPDATE survey SET stop_time=:val WHERE id =:id";
$stmt = dbConnect()->prepare($sql);                                 
$stmt->bindParam(':val', $stop_time, PDO::PARAM_STR);
$stmt->bindParam(':id',$int_id, PDO::PARAM_STR);  
$stmt->execute(); 

$query_getbank = dbConnect()->prepare("SELECT  survey.Q13,survey.Q13_S,survey.Q4_1,survey.id, survey.interviewer,survey.phone,survey.farmer,survey.clinic_visited,survey.date_visited,survey.Q50,survey.Q68,survey.Q5,survey.Q6,survey.lid FROM  survey WHERE  survey.id = '".$int_id."'");
$query_getbank->execute();
$row_end=$query_getbank->fetch();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- CSS main application styling. -->
    <link rel="icon" type="image/ico" href="../uploadedfiles/school_logo/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
    <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
    <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
    <link rel="stylesheet" type="text/css" href="../css/formelements.css" />
    <link rel="stylesheet" href="../css1/coda-slider-2.0.css" type="text/css" media="screen" />  
     <link rel="stylesheet" href="css/BeatPicker.min.css"/>
 
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/BeatPicker.min.js"></script>
     <script type="text/javascript" src="../../js/js/jquery-1.7.1.min.js"></script>
      <script type="text/javascript" src="../../js/js/chart/highcharts.js"></script>
    <script type="text/javascript" src="../../js/js/custom-form-elements.js"></script>   
   </script>
    <script type="text/javascript" src="../../js/js/jquery-ui.min.js"></script>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <script>
	$(document).ready(function() {
	$("#lodrop").click(function(){
	
            	if ($("#account_drop").is(':hidden')){
                	$("#account_drop").show();
				}
            	else{
                	$("#account_drop").hide();
            	}
            return false;
       			 });
				  $('#account_drop').click(function(e) {
            		e.stopPropagation();
        			});
        		$(document).click(function() {
					if (!$("#account_drop").is(':hidden')){
            		$('#account_drop').hide();
					}
        			});	
                
});
</script>

<script type="text/javascript">
$(document).ready(function()
{
$(".reporting_manager").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;

$.ajax
({
type: "POST",
url: "ajax_city.php",
data: dataString,
cache: false,
success: function(html)
{
$(".reporting_lead").html(html);
} 
});

});
});
</script>


<script>
$(document).ready(function() {
  $(".nav_drop_but").click(function() {
  $(".navigationbtm_wrapper_outer").slideToggle();
	});
});
</script>

<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<link type="text/css" href="../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
  <script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		$('#exdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>

    
</head>
<title>::Rapid Smart Survey on male and female farmers’ satisfaction with plant clinic visits ::</title>
<body>
<div class="wrapper">


    <div class="header">
     
   <div class="lo_drop" id="account_drop">
     <div class="lo_drop_hov"></div> 
     	<div class="lo_name">
        <?php ?><?php ?>
 <span> <?php echo $_SESSION['name']; ?> </span>
            <div class="clear"></div>
        </div>
    <ul>
        	<li><a href="profile.php"><?php echo 'My Account';?></li>
            <li><a href="settings.php"><?php echo 'Settings';?></a></li>
            <li> <a href="../logout.php"><?php echo 'Logout';?></a></li>
        </ul>
     </div>
     
     
   
	
    
        	<div class="logo">
            <a href="index.php"><img src="../images/logo-plantwise.png" alt=""  border="0" />		</a> </div>
            
			
			 <div class="">
            
<?php include('app_nav.php');?>
               
            </div>
    
    
      </div>
     
    
     
    <div class="midnav">
    
   
        <a class="first-letter"> Home</a>
		 <span>Leads Management</span>
		   <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
     </div>

     
     <div class="container">
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
  <?php include('../left_side.php');?>
    
    </td>
    <td valign="top">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top" width="75%"><div style="padding-left:20px; padding-right:10px;">
<h3 align="center">THANK RESPONDENT AND CLOSE INTERVIEW</h3>




<div class="formCon2" >

<div class="">


<form name="frmPriority" action="end_final.php" enctype="multipart/form-data" method="post">


      
      <p align="center"><strong>SELECT DISPOSITION<?php echo $int_id;?></strong></p>
      	
        <div id="spryradio1" align="center">
      	<table align="center">
		<?php if ($row_end['Q4']=='No' && $row_end['Q4_1']=='No')
		{
			?>
			
        <tr><td class="tbl"><input type="radio" name="disptype" value="Complete Survey-Didnt listen to any Agricultural Radio Program and did not receive sms" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>Complete Survey-Didnt listen to any Agricultural Radio Program and did not receive sms</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		<?php
		}
		 else if ($row_end['Q6']=='Others' && $row_end['Q4_1']=='No')
		{
			?>
			
        <tr><td class="tbl"><input type="radio" name="disptype" value="Complete Survey-Listened to different Radio Program not received sms" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>Complete Survey-Listened to different Radio Program not received sms</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		<?php
		}
                else if ($row_end['Q4']=='Yes' && $row_end['Q4_1']=='No' && $row_end['Q13']!='')
		{
			?>
			
        <tr><td class="tbl"><input type="radio" name="disptype" value="Complete Survey Listened to Radio-Not received sms" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>Complete Survey Listened to Radio-Not received sms</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		<?php
		}
                else if ($row_end['Q4']=='No' && $row_end['Q4_1']=='Yes' && $row_end['Q13_S']!='')
		{
			?>
			
        <tr><td class="tbl"><input type="radio" name="disptype" value="Complete Survey received SMS-Not Listened to Radio Programme" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>Complete Survey received SMS-Not Listened to Radio Programme</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		<?php
		}
                else if ($row_end['Q6']=='Others' && $row_end['Q4_1']=='Yes' && $row_end['Q13_S']!='')
		{
			?>
			
        <tr><td class="tbl"><input type="radio" name="disptype" value="Complete Survey SMS-Listened to Other Radio Programme" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>Complete Survey-Listened to Other Radio Program</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		<?php
		}
                else if ($row_end['Q13']!='' && $row_end['Q13_S']!='')
		{
			?>
			
        <tr><td class="tbl"><input type="radio" name="disptype" value="Complete Survey-Listened to Radio & Received Sms" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>Complete Survey-Listened to Radio & Received Sms</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		<?php
		}
		else if (isset($_POST['terminate']) && $_POST['terminate']=='Yes')
		{
			?>
			
        <tr>
          <td class="tbl"><input type="radio" name="disptype" value="Partial Survey-Call Back"  onclick = "showTable(2)"/>
            <strong>Partial Survey-Call Back</strong></td>
          <td class="tbl">&nbsp;</td>
        </tr>
        <tr><td class="tbl"><input type="radio" name="disptype" value="Partial Survey-Customer Hang Up"  onclick = "showTable(2)"/> <span style=""><strong>Partial Survey-Customer Hang Up</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		 <tr><td class="tbl"><input type="radio" name="disptype" value="Partial Survey-Call Disconnected"  onclick = "showTable(2)"/> <span style=""><strong>Partial Survey-Call Disconnected</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		 <tr><td class="tbl"><input type="radio" name="disptype" value="Partial Survey-Not Interested to continue Survey"  onclick = "showTable(2)"/> <span style=""><strong>Partial Survey-Not Interested to continue Survey</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		<?php
		}
		else
		{
			?>
		   <tr><td class="tbl"><input type="radio" name="disptype" value="Complete Survey" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>Complete Survey</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
		 <!-- <tr  id="tablecallC" style="display:none">
		
          <td class="tbl">
		  Kindly Confirm these Details as received from the Client<br/>
		  Appointment Date
            <label for="textfield"></label>
            <input type="text" name="appointment" id="datepicker2" size="15"/>
			
			
			</td>
      
	    <td class="tbl"></td>
        </tr>
        <tr><td class="tbl"><input type="radio" name="disptype" value="Partial Questionnaire" onclick = "showTable(2)"/> <span style="color:#138135"><strong>Partial Survey</strong></span></td>
          <td class="tbl">&nbsp;</td>
        </tr>
        <tr><td class="tbl"><input type="radio" name="disptype" value="Not Interested" onclick = "showTable(2)"/> Not Interested</td>
          <td class="tbl">&nbsp;</td>
        </tr>
        <tr><td class="tbl"><input type="radio" name="disptype" value="Language Barrier" onclick = "showTable(2)"/> Language Barrier</td>
          <td class="tbl">&nbsp;</td>
        </tr>
        <tr><td class="tbl"><input type="radio" name="disptype" value="Call Back" onclick = "showTable(1)"/> Call Back</td>
          <td class="tbl">&nbsp;</td>
        </tr>
        <tr  id="tablecallB" style="display:none">
          <td class="tbl">Date
            <label for="textfield"></label>
            <input type="text" name="callback" id="datepicker" size="15"/></td>
          <td class="tbl"><select name="time" id="time">
              <option value="">Choose</option>
              <option value="7:00am">7:00am</option>
              <option value="8:00am">8:00am</option>
              <option value="9:00am">9:00am</option>
              <option value="10:00am">10:00am</option>
              <option value="11:00am">11:00am</option>
              <option value="12:00noon">12:00noon</option>
              <option value="01:00pm">01:00pm</option>
              <option value="02:00pm">02:00pm</option>
              <option value="03:00pm">03:00pm</option>
              <option value="04:00pm">04:00pm</option>
              <option value="05:00pm">05:00pm</option>
              <option value="06:00pm">06:00pm</option>
              <option value="07:00pm">07:00pm</option>
              <option value="08:00pm">08:00pm</option>
              <option value="09:00pm">09:00pm</option>
            </select></td>
        </tr>
      
        <tr>
          <td class="tbl"><input type="radio" name="disptype" value="Customer not able to hear Interviewer" onclick = "showTable(2)"/> 
            Customer not able to hear Interviewer</td>
          <td class="tbl">&nbsp;</td>
        </tr>
        <tr>
          <td class="tbl"><input type="radio" name="disptype" value="Interviewer not able to hear customer" onclick = "showTable(2)"/> 
            Interviewer not able to hear customer/Farmer</td>
          <td class="tbl">&nbsp;</td>
        </tr>
       
        <tr>
          <td class="tbl"><input type="radio" name="disptype" value="Poor Network" onclick = "showTable(2)"/> 
            Poor Network</td>
          <td class="tbl">&nbsp;</td>
        </tr>
        <tr>
          <td class="tbl"><input type="radio" name="disptype" value="Terminated due to Validation " onclick = "showTable(2)"/> 
            Terminated due to Validation </td>
          <td class="tbl">&nbsp;</td>
        </tr>-->
		<?php
		}
		
		?>
		
		
        </table>
       
        
      <p></p>
        
        <p align="center"><strong>DISPOSITION COMMENTS</strong></p>
        <p align="center"><textarea cols="40" rows="3" name="disposition"></textarea></p>
	
	<p align="center">
      <input type="hidden" id="action" name="action" value="submitform" />
      <input type="hidden" id="int_id" name="int_id" value="<?php echo $row_end['id']; ?>" />
	  <input type="hidden" id="" name="page_id" value="<?php echo $last_page; ?>" />
      <input type="hidden" id="lid" name="lid" value="<?php echo $row_end['lid']; ?>" />
      <input type="submit" name="submit" 
	  style=" padding:0px 20px;
	background:red;
	height:30px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#FCFCFC;
	font-size:13px;
	cursor:pointer;
	"
	  value="Close Survey" />
	</p>
  	</form>
    
 

<tr>
                                 
</div>
</div>


                </form>
</td>
        
      </tr>
	  
    </table></form>
    </td>
  </tr>
  
</table>
    </div>
 <div class="midfooter">
    
   
        <a class="first-letter"> &copy <?php echo date('Y');?> Developed and Designed by Techno Brain BPO/ITES</a>
		
     </div>
	 
	 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
$(function() {
$( "#datepicker,#datepicker2,#datepicker3").datepicker({
dateFormat: 'yy-mm-dd' });
});
function showTable(which) {
if (which ==1) {
document.getElementById("tablecallB").style.display="table-row";
document.getElementById("tablecallC").style.display="none";
document.getElementById("tablecallD").style.display="none";
}
else if(which ==2)
{
	document.getElementById("tablecallB").style.display="none";
	document.getElementById("datepicker").value="";
	document.getElementById("time").value="";
	document.getElementById("tablecallC").style.display="none";
	document.getElementById("tablecallD").style.display="none";
}
else if(which ==3)
{
	document.getElementById("tablecallB").style.display="none";
	document.getElementById("datepicker").value="";
	document.getElementById("time").value="";
	document.getElementById("tablecallC").style.display="table-row";
	document.getElementById("tablecallD").style.display="none";
}
else if(which ==4)
{
	document.getElementById("tablecallB").style.display="none";
	document.getElementById("datepicker").value="";
	document.getElementById("time").value="";
	document.getElementById("tablecallC").style.display="none";
	document.getElementById("tablecallD").style.display="table-row";
}
}
</script>
</body>
</html>