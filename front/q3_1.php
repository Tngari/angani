<?php
include("../include/config.php");

session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../login.php");
    exit();
}

$int_id = $_GET['int_id'];

$query_getbank = dbConnect()->prepare("SELECT  survey.id,  survey.interviewer,survey.phone,survey.name, survey.lid FROM  survey WHERE  survey.id = '" . $int_id . "'");
$query_getbank->execute();
$row_getbank = $query_getbank->fetch();


$id2=$row_getbank['lid'];

$query_getbank2 = dbConnect()->prepare("SELECT  * FROM  leads WHERE  leads.id = '" . $id2 . "'");
$query_getbank2->execute();
$row_getbank2 = $query_getbank2->fetch();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <!-- CSS main application styling. -->
        <link rel="icon" type="image/ico" href="../uploadedfiles/school_logo/favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
        <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
        <link rel="stylesheet" type="text/css" href="../css/formelements.css" />
        <link rel="stylesheet" href="../css1/coda-slider-2.0.css" type="text/css" media="screen" />  
        <link rel="stylesheet" href="css/BeatPicker.min.css"/>

        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/BeatPicker.min.js"></script>
        <script type="text/javascript" src="../../js/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="../../js/js/chart/highcharts.js"></script>
        <script type="text/javascript" src="../../js/js/custom-form-elements.js"></script>   
        </script>
        <script type="text/javascript" src="../../js/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

		
		 <script>
                $(function () {
                    $("#v11").click(function () {
                        if ($(this).is(":checked")) {
                            $("#other1").removeAttr("disabled");
                            $("#other1").attr("required", "required");
                            $("#other1").focus();
                        } else {
                            $("#other1").attr("disabled", "disabled");
                            $("#other1").removeAttr("required");
                            document.getElementById("other1").value = "";
                        }
                    });
                });
            </script>
            <script>
                $(function () {
                    $("#v6").click(function () {
                        if ($(this).is(":checked")) {
                            $("#other").removeAttr("disabled");
                            $("#other").attr("required", "required");
                            $("#other").focus();
                        }
                    });
                    $("#v1,#v2,#v3,#v4,#v5").click(function () {
                        if ($(this).is(":checked")) {
                            $("#other").removeAttr("required");
                            $("#other").attr("disabled", "disabled");
                            document.getElementById("other").value = "";
                        }
                    });
                });
            </script>
        
<script type="text/javascript">
    
    function showTable(which) {
        if (which == 1) {
            document.getElementById("bvisited").style.display = "block";
            document.getElementById("medt").value = "";
            document.getElementById("time").value = "";
        } else if (which = 2)
        {
            document.getElementById("bvisited").style.display = "none";
            document.getElementById("medt").value = "";
            document.getElementById("time").value = "";
        }
    }


    function showTable2(which) {
        if (which == 1) {
            //document.getElementById("q20").style.display="none";
            document.getElementById("q21").style.display = "block";
          document.getElementById("1").required = true;
            //document.getElementById("intro_10").value = "";
            //document.getElementById("intro_11").value = "";
            //document.getElementById("intro_11").checked = false;
            //document.getElementById("intro_10").checked = false;
        } else if (which = 2)
        {
            //  document.getElementById("q20").style.display="none";
            document.getElementById("q21").style.display = "none";
            document.getElementById("1").value ="";
            document.getElementById("1").required = false;
            //document.getElementsByName("intro_11").required = false;
            //document.getElementsByName("intro_10").required = false;
        }
    }
    function showTable3(which) {
        if (which == 1) {
            //document.getElementById("q20").style.display="none";
            document.getElementById("q22").style.display = "block";
            document.getElementsByName("q18").required = false;

            document.getElementById("v1").checked = false;
            document.getElementById("v2").checked = false;
            document.getElementById("v3").checked = false;
            document.getElementById("v4").checked = false;
            $("#v1").removeAttr("required");
            $("#v2").removeAttr("required");
            $("#v3").removeAttr("required");
            $("#v4").removeAttr("required")
            //document.getElementById("intro_c0").value = "";
            //document.getElementById("intro_c1").value = "";
            //document.getElementById("intro_c2").value = "";
            //document.getElementById("intro_c3").value = "";
            //document.getElementById("intro_c0").checked = false;
            //document.getElementById("intro_c1").checked = false;
            //document.getElementById("intro_c2").checked = false;
            //document.getElementById("intro_c3").checked = false;
            //document.getElementById("intro_10").value = "";
            //document.getElementById("intro_11").value = "";
            //document.getElementById("intro_11").checked = false;
            //document.getElementById("intro_10").checked = false;
        } else if (which = 2)
        {
            //  document.getElementById("q20").style.display="none";
            document.getElementById("q22").style.display = "none";
            document.getElementsByName("q18").required = true;

            document.getElementById("v1").checked = false;
            document.getElementById("v2").checked = false;
            document.getElementById("v3").checked = false;
            document.getElementById("v4").checked = false;
            //document.getElementById("intro_10").value = "";
            //document.getElementById("intro_11").value = "";
            // document.getElementById("intro_11").checked = false;
            //document.getElementById("intro_10").checked = false;
            //document.getElementById("intro_10").value = "";
            // document.getElementById("intro_11").value = "";
            //document.getElementById("intro_11").checked = false;
            //document.getElementById("intro_10").checked = false;
            //document.getElementsByName("intro_11").required = false;
            //document.getElementsByName("intro_10").required = false;
        }
    }
    function showTableApointment(which) {
        if (which == 1) {
        document.getElementById("apointment").style.display = "none";
                document.getElementById("fromdt").val() = '';
        //document.getElementById("p2").style.display ="table-row";
        }
        else if (which = 2)
        {
            document.getElementById("apointment").style.display = "block";
                    document.getElementById("fromdt").val() = '';
        }
    }
    function showOther()
    {
        var c = document.getElementById("constraints").value;
        if (c == "Other reasons")
        {
            document.getElementById("other").style.display = "block";
        } else
        {

            document.getElementById("other").style.display = "none";
        }
    }


    function showOther2()
    {
        var c = document.getElementById("constraints2").value;
        if (c == "Other reasons")
        {
            document.getElementById("other2").style.display = "block";
        } else
        {

            document.getElementById("other2").style.display = "none";
        }
    }
</script>
	<script language="Javascript" type="text/javascript">

 

        function onlyNos(e, t) {

            try {

                if (window.event) {

                    var charCode = window.event.keyCode;

                }

                else if (e) {

                    var charCode = e.which;

                }

                else { return true; }

                if (charCode > 31 && (charCode < 48 || charCode > 57)) {

                    return false;

                }

                return true;

            }

            catch (err) {

                alert(err.Description);

            }

        }

 

    </script>	
        <script>
            $(document).ready(function () {
                $("#lodrop").click(function () {

                    if ($("#account_drop").is(':hidden')) {
                        $("#account_drop").show();
                    } else {
                        $("#account_drop").hide();
                    }
                    return false;
                });
                $('#account_drop').click(function (e) {
                    e.stopPropagation();
                });
                $(document).click(function () {
                    if (!$("#account_drop").is(':hidden')) {
                        $('#account_drop').hide();
                    }
                });

            });
        </script>

        <script type="text/javascript">
            $(document).ready(function ()
            {
                $(".reporting_manager").change(function ()
                {
                    var id = $(this).val();
                    var dataString = 'id=' + id;

                    $.ajax
                            ({
                                type: "POST",
                                url: "ajax_city.php",
                                data: dataString,
                                cache: false,
                                success: function (html)
                                {
                                    $(".reporting_lead").html(html);
                                }
                            });

                });
            });
        </script>


        <script>
            $(document).ready(function () {
                $(".nav_drop_but").click(function () {
                    $(".navigationbtm_wrapper_outer").slideToggle();
                });
            });
        </script>

        <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="../js/table2CSV.js" ></script>
        <link type="text/css" href="../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
        <script type="text/javascript">
            $(function () {
                $('#fromdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('#todt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
                $('#exdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });
        </script>
        <script>
$(function () {
                $("#intro_0").click(function () {
                    if ($(this).is(":checked")) {
                        $("#textbox").removeAttr("disabled");
                        $("#textbox").attr("required", "required");
                        $("#textbox").focus();
                    }
                });
                $("#yes4,#no4,#intro_3,#intro_4,#intro_5,#intro_6").click(function () {
                    if ($(this).is(":checked")) {
                        $("#textbox").removeAttr("required");
                        $("#textbox").attr("disabled", "disabled");
                        document.getElementById("textbox").value = "";
                    }
                });
            });

            $(function () {
                $("#intro_51").click(function () {
                    if ($(this).is(":checked")) {
                        $("#textbox1").removeAttr("disabled");
                        $("#textbox1").attr("required", "required");
                        $("#textbox1").focus();
                    }
                });
                $("#intro_11,#intro_21,#intro_31,#intro_41,#intro_01,#intro_61").click(function () {
                    if ($(this).is(":checked")) {
                        $("#textbox1").removeAttr("required");
                        $("#textbox1").attr("disabled", "disabled");
                        document.getElementById("textbox1").value = "";
                    }
                });
            });


            $(function () {
                $("#intro_50").click(function () {
                    if ($(this).is(":checked")) {
                        $("#textbox2").removeAttr("disabled");
                        $("#textbox2").attr("required", "required");
                        $("#textbox2").focus();
                    }
                });
                $("#intro_10,#intro_20,#intro_30,#intro_40,#intro_00,#intro_60").click(function () {
                    if ($(this).is(":checked")) {
                        $("#textbox2").removeAttr("required");
                        $("#textbox2").attr("disabled", "disabled");
                        document.getElementById("textbox2").value = "";
                    }
                });
            });
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
        </script>
    </head>
    <title>::ANGANI::</title>
    <body>
        <div class="wrapper">


            <div class="header">

                <div class="lo_drop" id="account_drop">
                    <div class="lo_drop_hov"></div> 
                    <div class="lo_name">
                        <?php ?><?php ?>
                        <span> <?php echo $_SESSION['name']; ?> </span>
                        <div class="clear"></div>
                    </div>
                    <ul>
                        <li><a href="profile.php"><?php echo 'My Account'; ?></li>
                        <li><a href="settings.php"><?php echo 'Settings'; ?></a></li>
                        <li> <a href="../logout.php"><?php echo 'Logout'; ?></a></li>
                    </ul>
                </div>





                <div class="logo">
                    <a href="leads.php"><img src="../images/logo.png" alt="" height="67" border="0" />		</a> </div>


                <div class="">

                    <?php include('app_nav.php'); ?>

                </div>


            </div>



            <div class="midnav">


                <a class="first-letter"> Home</a>
                <span>Leads Management</span>
                <span style="float:right"><a href="../logout.php"> Logout</a></span>
                <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>
            </div>


            <div class="container">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="247" valign="top">

                            <?php include('../left_side.php'); ?>

                        </td>
                        <td valign="top">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="top" width="75%"><div style="padding-left:20px; padding-right:10px;">


                                            <h3></h3>

 <div class="formCon" >
     
                                               
                                                <br></br>
                                <div class="">

                                    <form name="frmRespondent" action="3_2.php" enctype="multipart/form-data" method="post" >  

                                            <table width="708" align="center">
                                              <tr>
                        <td align="right"><div align="left"></tr>                              
                               
  
                    <tr>
                        <td align="right" id="q4"><div align="left">
                          <p><strong>
                            
                          <BR>Q4.  Fantastic…. The reason we called was to inform you about our disaster recovery solution that will significantly reduce your IT infrastructure costs while ensuring you have a secure and reliable platform to store your business data. Shall we continue?</a></strong></p>
                          <p><strong>
                            <input name="q4" type="radio" onclick="MM_showHideLayers('q5','','show','q6','','show','q6','','show')"  value="yes"  />
                            YES</strong></p>
                          <p>
                            <input name="q4" type="radio" onclick="MM_showHideLayers('q5','','hide','q6','','hide')" value="no" />
No </p>
                                </tr>
                   

                   
                   <tr>
                   
                    <td align="right" id="q5"><div align="left">
                     
 
	
    	<p> <strong>Q5. So I believe securing your business information is extremely critical. Is this something that your business is doing? </strong></p>
    	<p>
    	  <input type="radio" name="q5"  value="yes"  />
YES </p>
    	<p>
    	  <input type="radio" name="q5"  value="no" />
No </p>
                    </div> </td></tr>
                   <tr id="q6">
                    <td align="right" id="q6"><div align="left"> 
                      <p><strong>Q6. Ok. And you do agree that ensuring that, it’s backed up automatically in a secure, off site location gives you a piece of mind that your business info is safe?	</strong></p>
                  	<p>
    	  <input type="radio" name="q6"  value="yes"  />
YES </p>
    	<p>
    	  <input type="radio" name="q5"  value="no" />
No </p>
                    </div> </td></tr>
    


                                </table>	
                                </span>

   
																		

                                                                      <p align="center">
                                                                        <input type="hidden" id="action" name="action" value="submitform" />
                                                                         <input type="hidden" id="int_id" name="int_id" value="<?php echo $row_getbank['id'];?>" />
                                                                        <input type="hidden" id="int_id" name="lid" value="<?php echo $row_getbank['lid']; ?>" />

                                                                            <input type="submit" name="submit" 
                                                                                   style=" padding:0px 20px;
                                                                                   background:url(../img/fbut-bg.png) repeat-x;
                                                                                   height:30px;
                                                                                   -webkit-border-radius: 4px;
                                                                                   -moz-border-radius: 4px;
                                                                                   border-radius: 4px;
                                                                                   border:1px #b58530 solid;
                                                                                   color:#633c15;
                                                                                   font-size:13px;
                                                                                   cursor:pointer;
                                                                                   "
                                                                                     value="Save & Continue" />
                                      </p>

                                  </form>



                                                                        <tr>

                </div>
                                                                    </div>



                                                                </td>

                                                            </tr>

                                                        </table>
                                                                                                                                    <!--<form name="frmTerminate" action="end.php" enctype="multipart/form-data" method="post">
                                                                                                                                          <p align="right">
                                                                                                                                          <input type="hidden" name="pageid" value="<?php
                                                                                                                                    $currentFile = $_SERVER["PHP_SELF"];
                                                                                                                                    $parts = Explode('/', $currentFile);
                                                                                                                                    echo $parts[count($parts) - 1];
                                                                                                                                    ?>" />
                                                                                                                                  <input type="hidden" id="stop_time" name="stop_time" value="<?php echo $row_getbank['date']; ?>" />
                                                                                                                                  <input type="hidden" id="action" name="action" value="submitform" />
                                                                                                                                  <input type="hidden" id="int_id" name="int_id" value="<?php echo $int_id; ?>" />
                                                                                                                                          <input type="hidden" id="" name="terminate" value="Yes" />
                                                                                                                                  <input type="submit" name="submit" 
                                                                                                                                          style=" padding:0px 20px;
                                                                                                                                  background:red;
                                                                                                                                  height:30px;
                                                                                                                                  -webkit-border-radius: 4px;
                                                                                                                                  -moz-border-radius: 4px;
                                                                                                                                  border-radius: 4px;
                                                                                                                                  border:1px #b58530 solid;
                                                                                                                                  color:#FCFCFC;
                                                                                                                                  font-size:13px;
                                                                                                                                  cursor:pointer;
                                                                                                                                  "
                                                                                                                                          value="Click Here To Terminate" onclick="return confirm('Are you sure you want to TERMINATE? This operation cannot be undone!');" />
                                                                                                                                  </p>
                                                                                                                                  </form>-->
                                                                                                                            </td>
                                                                                                                        </tr>

                                                                                                                        </table>
                                <form name="frmTerminate" action="terminate.php" enctype="multipart/form-data" method="post">
                                                                                    <p align="right">
                                                                                        <input type="hidden" name="pageid" value="<?php $currentFile = $_SERVER["PHP_SELF"];
    $parts = Explode('/', $currentFile);
    echo $parts[count($parts) - 1]; ?>" />
                                                                                        <input type="hidden" id="stop_time" name="stop_time" value="<?php echo $row_getbank['date']; ?>" />
                                                                                        <input type="hidden" id="action" name="action" value="submitform" />
                                                                                        <input type="hidden" id="int_id" name="int_id" value="<?php echo $int_id; ?>" />
                                                                                        <input type="hidden" id="" name="terminate" value="Yes" />
                                                                                        <input type="submit" name="submit" 
                                                                                               style=" padding:0px 20px;
                                                                                               background:red;
                                                                                               height:30px;
                                                                                               -webkit-border-radius: 4px;
                                                                                               -moz-border-radius: 4px;
                                                                                               border-radius: 4px;
                                                                                               border:1px #b58530 solid;
                                                                                               color:#FCFCFC;
                                                                                               font-size:13px;
                                                                                               cursor:pointer;
                                                                                                 "
                                                                                               value="Click Here To Terminate" onclick="return confirm('Are you sure you want to TERMINATE? This operation cannot be undone!');" />
                                                                                    </p>
                                                                                </form>
                                                                                                                    </div>
                                                                                                                    <div class="midfooter">


                                                                                                                        <a class="first-letter"> &copy <?php echo date('Y'); ?> Developed and Designed by Techno Brain BPO/ITES</a>

                                                                                                                    </div>
                                                                                                                    </body>
                                                                                                                    </html>
