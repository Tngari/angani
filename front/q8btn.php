<?php
include("../include/config.php");

session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../login.php");
exit();

}
if(isset($_POST['action']) && $_POST['action'] == 'submitform')
{
	$q8=$_POST['q8'];
	$other=$_POST['other'];
	$int_id=$_POST['int_id'];
	$lid=$_POST['lid'];
	$sql="UPDATE survey SET Q8=:val,Q8other=:other WHERE id =:id";
$stmt = dbConnect()->prepare($sql);                                 
$stmt->bindParam(':val',$q8, PDO::PARAM_STR);
$stmt->bindParam(':other',$other, PDO::PARAM_STR);
$stmt->bindParam(':id',$int_id, PDO::PARAM_STR);  
$stmt->execute(); 

if($_POST['received_sms']=='Yes' || $_POST['received_sms']=='YES'  || $_POST['received_sms']=='yes'  ){
	
//	if($q8==''){
//		header("location:end.php?int_id=$int_id");	
//		exit;
//		}
		
	header("location:q8.php?int_id=$int_id");	
}else{
	header("location:end.php?int_id=$int_id");	
}

}
?>
