<?php
include("../include/config.php");
//error_reporting(0);
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../login.php");
exit();

}
if(isset($_GET['int_id']))
	{
  $int_id = $_GET['int_id'];
	}
	
if(isset($_POST['action']) && $_POST['action'] == 'submitform')
{
	$q2=$_POST['q2'];
	$q3=$_POST['q3'];
	$call_date=$_POST['callback'];
	$call_time=$_POST['time'];
	$int_id=$_POST['int_id'];
	$lid=$_POST['lid'];
	
	
	$sql="UPDATE survey SET Q2=:val WHERE id =:id";
$stmt = dbConnect()->prepare($sql);                                 
$stmt->bindParam(':val',$q2, PDO::PARAM_STR);
$stmt->bindParam(':id',$int_id, PDO::PARAM_STR);  
$stmt->execute(); 

if(isset($q3) && $q3=='Yes')
{
$vals=$call_date.' '.$call_time;
$dispo='Call Back';
	$sql2="UPDATE survey SET Q3=:val,Q4=:valx,disposition=:val2,disptype=:val3,callbacktime=:val4 WHERE id =:id";
$stmt2 = dbConnect()->prepare($sql2);                                 
$stmt2->bindParam(':val',$call_date, PDO::PARAM_STR);
$stmt2->bindParam(':valx',$call_time, PDO::PARAM_STR);
$stmt2->bindParam(':val2',$dispo, PDO::PARAM_STR);
$stmt2->bindParam(':val3',$dispo, PDO::PARAM_STR);
$stmt2->bindParam(':val4',$vals, PDO::PARAM_STR)
        
        ;
$stmt2->bindParam(':id',$int_id, PDO::PARAM_STR);  
$stmt2->execute();
header("location:end_final.php?int_id=$int_id");

}


if(isset($q3) && ($q3=='No' || $q3=='No'))

{
$dispo='Not interested';
$sql4="UPDATE survey SET Q2=:val,disposition=:val2,disptype=:val3 WHERE id =:id";
$stmt4 = dbConnect()->prepare($sql4);                                 
$stmt4->bindParam(':val',$q2, PDO::PARAM_STR);
$stmt4->bindParam(':val2',$dispo, PDO::PARAM_STR);
$stmt4->bindParam(':val3',$dispo, PDO::PARAM_STR);
$stmt4->bindParam(':id',$int_id, PDO::PARAM_STR);  
$stmt4->execute();
header("location:end_end.php?int_id=$int_id");

}


	
}


?>
