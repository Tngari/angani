<?php require_once('../Connections/angani.php'); ?>
<?php //require_once('../Connections/dbconnect.php'); ?>
<?php //require_once('connect.php');?>  
<?php //echo host.",".user.",".pass.",".db; exit; ?>
<?php 
// Turn off all error reporting
error_reporting(0);

if (!isset($_SESSION)) {
	session_start();  	
}
if (!isset($_GET['category'])) {
	$nocategoryGoTo = "faq.php?category=Ground Nuts Farming";
	header(sprintf("Location: %s", $nocategoryGoTo));
}
// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


?>
<?php //require_once('../Connections/dbconnect.php'); ?>
<?php require_once('config.php'); ?>
<?php
//Mapping DW with old code
# FileName="Connection_php_mysql.htm"
# Type="MYSQL"
# HTTP="true"
$hostname_dbconnect = host;
$database_dbconnect = db;
$username_dbconnect = user;
$password_dbconnect = pass;
$dbconnect = mysql_pconnect($hostname_dbconnect, $username_dbconnect, $password_dbconnect) or trigger_error(mysql_error(),E_USER_ERROR); 
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "editFrom")) {
  $updateSQL = sprintf("UPDATE chlfaq SET question=%s, answer=%s WHERE id=%s",
                       GetSQLValueString($_POST['question'], "text"),
                       GetSQLValueString($_POST['answer'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_dbconnect, $dbconnect);
  $Result1 = mysql_query($updateSQL, $dbconnect) or die(mysql_error());

  $updateGoTo = "faq.php?action=edit&editfaq=1&category=".$_GET['category'];
//  if (isset($_SERVER['QUERY_STRING'])) {
//    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
//    $updateGoTo .= $_SERVER['QUERY_STRING'];
//  }
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form")) {
  $insertSQL = sprintf("INSERT INTO chlfaq (question, answer, category, ip, user) VALUES (%s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['Question'], "text"),
                       GetSQLValueString($_POST['Answer'], "text"),
                       GetSQLValueString($_POST['Category'], "text"),
					   GetSQLValueString(get_client_ip() , "text"),
					   GetSQLValueString($_SESSION['name'], "text"));

  mysql_select_db($database_dbconnect, $dbconnect);
  $Result1 = mysql_query($insertSQL, $dbconnect) or die(mysql_error());

  $insertGoTo = "faq.php?savedfaq=1&category=".$_POST['Category'];
//  if (isset($_SERVER['QUERY_STRING'])) {
//    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
//    $insertGoTo .= $_SERVER['QUERY_STRING'];
//  }
  header(sprintf("Location: %s", $insertGoTo));
}

$colname_RecordsetGetFAQs = "-1";
if (isset($_GET['category'])) {
  $colname_RecordsetGetFAQs = $_GET['category'];
}
mysql_select_db($database_angani, $angani);
$query_RecordsetGetFAQs = sprintf("SELECT question, answer, id FROM chlfaq WHERE category = %s ORDER BY id DESC", GetSQLValueString($colname_RecordsetGetFAQs, "text"));
$RecordsetGetFAQs = mysql_query($query_RecordsetGetFAQs, $angani) or die(mysql_error());
$row_RecordsetGetFAQs = mysql_fetch_assoc($RecordsetGetFAQs);
$totalRows_RecordsetGetFAQs = mysql_num_rows($RecordsetGetFAQs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FAQ</title>
<script src="../SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">
<link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css">
<link href="../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="../SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
<link href="../SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css">
<link href="../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script type="text/javascript">
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
</script>
</head>

<body style="font-size:16px;">
<div style="padding:10px; margin:3px; background-color:#79A341;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td style=" width:190px;" align="left" valign="top">
        <ul id="MenuBarCategory" class="MenuBarVertical">
          <li<?php if ($_GET['category']=="Ground Nuts Farming"){echo " class='current' ";} ?>><a href="faq.php?category=Ground Nuts Farming">Ground Nuts Farming</a></li>
          <li<?php if ($_GET['category']=="Dairy Farming"){echo " class='current' ";} ?>><a href="faq.php?category=Dairy Farming">Dairy Farming</a></li>
          <li<?php if ($_GET['category']=="Irish Potatoes Farming"){echo " class='current' ";} ?>><a href="faq.php?category=Irish Potatoes Farming">Irish Potatoes Farming</a></li>
          <li<?php if ($_GET['category']=="Soya Beans farming"){echo " class='current' ";} ?>><a href="faq.php?category=Soya Beans farming">Soya Beans farming</a></li>
          <li<?php if ($_GET['category']=="General Inquiries"){echo " class='current' ";} ?>><a href="faq.php?category=General Inquiries">General Inquiries</a>
        </ul>
        </td>
        <td style="background-color:#FFC;" align="left">
        <div style="height:30px;">
        <input name="CloseFAQ" type="button" value="Close FAQ" onclick="window.open('','_self').close();" style="float:right;" />
        </div>
        <?php if ($_SESSION['status'] == "Supervisor"){ ?>
        <div style="margin-left:10px;">
        <div style="padding:20px 10px; border-bottom:#D5D5D5 solid 1px;">
        <input name="AddFAQ" type="submit" id="AddFAQ" onclick="MM_callJS('document.getElementById(\'addFAQdiv\').style.height = \'270px\';')" value="New FAQ" />
        <input name="EditFAQ" type="button" <?php if($_GET['action']=="edit"){ echo "disabled='disabled'"; } ?> onclick="MM_goToURL('parent','faq.php?action=edit&category=<?php echo $_GET['category'] ?>');return document.MM_returnValue" value="Edit FAQs" />
        
        </div>
      <form name="form" action="<?php echo $editFormAction; ?>" method="POST">
      <?php if ($_GET['savedfaq']==1){ ?>
      <div style="padding:10px; font-size:18px; font-weight:bold; color:#008040; margin:5px; border:#008040 solid 1px; background-color:#FFF;">
        FAQ Saved Successfully 
        </div>
         <?php }?>
           <?php if ($_GET['editfaq']==1){ ?>
      <div style="padding:10px; font-size:18px; font-weight:bold; color:#008040; margin:5px; border:#008040 solid 1px; background-color:#FFF;">
        FAQ Edited Successfully 
        </div>
         <?php }?>
         <div id="addFAQdiv" style="overflow:hidden; height:0;">
        <table width="100%" border="0" cellpadding="6" cellspacing="0" id="AddFAQFrm">
      
      <tr>
        <td colspan="2">
        <div style="padding:10px 0;  font-size:18px; font-weight:bold; color:#008040; margin:5px; border-bottom:#D5D5D5 solid 1px;">
        New Frequently Asked Question. 
        </div>
        </td>
       </tr>
     
      <tr>
        <td width="8%">Category</td>
        <td width="92%"><span id="spryselect1">
          <select name="Category" id="Category">
            <option>Please select category</option>
            <option value="Ground Nuts Farming">Ground Nuts Farming</option>
            <option value="Dairy Farming">Dairy Farming</option>
            <option value="Irish Potatoes Farming">Irish Potatoes Farming</option>
            <option value="Soya Beans farming">Soya Beans farming</option>
            <option value="General Inquiries">General Inquiries</option>
          </select>
          <span class="selectRequiredMsg">Please select a category.</span></span>
          </td>
      </tr>
      <tr>
        <td>Question</td>
        <td><span id="sprytextfield1">
          <input type="text" name="Question" id="Question">
          <span class="textfieldRequiredMsg">Required.</span></span></td>
      </tr>
      <tr>
        <td>Answer</td>
        <td><span id="sprytextarea1">
          <textarea name="Answer" id="Answer" cols="45" rows="5"></textarea>
          <span class="textareaRequiredMsg">Required.</span></span></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" name="SaveFAQ" id="SaveFAQ" value="Save New to FAQ"></td>
      </tr>
    </table>     
     </div>
    <input type="hidden" name="MM_insert" value="form">
   </form>
 
  
</div>

    
    
<?php }?>

    <?php if($_GET['action']=="edit"){ ?>
     <?php if ($totalRows_RecordsetGetFAQs > 0) { // Show if recordset not empty ?>
      
      <div id="AccordionFAQ" class="Accordion" tabindex="0">
        <?php do { ?>
        
          <div class="AccordionPanel">
              <div>
               <div class="AccordionPanelTab"><?php echo  ++$counter.". ". $row_RecordsetGetFAQs['question']; ?></div>
              </div>
            <div class="AccordionPanelContent">
            <form name="editFrom" action="<?php echo $editFormAction; ?>" method="POST" id="editFrom">
            <table width="100%" border="0" cellspacing="0" cellpadding="6">
              <tr>
                <td width="8%">Question: </td>
                <td width="92%"><input name="question" type="text"  style="width:95%;" value="<?php echo $row_RecordsetGetFAQs['question']; ?>" /></td>
              </tr>
              <tr>
                <td>Answer: </td>
                <td><textarea name="answer" cols="" rows="" style="width:95%; height:100px;"><?php echo $row_RecordsetGetFAQs['answer']; ?></textarea></td>
              </tr>
              <tr>
                <td><input name="id" type="hidden" id="id" value="<?php echo $row_RecordsetGetFAQs['id']; ?>" /></td>
                <td><input name="EditFAQ" type="submit" value="Save Chages to FAQ" /></td>
              </tr>
            </table>
            <input type="hidden" name="MM_update" value="editFrom" />
      </form>
            
            </div>      
          </div>
          
<?php } while ($row_RecordsetGetFAQs = mysql_fetch_assoc($RecordsetGetFAQs)); ?>
      </div>
      
    <?php }?>
    <?php }else{ ?>
    <?php if ($totalRows_RecordsetGetFAQs > 0) { // Show if recordset not empty ?>
      <div id="AccordionFAQ" class="Accordion" tabindex="0">
        <?php do { ?>
          <div class="AccordionPanel">
              <div>
               <div class="AccordionPanelTab"><?php echo ++$i; ?>. <?php echo $row_RecordsetGetFAQs['question']; ?></div>
              </div>
            
            <div class="AccordionPanelContent"><?php echo $row_RecordsetGetFAQs['answer']; ?></div>
          </div>
          <?php } while ($row_RecordsetGetFAQs = mysql_fetch_assoc($RecordsetGetFAQs)); ?>
      </div>
    <?php } // Show if recordset not empty ?>
    
    <?php } ?>
  
  <?php if ($totalRows_RecordsetGetFAQs == 0) { // Show if recordset empty ?>
  <div style="padding:10px; font-size:18px; font-weight:bold; color:#0080FF;">No Frequently Asked Question to display.</div>
  <?php } // Show if recordset empty ?>    </td>
  </tr>
</table>

</div>



<script type="text/javascript">
var Accordion1 = new Spry.Widget.Accordion("AccordionFAQ");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1");
var MenuBar1 = new Spry.Widget.MenuBar("MenuBarCategory", {imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body>
</html>
<?php
mysql_free_result($RecordsetGetFAQs);
?>