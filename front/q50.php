<?php
include("../include/config.php");

session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../login.php");
exit();

}
if(isset($_GET['int_id']))
{
	$int_id=$_GET['int_id'];
	
}
	
if(isset($_POST['action']) && $_POST['action'] == 'submitform')
{
 $int_id=$_POST['int_id'];
$lid=$_POST['lid'];
$other1=$_POST['other1'];
 $services = $_POST['field2']; // a should now be an array if you're using checkboxes.
  $service_list = array(); // keep track of the services ordered

  if(in_array('1', $services))
  { 
    $service_list[] = 'Urea';
  }
  if(in_array('2', $services)) 
  { 
    $service_list[]='NPK';
  }
  if(in_array('3', $services)) 
  {
    $service_list[] = 'Booster';
  }
  
  if(in_array('4', $services)) 
  {
    $service_list[] = 'CAN';
  }
  
   if(in_array('5', $services)) 
  {
     $service_list[] = 'DAP';
 
  }
  
   if(in_array('6', $services)) 
  {
    $service_list[] = 'SSP';
 
 
  }
  if(in_array('7', $services)) 
  {
     $service_list[] = 'TSP';
 
 
  }
  
   if(in_array('8', $services)) 
  {
     $service_list[] = 'Minjingu';
 
 
  }
  
   if(in_array('9', $services)) 
  {
    $service_list[] = 'Other1('.$other1.')';
 
 
  }
  
   if(in_array('10', $services)) 
  {
    $service_list[] = 'Never applied fertilizer in the last 3 years';
   header("location:end.php?int_id=$int_id");
  }
   if(in_array('11', $services)) 
  {
    $service_list[] = 'Dont Know';
   header("location:end.php?int_id=$int_id");
  }
  
 $q52 = implode(',',$service_list);
	$sql="UPDATE survey SET Q51=:val WHERE id =:id";
$stmt = dbConnect()->prepare($sql);                                 
$stmt->bindParam(':val',$q51, PDO::PARAM_STR);
$stmt->bindParam(':id',$int_id, PDO::PARAM_STR);  
$stmt->execute(); 
	$msg="Question 52 Information Saved Successfully!";
}

$query_getbank = dbConnect()->prepare("SELECT  survey.id,survey.interviewer,survey.phone,survey.Q6,survey.Q11,survey.Q31,survey.Q50,survey.lid FROM  survey WHERE  survey.id = '".$int_id."'");
$query_getbank->execute();
$row_getbank=$query_getbank->fetch();
$list=explode(",",$row_getbank['Q50']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- CSS main application styling. -->
    <link rel="icon" type="image/ico" href="../uploadedfiles/school_logo/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
    <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
    <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
    <link rel="stylesheet" type="text/css" href="../css/formelements.css" />
    <link rel="stylesheet" href="../css1/coda-slider-2.0.css" type="text/css" media="screen" />  
     <link rel="stylesheet" href="css/BeatPicker.min.css"/>
 
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/BeatPicker.min.js"></script>
     <script type="text/javascript" src="../../js/js/jquery-1.7.1.min.js"></script>
      <script type="text/javascript" src="../../js/js/chart/highcharts.js"></script>
    <script type="text/javascript" src="../../js/js/custom-form-elements.js"></script>   
   </script>
    <script type="text/javascript" src="../../js/js/jquery-ui.min.js"></script>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <script>
	$(document).ready(function() {
	$("#lodrop").click(function(){
	
            	if ($("#account_drop").is(':hidden')){
                	$("#account_drop").show();
				}
            	else{
                	$("#account_drop").hide();
            	}
            return false;
       			 });
				  $('#account_drop').click(function(e) {
            		e.stopPropagation();
        			});
        		$(document).click(function() {
					if (!$("#account_drop").is(':hidden')){
            		$('#account_drop').hide();
					}
        			});	
                
});
</script>

<script type="text/javascript">
$(document).ready(function()
{
$(".reporting_manager").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;

$.ajax
({
type: "POST",
url: "ajax_city.php",
data: dataString,
cache: false,
success: function(html)
{
$(".reporting_lead").html(html);
} 
});

});
});
</script>


<script>
$(document).ready(function() {
  $(".nav_drop_but").click(function() {
  $(".navigationbtm_wrapper_outer").slideToggle();
	});
});
</script>

<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<link type="text/css" href="../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
  <script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		$('#exdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>

    
</head>
<title>::Rapid Smart Survey on male and female farmers’ satisfaction with plant clinic visits ::</title>
<body>
<div class="wrapper">


    <div class="header">
     
   <div class="lo_drop" id="account_drop">
     <div class="lo_drop_hov"></div> 
     	<div class="lo_name">
        <?php ?><?php ?>
 <span> <?php echo $_SESSION['name']; ?> </span>
            <div class="clear"></div>
        </div>
    <ul>
        	<li><a href="profile.php"><?php echo 'My Account';?></li>
            <li><a href="settings.php"><?php echo 'Settings';?></a></li>
            <li> <a href="../logout.php"><?php echo 'Logout';?></a></li>
        </ul>
     </div>
     
     
   
	
    
        	<div class="logo">
            <a href="index.php"><img src="../images/logo-plantwise.png" alt=""  border="0" />		</a> </div>
            
			
			 <div class="">
            
<?php include('app_nav.php');?>
               
            </div>
    
    
      </div>
     
    
     
    <div class="midnav">
    
   
        <a class="first-letter"> Home</a>
		 <span>Leads Management</span>
		   <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
     </div>

     
     <div class="container">
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
  <?php include('../left_side.php');?>
    
    </td>
    <td valign="top">
	 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top" width="75%"><div style="padding-left:20px; padding-right:10px;">


<h3></h3>


<div class="formCon" >

<div class="">


 <form name="frmRespondent" action="q51.php" enctype="multipart/form-data" method="post" >  
     
           <div class="content">
               
			<div class="row">
			   <div class="col-md-6">
                            <div class="widget">
						
							 <div class="panel-group" id="accordion" style="margin-bottom:1px">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
      Call Opening</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
<table border="1" width="718">
<tbody>
<tr style="height: 15px;">
<td style="height: 15px;" width="82"><strong>Agent:</strong></td>
<td style="height: 15px;" width="636">Good morning / afternoon, Thank you for calling Huduma Kenya. My name is Jane how may I help you?</td>
</tr>
<tr style="height: 15px;">
<td style="height: 15px;" colspan="2"><em><strong>Client Response</strong></em></td>
</tr>
<tr style="height: 15px;">
<td style="height: 15px;"><strong>Agent:</strong></td>
<td style="height: 15px;" width="636">May I know who I&rsquo;m speaking with please?</td>
</tr>
<tr style="height: 15px;">
<td style="height: 15px;" colspan="2"><em><strong>Client Response</strong></em></td>
</tr>
<tr style="height: 15px;">
<td style="height: 15px;"><strong>Agent:</strong></td>
<td style="height: 15px;" width="636">John how are you today?</td>
</tr>
<tr style="height: 15px;">
<td style="height: 15px;" colspan="2"><em><strong>Client Response</strong></em></td>
</tr>
</tbody>
</table>
          
        <p align="center">
          <input type="hidden" id="action" name="action" value="submitform" />
          <input type="hidden" id="int_id" name="int_id" value="<?php echo $row_getbank['id']; ?>" />
		     <input type="hidden" id="int_id" name="lid" value="<?php echo $row_getbank['lid']; ?>" />
         
          <input type="submit" name="submit" 
		  style=" padding:0px 20px;
	background:url(../img/fbut-bg.png) repeat-x;
	height:30px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:13px;
	cursor:pointer;
	"
		  value="Save & Continue" />
          </p>
       
</form>
    
 
	

<tr>
                                 
</div>
</div>


            
</td>
        
      </tr>
	  
    </table>
	
	  <form name="frmTerminate" action="end_start.php" enctype="multipart/form-data" method="post">
		<p align="right">
        <input type="hidden" id="stop_time" name="stop_time" value="<?php echo $row_getbank['date']; ?>" />
        <input type="hidden" id="action" name="action" value="submitform" />
        <input type="hidden" id="int_id" name="int_id" value="<?php echo int_id; ?>" />
        <input type="submit" name="submit" 
		style=" padding:0px 20px;
	background:red;
	height:30px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#FCFCFC;
	font-size:13px;
	cursor:pointer;
	"
		value="Click Here To Terminate" onclick="return confirm('Are you sure you want to TERMINATE? This operation cannot be undone!');" />
        </p>
  	</form>
    </td>
  </tr>
  
</table>
    </div>
 <div class="midfooter">
    
   
        <a class="first-letter"> &copy <?php echo date('Y');?> Developed and Designed by Techno Brain BPO/ITES</a>
		
     </div>
</body>
</html>

