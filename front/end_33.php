<?php
include("../include/config.php");
error_reporting(1);
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../login.php");
    exit();
}
if (isset($_GET['int_id'])) {
    $int_id = $_GET['int_id'];
}
if (isset($_POST['action']) && $_POST['action'] == 'submitform') {

    $field_a=implode(',',$_POST['field_a']);
    $field1=implode(',',$_POST['field1']);
    $field2=implode(',',$_POST['field2']);
    $field3=implode(',',$_POST['field3']);
    
    $q19 = $_POST['q19'];
    $callback = $_POST['callback'];
    $disp = $_POST['disp'];
    $disp2=$_POST['disp2'];
    
    $reason1=$_POST['reason1'];
    $reason2=$_POST['reason2'];
    $reason3=$_POST['reason3'];
 
    $int_id = $_POST['int_id'];
    $lid = $_POST['lid'];

   

    $sql2 = "UPDATE survey SET Q5_a=:val1, Q5_b=:val2, Q5_c=:val3, Q5_e=:val4, Q5_d=:val5, Q5_d_1=:val6, Q5_d_2=:val7, Q5_d_3=:val8, service_issue_other_reason=:val9, content_issue_other_reason=:va20, competitor_other_reason=:va21 WHERE id =:id";
    $stmt2 = dbConnect()->prepare($sql2);
    
    $stmt2->bindParam(':val1', $q19, PDO::PARAM_STR);
    $stmt2->bindParam(':val2', $callback, PDO::PARAM_STR);
    $stmt2->bindParam(':val3', $disp, PDO::PARAM_STR);
    $stmt2->bindParam(':val4', $disp2, PDO::PARAM_STR);
    $stmt2->bindParam(':val5', $field_a, PDO::PARAM_STR);
    $stmt2->bindParam(':val6', $field1, PDO::PARAM_STR);
    $stmt2->bindParam(':val7', $field2, PDO::PARAM_STR);
    $stmt2->bindParam(':val8', $field3, PDO::PARAM_STR);
    
    $stmt2->bindParam(':val9', $reason1, PDO::PARAM_STR);
    $stmt2->bindParam(':va20', $reason2, PDO::PARAM_STR);
    $stmt2->bindParam(':va21', $reason3, PDO::PARAM_STR);
  
    $stmt2->bindParam(':id', $int_id, PDO::PARAM_STR);
    $stmt2->execute();
}


$query_getbank = dbConnect()->prepare("SELECT  survey.id,survey.interviewer,survey.phone,survey.lid FROM  survey WHERE  survey.id = '" . $int_id . "'");
$query_getbank->execute();
$row_getbank = $query_getbank->fetch();
//$query_getbank = dbConnect()->prepare("SELECT  survey.id,survey.interviewer,survey.phone,survey.lid,survey.Q3_Other, survey.Q1_other FROM  survey WHERE  survey.id = '".$int_id."'");
//$query_getbank->execute();
//$row_getbank=$query_getbank->fetch();
//$list=$row_getbank['Q3_Other'];
//$list2=$row_getbank['Q1_other'];
//if($list !=NULL)
//{
$id2=$row_getbank['lid'];

$query_getbank2 = dbConnect()->prepare("SELECT  * FROM  leads WHERE  leads.id = '" . $id2 . "'");
$query_getbank2->execute();
$row_getbank2 = $query_getbank2->fetch();


date_default_timezone_set("Africa/Nairobi");


if (isset($_GET['int_id'])) {
    $int_id = $_GET['int_id'];
} else {
    $int_id = $_POST['int_id'];
}

if (isset($_POST['stop_time'])) {
    $stop_time = $_POST['stop_time'];
} else {
    $stop_time = date("Y-m-d H:i:s");
}

if (isset($_POST['pageid'])) {
    $last_page = $_POST['pageid'];
} else {
    $last_page = 'end.php';
}


$sql = "UPDATE survey SET stop_time=:val WHERE id =:id";
$stmt = dbConnect()->prepare($sql);
$stmt->bindParam(':val', $stop_time, PDO::PARAM_STR);
$stmt->bindParam(':id', $int_id, PDO::PARAM_STR);
$stmt->execute();

$query_getbank = dbConnect()->prepare("SELECT  survey.id FROM  survey WHERE  survey.id = '" . $int_id . "'");
$query_getbank->execute();
$row_end = $query_getbank->fetch();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <!-- CSS main application styling. -->
        <link rel="icon" type="image/ico" href="../uploadedfiles/school_logo/favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
        <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
        <link rel="stylesheet" type="text/css" href="../css/formelements.css" />
        <link rel="stylesheet" href="../css1/coda-slider-2.0.css" type="text/css" media="screen" />  
        <link rel="stylesheet" href="css/BeatPicker.min.css"/>

        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/BeatPicker.min.js"></script>
        <script type="text/javascript" src="../../js/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="../../js/js/chart/highcharts.js"></script>
        <script type="text/javascript" src="../../js/js/custom-form-elements.js"></script>   
        </script>
        <script type="text/javascript" src="../../js/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

        <script>
            $(document).ready(function () {
                $("#lodrop").click(function () {

                    if ($("#account_drop").is(':hidden')) {
                        $("#account_drop").show();
                    } else {
                        $("#account_drop").hide();
                    }
                    return false;
                });
                $('#account_drop').click(function (e) {
                    e.stopPropagation();
                });
                $(document).click(function () {
                    if (!$("#account_drop").is(':hidden')) {
                        $('#account_drop').hide();
                    }
                });

            });
        </script>

        <script type="text/javascript">
            $(document).ready(function ()
            {
                $(".reporting_manager").change(function ()
                {
                    var id = $(this).val();
                    var dataString = 'id=' + id;

                    $.ajax
                            ({
                                type: "POST",
                                url: "ajax_city.php",
                                data: dataString,
                                cache: false,
                                success: function (html)
                                {
                                    $(".reporting_lead").html(html);
                                }
                            });

                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                    $("#checkbox1").click(function () {
                        if ($(this).is(":checked")) {
                            $("#textbox").removeAttr("disabled");
                            $("#textbox").attr("required", "required");
                            $("#textbox").focus();
                        } else {
                            $("#textbox").attr("disabled", "disabled");
                            $("#textbox").removeAttr("required");
                            document.getElementById("textbox").value = "";

                        }
                    });
                });
 $(function () {
                    $("#1,#2,#3,#4,#5,#6,#7,#8,#9,#10").click(function () {
                        if ($(this).is(":checked")) {
                            $("#textbox").removeAttr("disabled");
                           
                             $("#textbox").attr("disabled", "disabled");
                              document.getElementById("textbox").value = "";
                        } else {
                            $("#textbox").attr("disabled", "disabled");
                            $("#textbox").removeAttr("required");
                            document.getElementById("textbox").value = "";

                        }
                    });
                });

            </script>

        <script>
            $(document).ready(function () {
                $(".nav_drop_but").click(function () {
                    $(".navigationbtm_wrapper_outer").slideToggle();
                });
            });
        </script>

        <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="../js/table2CSV.js" ></script>
        <link type="text/css" href="../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
        <script type="text/javascript">
            $(function () {
                $('#fromdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('#todt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
                $('#exdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });
        </script>


    </head>
    <title>::Tele-Radiology Initiative::</title>
    <body>
        <div class="wrapper">


            <div class="header">

                <div class="lo_drop" id="account_drop">
                    <div class="lo_drop_hov"></div> 
                    <div class="lo_name">
                        <?php ?><?php ?>
                        <span> <?php echo $_SESSION['name']; ?> </span>
                        <div class="clear"></div>
                    </div>
                    <ul>
                        <li><a href="profile.php"><?php echo 'My Account'; ?></li>
                        <li><a href="settings.php"><?php echo 'Settings'; ?></a></li>
                        <li> <a href="../logout.php"><?php echo 'Logout'; ?></a></li>
                    </ul>
                </div>





                <div class="logo">
                    <a href="index.php"><img src="../images/logo.png" alt="" height="67" border="0" />		</a> </div>


                <div class="">

                    <?php include('app_nav.php'); ?>

                </div>


            </div>



            <div class="midnav">


                <a class="first-letter"> Home</a>
                <span>Leads Management</span>
                <span style="float:right"><a href="../logout.php"> Logout</a></span>
                <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>
            </div>


            <div class="container">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="247" valign="top">

                            <?php include('../left_side.php'); ?>

                        </td>
                        <td valign="top">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="top" width="75%"><div style="padding-left:20px; padding-right:10px;">
                                            <h3 align="center">THANK RESPONDENT AND CLOSE INTERVIEW</h3>

                                            <div class="formCon2" >

                                                <div class="">
                                                    <font color="blue" size="3"><center><strong>Fine, Thank you for your time. Have a nice day!</strong></center></font>

                                                    <form name="frmPriority" action="end_final.php" enctype="multipart/form-data" method="post">

  <br><br>
 
                                                        <div id="spryradio1" align="center">
                                                            <table align="center">

                                                                <?php
                                                                if ($row_getbank['Q11_cs_email'] != '') {
                                                                    ?>	
                                                                    <tr><td class="tbl"><input type="radio" name="disptype" value="Complete Survey" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>Complete Survey</strong></span></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                                 
                                                                    <?php
                                                                } 
                                                                else if ($row_getbank['Q2_1'] == 'No') {
                                                                    ?>	
                                                                    <tr><td class="tbl"><input type="radio" name="disptype" value="No scanning or Xray taken" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>No scanning or Xray taken</strong></span></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                else if (isset($_POST['terminate']) && $_POST['terminate'] == 'Yes') {
                                                                    ?>

                                                                    <tr>
                                                                        <td class="tbl"><input type="radio" name="disptype" value="Partial Survey-Call Back"  onclick = "showTable(1)"/>
                                                                            <strong>Partial Survey-Call Back</strong></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                                    <tr style="display:none" id='p1'>
                                                                        <td colspan="2" align="right"><div align="left"><strong>Please indicate a convenient date & Time to call you back</strong></div></td>
                                                                    </tr>
                                                                    <tr style="display:none;text-align:center"  id='p3'>
                                                                        <td>

                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display:none;text-align:right"  id='p2'>
                                                                        <td><label>
                                                                                Convenient Date/Time  </label>
                                                                            <label for="textfield"></label>
                                                                            <input type="text" name="callback" id="datepicker" size=""/></td>
                                                                        <td><select name="time" id="time">
                                                                                <option value="">Choose</option>
                                                                                <option value="7:00am">7:00am</option>
                                                                                <option value="8:00am">8:00am</option>
                                                                                <option value="9:00am">9:00am</option>
                                                                                <option value="10:00am">10:00am</option>
                                                                                <option value="11:00am">11:00am</option>
                                                                                <option value="12:00noon">12:00noon</option>
                                                                                <option value="01:00pm">01:00pm</option>
                                                                                <option value="02:00pm">02:00pm</option>
                                                                                <option value="03:00pm">03:00pm</option>
                                                                                <option value="04:00pm">04:00pm</option>
                                                                                <option value="05:00pm">05:00pm</option>
                                                                                <option value="06:00pm">06:00pm</option>
                                                                                <option value="07:00pm">07:00pm</option>
                                                                                <option value="08:00pm">08:00pm</option>
                                                                                <option value="09:00pm">09:00pm</option>
                                                                            </select></td>


                                                                        <br/>
                                                                        </p></td>
                                                                        <td >&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td class="tbl"><input type="radio" name="disptype" id="1" value="Partial Survey-Customer Hang Up"  onclick = "showTable(2)"/> <span style=""><strong>Partial Survey-Customer Hang Up</strong></span></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td class="tbl"><input type="radio" name="disptype"  id="2" value="Partial Survey-Call Disconnected"  onclick = "showTable(2)"/> <span style=""><strong>Partial Survey-Call Disconnected</strong></span></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                                    <tr><td class="tbl"><input type="radio" name="disptype" id="3" value="Partial Survey-Not Interested to continue Survey"  onclick = "showTable(2)"/> <span style=""><strong>Partial Survey-Not Interested to continue Survey</strong></span></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                                <tr><td class="tbl"><input type="radio" name="disptype" id="4"  value="Partial Survey-Not Account Owner"  onclick = "showTable(2)"/> <span style=""><strong>Partial Survey-Not Account Owner</strong></span></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                               
                                                                 <tr><td class="tbl"><input type="radio" name="disptype"  id="7" value="Partial Survey-Already Contacted"  onclick = "showTable(2)"/> <span style=""><strong>Partial Survey-Already Contacted</strong></span></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                                <tr><td class="tbl"><input type="radio" name="disptype"  id="8" value="Number Not belongs to the laboratory"  onclick = "showTable(2)"/> <span style=""><strong>Number Not belongs to the laboratory</strong></span></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                                

                                                              
                                                                    <?php
                                                                } 
                                                                
                                                                
                                                                else {
                                                                    ?>

                                                                
                                                                
                                                                 <tr><td class="tbl"><input type="radio" name="disptype" value="Complete Survey" checked onclick = "showTable(2)"/> <span style="color:#9018cf"><strong>Complete Survey</strong></span></td>
                                                                        <td class="tbl">&nbsp;</td>
                                                                    </tr>
                                                                
                                                                
                                                                           
                                                                    <?php
                                                                }
                                                                ?>

           <tr><td>
                                                                                                        
                                                                                                        <p align="center"><strong>(<font color="red">
                                                                            ** Check the box below iff the Issue raised by the client needs Escalation **
                                                                            </font>)</BR>
<input type="checkbox" name="esc" value="Escalation" id="esc"> Escalation  
    </strong></p>   
                                                                                                        
                                                                                                    </td></tr>
                                                            </table>


                                                            <p></p>

                                                            <p align="center"><strong>DISPOSITION COMMENTS</strong></p>
                                                            <p align="center"><textarea cols="40" rows="3" name="disposition"></textarea></p>

                                                            <p align="center">
                                                                <input type="hidden" id="action" name="action" value="submitform" />
                                                                <input type="hidden" id="int_id" name="int_id" value="<?php echo $row_end['id']; ?>" />
                                                                <input type="hidden" id="" name="page_id" value="<?php echo $last_page; ?>" />
                                                                <input type="hidden" id="lid" name="lid" value="<?php echo $row_end['lid']; ?>" />
                                                                <input type="submit" name="submit" 
                                                                       style=" padding:0px 20px;
                                                                       background:red;
                                                                       height:30px;
                                                                       -webkit-border-radius: 4px;
                                                                       -moz-border-radius: 4px;
                                                                       border-radius: 4px;
                                                                       border:1px #b58530 solid;
                                                                       color:#FCFCFC;
                                                                       font-size:13px;
                                                                       cursor:pointer;
                                                                       "
                                                                       value="Close Survey" />
                                                            </p>
                                                    </form>



                                                    <tr>

                                                </div>
                                            </div>


                                            </form>
                                    </td>

                                </tr>

                            </table></form>
                        </td>
                    </tr>

                </table>
            </div>
            <div class="midfooter">


                <a class="first-letter"> &copy <?php echo date('Y'); ?> Developed and Designed by Techno Brain BPO/ITES</a>

            </div>

            <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
                <script src="//code.jquery.com/jquery-1.9.1.js"></script>
                <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
                <script>
                                                                    $(function () {
                                                                        $("#datepicker,#datepicker2,#datepicker3").datepicker({
                                                                            dateFormat: 'yy-mm-dd'});
                                                                    });
                                                                    function showTable(which) {
                                                                        if (which == 1) {
                                                                            //document.getElementById("p1").style.display="table-row";
                                                                            document.getElementById("p2").style.display = "table-row";
                                                                            //document.getElementById("p3").style.display="table-row";
                                                                            //document.getElementById("p4").style.display ="table-row";
                                                                            //document.getElementById("op1").checked="true";
                                                                        } else if (which == 2)
                                                                        {
                                                                            //document.getElementById("p2").style.display="none";
                                                                            //document.getElementById("tablecallB").style.display="none";
                                                                            document.getElementById("datepicker").value = "";
                                                                            document.getElementById("time").value = "";
                                                                            document.getElementById("p2").style.display = "none";

                                                                            //document.getElementById("tablecallC").style.display="none";
                                                                            //document.getElementById("tablecallD").style.display="none";
                                                                        } else if (which == 3)
                                                                        {
                                                                            document.getElementById("tablecallB").style.display = "none";
                                                                            document.getElementById("datepicker").value = "";
                                                                            document.getElementById("time").value = "";
                                                                            document.getElementById("tablecallC").style.display = "table-row";
                                                                            document.getElementById("tablecallD").style.display = "none";
                                                                        } else if (which == 4)
                                                                        {
                                                                            document.getElementById("tablecallB").style.display = "none";
                                                                            document.getElementById("datepicker").value = "";
                                                                            document.getElementById("time").value = "";
                                                                            document.getElementById("tablecallC").style.display = "none";
                                                                            document.getElementById("tablecallD").style.display = "table-row";
                                                                        }
                                                                    }
                </script>
                </body>
                </html>
