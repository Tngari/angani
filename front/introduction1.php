<?php
include("../include/config.php");

session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../login.php");
    exit();
}

if (isset($_GET['id'])) {
    $int_id = $_GET['id'];
} else if (isset($_POST['action']) && $_POST['action'] == 'submitform') {
    $int_id = $_POST['dbphone'];
}


$query_getbank = dbConnect()->prepare("SELECT survey.last_page, survey.id,  survey.interviewer,survey.phone,survey.phone1,survey.phone2,survey.name, survey.lid FROM  survey WHERE  survey.id = '" . $int_id . "'");
$query_getbank->execute();
$row_getbank = $query_getbank->fetch();


//$getbank = mysql_query($query_getbank, $air2013) or die(mysql_error());
//$row_getbank = mysql_fetch_assoc($getbank);
//$totalRows_getbank = mysql_num_rows($getbank);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <!-- CSS main application styling. -->
        <link rel="icon" type="image/ico" href="../uploadedfiles/school_logo/favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
        <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
        <link rel="stylesheet" type="text/css" href="../css/formelements.css" />
        <link rel="stylesheet" href="../css1/coda-slider-2.0.css" type="text/css" media="screen" />  
        <link rel="stylesheet" href="css/BeatPicker.min.css"/>
        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/BeatPicker.min.js"></script>
        <script type="text/javascript" src="../../js/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="../../js/js/chart/highcharts.js"></script>
        <script type="text/javascript" src="../../js/js/custom-form-elements.js"></script>   
        <script type="text/javascript" src="../../js/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

        <script>
            $(document).ready(function () {
                $("#lodrop").click(function () {

                    if ($("#account_drop").is(':hidden')) {
                        $("#account_drop").show();
                    } else {
                        $("#account_drop").hide();
                    }
                    return false;
                });
                $('#account_drop').click(function (e) {
                    e.stopPropagation();
                });
                $(document).click(function () {
                    if (!$("#account_drop").is(':hidden')) {
                        $('#account_drop').hide();
                    }
                });

            });
        </script>

        <script type="text/javascript">
            $(document).ready(function ()
            {
                $(".reporting_manager").change(function ()
                {
                    var id = $(this).val();
                    var dataString = 'id=' + id;

                    $.ajax
                            ({
                                type: "POST",
                                url: "ajax_city.php",
                                data: dataString,
                                cache: false,
                                success: function (html)
                                {
                                    $(".reporting_lead").html(html);
                                }
                            });

                });
            });
        </script>


        <script>
            $(document).ready(function () {
                $(".nav_drop_but").click(function () {
                    $(".navigationbtm_wrapper_outer").slideToggle();
                });
            });
        </script>

        <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="../js/table2CSV.js" ></script>
        <link type="text/css" href="../css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
        <script type="text/javascript">
            $(function () {
                $('#fromdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('#todt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
                $('#exdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });
        </script>

    </head>
    <title>:: Angani::</title>
    <body>
        <div class="wrapper">


            <div class="header">

                <div class="lo_drop" id="account_drop">
                    <div class="lo_drop_hov"></div> 
                    <div class="lo_name">
                        <span> <?php echo $_SESSION['name']; ?> </span>
                        <div class="clear"></div>
                    </div>
                    <ul>
                        <li><a href="profile.php"><?php echo 'My Account'; ?></li>
                        <li><a href="settings.php"><?php echo 'Settings'; ?></a></li>
                        <li> <a href="../logout.php"><?php echo 'Logout'; ?></a></li>
                    </ul>
                </div>

                <div class="logo">
                    <a href="leads.php"><img src="../images/logo.png" alt="" height="67" border="0" />		</a> </div>
                <div class="">

                    <?php include('app_nav.php'); ?>

                </div>

            </div>
            <div class="midnav">


                <a class="first-letter"> Home</a>
                <span>Leads Management</span>
                <span style="float:right"><a href="../logout.php"> Logout</a></span>
                <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>
            </div>

            <div class="container">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="247" valign="top">

                            <?php include('../left_side.php'); ?>

                        </td>
                        <td valign="top">
                            <h1 align="center">Phone Number <?php echo $row_getbank['phone']; ?> &nbsp;&nbsp;&nbsp;
                            ( Alternative Lines: <?php echo $row_getbank['phone1'].','.$row_getbank['phone2']; ?>)
                            </h1>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="top" width="75%"><div style="padding-left:20px; padding-right:10px;">
                                            <h3 align="center">Call Status</h3>

                                            <div class="" >

                                                <div class="">
                                                    <form name="frmRespondent" action="process.php" enctype="multipart/form-data" method="post" >  

                                                        <p align="center">
                                                            <input type="hidden" id="action" name="action" value="submitform" />
                                                            <input type="hidden" id="last_page" name="last_page" value="<?php echo $row_getbank['last_page']; ?>" />
                                                            <input type="hidden" id="int_id" name="int_id" value="<?php echo $row_getbank['id']; ?>" />
                                                            <input type="hidden" id="start_time" name="start_time" value="<?php echo $row_getbank['date']; ?>" />
                                                            <input name="submit" type="submit" id="save" style=" padding:0px 20px;
                                                                   background:url(../img/fbut-bg.png) repeat-x;
                                                                   height:37px;
                                                                   -webkit-border-radius: 4px;
                                                                   -moz-border-radius: 4px;
                                                                   border-radius: 4px;
                                                                   border:1px #b58530 solid;
                                                                   color:#633c15;
                                                                   font-size:13px;
                                                                   cursor:pointer;
                                                                   "  value="Reachable" />

                                                            <input type="submit" name="submit2" style=" padding:0px 20px;
                                                                   background:url(../img/fbut-bg.png) repeat-x;
                                                                   height:37px;
                                                                   -webkit-border-radius: 4px;
                                                                   -moz-border-radius: 4px;
                                                                   border-radius: 4px;
                                                                   border:1px #b58530 solid;
                                                                   color:#633c15;
                                                                   font-size:13px;
                                                                   cursor:pointer;
                                                                   "  value="Unreachable" onclick="return confirm('Are you sure you want to TERMINATE? This operation cannot be undone!');" />

                                                        </p>
                                                        </fieldset>
                                                    </form>

                                                    <tr>

                                                </div>
                                            </div>


                                            </form>
                                    </td>

                                </tr>

                            </table></form>
                        </td>
                    </tr>

                </table>
            </div>
            <div class="midfooter">


                <a class="first-letter"> &copy <?php echo date('Y'); ?> Developed and Designed by Techno Brain BPO/ITES</a>

            </div>
    </body>
</html>