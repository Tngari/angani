<?php
//Start session
include("../include/config.php");
//error_reporting(0); 
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../../index.php");
exit();
}
if($_SESSION['level']=="Admin" || $_SESSION['level']=="Supervisor" )
			{
$fromdt =$_GET["fromdate"];
            $todt = $_GET["todate"];
			 // $batch = $_GET["batch"];
			
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::ilogix Survey ::</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
	

</script>
<script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="../js/jquery.highchartTable.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart').highchartTable();
});


</script>
 <style type="text/css">
	.disabled
	{
		background-color:#666;
		font-size:11px;
		color:#FFF;
		font-weight:bold;
	}
	
	/*dashlist style*/
.pdtab_Con{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	border-left: 1px #d3dde6 solid;
	border-bottom: 1px #d3dde6 solid;
	
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con table td{
	padding:8px 0px;
	
	border-style: double;
	font-size:11px;
}

.pdtab_Con table tr{
		background: #b8d1f3;
	}
	/*  Define the background color for all the ODD background rows  */
	
	
	
	.pdtab_Con table tr:nth-child(odd){ 
		background: #b8d1f3;
	}
	/*  Define the background color for all the EVEN background rows  */
	.pdtab_Con table tr:nth-child(even){
		background: #FFF2CC;
	}

.pdtab-h{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}
/*Table 2 */
.pdtab_Con2{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con2 table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	border-left: 1px #d3dde6 solid;
	border-bottom: 1px #d3dde6 solid;
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con2 table td{
	padding:8px 0px;
	border-style: double;
	font-size:11px;
}

.pdtab_Con2 table tr{
		background: #FCFCFC;
	}
	/*  Define the background color for all the ODD background rows  
	
	
	
	.pdtab_Con2 table tr:nth-child(even) td:nth-last-child(10)
	{
		background: #b8d1f3;
	}
	
	.pdtab_Con2 table tr:nth-child(odd) td:nth-last-child(10)
	{
		background: #FFF2CC;
	}
	
	*/



.pdtab-h2{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h2{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}



/*Table 3 */
.pdtab_Con3{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con3 table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	border-left: 1px #d3dde6 solid;
	border-bottom: 1px #d3dde6 solid;
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con3 table td{
	padding:8px 0px;
	border-style: double;
	font-size:11px;
}

.pdtab_Con3 table tr{
		background: #FCFCFC;
	}
	/*  Define the background color for all the ODD background rows  
	
	
	
	.pdtab_Con3 table tr:nth-child(even) td:nth-last-child(5)
	{
		background: #b8d1f3;
	}
	
	.pdtab_Con3 table tr:nth-child(odd) td:nth-last-child(5)
	{
		background: #FFF2CC;
	}
	
	*/



.pdtab-h3{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h3{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}


/*Table 4 */
.pdtab_Con4{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con4 table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	border-left: 1px #d3dde6 solid;
	border-bottom: 1px #d3dde6 solid;
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con4 table td{
	padding:8px 0px;
	border-style: double;
	font-size:11px;
}

.pdtab_Con4 table tr
{
		background: #FCFCFC;
	}
	/*  Define the background color for all the ODD background rows 
	
	
	
	.pdtab_Con4 table tr:nth-child(even) td:nth-last-child(14)
	{
		background: #b8d1f3;
	}
	
	.pdtab_Con4 table tr:nth-child(odd) td:nth-last-child(14)
	{
		background: #FFF2CC;
	}
	 */
	

	



.pdtab-h4{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h4{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}


/*Table 4 */
.pdtab_Con5{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con5 table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con5 table td{
	padding:8px 0px;
	border-style: double;
	font-size:11px;
}

.pdtab_Con5 table tr
{
		background: #FCFCFC;
	}
	/*  Define the background color for all the ODD background rows 
	
	
	
	.pdtab_Con5 table tr:nth-child(even) td:nth-last-child(9)
	{
		background: #FC;
	}
	
	.pdtab_Con5 table tr:nth-child(odd) td:nth-last-child(9)
	{
		background: #FFF2CC;
	}
	 */
	

	



.pdtab-h5{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h5{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}
	</style>
</head>

<body>
<div class="wrapper">
	
    	<div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/chasebank.png" alt="" height="67"  border="0" />	</a> 
			</div>
            
           <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav" style="width:1190px">
    
   
        
		 <span>Reports</span>
		  <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
		 
     </div>
	<div class="container-fluid" style="background-color:#FFF;	width:1200px;
	min-height:800px;
	margin-left:0px auto 0px auto;
	padding:0px;
	-webkit-border-top-left-radius: 3px;
-webkit-border-top-right-radius: 3px;
-moz-border-radius-topleft: 3px;
-moz-border-radius-topright: 3px;
border-top-left-radius: 3px;
border-top-right-radius: 3px;
box-shadow:  0px 1px 1px #000;
    -moz-box-shadow: 0px 1px 1px #000;
    -webkit-box-shadow: 0px 1px 1px #000;
box-shadow: 0px 8px 18px #1c1c1c;
    -moz-box-shadow: 0px 8px 18px #1c1c1c;
    -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
	<div class="captionWrapper">
<!--	<ul>
		<?php
			$later="2014-01-01";
			$leo=date('Y-m-d');
			?>
	<li><a href="disposition.php?fromdate=<?php echo $later;?>&todate=<?php echo $leo;?>"><h2  class="curr">Batch I </h2></a></li>
	<li><a href="disposition2.php?fromdate=<?php echo $later;?>&todate=<?php echo $leo;?>"><h2>Batch II</h2></a></li>
	
		
    </ul>-->
</div>
 <div class="formCon" style="float:center; width:95%; margin-left:10px;margin-right:10px;padding:10px" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 <tr>
<form id="form1" name="form1" method="get" action="data_analytics.php">
       

				  		
		
				  <tr>
       <td >From:</td>
                	<td ><input name='fromdate' type='text'  value="<?php echo $fromdt;?>" id="fromdt"  /></td>
                    </tr>
					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr><td >To:</td>
                    <td ><input name='todate' type='text'  value="<?php echo $todt;?>" id="todt" /></td>
    	</tr>	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

  
           <td>&nbsp;</td><td ><label>
            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> </td></form>

	 
	<!--
<td>
		  <form action="getCSV.php" method ="post" > <label>
		 <input type="hidden" name="csv_text" id="csv_text">
            <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	font-weight:bold;"/>
          </label> 	
		  </form>
		  <script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
		  </td>
	-->
        </tr>
      </table>
            
			</div>
			
				<div class="" >
    
    <div class="clear"></div>
                                                
    
        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
         <div class="pagecon" style="float:center; margin-left:10px;">
                                                 
                                                  </div>     
		<div style="float:left; width:40%; margin-left:10px;margin-right:10px;padding:10px" class="pdtab_Con2">									  
 <table width="100%" class="pdtab_Con2" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
  <caption>Unreachable Representation</caption>
     <thead>
      <tr style="background-color:#FCE4D6;color:#000;font-weight:bold">
            <th class="tblRB">Attempts</th>
  <?php
  $disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <th class="tblRB"><?php echo $dispo['batch'];?></th>
 <?php
						}
						
						?>
	  
      </tr>
   </thead>

 <tbody>
	<?php 
			
	$attempts=dbConnect()->prepare("SELECT * FROM attempts_list GROUP BY name");			
	  $attempts->execute();	
						
						$t=0;
						while($row=$attempts->fetch(PDO::FETCH_ASSOC))
						{
							$dt=$row['code'];
							
							
							echo '<tr>
    <td class="tblR">'.$row['name'].'</td>';
	$disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	

	   while($rows=$disp->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['batch'];
			$m=1;
			

			if($dt==1)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.disposation!='Complete Survey' AND attempts.attempt='".$dt."' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=2)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==2)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.disposation!='Complete Survey' AND attempts.attempt='".$dt."' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=3)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==3)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.disposation!='Complete Survey' AND attempts.attempt='".$dt."' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=4)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==4)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.disposation!='Complete Survey' AND attempts.attempt='".$dt."' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=5)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==5)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.disposation!='Complete Survey' AND attempts.attempt='".$dt."' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=6)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
				if($dt==6)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.disposation!='Complete Survey' AND attempts.attempt='".$dt."' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=7)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			
			
			echo '<td class="tblR">'.$count.'</td>';		
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>
  
		
			
		
	
</table>

</div>

	<div style="float:right; width:40%; margin-left:10px;margin-right:10px;padding:10px">									  
<table width="200px" class="highchart"  data-graph-container-before="1" data-graph-type="column" style="display:none;">
  <caption>Graph Representation</caption>
     <thead>
      <tr>
            <th class="tblRB">Attempts</th>
  <?php
  $disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <th class="tblRB"><?php echo $dispo['batch'];?></th>
 <?php
						}
						
						?>
	  
      </tr>
   </thead>

 <tbody>
	<?php 
			
	$attempts=dbConnect()->prepare("SELECT * FROM attempts_list GROUP BY name");			
	  $attempts->execute();	
						
						$t=0;
						while($row=$attempts->fetch(PDO::FETCH_ASSOC))
						{
							$dt=$row['code'];
							
							
							echo '<tr>
    <td class="tblR">'.$row['name'].'</td>';
	$disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	

	   while($rows=$disp->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['batch'];
		   	//$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."'");			
	  //$pls->execute();
	//$count=$pls->rowCount();
	if($dt==1)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation!='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=2)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==2)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation!='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=3)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==3)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation!='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=4)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==4)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation!='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=5)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==5)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation!='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=6)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==6)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation!='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=7)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
	  
	  
					echo '<td class="tblR">'.$count.'</td>';
					
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>
  
		
			
		
	
</table>

</div>
<div class="pagecon">
  
                                              </div>
<div class="clear"></div>
    </div>
    
    
    
    </div>
			<div style="margin-top:100px">
    
    <div class="clear"></div>
                                                
    
        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
         <div class="pagecon" style="float:center; margin-left:10px;">
                                                 
                                                  </div>     
		<div style="float:left; width:40%; margin-left:10px;margin-right:10px;padding:10px" class="pdtab_Con2">									  
 <table width="100%"  id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
  <caption>Complete Lead Representation</caption>
     <thead>
      <tr style="background-color:#FCE4D6;color:#000;font-weight:bold">
            <th class="tblRB">Attempts</th>
  <?php
  $disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <th class="tblRB"><?php echo $dispo['batch'];?></th>
 <?php
						}
						
						?>
	  
      </tr>
   </thead>
 <tbody>
	<?php 
			
	$attempts=dbConnect()->prepare("SELECT * FROM attempts_list GROUP BY name");			
	  $attempts->execute();	
						
						$t=0;
						while($row=$attempts->fetch(PDO::FETCH_ASSOC))
						{
							$dt=$row['code'];
							
							
							echo '<tr>
    <td class="tblR">'.$row['name'].'</td>';
	$disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	

	   while($rows=$disp->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['batch'];
			$m=1;
			

			if($dt==1)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=2)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==2)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=3)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==3)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=4)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==4)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=5)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==5)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=6)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
				if($dt==6)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=7)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			
			
			echo '<td class="tblR">'.$count.'</td>';		
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>
   <!--
 <tbody>
	<?php 
			
	$attempts=dbConnect()->prepare("SELECT * FROM attempts_list GROUP BY name");			
	  $attempts->execute();	
						
						$t=0;
						while($row=$attempts->fetch(PDO::FETCH_ASSOC))
						{
							$dt=$row['code'];
							
							
							echo '<tr>
    <td class="tblR">'.$row['name'].'</td>';
	$disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	

	   while($rows=$disp->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['batch'];
		   	$pls=dbConnect()->prepare("SELECT DISTINCT survey.phone,leads.batch FROM survey INNER JOIN leads ON survey.lid=leads.id WHERE date(survey.date) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND survey.redial_times='".$dt."' AND disposition='Complete Survey'");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
					echo '<td class="tblR">'.$count.'</td>';
					
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>-->
  
		
			
		
	
</table>

</div>

	<div style="float:right; width:40%; margin-left:10px;margin-right:10px;padding:50px" >									  
<table width="200px" class="highchart"  data-graph-container-before="1" data-graph-type="column" style="display:none;">
  <caption>Graph Representation</caption>
     <thead>
      <tr>
            <th class="tblRB">Attempts</th>
  <?php
  $disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <th class="tblRB"><?php echo $dispo['batch'];?></th>
 <?php
						}
						
						?>
	  
      </tr>
   </thead>
    <tbody>
	<?php 
			
	$attempts=dbConnect()->prepare("SELECT * FROM attempts_list GROUP BY name");			
	  $attempts->execute();	
						
						$t=0;
						while($row=$attempts->fetch(PDO::FETCH_ASSOC))
						{
							$dt=$row['code'];
							
							
							echo '<tr>
    <td class="tblR">'.$row['name'].'</td>';
	$disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	

	   while($rows=$disp->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['batch'];
			$m=1;
			

			if($dt==1)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=2)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==2)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=3)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==3)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=4)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==4)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=5)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			if($dt==5)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=6)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
				if($dt==6)
			{
			 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND attempts.disposation='Complete Survey' AND attempts.phone NOT IN(SELECT phone FROM attempts WHERE attempt=7)");			
	  $pls->execute();
	$count=$pls->rowCount();
			}
			
			
			echo '<td class="tblR">'.$count.'</td>';		
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>
<!--
 <tbo!dy>
	<?php 
			
	$attempts=dbConnect()->prepare("SELECT * FROM attempts_list GROUP BY name");			
	  $attempts->execute();	
						
						$t=0;
						while($row=$attempts->fetch(PDO::FETCH_ASSOC))
						{
							$dt=$row['code'];
							
							
							echo '<tr>
    <td class="tblR">'.$row['name'].'</td>';
	$disp=dbConnect()->prepare("SELECT batch FROM leads GROUP BY batch");			
	  $disp->execute();	

	   while($rows=$disp->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['batch'];
			 	$pls=dbConnect()->prepare("SELECT DISTINCT survey.phone,leads.batch FROM survey INNER JOIN leads ON survey.lid=leads.id WHERE date(survey.date) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND survey.redial_times='".$dt."' AND disposition='Complete Lead'");
		  // 	$pls=dbConnect()->prepare("SELECT DISTINCT attempts.phone,leads.batch FROM attempts INNER JOIN leads ON attempts.lid=leads.id WHERE date(attempts.date_done) between '". $fromdt . "' AND '". $todt . "' AND leads.batch='".$value."' AND attempts.attempt='".$dt."' AND disposation='Complete Lead'");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
					echo '<td class="tblR">'.$count.'</td>';
					
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>-->
  
		
			
		
	
</table>

</div>
<div class="pagecon">
  
                                              </div>
<div class="clear"></div>
    </div>
    
    
    
    </div>				
	</div>
	</td>
	</tr>
    <?php // echo pagination($statement,$per_page,$page,$url='?');?>   
    </div>
			</div>
	 
	</div>
	
  <!-- end .content -->
</body>
</html>

<?php
			}
			
			else
			{
			echo "You are not Authorized to access this page.Please get out of here!";	
				
			}
				
?>
