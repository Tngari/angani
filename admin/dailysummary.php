<?php
//Start session
include("../include/config.php");
//error_reporting(0); 
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../../index.php");
exit();
}
if($_SESSION['level']=="Admin" || $_SESSION['level']=="Supervisor" )
			{
$fromdt =$_GET["fromdate"];
            $todt = $_GET["todate"];
			
			
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::ilogix Survey ::</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
	

</script>
<script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="../js/jquery.highchartTable.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart').highchartTable();
});


</script>
</head>

<body>
<div class="wrapper">
	
    	<div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/chasebank.png" alt="" height="67"  border="0" />	</a> 
			</div>
            
           <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav" style="width:1190px">
    
   
        
		 <span>Reports</span>
		  <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
		 
     </div>
	<div class="container-fluid" style="background-color:#FFF;	width:1200px;
	min-height:800px;
	margin-left:0px auto 0px auto;
	padding:0px;
	-webkit-border-top-left-radius: 3px;
-webkit-border-top-right-radius: 3px;
-moz-border-radius-topleft: 3px;
-moz-border-radius-topright: 3px;
border-top-left-radius: 3px;
border-top-right-radius: 3px;
box-shadow:  0px 1px 1px #000;
    -moz-box-shadow: 0px 1px 1px #000;
    -webkit-box-shadow: 0px 1px 1px #000;
box-shadow: 0px 8px 18px #1c1c1c;
    -moz-box-shadow: 0px 8px 18px #1c1c1c;
    -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
	<div class="captionWrapper">
	<ul>
		<?php
			$later="2014-01-01";
			$leo=date('Y-m-d');
			?>
            	<li><a href="dailysummary.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2  class="curr">Overall Daily Summary </h2></a></li>

	<li><a href="overalldailysummary.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2  class="curr">Overall Daily Summary Reachable</h2></a></li>
        <li><a href="overalldailysummary_unreachable.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2  class="curr">Overall Daily Summary Unreachable</h2></a></li>

<!--        <li><a href="calldisposition.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>call Disposition</h2></a></li>-->
	<li><a href="pta.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>PTA</h2></a></li>
        <li><a href="outstandingissues.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Outstanding Issues</h2></a></li>
        <li><a href="inactivity.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Inactivity</h2></a></li>
        <li><a href="mfukoni.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Mfukoni</h2></a></li>
        <li><a href="marketintelligence.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Market Intelligence</h2></a></li>
<!--        <li><a href="recommendations.php?fromdate=<?php echo $later;?>&todate=<?php echo $leo;?>"><h2>Recommendations</h2></a></li>-->
        
		
    </ul>
</div>
 <div class="formCon" style="float:center; width:40%; margin-left:10px;margin-right:10px;padding:10px" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 <tr>
<form id="form1" name="form1" method="get" action="dailysummary.php">
       

				  		
		
				  <tr>
       <td >From:</td>
                	<td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                    </tr>
					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr><tr><td >To:</td>
                    <td ><input name='todate' type='text'  id="todt" /></td>
    	</tr>	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
           <td>&nbsp;</td><td ><label>
            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> </td></form>

	 
	
<td>
		  <form action="getCSV.php" method ="post" > <label>
		 <input type="hidden" name="csv_text" id="csv_text">
            <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	font-weight:bold;"/>
          </label> 	
		  </form>
		  <script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
		  </td>
	
        </tr>
      </table>
            
			</div>
			
				<div class="" >
    
    <div class="clear"></div>
                                                
    
        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
         <div class="pagecon" style="float:center; margin-left:10px;">
                                                 
                                                  </div>     
		<div id="files">									  
    <table width="100%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
    
  <tr class="tablebx_topbg">
  <td class="tblRB">Date</td>
    <td class="tblRB">Dialed</td>
  <td class="tblRB">Contacted</td>
  <td class="tblRB">Call Back</td>
  <td class="tblRB">Partial Call</td>
  <td class="tblRB">Complete Calls</td>
  <td class="tblRB">PTA</td>
  <td class="tblRB">Account Closure</td>
  <td class="tblRB">Cant Activate</td>
  <?php
  $disp=dbConnect()->prepare("SELECT disposition FROM survey WHERE cs=1 AND disposition!='' GROUP BY disposition");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
<!-- <td class="tblRB"><?php echo $dispo[''];?></td>-->
 <?php
						}
						
						?>
<!-- <td class="tblRB">Total</td>-->
 
	  
  </tr>

			<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
							
							
				 echo '<tr>    <td class="tblR">'.$dt.'</td>';
//	$full=dbConnect()->prepare("SELECT disposition FROM survey WHERE cs=1 AND disposition!='' GROUP BY disposition");			
//	  $full->execute();
          
          
          $link = mysql_connect("localhost", "root", "@ct!v3@2015");
                    mysql_select_db("chasebank", $link);

                    $result = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND disposition !=''", $link);
                    $num_rows = mysql_num_rows($result);
          
                     $result2 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND disposition !='' AND cs=1", $link);
                    $num_rows2 = mysql_num_rows($result2);
                  
                    $result3 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (disposition ='Call Back' || disposition='Partial Survey-Call Back') AND cs=1", $link);
                    $num_rows3 = mysql_num_rows($result3);
                   
                    $result4 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (disposition ='Partial Survey - Language barrier' || disposition ='Partial Survey-Accout already closed' || disposition ='Partial Survey-Active Account' || disposition ='Partial Survey-Already Contacted' || disposition ='Customer Not sure to Continue' || disposition ='Partial Survey-Call Disconnected' || disposition ='Partial Survey-Customer Hang Up' || disposition ='Partial Survey-Not Account Owner' || disposition ='Partial Survey-Not Interested to continue Survey' || disposition ='Partial Survey-To consult partner' || disposition ='Not interested') AND cs=1", $link);
                    $num_rows4 = mysql_num_rows($result4);
                    
                    $result5 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (disposition ='Complete Survey' || disposition ='Complete Survey-Promised to activate the Account' || disposition ='Complete Survey-Will close the Account' || disposition ='Complete Survey-Will not activate Account' || disposition='Partial Survey-Will Activate' || disposition='Partial Survey-Will Activate hang up') AND cs=1", $link);
                    $num_rows5 = mysql_num_rows($result5);
          
                     $result6 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (disposition ='Complete Survey-Promised to activate the Account' || disposition ='Partial Survey-Will Activate' || disposition ='Partial Survey-Will Activate hang up' ) AND cs=1", $link);
                    $num_rows6 = mysql_num_rows($result6);
                    
                        $result7 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND disposition ='Complete Survey-Will close the Account' AND cs=1", $link);
                    $num_rows7 = mysql_num_rows($result7);
                    
                    $result8 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND disposition ='Complete Survey-Will not activate Account' AND cs=1", $link);
                    $num_rows8 = mysql_num_rows($result8);
//            $link = mysql_connect("localhost", "root", "@ct!v3@2015");
//                    mysql_select_db("chasebank", $link);
//          $result = mysqli_query( "SELECT * FROM survey WHERE date(date)='2017-01-13' AND Q6_1 !='' AND cs=1", $link);
//$num_rows = mysqli_num_rows($result);
//          $q = $db->query("SELECT COUNT(*) as counted FROM ...");
//$nb = $q->fetch(PDO::FETCH_OBJ);
//$nb = $nb->counted;
     

// $vv=dbConnect()->query("SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 ");
// $nb = $vv->fetch(PDO::FETCH_OBJ);   
// $nb = $nb->counted;
 //$link = mysqli_connect("localhost", "root", "@ct!v3@2015", "chasebank"); 

// $vv->execute();
//        $countall=$vv->num_rows;
//	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
//	   {
//		     $value=$rows['disposition'];
//		   	$pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND disposition='".$value."' AND cs=1");			
//	
//                	    
////                         $value2=$rows['disposition'];
////      		   	$countall=dbConnect()->prepare("SELECT count(*) FROM survey WHERE date(date)='".$dt."'  AND cs=1");			
////                  
//                //        echo $num_rows;
////                        
//          $pls->execute();
//	$count=$pls->rowCount();
//        
//      //  $countall=mysql_num_rows($pls);
//        
//        
//       // $countall=$pls->num_rows;
//	 //$countall+= $count;
//					echo '<td class="tblR">'.$count.'</td>';
//					
//	   }

					?>
  <?php echo '<td class="tblR">'.$num_rows.'</td>'; ?>
        <?php echo '<td class="tblR">'.$num_rows2.'</td>'; ?>
        <?php echo '<td class="tblR">'.$num_rows3.'</td>'; ?>
        <?php echo '<td class="tblR">'.$num_rows4.'</td>'; ?> 
        <?php echo '<td class="tblR">'.$num_rows5.'</td>'; ?> 
        <?php echo '<td class="tblR">'.$num_rows6.'</td>'; ?> 
        <?php echo '<td class="tblR">'.$num_rows7.'</td>'; ?> 
        <?php echo '<td class="tblR">'.$num_rows8.'</td>'; ?> 
        
    </tr>
	<?php 
        
        
   
	} 
    
        
//        $sql="SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition!='' AND disposition=NULL  AND cs=1 GROUP BY disposition";
//        echo $sql;
        
 echo '<tr bgcolor="#FFFF00"> <td class="tblR">Total</td>';
 
  $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition!=''  ");   
   $disp->execute(); 
      
      $t=0;
      while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
      {
       
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
     }
      $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition!=''   AND cs=1 ");   
   $disp->execute(); 
      
      $t=0;
      while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
      {
       
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
     }
      $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (disposition='Call Back' || disposition='Partial Survey-Call Back')   AND cs=1 ");   
   $disp->execute(); 
      
      $t=0;
      while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
      {
       
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
     }
      $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (disposition ='Partial Survey - Language barrier' || disposition ='Partial Survey-Accout already closed' || disposition ='Partial Survey-Active Account' || disposition ='Partial Survey-Already Contacted' || disposition ='Customer Not sure to Continue' || disposition ='Partial Survey-Call Disconnected' || disposition ='Partial Survey-Customer Hang Up' || disposition ='Partial Survey-Not Account Owner' || disposition ='Partial Survey-Not Interested to continue Survey' || disposition ='Partial Survey-To consult partner' || disposition ='Not interested')   AND cs=1 ");   
   $disp->execute(); 
      
      $t=0;
      while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
      {
       
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
     }
      $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (disposition ='Complete Survey' || disposition ='Complete Survey-Promised to activate the Account' || disposition ='Complete Survey-Will close the Account' || disposition ='Complete Survey-Will not activate Account' || disposition='Partial Survey-Will Activate' || disposition='Partial Survey-Will Activate hang up')  AND cs=1 ");   
   $disp->execute(); 
      
      $t=0;
      while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
      {
       
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
     }
      $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (disposition ='Complete Survey-Promised to activate the Account' || disposition ='Partial Survey-Will Activate' || disposition ='Partial Survey-Will Activate hang up' )   AND cs=1 ");   
   $disp->execute(); 
      
      $t=0;
      while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
      {
       
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
     }
      $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL1 FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition='Complete Survey-Will close the Account'   AND cs=1 ");   
   $disp->execute(); 
      
      $t=0;
      while($dispo1=$disp->fetch(PDO::FETCH_ASSOC))
      {
       
 echo'<td class="tblRB">';
         echo $dispo1['TOTAL1'];
    
    
    
    
   echo '</td>';
     }
      $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL2 FROM survey WHERE  date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition='Complete Survey-Will not activate Account'   AND cs=1 ");   
   $disp->execute(); 
      
      $t=0;
      while($dispo2=$disp->fetch(PDO::FETCH_ASSOC))
      {
       
 echo'<td class="tblRB">';
         echo $dispo2['TOTAL2'];
    
    
    
    
   echo '</td>';
     }
     echo '<tr bgcolor="#B6EEDA"> <td class="tblR">Percentage breakdown</td>';
      echo '</td>';
      
      
       echo '<tr bgcolor="#FFFF00"> <td class="tblR">Total Dialed</td>';
      echo '</td>';
      
      echo'<td class="tblRB">';
       $result11 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition !=''", $link);
                    $num_rows11 = mysql_num_rows($result11);   
                    
      $result12 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition !=''", $link);
                    $num_rows12 = mysql_num_rows($result12);  
      
      echo round(($num_rows11/$num_rows12), 4)*100;     echo ' ';     echo '%';
      
       echo '<tr bgcolor="#FFFF00"> <td class="tblR">Total Contacted</td>';
      echo '</td>';
      echo'<td class="tblRB">';
       $result13 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition !=''", $link);
                    $num_rows13 = mysql_num_rows($result13);   
                    
      $result14 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition !='' AND cs=1", $link);
                    $num_rows14 = mysql_num_rows($result14);  
      
      echo round(($num_rows14/$num_rows13), 4)*100;     echo ' ';     echo '%';
      
      
      
      
       echo '<tr bgcolor="#FFFF00"> <td class="tblR">PTA</td>';
      echo '</td>';
      
      echo'<td class="tblRB">';
       $result15 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (disposition ='Complete Survey' || disposition ='Complete Survey-Promised to activate the Account' || disposition ='Complete Survey-Will close the Account' || disposition ='Complete Survey-Will not activate Account' || disposition='Partial Survey-Will Activate' || disposition='Partial Survey-Will Activate hang up') AND cs=1", $link);
                    $num_rows15 = mysql_num_rows($result15);   
                    
      $result18 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (disposition ='Complete Survey-Promised to activate the Account' || disposition ='Partial Survey-Will Activate' || disposition ='Partial Survey-Will Activate hang up' ) AND cs=1", $link);
                    $num_rows18 = mysql_num_rows($result18);      
                    
                    
      echo round(($num_rows18/$num_rows15), 4)*100;     echo ' ';     echo '%';

      
      
       echo '<tr bgcolor="#FFFF00"> <td class="tblR">Account Closure</td>';
      echo '</td>';
      
      echo'<td class="tblRB">';
       $result16 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition ='Complete Survey-Will close the Account' AND cs=1", $link);
                    $num_rows16 = mysql_num_rows($result16);   
                    
     
      echo round(($num_rows16/$num_rows15), 4)*100;     echo ' ';     echo '%';

      
      echo '<tr bgcolor="#FFFF00"> <td class="tblR">Cant Activate</td>';
      echo '</td>';
      
      
      echo'<td class="tblRB">';
       $result17 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND disposition ='Complete Survey-Will not activate Account' AND cs=1", $link);
                    $num_rows17 = mysql_num_rows($result17);   
                    
     
      
      echo round(($num_rows17/$num_rows15), 4)*100;     echo ' ';     echo '%';

//     $link = mysql_connect("localhost", "root", "@ct!v3@2015");
//                    mysql_select_db("chasebank", $link);
//
//                    $result2 = mysql_query("SELECT * FROM survey WHERE  disposition !='' AND cs=1", $link);
//                    $num_rows2 = mysql_num_rows($result2);
//    echo'<td class="tblRB">';
//         echo $num_rows2;
        ?>
 </tr>
                                             
    
    
	
</table>

<div  style="margin-top:100px">

<!--<table width="100%" class="highchart"  data-graph-container-before="1" data-graph-type="column" style="display:none;">
  <caption>Graphical Representation</caption>
   <thead>
      <tr>
            <th class="tblRB">Date</th>
            
  <?php
  echo' <td class="tblRB">Dialed</td>
  <td class="tblRB">Contacted</td>
  <td class="tblRB">Call Back</td>
  <td class="tblRB">Partial Call</td>
  <td class="tblRB">Complete Calls</td>
  <td class="tblRB">PTA</td>
  <td class="tblRB">Account Closure</td>
  <td class="tblRB">Cant Activate</td>'
						
						?>
	  
      </tr>
   </thead>
  
     <tbody>
	<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
							
							
							echo '<tr>
    <td class="tblR">'.$dt.'</td>';
	$full=dbConnect()->prepare("SELECT disposition FROM survey WHERE cs=1 AND disposition!='' GROUP BY disposition");			
	  $full->execute();
	 
	//$vv="SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 GROUP BY disposition";
	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['disposition'];
		   	$pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND disposition='".$value."' AND cs=1");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
					echo '<td class="tblR">'.$count.'</td>';
					
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>
  		
</table>-->

</div>
</div>
<div class="pagecon">
  
                                              </div>
<div class="clear"></div>
    </div>
    
    
    
    </div>
						
	</div>
	</td>
	</tr>
    <?php // echo pagination($statement,$per_page,$page,$url='?');?>   
    </div>
			</div>
	 
	</div>
	
  <!-- end .content -->
</body>
</html>

<?php
			}
			
			else
			{
			echo "You are not Authorized to access this page.Please get out of here!";	
				
			}
				
?>
