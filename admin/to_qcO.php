<?php
//Start session
include("../include/config.php");
error_reporting(0); 
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../../index.php");
exit();
}
if($_SESSION['level']=="Admin" || $_SESSION['level']=="Supervisor" || $_SESSION['level']=="Client"  )
			{
$fromdt =$_GET["fromdate"];
            $todt = $_GET["todate"];
			if($_SESSION['level']=="Supervisor")
			{
				$adwhere="AND disposition='Complete Lead'";
			}
			else
			{
				$adwhere="AND disposition='Complete Lead'";
			}
	
function find_file($dirs,$filename,$exact=false){

	$dir = @scandir($dirs);
	if(is_array($dir) AND !empty($dir)){
	foreach($dir as $file){
	if(($file !== '.') AND ($file!=='..')){
	if (is_file($dirs.'/'.$file)){
		$filepath =  realpath($dirs.'/'.$file);

		if(!$exact){
			$pos = strpos($file,$filename);
			if($pos === false) {
			}
			else {
				if(file_exists($filepath) AND is_file($filepath)){
				echo str_replace($filename,'<span style="color:red;font-weight:bold">'.$filename.'</span>',$filepath).' ('.round(filesize($filepath)/1024).'kb)<br />';
				}
			}
		}
		elseif(($file==$filename)){

			if(file_exists($filepath) AND is_file($filepath)){
				echo str_replace($filename,'<span style="color:red;font-weight:bold">'.$filename.'</span>',$filepath).' ('.round(filesize($filepath)/1024).'kb)<br />';
			}
		}
	}
	else{
		find_file($dirs.'/'.$file,$filename,$exact);
	}
	}
	}
	}
}


$dirs = 'Classes/';	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::ilogix Survey ::</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
	

</script>
<style type="text/css">
	.disabled
	{
		background-color:#666;
		font-size:11px;
		color:#FFF;
		font-weight:bold;
	}
	
	/*dashlist style*/
.pdtab_Con{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	border-left: 1px #d3dde6 solid;
	border-bottom: 1px #d3dde6 solid;
	
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con table td{
	padding:8px 0px;
	
	border-style: double;
	font-size:11px;
}

.pdtab_Con table tr{
		background: #b8d1f3;
	}
	/*  Define the background color for all the ODD background rows  */
	
	
	
	.pdtab_Con table tr:nth-child(odd){ 
		background: #b8d1f3;
	}
	/*  Define the background color for all the EVEN background rows  */
	.pdtab_Con table tr:nth-child(even){
		background: #FFF2CC;
	}

.pdtab-h{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}
/*Table 2 */
.pdtab_Con2{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con2 table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	border-left: 1px #d3dde6 solid;
	border-bottom: 1px #d3dde6 solid;
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con2 table td{
	padding:8px 0px;
	border-style: double;
	font-size:11px;
}

.pdtab_Con2 table tr{
		background: #FCFCFC;
	}
	/*  Define the background color for all the ODD background rows  
	
	
	
	.pdtab_Con2 table tr:nth-child(even) td:nth-last-child(10)
	{
		background: #b8d1f3;
	}
	
	.pdtab_Con2 table tr:nth-child(odd) td:nth-last-child(10)
	{
		background: #FFF2CC;
	}
	
	*/



.pdtab-h2{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h2{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}



/*Table 3 */
.pdtab_Con3{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con3 table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	border-left: 1px #d3dde6 solid;
	border-bottom: 1px #d3dde6 solid;
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con3 table td{
	padding:8px 0px;
	border-style: double;
	font-size:11px;
}

.pdtab_Con3 table tr{
		background: #FCFCFC;
	}
	/*  Define the background color for all the ODD background rows  
	
	
	
	.pdtab_Con3 table tr:nth-child(even) td:nth-last-child(5)
	{
		background: #b8d1f3;
	}
	
	.pdtab_Con3 table tr:nth-child(odd) td:nth-last-child(5)
	{
		background: #FFF2CC;
	}
	
	*/



.pdtab-h3{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h3{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}


/*Table 4 */
.pdtab_Con4{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con4 table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	border-left: 1px #d3dde6 solid;
	border-bottom: 1px #d3dde6 solid;
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con4 table td{
	padding:8px 0px;
	border-style: double;
	font-size:11px;
}

.pdtab_Con4 table tr
{
		background: #FCFCFC;
	}
	/*  Define the background color for all the ODD background rows 
	
	
	
	.pdtab_Con4 table tr:nth-child(even) td:nth-last-child(14)
	{
		background: #b8d1f3;
	}
	
	.pdtab_Con4 table tr:nth-child(odd) td:nth-last-child(14)
	{
		background: #FFF2CC;
	}
	 */
	

	



.pdtab-h4{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h4{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}


/*Table 4 */
.pdtab_Con5{
	margin:0px;
	padding:30px 0px 0px 0px;
}
.pdtab_Con5 table{
	padding:0px 0px 0px 0px;
	margin:0px 0px 0px 0px;
	
	/*-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;*/
}
.pdtab_Con5 table td{
	padding:8px 0px;
	border-style: double;
	font-size:11px;
}

.pdtab_Con5 table tr
{
		background: #FCFCFC;
	}
	/*  Define the background color for all the ODD background rows 
	
	
	
	.pdtab_Con5 table tr:nth-child(even) td:nth-last-child(9)
	{
		background: #FC;
	}
	
	.pdtab_Con5 table tr:nth-child(odd) td:nth-last-child(9)
	{
		background: #FFF2CC;
	}
	 */
	

	



.pdtab-h5{
	padding:0px 0px;
	margin:0px;
	font-size:11px;
	font-weight:bold;
	/*background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;*/
	background:url(../images/item_th_bg.png) repeat-x;
}
.pd_dbtab-h5{
	padding:0px 0px;
	margin:0px;
	font-size:12px;
	font-weight:bold;
	text-shadow: 0px 1px 0px #fff;
	background:#f0f0f0 url(../images/pdtls_th_bg.png) repeat-x bottom;
}

.tb
{
	background-color:#F30;
	color:#FFF;
	text-align:center;
	font-weight:bold;
	border:1px solid #000;
	border-radius:5px;
}
	</style>
</head>

<body>
<div class="wrapper">
	
    	<div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/logo-plantwise.png" alt=""  border="0" />	</a> 
			</div>
            
           <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav" style="width:2590px">
    
   
        
		 <span>Reports</span>
		  <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
		 
     </div>
	<div class="container-fluid" style="background-color:#FFF;	width:2600px;
	min-height:800px;
	margin-left:0px auto 0px auto;
	padding:0px;
	-webkit-border-top-left-radius: 3px;
-webkit-border-top-right-radius: 3px;
-moz-border-radius-topleft: 3px;
-moz-border-radius-topright: 3px;
border-top-left-radius: 3px;
border-top-right-radius: 3px;
box-shadow:  0px 1px 1px #000;
    -moz-box-shadow: 0px 1px 1px #000;
    -webkit-box-shadow: 0px 1px 1px #000;
box-shadow: 0px 8px 18px #1c1c1c;
    -moz-box-shadow: 0px 8px 18px #1c1c1c;
    -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
	<div class="captionWrapper">
	<ul>
	<li><a href="index.php"><h2 class="curr">CSV Reports</h2></a></li>
	
    	
		
    </ul>
</div>
 <div class="formCon" style="float:center; width:40%; margin-left:10px;margin-right:10px;padding:10px" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 <tr>
<form id="form1" name="form1" method="get" action="index.php">
       

				  		
		
				  <tr>
       <td >From:</td>
                	<td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                    </tr>
					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr><tr><td >To:</td>
                    <td ><input name='todate' type='text'  id="todt" /></td>
    	</tr>	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
           <td>&nbsp;</td><td ><label>
            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> </td></form>

	 
	
<td>
		  <form action="getCSV.php" method ="post" > <label>
		 <input type="hidden" name="csv_text" id="csv_text">
            <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	font-weight:bold;"/>
          </label> 	
		  </form>
		  <script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
		  </td>
	
        </tr>
      </table>
            
			</div>
			
				<div class="" >
    
    <div class="clear"></div>
                                                
    
        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
         <div class="pagecon" style="float:center; margin-left:10px;">
                                                 
                                                  </div>     
		<div id="files" class="pdtab_Con2">									  
    <table width="100%"  id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
    
  <tr class="tablebx_topbg">
 <td class="tblRB">#</td>
    <td class="tblRB">Phone No</td>
 <td class="tblRB">Enumerator</td>
	    <td class="tblRB">Call Status</td>
	   <td class="tblRB">Q1</td>
	    <td class="tblRB">Q2</td>
	  <td class="tblRB">Q3</td>
	  	  <td class="tblRB">Q4</td>
	  <td class="tblRB">Q5</td>
	  <td class="tblRB">Q5 Other</td>
	  <td class="tblRB">Q6</td>
    <td class="tblRB">Q7</td>
	      <td>Q8</td>
	      <td>Q9</td>
		   <td>Q9 Other 1</td>
		    <td>Q9 Other 2</td>
	      <td>Q10</td>
	       <td>Q11</td>
	       <td>Q12 dd/mm/yy</td>
	      	<td>Q13</td>
	        <td>Q14</td>
	       <td>Q15 In Kgs</td>
		     <td>Q16</td>
			<td>Q17</td>
	        <td>Q18</td>
			 <td>Q18 Other</td>
			  <td>Q18 Other2</td>
		    <td>Q19 In kgs</td>
		    <td>Q20 In kgs</td>
		    <td>Q21 In kgs</td>
		   <td>Q22 In kgs</td>
		    <td>Q23 In kgs</td>
		    <td>Q24 In kgs</td>
			<td>Q25 In kgs</td>
			<td>Q26 In kgs</td>
			<td>Q27 In kgs</td>
		    <td>Q28</td>
		    <td>Q29</td>
	        <td>Q30</td>
		    <td>Q31</td>
			<td>Q32</td>
			<td>Q32 Other1</td>
			<td>Q33</td>
			<td>Q34</td>
			<td>Q35</td>
			<td>Q36</td>
			<td>Q37</td>
			<td>Q38 </td>
			<td>Q39</td>
			<td>Q40</td>
			<td>Q41</td>
			<td>Q42</td>
			<td>Q43</td>
			<td>Q44</td>
			<td>Q45 in Acres</td>
			<td>Q46 in Acres</td>
			<td>Q47</td>
			<td>Q48</td>
			<td>Q49</td>
			<td>Q50</td>
			<td>Q51</td>
			<td>Q52</td>
			<td>Q53</td>
		<td>Survey Disposition</td>
		<td>Disposition Comments</td>
	      <td>Date</td>
		    <td>No.of Dials</td>
			<td>Batch</td>
			<?php 
			if($_SESSION['level']=="Admin" || $_SESSION['level']=="Supervisor")
				
				{
					
					?>
					
				    <td>Audio Record</td>
		    <td>Quality</td>
		

<?php 				
				}
				
				else
				{
					
					
				}
				
?>
			<?php 	
				$sel=dbConnect()->prepare("SELECT survey.*,leads.batch FROM survey INNER JOIN leads ON survey.lid=leads.id WHERE date(date) between '". $fromdt . "' AND '". $todt . "' $adwhere");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$t+=1;
							$q5list=explode(",",$row['Q5a']);
						?>
  
 <tr class=<?php echo $cls;?>>
    <td class="tblR"><?php echo $t;?></td>
		<td class="tblR"><?php echo $row['phone']; ?></td>
    <td class="tblR"><a href=""><?php echo $row['interviewer']; ?></a></td>
		<td class="tblR"><?php $cs=$row['cs']; if($cs==1){echo "Reachable";}if($cs==2){echo "Unreachable";}?></td>
   	<td class="tblR"><?php echo $row['Q1']; ?></td>
	 <td class="tblR"><?php echo $row['Q2']; ?></td>
	 <td class="tblR"><?php echo $row['Q3']; ?></td>
<td><?php echo $visit=$row['Q4']; ?></td>
<td><?php echo $visit=$row['Q5a']; ?></td>
<td><?php echo $visit=$row['Q5o']; ?></td>
  <td ><?php echo $row['Q6']; ?></td>
<td ><?php echo $row['Q7'];?></td>
  <td  ><?php echo $row['Q8']; ?></td>
<td ><?php echo $row['Q9a']; ?></td>
<td ><?php echo $row['other1']; ?></td>
<td ><?php echo $row['other2']; ?></td>
  <td  ><?php echo $row['Q10']; ?></td>
<td><?php echo $row['Q11']; ?></td>
<td><?php echo $row['Q12']; ?></td>
<td><?php echo $row['Q13']; ?></td>
<td><?php echo $row['Q14']; ?></td>
<td><?php echo $row['Q15']; ?></td>
<td><?php echo $row['Q16']; ?></td>
<td><?php echo $row['Q17']; ?></td>
<td><?php echo $row['Q18a']; ?></td>
<td ><?php echo $row['other_1']; ?></td>
<td ><?php echo $row['other_2']; ?></td>
<td><?php echo $row['Q19']; ?></td>
<td><?php echo $row['Q20']; ?></td>
<td><?php echo $row['Q21']; ?></td>
<td><?php echo $row['Q22']; ?></td>
<td><?php echo $row['Q22a']; ?></td>
<td><?php echo $row['Q22b']; ?></td>
<td><?php echo $row['Q22c']; ?></td>
<td><?php echo $row['Q23']; ?></td>
<td><?php echo $row['Q24']; ?></td>
<td><?php echo $row['Q25']; ?></td>
<td><?php echo $row['Q26']; ?></td>
<td><?php echo $row['Q27']; ?></td>
<td><?php echo $row['Q28']; ?></td>
<td><?php echo $row['Q29a']; ?></td>
<td ><?php echo $row['Q29_other']; ?></td>
<td><?php echo $row['Q30']; ?></td>
 <td><?php echo $row['Q31']; ?></td>
 <td><?php echo $row['Q32']; ?></td>
 <td><?php echo $row['Q33']; ?></td>
<td><?php echo $row['Q34']; ?></td>
<td><?php echo $row['Q35']; ?></td>
<td class="tblR"><?php echo $row['Q36']; ?></td>
<td class="tblR"><?php echo $row['Q37']; ?></td>
	 <td class="tblR"><?php echo $row['Q38']; ?></td>
<td><?php echo $visit=$row['Q39']; ?></td>
<td><?php echo $visit=$row['Q40']; ?></td>
  <td ><?php echo $row['Q41']; ?></td>
<td ><?php echo $row['Q42'];?></td>
  <td  ><?php echo $row['Q43']; ?></td>
<td ><?php echo $row['Q44']; ?></td>
  <td  ><?php echo $row['Q45']; ?></td>
<td><?php echo $row['Q46']; ?></td>
<td><?php echo $row['Q47']; ?></td>
<td><?php echo $row['Q48']; ?></td>
<td><?php echo $row['Q49']; ?></td>
<td><?php echo $row['Q50']; ?></td>
<td><?php echo $row['disposition']; ?></td>
<td ><?php echo $row['disptype']; ?></td>
  <td><?php echo $row['date']; ?></td>
   <td><?php $cs=$row['cs']; if($cs==2){echo $row['redial_times'];} ?></td>
  <td><?php echo $row['batch']; ?></td>
  <?php 
			if($_SESSION['level']=="Admin" || $_SESSION['level']=="Supervisor")
				
				{
					
					?>
					<td>
				    <?php 
					if($row['disposition']=='Complete Lead')
					{
						
						
$filename=$row['phone'];
$siku=date('Y-m-d',strtotime($row['date']));
$exact=false;
$conn_id = ftp_connect('192.168.2.200');
$login_result = ftp_login($conn_id, 'chase', 'ch@se@15');
if (!$login_result)
{
    exit();
}
ftp_pasv($conn_id, true);
$contents = ftp_nlist($conn_id, "Technology/ilogix/$siku/");
foreach((array)$contents as $file)
{
	//echo $file;


		$filepath=$file;

		if(!$exact){
			$pos = strpos($file,$filename);
			if($pos === false) {
			}
			else {

				echo '<a href="ftp:192.168.2.200/'.$filepath.'" target="_blank">'.str_replace($filename,'<span style="color:red;font-weight:bold">'.$filename.'</span>',$filepath).'</a><br/>';
				
			}
		}
		elseif(($file==$filename)){

		
				echo '<a href="ftp:192.168.2.200/'.$filepath.'" target="_blank">'.str_replace($filename,'<span style="color:red;font-weight:bold">'.$filename.'</span>',$filepath).'</a><br />';
			
		}
		else
		{
			
			echo "No File";
		}
	
	
	
}	

}
				
					else
					{
						?>
						
		   
		<?php
					}
					?>
					</td>
					<?php
					if($row['disposition']=='Complete Lead' && $row['quality']=='')
					{
						?>
						
		    <td><a href="../qc/loading.php?id=<?php echo $row['id']; ?>&qadate=<?php echo date('Y-m-d H:i:s') ?>">Start QA</a></td>
		<?php
					}
				
					else if($row['disposition']=='Complete Lead' && $row['quality']=='Y')
					{
						?>
						
		    <td style="background-color:#FCE4D6;color:#000;font-weight:bold">In Progress</td>
		<?php
					}
				
					else if($row['disposition']=='Complete Lead' && $row['quality']=='F')
					{
						?>
						
		   <td style="background-color:#008A00;color:#000;font-weight:bold">QA Done</td>
		<?php
					}
					else
					{
						?>
						
						<td style="background-color:#808080" class="tblR"></td>
						
						<?php
					}
					
					?>

<?php 				
				}
				
				else
				{
					
					
				}
				
?>
    </tr>
	<?php 

	} ?>
	
</table>
</div>
<div class="pagecon">
  
                                              </div>
<div class="clear"></div>
    </div>
    
    
    
    </div>
						
	</div>
	</td>
	</tr>
    <?php // echo pagination($statement,$per_page,$page,$url='?');?>   
    </div>
			</div>
	 
	</div>
	
  <!-- end .content -->
</body>
</html>

<?php
			}
			
			else
			{
			echo "You are not Authorized to access this page.Please get out of here!";	
				
			}
				
?>
