<?php
//Start session
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_username']) || (trim($_SESSION['sess_username']) == '')) {
header("location:index.php");
exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>::ZUKU AGENT'S MONITORING SYSTEM::-EVALUATION FORM </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script script type="text/javascript" src="../demo.js"></script>

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">


 $(document).ready(function () 
 {
	  //Attaching the change event of dropdown listtoatt
	   $('#picupload').hide();
	   //$('#dot').hide();
	   //$('#doa').hide();
	   $('#piulabel').hide();
	  
$('#cn1').click(function () {
         var $this = $(this);
         if ($this.is(':checked')) {
             $('#piulabel').show();
			   $('#picupload').show();
			  //  $('#cn1').checked;
         } 
		 
		 else{
		   $('#piulabel').hide();
			   $('#picupload').hide();
		 }
     });
	 $('#cn2').click(function () {
         var $this = $(this);
         if ($this.is(':checked')) {
             $('#piulabel').hide();
			   $('#picupload').hide();
         } 
     });
  
  });
  
   
  </script>
</head>
<body>
<div class="wrapper">
 <div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/logo.png" alt=""  border="0" />	</a> 
			</div>
            
            <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav">
    
   
        
		 <span> Evaluation Form</span>
		  <span style="float:right"><a href="logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['sess_username'];?></span>
		 
     </div>
<?php 

$id=$_GET["id"];

 require_once('../Connections/db.php'); error_reporting(0);
$query_airtel_reports = "SELECT * FROM appraisal_data WHERE  id=$id ";
$air2013_reports = mysql_query($query_airtel_reports) or die(mysql_error());
$row_airtel_reports = mysql_fetch_assoc($air2013_reports);
//$totalRows_airtel_reports = mysql_num_rows($air2013_reports); 

?>
<div class="container-fluid" >
 
 <form action="form_update.php" method="POST" enctype="multipart/form-data"  name="form" id="form"><br/>
    <div class="formCon2" >
        
		<h3>General Details</h3>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			
					<td style="padding-left:8px;"><input type="hidden" name="id" value="<?php echo $id;?>"></td>
			</tr>
                <tr>
                    <td align="right"><label> Agent's Name: </td>
                    <td style="padding-left:8px;">
						 <select class="" name="name" id="name">
						 <option value="0"><?php echo $row_airtel_reports['agent_name'];?></option>
			 <option value="1">Njeri Mwendwa</option>
            <option value="2">Peterson Kipkorir</option>
            <option value="3">Sospeter Williams</option>
			 <option value="4">G.K Omondi</option>
			 
          </select>
                    </td>
                    
                    <td align="right"><label>QA Name:</td>
                    <td style="padding-left:8px;">
						 <select class="" name="qa">
						 <option value="0"><?php echo $row_airtel_reports['qc_person_id'];?></option>
			 <option value="1">BPO-Airtel call center</option>
            <option value="2">BPO-DT Solar Systems CC </option>
            <option value="3">BPO-Safaricom call center</option>
			 <option value="4">BPO-GE-Africa CC</option>
			 
          </select>
                    </td>
                </tr>
				
				<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
				 <tr>
                    <td align="right"><label> Call Date/Time: </td>
                    <td style="padding-left:8px;">
						 <input type="text" name="call_date" id="todt" value="<?php echo $row_airtel_reports['date_of_call'];?>" >
                    </td>
                    
                    <td align="right"><label>Account No:</td>
                    <td style="padding-left:8px;">
						<input type="text" name="acc_no" value="<?php echo $row_airtel_reports['acc_no'];?>">
                    </td>
                </tr>
				<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
   <tr>
                    <td align="right"><label> Call Duration: </td>
                    <td style="padding-left:8px;">
						 <input type="text" name="duration" value="<?php echo $row_airtel_reports['call_duration'];?>">
                    </td>
                    
                    <td align="right"><label>Call Number:</td>
                    <td style="padding-left:8px;">
						<input type="text" name="call_no" value="<?php echo $row_airtel_reports['call_no'];?>">
                    </td>
                </tr>
				<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
    <tr>
                    <td align="right"><label> Automatic Fail: </td>
                    <td style="padding-left:8px;">
						 <input type="text" name="fail" value="<?php echo $row_airtel_reports['automatic_fail'];?>" >
                    </td>
                    
                   
                </tr>
							<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
				  <tr>
                    <td align="right"><label> Monitor Percentage: </td>
                    <td style="padding-left:8px;">
						 <input name="sum" type="text" id="sum" size="5" readonly="" value="<?php echo $row_airtel_reports['total_score'];?>" >
                    </td>

                </tr>
            </table>
        </div>
 
	
	
	<div class="formCon">
       
		<h2>Monitoring/Evaluation </h2>
		<br>
		
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                
				<tr>
				<td>&nbsp;</td><td>Weighting(%)</td><td>Comments</td>
				</tr>
				<tr> <td><h3>Welcome</h3></td></tr>
				<tr>
				
                    <td ><label> 1) Used correct greeting-  Welcome, Brand name,CSR name,willingness to assist</td>
                   
					 <td style="padding-left:8px;">
						<input type="text" name="s1" id="s1"  value="<?php echo $row_airtel_reports['q1'];?>" size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c1" id="c1" value="<?php echo $row_airtel_reports['c1'];?>"></textarea>
        </label></td>
					</tr>
						<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
					<tr>
                     <td ><label> 2)  Asked for customer's account number or Transaction No</td>
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s2" id="s2"  value="<?php echo $row_airtel_reports['q2'];?>" size="3">
                    </td>
                   <td width="180" class="tbl"><label>
          <textarea name="c2" id="c2" value="<?php echo $row_airtel_reports['c2'];?>"></textarea>
        </label></td>
                </tr>
				<tr> <td><h3>Data Protection</h3></td></tr>
				<tr>
				
                    <td ><label> 3) Customer BIO data/ Name contact numbers </td>
                    
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s3"  id="s3"  value="<?php echo $row_airtel_reports['q3'];?>" size="3">
                    </td>
					 <td width="180" class="tbl"><label>
          <textarea name="c3" id="c3" value="<?php echo $row_airtel_reports['c3'];?>"></textarea>
        </label></td>
                </tr>
									<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
				<tr>
				
                    <td ><label> 4) Account name </td>
                   
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s4" id="s4" value="<?php echo $row_airtel_reports['q4'];?>" disabled='true' size="3">
                    </td>
					 <td width="180" class="tbl"><label>
          <textarea name="c4" id="c4" value="<?php echo $row_airtel_reports['q4'];?>"></textarea>
        </label></td>
                </tr>
												<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
				<tr> <td><h3>Trouble shooting</h3></td></tr>
				<tr>
				
                    <td ><label> 5)Appropriate and correct trouble shooting was done. </td>
                 
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s5" id="s5" value="<?php echo $row_airtel_reports['q5'];?>" disabled='true' size="3">
                    </td>
					 <td width="180" class="tbl"><label>
          <textarea name="c5" id="c5" value="<?php echo $row_airtel_reports['c5'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
				
											<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
				<tr> <td><h3>Issue Resolution</h3></td></tr>
				<tr>
				
                    <td ><label> 6)The customer's issue was handled apprpriately </td>
                   
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s6" id="s6" value="<?php echo $row_airtel_reports['q6'];?>" disabled='true' size="3">
                    </td>
					 <td width="180" class="tbl"><label>
          <textarea name="c6" id="c6" value="<?php echo $row_airtel_reports['c6'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
														<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
				<tr>
				
                    <td ><label> 7) If escalation needed, the escalation was made accordingly </td>
                   
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s7" id="s7" value="<?php echo $row_airtel_reports['q7'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c7" id="c7" value="<?php echo $row_airtel_reports['c7'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
														<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
				<tr>
				
                    <td ><label>8) Resolution SLA given to the client </td>
                  
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s8" id="s8" value="<?php echo $row_airtel_reports['q8'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c8" id="c8" value="<?php echo $row_airtel_reports['c8'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
				
				<tr> <td><h3>Soft Skills</h3></td></tr>
				<tr>
				
                    <td ><label> 9) Used the customer's name at least once during the call </td>
                   
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s9" id="s9" value="<?php echo $row_airtel_reports['q9'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c9" id="c9"></textarea>
        </label></td>
                </tr>
				
														<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
				<tr>
				
                    <td ><label>10) Voice reflected energy and enthusiasm</td>
                   
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s10" id="s10" value="<?php echo $row_airtel_reports['q10'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c10" id="c10" value="<?php echo $row_airtel_reports['c10'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
				
														<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
				<tr>
				
                    <td ><label> 11)No Dead Air </td>
                  
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s11" id="s11" value="<?php echo $row_airtel_reports['q11'];?>" disabled='true' size="3">
                    </td>
						<td width="180" class="tbl"><label>
          <textarea name="c11" id="c11" value="<?php echo $row_airtel_reports['c11'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
				
														<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
				<tr>
				
                    <td ><label> 12) Used polite/appropriate tone and pitch </td>
                  
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s12" id="s12" value="<?php echo $row_airtel_reports['q12'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c12" id="c12" value="<?php echo $row_airtel_reports['c12'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																		<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
				<tr>
				
                    <td ><label> 13)Used proper grammar and business language no Jargon </td>
                  
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s13" id="s13" value="<?php echo $row_airtel_reports['q13'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c13" id="c13" value="<?php echo $row_airtel_reports['c13'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
					<tr>
				
                    <td ><label> 14)Followed hold procedure; Did not use mute button </td>
              
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s14" id="s14" value="<?php echo $row_airtel_reports['c14'];?>" disabled='true' size="3">
                    </td>
							<td width="180" class="tbl"><label>
          <textarea name="c14" id="c14" value="<?php echo $row_airtel_reports['c14'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
					<tr>
				
                    <td ><label> 15)Followed correct transfer procedure</td>
                   
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s15" id="s15" value="<?php echo $row_airtel_reports['q15'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c15" id="c15" value="<?php echo $row_airtel_reports['c15'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
					<tr>
				
                    <td ><label> 16)Listened and understood customer's query </td>
                   
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s16" id="s16" value="<?php echo $row_airtel_reports['q16'];?>" disabled='true' size="3">
                    </td>
						<td width="180" class="tbl"><label>
          <textarea name="c16" id="c16" value="<?php echo $row_airtel_reports['c16'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
					<tr>
				
                    <td ><label> 17)Used verbal nods</td>
                 
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s17" id="s17" value="<?php echo $row_airtel_reports['q17'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c17" id="c17" value="<?php echo $row_airtel_reports['c17'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
					<tr>
				
                    <td ><label> 18)Customer did no repeat themselves </td>
                   
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s18" id="s18" value="<?php echo $row_airtel_reports['q18'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c18" id="c18"></textarea>
        </label></td>
                </tr>
					
																								<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
			
	<tr> <td><h3>Value Add</h3></td></tr>
				<tr>
				
                    <td ><label> 19) Winback for Chan customers </td>
                  
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s19" id="s19" value="<?php echo $row_airtel_reports['q19'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c19" id="c19" value="<?php echo $row_airtel_reports['c19'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																								<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
				<tr>
				
                    <td ><label> 20)Product promotion</td>
                  
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s20" id="s20" value="<?php echo $row_airtel_reports['q20'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c20" id="c20" value="<?php echo $row_airtel_reports['c20'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
			
																								<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
	
	<tr> <td><h3>Equipment Use</h3></td></tr>
				<tr>
				
                    <td ><label> 21) Accessed correct customer's account </td>
                
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s21" id="s21" value="<?php echo $row_airtel_reports['q21'];?>" disabled='true' size="3">
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c21" id="c21" value="<?php echo $row_airtel_reports['c21'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																								<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
				<tr>
				
                    <td ><label> 22) Correctly dispossed the call </td>
                
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s22" id="s22" value="<?php echo $row_airtel_reports['q22'];?>" disabled='true' size="3">
                    </td>
						<td width="180" class="tbl"><label>
          <textarea name="c22" id="c22" value="<?php echo $row_airtel_reports['c22'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																								<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
					<tr>
				
                    <td ><label> 23) Full and complete notes updated</td>
                 
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s23" id="s23" value="<?php echo $row_airtel_reports['q23'];?>" disabled='true' size="3">
                    </td>
							<td width="180" class="tbl"><label>
          <textarea name="c23" id="c23" value="<?php echo $row_airtel_reports['c23'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																								<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
				<tr> <td><h3>Closing Skills</h3></td></tr>
				<tr>
				
                    <td ><label> 24) Used proper closing -Further assistance, branding,thank customer </td>
                  
                    
                    
                    <td style="padding-left:8px;">
						<input type="text" name="s24" id="s24" value="<?php echo $row_airtel_reports['q24'];?>" disabled='true' size="3">
                    </td>
							<td width="180" class="tbl"><label>
          <textarea name="c24" id="c24" value="<?php echo $row_airtel_reports['c24'];?>" disabled='true'></textarea>
        </label></td>
                </tr>
																								<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
		
				  </table>
				<div class="formCon2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> <td><h3>Automatic Fail</h3></td></tr>
				<tr>
                    <td ><label> 25)Incorrect information </td>
                    <td style="padding-left:8px;">
						 <input type="radio" name="qn25" id="qn25" value="1">Yes
                    </td>
				<td width="180" class="tbl"><label>
          <textarea name="c25" id="c25"></textarea>
        </label></td>
                </tr>
				<tr>
                    <td ><label> 26)Call avoidance- disconnect, Brush issue off, transfer back to que </td>
                    <td style="padding-left:8px;">
						 <input type="radio" name="qn26" id="qn26" value="1" >Yes
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c26" id="c26"></textarea>
        </label></td>
                </tr>
				<tr>
                    <td ><label> 27)Failure to answer call/ delayed answer </td>
                    <td style="padding-left:8px;">
						 <input type="radio" name="qn27" id="qn27" value="1" >Yes
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c27" id="c27"></textarea>
        </label></td>
                </tr>
				<tr>
                    <td ><label> 28)Failed to verify authorized user when required </td>
                    <td style="padding-left:8px;">
						 <input type="radio" name="qn28" id="qn28" value="1" onclick="return empty(4)">Yes
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c28" id="c28"></textarea>
        </label></td>
                </tr>
				<tr>
                    <td ><label> 29)Failed to access customer account </td>
                    <td style="padding-left:8px;">
						 <input type="radio" name="qn29" id="qn29" value="1" onclick="return empty(5)">Yes
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c29" id="c29"></textarea>
        </label></td>
                </tr>
				<tr>
                    <td ><label> 30)Rude/Inpolite </td>
                    <td style="padding-left:8px;">
						 <input type="radio" name="qn30" name="qn30" value="1" onclick="return empty(6)">Yes
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c30" id="c30"></textarea>
        </label></td>
                </tr>
				<tr>
                    <td ><label> 31)Incorrect disposition on CRM </td>
                    <td style="padding-left:8px;">
						 <input type="radio" name="qn31" name="qn31" value="1" onclick="return empty(7)">Yes
                    </td>
					<td width="180" class="tbl"><label>
          <textarea name="c31" id="c31"></textarea>
        </label></td>
                </tr>
				</table>
				</div>
			<?php 
				if($row_airtel_reports['coached']=='Yes' && $row_airtel_reports['coachfile']!='')
				{
				?>
				<tr>
                    <td><label>Coach File:<?php echo '<a href="../../uploads/'.$row_airtel_reports['coachfile'].'" target="_blank">'.$row_airtel_reports['coachfile'].'</a>' ; ?></label></td>
                  
                </tr>
				
          <?php }
		  
		  else{
		  ?>
		  
		  <tr>
                    <td><label> Coached?:</label></td>
                    <td style="padding-left:8px;">
						 <input type="radio" name="cn" id="cn1" value="Yes" >Yes
						  <input type="radio" name="cn" id="cn2" value="No" >No
                    </td>
					
					<td width="180" class="tbl"><label>
       
        </label></td>
                </tr>
				<br/>
					<tr>
                    <td id="coaching"><label id="piulabel">Upload File:</label></td>
                    <td style="padding-left:8px;">
						 <input type="file" name="uploaded_file" id="picupload" >
                 </td>
                </tr>
          <?php 
		  }
		 
		  
		  ?>
          
			
        </div>
		<input type="submit" name="submit" value="submit form" style=" padding:0px 20px;
	background:url(images/fbut-bg.png) repeat-x;
	height:37px;
	margin-left:50px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	text-shadow: 0.1em 0.1em #fdbd59;
	font-weight:bold;" />
			
    </div>


<script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript">
  function calcscore(){
    var score =0;
    $(".calc:checked").each(function(){
        score+=parseInt($(this).val(),10);
    });
    $("input[name=sum]").val(score)

}
$().ready(function(){
    $(".calc").change(function(){
        calcscore()
    }
	
	);
	
	//q1
	$('.hover-star').click(function() {
   // alert($(this).val());
	
	document.getElementById('s1').value=($(this).val());
});
//q2

$('.cq2').click(function() {
   // alert($(this).val());
	
	document.getElementById('s2').value=($(this).val());
});

//q3
$('.cq3').click(function() {
   // alert($(this).val());
	
	document.getElementById('s3').value=($(this).val());
});

//q4
$('.cq4').click(function() {
   // alert($(this).val());
	
	document.getElementById('s4').value=($(this).val());
});

//q5
$('.cq5').click(function() {
   // alert($(this).val());
	
	document.getElementById('s5').value=($(this).val());
});
//q6
$('.cq6').click(function() {
   // alert($(this).val());
	
	document.getElementById('s6').value=($(this).val());
});
//q7
$('.cq7').click(function() {
   // alert($(this).val());
	
	document.getElementById('s7').value=($(this).val());
});
//q8
$('.cq8').click(function() {
   // alert($(this).val());
	
	document.getElementById('s8').value=($(this).val());
});
//q9
$('.cq9').click(function() {
   // alert($(this).val());
	
	document.getElementById('s9').value=($(this).val());
});
//q10
$('.cq10').click(function() {
   // alert($(this).val());
	
	document.getElementById('s10').value=($(this).val());
});
//q11
$('.cq11').click(function() {
   // alert($(this).val());
	
	document.getElementById('s11').value=($(this).val());
});
//q12
$('.cq12').click(function() {
   // alert($(this).val());
	
	document.getElementById('s12').value=($(this).val());
});
//q13
$('.cq13').click(function() {
   // alert($(this).val());
	
	document.getElementById('s13').value=($(this).val());
});
//q14
$('.cq14').click(function() {
   // alert($(this).val());
	
	document.getElementById('s14').value=($(this).val());
});
//q15
$('.cq15').click(function() {
   // alert($(this).val());
	
	document.getElementById('s15').value=($(this).val());
});
//q16
$('.cq16').click(function() {
   // alert($(this).val());
	
	document.getElementById('s16').value=($(this).val());
});
//q17
$('.cq17').click(function() {
   // alert($(this).val());
	
	document.getElementById('s17').value=($(this).val());
});
//q18
$('.cq18').click(function() {
   // alert($(this).val());
	
	document.getElementById('s18').value=($(this).val());
});
//q19
$('.cq19').click(function() {
   // alert($(this).val());
	
	document.getElementById('s19').value=($(this).val());
});
//q20
$('.cq20').click(function() {
   // alert($(this).val());
	
	document.getElementById('s20').value=($(this).val());
});
//q21
$('.cq21').click(function() {
   // alert($(this).val());
	
	document.getElementById('s21').value=($(this).val());
});
//q22
$('.cq22').click(function() {
   // alert($(this).val());
	
	document.getElementById('s22').value=($(this).val());
});
//q23
$('.cq23').click(function() {
   // alert($(this).val());
	
	document.getElementById('s23').value=($(this).val());
});

//q24
$('.cq24').click(function() {
   // alert($(this).val());
	
	document.getElementById('s24').value=($(this).val());
});



});

/*function showTable(which) {
if (which ==1) {
document.getElementById("q16_b").checked=true;
}
else if(which ==2)
{
	document.getElementById("q16_b").checked=false;
}
else if(which ==3)
{
	document.getElementById("q16_b").checked=false;
}
else if(which ==4)
{
	document.getElementById("q16_b").checked=false;
}

}*/

//Table2
function empty(which) {
if (which ==1 || which ==2 || which ==3 || which ==4 || which ==5 || which ==6 ||which ==7) {
document.getElementById("q1_a").unchecked=true;
document.getElementById("q1_a").disabled=true;
document.getElementById("q1_b").unchecked=true;
document.getElementById("q1_b").disabled=true;
document.getElementById("q2_a").unchecked=true;
document.getElementById("q2_a").disabled=true;
document.getElementById("q2_b").unchecked=true;
document.getElementById("q2_b").disabled=true;
document.getElementById("q3_a").unchecked=true;
document.getElementById("q3_a").disabled=true;
document.getElementById("q3_b").unchecked=true;
document.getElementById("q3_b").disabled=true;
document.getElementById("q3_c").unchecked=true;
document.getElementById("q3_c").disabled=true;
document.getElementById("q4_a").unchecked=true;
document.getElementById("q4_a").disabled=true;
document.getElementById("q4_b").unchecked=true;
document.getElementById("q4_b").disabled=true;
document.getElementById("q4_c").unchecked=true;
document.getElementById("q4_c").disabled=true;
document.getElementById("q5_a").unchecked=true;
document.getElementById("q5_a").disabled=true;
document.getElementById("q5_b").unchecked=true;
document.getElementById("q5_b").disabled=true;
document.getElementById("q5_c").unchecked=true;
document.getElementById("q5_c").disabled=true;
document.getElementById("q6_a").unchecked=true;
document.getElementById("q6_a").disabled=true;
document.getElementById("q6_b").unchecked=true;
document.getElementById("q6_b").disabled=true;
document.getElementById("q6_c").unchecked=true;
document.getElementById("q6_c").disabled=true;
document.getElementById("q7_a").unchecked=true;
document.getElementById("q7_a").disabled=true;
document.getElementById("q7_b").unchecked=true;
document.getElementById("q7_b").disabled=true;
document.getElementById("q7_c").unchecked=true;
document.getElementById("q7_c").disabled=true;
document.getElementById("q8_a").unchecked=true;
document.getElementById("q8_a").disabled=true;
document.getElementById("q8_b").unchecked=true;
document.getElementById("q8_b").disabled=true;
document.getElementById("q8_c").unchecked=true;
document.getElementById("q8_c").disabled=true;
document.getElementById("q9_a").unchecked=true;
document.getElementById("q9_a").disabled=true;
document.getElementById("q9_b").unchecked=true;
document.getElementById("q9_b").disabled=true;
document.getElementById("q10_a").unchecked=true;
document.getElementById("q10_a").disabled=true;
document.getElementById("q10_b").unchecked=true;
document.getElementById("q10_b").disabled=true;
document.getElementById("q11_a").unchecked=true;
document.getElementById("q11_a").disabled=true;
document.getElementById("q11_b").unchecked=true;
document.getElementById("q11_b").disabled=true;
document.getElementById("q12_a").unchecked=true;
document.getElementById("q12_a").disabled=true;
document.getElementById("q12_b").unchecked=true;
document.getElementById("q12_b").disabled=true;
document.getElementById("q13_a").unchecked=true;
document.getElementById("q13_a").disabled=true;
document.getElementById("q13_b").unchecked=true;
document.getElementById("q13_b").disabled=true;
document.getElementById("q14_a").unchecked=true;
document.getElementById("q14_a").disabled=true;
document.getElementById("q14_b").unchecked=true;
document.getElementById("q14_b").disabled=true;
document.getElementById("q14_a").unchecked=true;
document.getElementById("q15_a").disabled=true;
document.getElementById("q15_b").unchecked=true;
document.getElementById("q15_b").disabled=true;
document.getElementById("q15_a").unchecked=true;
document.getElementById("q15_a").disabled=true;
document.getElementById("q15_b").unchecked=true;
document.getElementById("q15_b").disabled=true;
document.getElementById("q16_a").unchecked=true;
document.getElementById("q16_a").disabled=true;
document.getElementById("q16_b").unchecked=true;
document.getElementById("q16_b").disabled=true;
document.getElementById("q17_a").unchecked=true;
document.getElementById("q17_a").disabled=true;
document.getElementById("q17_b").unchecked=true;
document.getElementById("q17_b").disabled=true;
document.getElementById("q18_a").unchecked=true;
document.getElementById("q18_a").disabled=true;
document.getElementById("q18_b").unchecked=true;
document.getElementById("q18_b").disabled=true;
document.getElementById("q19_a").unchecked=true;
document.getElementById("q19_a").disabled=true;
document.getElementById("q19_b").unchecked=true;
document.getElementById("q19_b").disabled=true;
document.getElementById("q19_c").unchecked=true;
document.getElementById("q19_c").disabled=true;
document.getElementById("q20_a").unchecked=true;
document.getElementById("q20_a").disabled=true;
document.getElementById("q20_b").unchecked=true;
document.getElementById("q20_b").disabled=true;
document.getElementById("q20_c").unchecked=true;
document.getElementById("q20_c").disabled=true;
document.getElementById("q20_a").unchecked=true;
document.getElementById("q21_a").disabled=true;
document.getElementById("q21_b").unchecked=true;
document.getElementById("q21_b").disabled=true;
document.getElementById("q21_c").unchecked=true;
document.getElementById("q21_c").disabled=true;
document.getElementById("q22_a").unchecked=true;
document.getElementById("q22_a").disabled=true;
document.getElementById("q22_b").unchecked=true;
document.getElementById("q22_b").disabled=true;
document.getElementById("q22_c").unchecked=true;
document.getElementById("q22_c").disabled=true;
document.getElementById("q23_a").unchecked=true;
document.getElementById("q23_a").disabled=true;
document.getElementById("q23_b").unchecked=true;
document.getElementById("q23_b").disabled=true;
document.getElementById("q23_c").unchecked=true;
document.getElementById("q23_c").disabled=true;
document.getElementById("q24_a").unchecked=true;
document.getElementById("q24_a").disabled=true;
document.getElementById("q24_b").unchecked=true;
document.getElementById("q24_b").disabled=true;
document.getElementById("q24_c").unchecked=true;
document.getElementById("q24_c").disabled=true;
document.getElementById("s1").value=0;
document.getElementById("s2").value=0;
document.getElementById("s3").value=0;
document.getElementById("s4").value=0;
document.getElementById("s5").value=0;
document.getElementById("s6").value=0;
document.getElementById("s7").value=0;
document.getElementById("s8").value=0;
document.getElementById("s9").value=0;
document.getElementById("s10").value=0;
document.getElementById("s11").value=0;
document.getElementById("s12").value=0;
document.getElementById("s13").value=0;
document.getElementById("s14").value=0;
document.getElementById("s15").value=0;
document.getElementById("s16").value=0;
document.getElementById("s17").value=0;
document.getElementById("s18").value=0;
document.getElementById("s19").value=0;
document.getElementById("s20").value=0;
document.getElementById("s21").value=0;
document.getElementById("s22").value=0;
document.getElementById("s23").value=0;
document.getElementById("s24").value=0;
//document.getElementById("q17_b").disabled=true;
//document.getElementById("q17_c").disabled=true;

//document.getElementById("q17v").value=10;
//document.getElementById("q18v").value=6;
//document.getElementById("q19v").value=4;
//document.getElementById("q20v").value=2;
//document.getElementById("q21v").value=2;


//document.getElementById("q18_d").checked=true;
//document.getElementById("q18_a").disabled=true;
//document.getElementById("q18_b").disabled=true;
//document.getElementById("q18_c").disabled=true;

//document.getElementById("q19_d").checked=true;
//document.getElementById("q19_a").disabled=true;
//document.getElementById("q19_b").disabled=true;

//document.getElementById("q20_d").checked=true;
//document.getElementById("q20_a").disabled=true;
//document.getElementById("q20_b").disabled=true;


//document.getElementById("q21_d").checked=true;
//document.getElementById("q21_a").disabled=true;
//document.getElementById("q21_b").disabled=true;

}
else 
{

}
}


//Function to output values
  </script>
  <script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
  <script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script></div>
		</body>
		</html>