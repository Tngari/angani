<?php
//Start session
include("../include/config.php");
error_reporting(1);
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../../index.php");
    exit();
}
if ($_SESSION['level'] == "Admin" || $_SESSION['level'] == "Supervisor") {
    $fromdt = $_GET["fromdate"];
    $todt = $_GET["todate"];

    $month = date('m', strtotime($fromdt));
    
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>::ZUKU PAYMENT REMINDER::</title>
            <link href="../css/style.css" rel="stylesheet" type="text/css" />
            <link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
            <link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
            <script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
            <link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
            <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
            <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
            <script type="text/javascript" src="../js/table2CSV.js" ></script>
            <script type="text/javascript">
                $(function () {
                    $('#fromdt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('#todt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });
                });


            </script>
        </head>

        <body>
            <div class="wrapper">

                <div class="header">



                    <div class="logo">
                        <a href="index.php"><img src="../images/logo.png" alt="" height="67" border="0" />	</a> 
                    </div>

                    <div class="">

                        <?php include('admin_nav.php'); ?>

                    </div>

                </div>
                <div class="midnav" style="width:2590px">



                    <span>Reports</span>
                    <span style="float:right"><a href="../logout.php"> Logout</a></span>
                    <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>

                </div>
                <div class="container-fluid" style="background-color:#FFF;	width:2600px;
                     min-height:800px;
                     margin-left:0px auto 0px auto;
                     padding:0px;
                     -webkit-border-top-left-radius: 3px;
                     -webkit-border-top-right-radius: 3px;
                     -moz-border-radius-topleft: 3px;
                     -moz-border-radius-topright: 3px;
                     border-top-left-radius: 3px;
                     border-top-right-radius: 3px;
                     box-shadow:  0px 1px 1px #000;
                     -moz-box-shadow: 0px 1px 1px #000;
                     -webkit-box-shadow: 0px 1px 1px #000;
                     box-shadow: 0px 8px 18px #1c1c1c;
                     -moz-box-shadow: 0px 8px 18px #1c1c1c;
                     -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
                    <div class="captionWrapper">
                        <ul>
                            <li><a href="index.php"><h2 class="curr">CSV Reports</h2></a></li>



                        </ul>
                    </div>
                    <div class="formCon" style="float:center; width:40%; margin-left:10px;margin-right:10px;padding:10px" >
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                                <form id="form1" name="form1" method="get" action="index.php">




                                    <tr>
                                        <td >From:</td>
                                        <td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr><tr><td >To:</td>
                                        <td ><input name='todate' type='text'  id="todt" /></td>
                                    </tr>	<tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <td>&nbsp;</td><td ><label>
                                            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
                                                   background-color:#F27F22;
                                                   height:25px;
                                                   -webkit-border-radius: 4px;
                                                   -moz-border-radius: 4px;
                                                   border-radius: 4px;
                                                   border:1px #b58530 solid;
                                                   color:#633c15;
                                                   font-size:15px;
                                                   cursor:pointer;

                                                   font-weight:bold;"/>
                                        </label> </td></form>



                                <td>
                                    <form action="getCSV.php" method ="post" > <label>
                                            <input type="hidden" name="csv_text" id="csv_text">
                                                <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
                                                       background-color:#F27F22;
                                                       height:25px;
                                                       -webkit-border-radius: 4px;
                                                       -moz-border-radius: 4px;
                                                       border-radius: 4px;
                                                       border:1px #b58530 solid;
                                                       color:#633c15;
                                                       font-size:15px;
                                                       cursor:pointer;
                                                       font-weight:bold;"/>
                                        </label> 	
                                    </form>
                                    <script>
                                        function getCSVData() {
                                            var csv_value = $('#csvdownload').table2CSV({delivery: 'value'});
                                            $("#csv_text").val(csv_value);
                                        }
                                    </script>
                                </td>

                            </tr>
                        </table>

                    </div>

                    <div class="" >

                        <div class="clear"></div>


                        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
                            <div class="pagecon" style="float:center; margin-left:10px;">

                            </div>     
                            <div id="files">									  
                                <table width="100%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
                                    <tr class="tablebx_topbg">
                                        <td class="tblRB">#</td>
                                        <td class="tblRB">Subs</td>
                                        <td class="tblRB">Name</td>
                                         <td class="tblRB">Address</td>
                                        <td class="tblRB">Phone Number</td>
                                                                                                                      
                                        <td class="tblRB">Alternative No1</td>
                                        
                                        <td class="tblRB">Alternative No2</td>
                                        <td class="tblRB">Alternative No3</td>
                                        <td class="tblRB">City</td>
                                        <td class="tblRB">Previous Package</td>
                                        <td class="tblRB">Previous Package Speed</td>
                                        <td class="tblRB">Current Package</td>
                                        <td class="tblRB">Current Package Speed</td>
                                        <td class="tblRB">Pay By</td>
                                        <td class="tblRB">Cycle</td>
                                        <td class="tblRB">Amount Due</td>
                                        <td class="tblRB">Amount In</td>
                                        <td class="tblRB">Amount To Pay</td>
                                          <td class="tblRB">Loyalty Points</td>
                                        <td class="tblRB">Enumerator</td>
                                        <td class="tblRB">Call Status</td>
                                         <td class="tblRB">Dialled Number</td>
                                        <td class="tblRB">Hello, my name is ..... calling from Zuku, We are calling to update you a benefit on your Zuku account.

                                           Shall I continue? </td>
                                         <td class="tblRB">When would it be best for me to call you back?</td>
                                        
                                        <td class="tblRB">Please indicate a tentative date to call you back</td>
                                        <td class="tblRB">Please indicate a tentative  Time to call you back</td>

                                       <td class="tblRB">Q4.I am happy to inform you that you have accrued __________ loyalty points. You earn 1-point every time you pay before disconnection. Every 3 points are redeemed for 1-month speed upgrade.</td>
                                       
                                       <td class="tblRB">Q5(a). By the Way, your account is due for payment tomorrow, kindly make your payment by today to avoid disconnection </td>
                                        <td class="tblRB">Q5(b). Tentative Date of Payment</td>
                                        <td class="tblRB">Q5(c). Capture any other issue raised (for not sure to pay) </td> 
                                        
                                         <td class="tblRB">Q5(d). Kindly let me know the reason that you are not going to pay and continue the zuku services 
                                                              (Service Issue)</td> 
                                         <td class="tblRB">Service Issue Other Reason</td>
                                         
                                        <td class="tblRB">Q5(d). Content issue</td>
                                        <td class="tblRB">Content Issue Other Reason</td>
                                        
                                        
                                         <td class="tblRB">Q5(d). Competitor/dealer</td>
                                         <td class="tblRB">Competitor/Dealer Issue Other Reason</td>
                                         
                                         <td class="tblRB">Q5(d). Travel</td>
                                         
                                         <td class="tblRB">Q5(d). Temp Disconnect</td>
                                         
                                         <td class="tblRB">Q5(d). Transfer of Ownership</td>
                                         
                                         <td class="tblRB">Q5(d). Not Sure</td>
                                         
                                          <td class="tblRB">Q5(d). Others</td>
                                         
                                                
                                        
                                        <td class="tblRB"> Capture any other reasons provided (for not paying)</td>
                                              
                                         <td class="tblRB">Escalations</td>
                                         
                                        <td>Call Back Date & Time</td>
                                        <td>Survey Disposition</td>
                                          
                                        <td>Disposition Comments</td>
                                        <td>Date</td>

                                      
                                    </tr>

                                    <?php
                                    $sel = dbConnect()->prepare("SELECT * FROM survey INNER JOIN leads ON survey.lid=leads.id WHERE survey.id IN (SELECT MAX(id) FROM survey WHERE date(date) between '" . $fromdt . "' AND '" . $todt . "' AND disposition !='' GROUP BY phone) ");
									
                                    $sel->execute();

                                    $t = 0;
                                    while ($row = $sel->fetch(PDO::FETCH_ASSOC)) {
                                        $t += 1;
                                        ?>
                                        <tr class=<?php echo $cls; ?>>
                                            <td class="tblR"><?php echo $t; ?></td>

                                            <td class="tblR"><?php echo $row['subs']; ?></td>
                                             <td class="tblR"><?php echo $row['name']; ?></td>
                                             <td class="tblR"><?php echo $row['address']; ?></td>
                                                   <td class="tblR"><?php echo $row['phone']; ?></td>
                                
                                            <td class="tblR"><?php echo $row['phone1']; ?></td>
                                            <td class="tblR"><?php echo $row['phone2']; ?></td>
                                            <td class="tblR"><?php echo $row['phone3']; ?></td>
                                            <td class="tblR"><?php echo $row['city']; ?></td>
                                            <td class="tblR"><?php echo $row['previous_package']; ?></td>
                                            <td class="tblR"><?php echo $row['previous_package_speed']; ?></td>
                                            <td class="tblR"><?php echo $row['current_package']; ?></td>
                                             <td class="tblR"><?php echo $row['current_package_speed']; ?></td>
                                             <td class="tblR"><?php echo $row['payment_date']; ?></td>
                                             <td class="tblR"><?php echo $row['cycle']; ?></td>
                                             <td class="tblR"><?php echo $row['amount_due']; ?></td>
                                             <td class="tblR"><?php echo $row['amount_in']; ?></td>
                                             <td class="tblR"><?php echo $row['amount_to_pay']; ?></td>
                                             <td class="tblR"><?php echo $row['loyalty_points']; ?></td>
                                            <td class="tblR"><a href=""><?php echo $row['interviewer']; ?></a></td>
                                            <td class="tblR"><?php
                                                $cs = $row['cs'];
                                                if ($cs == 1) {
                                                    echo "Reachable";
                                                }if ($cs == 2) {
                                                    echo "Unreachable";
                                                }
                                                ?></td>
                                          <td class="tblR"><?php echo $row['dialledno']; ?></td>
                                            <td class="tblR"><?php echo $row['Q1']; ?></td>
                                               <td class="tblR"><?php echo $row['Q2']; ?></td>
                                           
                                            <td class="tblR"><?php echo $row['Q3']; ?></td>
                                            <td class="tblR"><?php echo $row['Q4']; ?></td>



                                          <td class="tblR"><?php echo $row['Q4_comments']; ?></td>
                                         
                                              <td class="tblR"><?php echo $row['Q5_a']; ?></td>
                                            <td class="tblR"><?php echo $row['Q5_b']; ?></td>
                                            <td class="tblR"><?php echo $row['Q5_c']; ?></td>

                                                                                       
                                             <td><?php
                                                $topic = explode(',', $row['Q5_d_1']);
                                                if (in_array('1', $topic)) {
                                                    echo '1,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('2', $topic)) {
                                                    echo '2,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('3', $topic)) {
                                                    echo '3,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('4', $topic)) {
                                                    echo '4,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('5', $topic)) {
                                                    echo '5,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('6', $topic)) {
                                                    echo '6,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('7', $topic)) {
                                                    echo '7,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('8', $topic)) {
                                                    echo '8,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('9', $topic)) {
                                                    echo '9';
                                                }
                                                ?></td>
                                             <td class="tblR"><?php echo $row['service_issue_other_reason']; ?></td>
                                             
                                             
                                             <td><?php
                                                $topic = explode(',', $row['Q5_d_2']);
                                                if (in_array('1', $topic)) {
                                                    echo '1,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('2', $topic)) {
                                                    echo '2,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('3', $topic)) {
                                                    echo '3,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('4', $topic)) {
                                                    echo '4,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('5', $topic)) {
                                                    echo '5,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('6', $topic)) {
                                                    echo '6';
                                                }
                                                ?></td>
                                             
                                             <td class="tblR"><?php echo $row['content_issue_other_reason']; ?></td>
                                             
                                            
                                             <td><?php
                                                $topic = explode(',', $row['Q5_d_3']);
                                                if (in_array('1', $topic)) {
                                                    echo '1,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('2', $topic)) {
                                                    echo '2,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('3', $topic)) {
                                                    echo '3,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('4', $topic)) {
                                                    echo '4,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('5', $topic)) {
                                                    echo '5,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('6', $topic)) {
                                                    echo '6,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('7', $topic)) {
                                                    echo '7,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('8', $topic)) {
                                                    echo '8,';
                                                }
                                                ?>
                                            <?php
                                                if (in_array('9', $topic)) {
                                                    echo '9,';
                                                }
                                                ?>
                                             
                                               <?php
                                                if (in_array('10', $topic)) {
                                                    echo '10,';
                                                }
                                                ?>
                                             
                                               <?php
                                                if (in_array('11', $topic)) {
                                                    echo '11,';
                                                }
                                                ?>
                                             
                                               <?php
                                                if (in_array('12', $topic)) {
                                                    echo '12,';
                                                }
                                                ?>
                                             
                                               <?php
                                                if (in_array('13', $topic)) {
                                                    echo '13,';
                                                }
                                                ?>
                                               <?php
                                                if (in_array('14', $topic)) {
                                                    echo '14,';
                                                }
                                                ?>
                                               <?php
                                                if (in_array('15', $topic)) {
                                                    echo '15';
                                                }
                                                ?></td>
                                             
                                             
                                             <td class="tblR"><?php echo $row['competitor_other_reason']; ?></td>
                                             
                                             <td><?php
                                                $topic = explode(',', $row['Q5_d']);
                                                if (in_array('Travel', $topic)) {
                                                    echo 'Travel';
                                                }
                                                ?></td>
                                            <td><?php
                                                if (in_array('Temp Disconnect', $topic)) {
                                                    echo 'Temp Disconnect';
                                                }
                                                ?></td>
                                            <td><?php
                                                if (in_array('Transfer of Ownership', $topic)) {
                                                    echo 'Transfer of Ownership';
                                                }
                                                ?></td>
                                            <td><?php
                                                if (in_array('Not Sure', $topic)) {
                                                    echo 'Not Sure';
                                                }
                                                ?></td>
                                            <td><?php
                                                if (in_array('Others', $topic)) {
                                                    echo 'Others';
                                                }
                                                ?></td>
                                            
                                      
                                             
                                            <td class="tblR"><?php echo $row['Q5_e']; ?></td>
                                           
                                            <td class="tblR"><?php echo $row['escalation']; ?></td>

                                            <!--END OF SMS PART REPORT -->
                                            <td><?php echo $row['callbacktime']; ?></td>
                                            <td><?php echo $row['disposition']; ?></td>
                                           
                                            <td ><?php echo $row['disptype']; ?></td>
                                            <td><?php echo $row['date']; ?></td>
                                            <!-- <td><?php
                                            $cs = $row['cs'];
                                            if ($cs == 2) {
                                                echo $red;
                                            }
                                            ?></td>-->
                                           
                                        </tr>
                                    <?php }
                                    ?>

                                </table>
                            </div>
                            <div class="pagecon">

                            </div>
                            <div class="clear"></div>
                        </div>



                    </div>

                </div>
                </td>
                </tr>
                <?php // echo pagination($statement,$per_page,$page,$url='?');   ?>   
            </div>
            </div>

            </div>

            <!-- end .content -->
        </body>
    </html>

    <?php
} else {
    echo "You are not Authorized to access this page.Please get out of here!";
}
?>
