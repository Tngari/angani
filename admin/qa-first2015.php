<?php
include("../include/config.php");

session_start();
 error_reporting(0);
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../login.php");
exit();

}

$qadate = $_GET['qadate'];
 $real = $_GET['real'];
$lid = $_GET['lid'];
$qaid = $_GET['id'];
$query_getbank = dbConnect()->prepare("SELECT * FROM  qc_data WHERE  qc_data.id = '".$qaid."'");
$query_getbank->execute();
$row_getbank=$query_getbank->fetch();

$query_getbank_agent= dbConnect()->prepare("SELECT * FROM  survey WHERE survey.lid = '".$lid."'");
$query_getbank_agent->execute();
$row_getbank_agent=$query_getbank_agent->fetch();
if($real=='agent')
{
$sql_agent="UPDATE qc_data SET 
cs=:cs,
Q1=:q1,
Q2=:q2,
Q3=:q3,
Q4=:q4,
Q5=:q5,
Q5a=:q5a,
Q5o=:q5o,
Q6=:q6,
Q7=:q7,
Q8=:q8,
Q9=:q9o,
Q9a=:q9,
other1=:other1,
other2=:other2,
Q10=:q10,
Q11=:q11,
Q12=:q12,
Q13=:q13,
Q14=:q14,
Q15=:q15,
Q16=:q16,
Q17=:q17,
Q18=:q18,
Q18a=:q18a,
other_1=:other_1,
other_2=:other_2,
Q19=:q19,
Q20=:q20,
Q21=:q21,
Q22=:q22,
Q22a=:q22a,
Q22b=:q22b,
Q22c=:q22c,
Q23=:q23,
Q24=:q24,
Q25=:q25,
Q26=:q26,
Q27=:q27,
Q28=:q28,
Q29=:q29,
Q29a=:q29a,
Q29_other=:q29other,
Q30=:q30,
Q31=:q31,
Q32=:q32,
Q33=:q33,
Q34=:q34,
Q35=:q35,
Q36=:q36,
Q37=:q37,
Q38=:q38,
Q39=:q39,
Q40=:q40,
Q41=:q41,
Q42=:q42,
Q43=:q43,
Q44=:q44,
Q45=:q45,
Q46=:q46,
Q47=:q47,
Q48=:q48,
Q49=:q49,
Q50=:q50,
disposition=:disp,
disptype=:dispt
WHERE id=:id";
$stmta= dbConnect()->prepare($sql_agent);                                 
$stmta->bindParam(':cs',$row_getbank_agent['cs'], PDO::PARAM_STR);
$stmta->bindParam(':q1',$row_getbank_agent['Q1'], PDO::PARAM_STR);
$stmta->bindParam(':q2',$row_getbank_agent['Q2'], PDO::PARAM_STR);
$stmta->bindParam(':q3',$row_getbank_agent['Q3'], PDO::PARAM_STR);
$stmta->bindParam(':q4',$row_getbank_agent['Q4'], PDO::PARAM_STR);
$stmta->bindParam(':q5',$row_getbank_agent['Q5'], PDO::PARAM_STR);
$stmta->bindParam(':q5a',$row_getbank_agent['Q5a'], PDO::PARAM_STR);
$stmta->bindParam(':q5o',$row_getbank_agent['Q5o'], PDO::PARAM_STR);
$stmta->bindParam(':q6',$row_getbank_agent['Q6'], PDO::PARAM_STR);
$stmta->bindParam(':q7',$row_getbank_agent['Q7'], PDO::PARAM_STR);
$stmta->bindParam(':q8',$row_getbank_agent['Q8'], PDO::PARAM_STR);
$stmta->bindParam(':q9o',$row_getbank_agent['Q9'], PDO::PARAM_STR);
$stmta->bindParam(':q9',$row_getbank_agent['Q9a'], PDO::PARAM_STR);
$stmta->bindParam(':other1',$row_getbank_agent['other1'], PDO::PARAM_STR);
$stmta->bindParam(':other2',$row_getbank_agent['other2'], PDO::PARAM_STR);
$stmta->bindParam(':q10',$row_getbank_agent['Q10'], PDO::PARAM_STR);
$stmta->bindParam(':q11',$row_getbank_agent['Q11'], PDO::PARAM_STR);
$stmta->bindParam(':q12',$row_getbank_agent['Q12'], PDO::PARAM_STR);
$stmta->bindParam(':q13',$row_getbank_agent['Q13'], PDO::PARAM_STR);
$stmta->bindParam(':q14',$row_getbank_agent['Q14'], PDO::PARAM_STR);
$stmta->bindParam(':q15',$row_getbank_agent['Q15'], PDO::PARAM_STR);
$stmta->bindParam(':q16',$row_getbank_agent['Q16'], PDO::PARAM_STR);
$stmta->bindParam(':q17',$row_getbank_agent['Q17'], PDO::PARAM_STR);
$stmta->bindParam(':q18',$row_getbank_agent['Q18'], PDO::PARAM_STR);
$stmta->bindParam(':q18a',$row_getbank_agent['Q18a'], PDO::PARAM_STR);
$stmta->bindParam(':other_1',$row_getbank_agent['other_1'], PDO::PARAM_STR);
$stmta->bindParam(':other_2',$row_getbank_agent['other_2'], PDO::PARAM_STR);
$stmta->bindParam(':q19',$row_getbank_agent['Q19'], PDO::PARAM_STR);
$stmta->bindParam(':q20',$row_getbank_agent['Q20'], PDO::PARAM_STR);
$stmta->bindParam(':q21',$row_getbank_agent['Q21'], PDO::PARAM_STR);
$stmta->bindParam(':q22',$row_getbank_agent['Q22'], PDO::PARAM_STR);
$stmta->bindParam(':q22a',$row_getbank_agent['Q22a'], PDO::PARAM_STR);
$stmta->bindParam(':q22b',$row_getbank_agent['Q22b'], PDO::PARAM_STR);
$stmta->bindParam(':q22c',$row_getbank_agent['Q22c'], PDO::PARAM_STR);
$stmta->bindParam(':q23',$row_getbank_agent['Q23'], PDO::PARAM_STR);
$stmta->bindParam(':q24',$row_getbank_agent['Q24'], PDO::PARAM_STR);
$stmta->bindParam(':q25',$row_getbank_agent['Q25'], PDO::PARAM_STR);
$stmta->bindParam(':q26',$row_getbank_agent['Q26'], PDO::PARAM_STR);
$stmta->bindParam(':q27',$row_getbank_agent['Q27'], PDO::PARAM_STR);
$stmta->bindParam(':q28',$row_getbank_agent['Q28'], PDO::PARAM_STR);
$stmta->bindParam(':q29',$row_getbank_agent['Q29'], PDO::PARAM_STR);
$stmta->bindParam(':q29a',$row_getbank_agent['Q29a'], PDO::PARAM_STR);
$stmta->bindParam(':q29other',$row_getbank_agent['Q29_other'], PDO::PARAM_STR);
$stmta->bindParam(':q30',$row_getbank_agent['Q30'], PDO::PARAM_STR);
$stmta->bindParam(':q31',$row_getbank_agent['Q31'], PDO::PARAM_STR);
$stmta->bindParam(':q32',$row_getbank_agent['Q32'], PDO::PARAM_STR);
$stmta->bindParam(':q33',$row_getbank_agent['Q33'], PDO::PARAM_STR);
$stmta->bindParam(':q34',$row_getbank_agent['Q34'], PDO::PARAM_STR);
$stmta->bindParam(':q35',$row_getbank_agent['Q35'], PDO::PARAM_STR);
$stmta->bindParam(':q36',$row_getbank_agent['Q36'], PDO::PARAM_STR);
$stmta->bindParam(':q37',$row_getbank_agent['Q37'], PDO::PARAM_STR);
$stmta->bindParam(':q38',$row_getbank_agent['Q38'], PDO::PARAM_STR);
$stmta->bindParam(':q39',$row_getbank_agent['Q39'], PDO::PARAM_STR);
$stmta->bindParam(':q40',$row_getbank_agent['Q40'], PDO::PARAM_STR);
$stmta->bindParam(':q41',$row_getbank_agent['Q41'], PDO::PARAM_STR);
$stmta->bindParam(':q42',$row_getbank_agent['Q42'], PDO::PARAM_STR);
$stmta->bindParam(':q43',$row_getbank_agent['Q43'], PDO::PARAM_STR);
$stmta->bindParam(':q44',$row_getbank_agent['Q44'], PDO::PARAM_STR);
$stmta->bindParam(':q45',$row_getbank_agent['Q45'], PDO::PARAM_STR);
$stmta->bindParam(':q46',$row_getbank_agent['Q46'], PDO::PARAM_STR);
$stmta->bindParam(':q47',$row_getbank_agent['Q47'], PDO::PARAM_STR);
$stmta->bindParam(':q48',$row_getbank_agent['Q48'], PDO::PARAM_STR);
$stmta->bindParam(':q49',$row_getbank_agent['Q49'], PDO::PARAM_STR);
$stmta->bindParam(':q50',$row_getbank_agent['Q50'], PDO::PARAM_STR);
$stmta->bindParam(':disp',$row_getbank_agent['disposition'], PDO::PARAM_STR);
$stmta->bindParam(':dispt',$row_getbank_agent['disptype'], PDO::PARAM_STR);
$stmta->bindParam(':id',$qaid, PDO::PARAM_STR);  
$stmta->execute(); 
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>::Ilogix QA Parameters SYSTEM::-EVALUATION FORM </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	
    <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
    <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
    <link rel="stylesheet" type="text/css" href="../css/formelements.css" />

<script src="http://code.jquery.com/jquery-latest.js"></script>
<style>
.tblBorder {
		border:#232b81 solid 1px;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	.tblBorder a {
	text-decoration:none;
	}
	.tblBorder a:link {
	text-decoration:none;
	}
	.tblBorder a:visited {
	text-decoration:none;
	}
	.tblBorder a:hover, a:active, a:focus {
	text-decoration:underline;
	}
	
	.tblBorder2 {
		border:#e67817 solid 1px;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblBorder2a {
		border:#e67817 solid 1px;
		text-align:left;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
		background-color:#f4cca8;
	}
	
	.tblBorder3 {
		border:#232b81 solid 1px;
		text-align:left;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblBorder3b {
		border:#232b81 solid 1px;
		background-color:#fdf3cb;
		color:#ff0000;
		text-align:left;
		font-size:14px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
		padding-bottom:5px;
		padding-top:5px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblShowing {
		text-align:center;
		font-size:15px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
	}
	
	.tblQO {
		background-color:#4d485c;
		color:#FFFFFF;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblQO2 {
		background-color:#e67817;
		color:#FFFFFF;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tbl1a {
		background-color:#fac090;
		font-weight:bold;
		text-align:right;
	}
	
	.tbl1aa {
		background-color:#fac090;
		font-weight:bold;
		text-align:center;
		padding:5px;
	}
	
	.tbl1b {
		background-color:#a15b21;
		color:#FFFFFF;
		font-weight:bold;
		text-align:right;
	}
	
	.tbl1bb {
		background-color:#a15b21;
		color:#FFFFFF;
		font-weight:bold;
		text-align:center;
		padding:5px;
	}
	
	.tbl2b {
		background-color:#f05454;
		text-align:left;
		color:#000000;
		font-weight:bold;
	}
	.tbl2a {
		background-color:#c42727;
		color:#FFFFFF;
		font-weight:bold;
		text-align:left;
	}
	
	.tbl3a {
		background-color:#ff9136;
		color:#000000;
		text-align:left;
		font-weight:bold;
	}
	.tbl3b {
		background-color:#cc5c00;
		color:#FFFFFF;
		text-align:left;
	}
	
	.tbl4b {
		background-color:#3e7804;
		color:#FFFFFF;
		text-align:center;
	}
	
	.tbl5b {
		background-color:#ff9136;
		color:#000000;
		text-align:center;
	}
</style>
</head>
<body>
<div class="wrapper">
 <div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/logo-plantwise.png" alt=""  border="0" />		</a> </div>
            
            
              <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav">
    
   
        <a class="first-letter"> Home</a>
		 <span>QA</span>
		   <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
     </div>

<div class="container formCon" >
 
 <form action="qadone-st2015.php" method="POST" enctype="multipart/form-data"  name="form" id="form"><br/>
    <div class="formCon2" >
        
		<h3>General Details</h3>
          <table align="center" cellpadding="10" cellspacing="5" width="100%">
    <tr>
    <td nowrap="nowrap" class="tbl1a">Questionnaire Number:</td>
    <td nowrap="nowrap" class="tbl2a"><?php echo 'QNR'.$_GET['id']; ?></td>
    <td nowrap="nowrap" class="tbl1a">Phone Number:</td>
    <td nowrap="nowrap" class="tbl2a"><?php echo $row_getbank_agent['phone']; ?></td>
    </tr>
    <tr>
    <td nowrap="nowrap" class="tbl1b">Process Name:</td>
    <td nowrap="nowrap" class="tbl2b">Ilogix</td>
   
    <td nowrap="nowrap" class="tbl1b">Auditor:</td>
    <td nowrap="nowrap" class="tbl2b"><?php echo $_SESSION['name']; ?></td>
    </tr>
    <tr>
    <td nowrap="nowrap" class="tbl1a">QA Date:</td>
    <td nowrap="nowrap" class="tbl2a"><?php echo date('Y-m-d'); ?></td>
    <td nowrap="nowrap" class="tbl1b">Transanction Date:</td>
    <td nowrap="nowrap" class="tbl2b"><?php echo date('Y-m-d', strtotime($row_getbank_agent['date'])); ?></td>
    
    </tr>
	</table>
        </div>
 
	
	
	<div class="formCon">
       
		<h2>Monitoring/Evaluation </h2>
		<br>
		
           <table align="center" width="100%" cellpadding="5">
<tr>
<td class="tbl1aa">Airtel QA Parameters</td>
<td class="tbl2a" nowrap="nowrap">Full Score</td>
<td class="tbl1aa" nowrap="nowrap">Achieved Score</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent greet and introduce him/herself clearly to the customer?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par1">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent represent Ilogix as a reseach company appropriately?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par2">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Was the agent able to convince the customer to take up the survey?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par3">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent mention the appriaporite reason for the call?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par4">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent use relevant questions to ask the customer's name and age?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par5">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent address the customer by their name during the entire conversation?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par6">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent use 'please' and 'thank you' appropriately during the conversation?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par7">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent articulate the questions properly?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par8">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent mirror the client's pace and tone?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par9">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Was the agent mono-toned?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par10">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Were the questions read out as displayed on the screen by the agent?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par11">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Were the anwers from the customer heard clearly and marked correctly?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par12">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent apologise and politely request for the client to repeat the responses when they were not clear or when the lines were bad?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par13">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent actively listen to the clients responses?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par14">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Were the questions rephrased when the customer did not understand the read out question?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par15">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Was the preferred language used thorughout the call?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par16">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent maintain professionalism throughout the call?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par17">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent get distracted during the conversation of the call?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par18">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent get the customer's attention to answer the questions when the customer got distracted?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par19">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent use effective rebuttals?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par20">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent build rapport with the client where applicable?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par21">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent sound confident?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par22">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>

<tr>
<td class="tblBorder3">Did the agent thank the customer before closing the call?</td>
<td class="tblBorder2">100</td>
<td class="tblBorder3">
<select name="par23">
<option value="100">100</option>
<option value="80">80</option>
<option value="60">60</option>
<option value="40">40</option>
<option value="20">20</option>
<option value="0">0</option>
</select>
</td>
</tr>


<tr><td colspan="3" align="center"><strong>Enter Comments Here</strong></td></tr>
<tr><td colspan="3" align="center"><textarea name="comments" cols="70" rows="5"></textarea></td></tr>
<tr><td colspan="3" align="center">
<?php
if($row_getbank['disposition']=='Complete Lead')
{
?>
<input type="hidden" name="qualitypassed" value="Yes" />	
<?php	
}
else
{
	?>
	
	<input type="hidden" name="qualitypassed" value="No" />
	<?php
}

?>
<input type="hidden" name="processor" value="<?php echo $row_getbank_agent['interviewer']; ?>" />
<input type="hidden" name="processor" value="<?php echo $row_getbank_agent['interviewer']; ?>" />
<input type="hidden" name="customername" value="<?php echo ucwords($row_getbank_agent['farmer']); ?>" />
<input type="hidden" name="phoneno" value="<?php echo $row_getbank_agent['phone']; ?>" />
<input type="hidden" name="supervisor" value="<?php echo $_SESSION['name']?>" />
<input type="hidden" name="auditor" value="<?php echo $_SESSION['name']; ?>" />
<input type="hidden" name="transactiondate" value="<?php echo date('Y-m-d', strtotime($row_getbank_agent['date'])); ?>" />
<input type="hidden" name="samaid" value="<?php echo $_GET['lid']; ?>" />
<input type="hidden" name="sid" value="<?php echo $row_getbank_agent['id']; ?>" />
<input type="hidden" name="qcid" value="<?php echo $_GET['id']; ?>" />
</td></tr>
<tr><td colspan="3" align="center">
<input type="hidden" id="action" name="action" value="submitform" />
<input type="hidden" id="qadate" name="qadate" value="<?php echo date("Y-m-d"); ?>" />
<input type="submit" name="submit" value="FINISH QA" style=" padding:0px 20px;
	background:url(../images/fbut-bg.png) repeat-x;
	height:37px;
	margin-left:50px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	text-shadow: 0.1em 0.1em #fdbd59;
	font-weight:bold;" />
</td></tr>
</table>
    
			
        </div>
	
    </div>


<script type="text/javascript" src="js/jquery.js"></script>
  
  <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
  <script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script></div>
		</body>
		</html>