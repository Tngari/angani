<?php
//Start session
include("../../include/config.php");
//error_reporting(0); 
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../../index.php");
exit();
}
/* first part*/
   

if(isset ($_POST['submit']))
{
	 $client=mysql_real_escape_string($_POST['client']);
    $project=mysql_real_escape_string($_POST['project']);
	$type=mysql_real_escape_string($_POST['type']);
	$pricing=mysql_real_escape_string($_POST['pricing']);
	$tpt=mysql_real_escape_string($_POST['tpt']);
$rate=mysql_real_escape_string($_POST['rate']);
$status="Active";
$leo=date('Y-m-d');
$sql="INSERT INTO projects(
            client,
            sub_project,
            billing_type,
            pricing,
            tpt_mins,
			casuals_rate,
			date_created,
			status) VALUES (
            :client,
            :project,
            :type,
            :price,
			:tpt,
			:rate,
			:leo,
            :state)";
                                          
$stmt= dbConnect()->prepare($sql);                                             
$stmt->bindParam(':client',$client, PDO::PARAM_STR);      
$stmt->bindParam(':project',$project, PDO::PARAM_STR);
$stmt->bindParam(':type',$type, PDO::PARAM_STR);
$stmt->bindParam(':price',$pricing, PDO::PARAM_STR);
$stmt->bindParam(':tpt',$tpt, PDO::PARAM_STR);  
$stmt->bindParam(':rate',$rate, PDO::PARAM_STR);                                        
$stmt->bindParam(':leo',$leo, PDO::PARAM_STR);  	
$stmt->bindParam(':state',$status, PDO::PARAM_STR);
$stmt->execute();

if($stmt==true)
{
 $data='Successfully added new project';
 header("Location:projects.php?msg=$data");
}
else
{
	echo "problem adding the Project";
}

}
elseif(isset ($_POST['update']))
{
	$id=mysql_real_escape_string($_POST['id']);
 $client=mysql_real_escape_string($_POST['client']);
    $project=mysql_real_escape_string($_POST['project']);
	$type=mysql_real_escape_string($_POST['type']);
	$pricing=mysql_real_escape_string($_POST['pricing']);
	$tpt=mysql_real_escape_string($_POST['tpt']);
$rate=mysql_real_escape_string($_POST['rate']);
$status=mysql_real_escape_string($_POST['status']);
$leo=date('Y-m-d');
$sql2="UPDATE projects SET  
            client=:client,
            sub_project=:project,
            billing_type=:type,
            pricing=:price,
            tpt_mins=:tpt,
			casuals_rate=:rate,
			date_removed=:leo,
			status=:state
            WHERE id =:id";
                                          
$stmt2= dbConnect()->prepare($sql2);                                             
$stmt2->bindParam(':client',$client, PDO::PARAM_STR);      
$stmt2->bindParam(':project',$project, PDO::PARAM_STR);
$stmt2->bindParam(':type',$type, PDO::PARAM_STR);
$stmt2->bindParam(':price',$pricing, PDO::PARAM_STR);
$stmt2->bindParam(':tpt',$tpt, PDO::PARAM_STR);  
$stmt2->bindParam(':rate',$rate, PDO::PARAM_STR);                                        
$stmt2->bindParam(':leo',$leo, PDO::PARAM_STR);  	
$stmt2->bindParam(':state',$status, PDO::PARAM_STR);
$stmt2->bindParam(':id',$id, PDO::PARAM_STR);
$stmt2->execute();

if($stmt2==true)
{
 $data='Successfully updated project';
 header("Location:projects.php?msg=$data");
}
else
{
	echo "problem adding the KPI";
}
	
	
	
}

                        else{
			echo "<br><font color=red size=+1 >Problem in Adding !</font>" ;
			
			}?>