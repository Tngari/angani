<?php
//Start session
include("../include/config.php");
//error_reporting(0); 
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../../index.php");
exit();
}
if($_SESSION['level']=="Admin" || $_SESSION['level']=="Supervisor" )
			{
$fromdt =$_GET["fromdate"];
            $todt = $_GET["todate"];
			
			
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::ilogix Survey ::</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
	

</script>
<script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="../js/jquery.highchartTable.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart').highchartTable();
});


</script>
</head>

<body>
<div class="wrapper">
	
    	<div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/chasebank.png" alt="" height="67"  border="0" />	</a> 
			</div>
            
           <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav" style="width:1190px">
    
   
        
		 <span>Reports</span>
		  <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
		 
     </div>
	<div class="container-fluid" style="background-color:#FFF;	width:1200px;
	min-height:800px;
	margin-left:0px auto 0px auto;
	padding:0px;
	-webkit-border-top-left-radius: 3px;
-webkit-border-top-right-radius: 3px;
-moz-border-radius-topleft: 3px;
-moz-border-radius-topright: 3px;
border-top-left-radius: 3px;
border-top-right-radius: 3px;
box-shadow:  0px 1px 1px #000;
    -moz-box-shadow: 0px 1px 1px #000;
    -webkit-box-shadow: 0px 1px 1px #000;
box-shadow: 0px 8px 18px #1c1c1c;
    -moz-box-shadow: 0px 8px 18px #1c1c1c;
    -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
	<div class="captionWrapper">
	<ul>
		<?php
			$later="2014-01-01";
			$leo=date('Y-m-d');
			?>
                        	<li><a href="dailysummary.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2  class="curr">Overall Daily Summary </h2></a></li>

	<li><a href="overalldailysummary.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2  class="curr">Overall Daily Summary Reachable</h2></a></li>
        <li><a href="overalldailysummary_unreachable.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2  class="curr">Overall Daily Summary Unreachable</h2></a></li>

<!--	<li><a href="calldisposition.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>call Disposition</h2></a></li>-->
	<li><a href="pta.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>PTA</h2></a></li>
        <li><a href="outstandingissues.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Outstanding Issues</h2></a></li>
        <li><a href="inactivity.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Inactivity</h2></a></li>
        <li><a href="mfukoni.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Mfukoni</h2></a></li>
        <li><a href="marketintelligence.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Market Intelligence</h2></a></li>
<!--        <li><a href="recommendations.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Recommendations</h2></a></li>-->
        
		
    </ul>
</div>
 <div class="formCon" style="float:center; width:40%; margin-left:10px;margin-right:10px;padding:10px" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 <tr>
     <form id="form1" name="form1" method="get" action="outstandingissues.php">
       

				  		
		
				  <tr>
       <td >From:</td>
                	<td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                    </tr>
					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr><tr><td >To:</td>
                    <td ><input name='todate' type='text'  id="todt" /></td>
    	</tr>	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
           <td>&nbsp;</td><td ><label>
            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> </td></form>

	 
	
<td>
		  <form action="getCSV.php" method ="post" > <label>
		 <input type="hidden" name="csv_text" id="csv_text">
            <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	font-weight:bold;"/>
          </label> 	
		  </form>
		  <script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
		  </td>
	
        </tr>
      </table>
            
			</div>
			
				<div class="" >
    
    <div class="clear"></div>
                                                
    
        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
         <div class="pagecon" style="float:center; margin-left:10px;">
                                                 
                                                  </div>     
		<div id="files">									  
    <table width="100%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
    
  <tr class="tablebx_topbg">
  <td class="tblRB">Date</td>
  <?php
  $disp=dbConnect()->prepare("SELECT Q3_a,Q3_b,Q3_c,Q3_Escalation FROM survey WHERE cs=1 AND Q3_a='Yes' AND Q3_b!='' GROUP BY Q3_b");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <td class="tblRB"><?php echo $dispo['Q3_b'];?></td>
 <?php
						}
						
						?>
	<td class="tblRB">Total</td>  
  </tr>

			<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
		
                                                        
                       echo '<tr>    <td class="tblR"><a href="outstandingissues_report.php?fromdate='.$dt.'">'.$dt.'</a></td>';                                 
							
	//echo '<tr> <a href="leads_report.php?fromdate=.$dt.">'.$dt.'</a ></td>';

	$full=dbConnect()->prepare("SELECT Q3_b FROM survey WHERE cs=1 AND Q3_a='Yes' AND Q3_b!='' GROUP BY Q3_b");			
	  $full->execute();
	 
           $link = mysql_connect("localhost", "root", "@ct!v3@2015");
                    mysql_select_db("chasebankV2", $link);

                    $result = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q3_b!='' AND cs=1", $link);
                    $num_rows = mysql_num_rows($result);
          
	//$vv="SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 GROUP BY disposition";
	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['Q3_b'];
		   	$pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND  Q3_b='".$value."' AND Q3_a='Yes' AND Q3_b!='' AND cs=1");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
					echo '<td class="tblR">'.$count.'</td>';
					
	   }

					?>
    <?php echo '<td class="tblR">'.$num_rows.'</td>'; ?>

    </tr>
	<?php 

	} 
        
         echo '<tr bgcolor="#FFFF00"> <td class="tblR">Total</td>';
  $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND Q3_a='Yes' AND Q3_b!='' AND cs=1 GROUP BY Q3_b");   
   $disp->execute(); 
      
      $t=0;
      while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
      {
       
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
     }
     $link = mysql_connect("localhost", "root", "@ct!v3@2015");
                    mysql_select_db("chasebankV2", $link);

                    $result2 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND Q3_a='Yes' AND Q3_b!='' AND cs=1", $link);
                    $num_rows2 = mysql_num_rows($result2);
    echo'<td class="tblRB">';
         echo $num_rows2;
        
        
        ?>
	
</table>

<div  style="margin-top:100px">

<table width="100%" class="highchart"  data-graph-container-before="1" data-graph-type="column" style="display:none;">
  <caption>Graphical Representation</caption>
   <thead>
      <tr>
            <th class="tblRB">Date</th>
  <?php
  $disp=dbConnect()->prepare("SELECT Q3_b,Q3_a FROM survey WHERE cs=1 AND Q3_a='Yes' AND Q3_b!='' GROUP BY Q3_b");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <th class="tblRB"><?php echo $dispo['Q3_b'];?></th>
 <?php
						}
						
						?>
	  
      </tr>
   </thead>
  
     <tbody>
	<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
							
							
							echo '<tr>
    <td class="tblR">'.$dt.'</td>';
	$full=dbConnect()->prepare("SELECT Q3_b,Q3_a FROM survey WHERE cs=1 AND Q3_a='Yes' AND Q3_b!='' GROUP BY Q3_b");			
	  $full->execute();
	 
	//$vv="SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 GROUP BY disposition";
	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['Q3_b'];
		   	$pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q3_b='".$value."' AND cs=1");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
					echo '<td class="tblR">'.$count.'</td>';
					
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>
  		
</table>

</div>
</div>
<div class="pagecon">
  
                                              </div>
<div class="clear"></div>
    </div>
    
    
    
    </div>
						
	</div>
	</td>
	</tr>
    <?php // echo pagination($statement,$per_page,$page,$url='?');?>   
    </div>
			</div>
	 
	</div>
	
  <!-- end .content -->
</body>
</html>

<?php
			}
			
			else
			{
			echo "You are not Authorized to access this page.Please get out of here!";	
				
			}
				
?>
