<?php
//Start session
include("../include/config.php");
set_time_limit(6000); ini_set("memory_limit", -1);
error_reporting(0);
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../../index.php");
    exit();
}
if ($_SESSION['level'] == "Admin" || $_SESSION['level'] == "Supervisor") {
    $fromdt = $_GET["fromdate"];
    $todt = $_GET["todate"];

    $month = date('m', strtotime($fromdt));
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>::Chase Bank Dormant Accounts ::</title>
            <link href="../css/style.css" rel="stylesheet" type="text/css" />
            <link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
            <link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
            <script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
            <link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
            <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
            <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
            <script type="text/javascript" src="../js/table2CSV.js" ></script>
            <script type="text/javascript">
                $(function () {
                    $('#fromdt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('#todt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });
                });


            </script>
        </head>

        <body>
            <div class="wrapper">

                <div class="header">



                    <div class="logo">
                        <a href="index.php"><img src="../images/chasebank.png" alt="" height="67" border="0" />	</a> 
                    </div>

                    <div class="">

                        <?php include('admin_nav.php'); ?>

                    </div>

                </div>
                <div class="midnav" style="width:2590px">



                    <span>Reports</span>
                    <span style="float:right"><a href="../logout.php"> Logout</a></span>
                    <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>

                </div>
                <div class="container-fluid" style="background-color:#FFF;	width:2600px;
                     min-height:800px;
                     margin-left:0px auto 0px auto;
                     padding:0px;
                     -webkit-border-top-left-radius: 3px;
                     -webkit-border-top-right-radius: 3px;
                     -moz-border-radius-topleft: 3px;
                     -moz-border-radius-topright: 3px;
                     border-top-left-radius: 3px;
                     border-top-right-radius: 3px;
                     box-shadow:  0px 1px 1px #000;
                     -moz-box-shadow: 0px 1px 1px #000;
                     -webkit-box-shadow: 0px 1px 1px #000;
                     box-shadow: 0px 8px 18px #1c1c1c;
                     -moz-box-shadow: 0px 8px 18px #1c1c1c;
                     -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
                    <div class="captionWrapper">
                        <ul>
                             <?php
			$later="2017-01-01";
			$leo=date('Y-m-d');
			?> 
                            <li><a href="crm_vs_ameyo.php?fromdate=<?php echo $later;?>&todate=<?php echo $leo;?>"><h2 class="curr">CRM VS AMEYO</h2></a></li>
                              <li><a href="ameyo.php?fromdate=<?php echo $later;?>&todate=<?php echo $leo;?>"><h2 class="curr">AMEYO ONLY</h2></a></li>
                               <li><a href="crm_capture.php?fromdate=<?php echo $later;?>&todate=<?php echo $leo;?>"><h2 class="curr">CRM DATA CAPTURE</h2></a></li>
               
                              <li><a href="ameyo_uploadcsv.php"<h2 class="curr">AMEYO Upload CSV</h2></a></li>

                        </ul>
                    </div>
                    <div class="formCon" style="float:center; width:40%; margin-left:10px;margin-right:10px;padding:10px" >
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                                <form id="form1" name="form1" method="get" action="crm_vs_ameyo.php">




                                    <tr>
                                        <td >From:</td>
                                        <td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr><tr><td >To:</td>
                                        <td ><input name='todate' type='text'  id="todt" /></td>
                                    </tr>	<tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <td>&nbsp;</td><td ><label>
                                            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
                                                   background-color:#F27F22;
                                                   height:25px;
                                                   -webkit-border-radius: 4px;
                                                   -moz-border-radius: 4px;
                                                   border-radius: 4px;
                                                   border:1px #b58530 solid;
                                                   color:#633c15;
                                                   font-size:15px;
                                                   cursor:pointer;

                                                   font-weight:bold;"/>
                                        </label> </td></form>



                                <td>
                                    <form action="getCSV.php" method ="post" > <label>
                                            <input type="hidden" name="csv_text" id="csv_text">
                                                <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
                                                       background-color:#F27F22;
                                                       height:25px;
                                                       -webkit-border-radius: 4px;
                                                       -moz-border-radius: 4px;
                                                       border-radius: 4px;
                                                       border:1px #b58530 solid;
                                                       color:#633c15;
                                                       font-size:15px;
                                                       cursor:pointer;
                                                       font-weight:bold;"/>
                                        </label> 	
                                    </form>
                                    <script>
                                        function getCSVData() {
                                            var csv_value = $('#csvdownload').table2CSV({delivery: 'value'});
                                            $("#csv_text").val(csv_value);
                                        }
                                    </script>
                                </td>

                            </tr>
                        </table>

                    </div>

                    <div class="" >

                        <div class="clear"></div>


                        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
                            <div class="pagecon" style="float:center; margin-left:10px;">

                            </div>     
                            <div id="files">									  
                                <table width="100%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
                                    <tr class="tablebx_topbg">
                                        <td class="tblRB">#</td>
                                        <td class="tblRB">CALL ID</td>
                                        <td class="tblRB">AMEYO PHONE</td>
                                        <td class="tblRB">CRM Phone</td>
                                           
                                        <td class="tblRB">AMEYO SYSTEM DISPOSITION</td>
                                         <td class="tblRB">AMEYO USER DISPOSITION</td>
                                          <td class="tblRB">CRM DISPOSITION</td>
                                           <td class="tblRB">AMEYO DATE</td>
                                        <td class="tblRB">CRM DATE</td>
<!--                                        <td class="tblRB">Q3_d</td>-->
                                        
    <!--                                        <td class="tblRB">Q3_2</td>-->
                                        

                                        
                                    </tr>

                                    <?php
                                    $sel = dbConnect()->prepare("SELECT 
                                        (SELECT call_id FROM ameyo_dialler where id=(SELECT max(id) FROM ameyo_dialler p3
                                        WHERE p3.phone = a.phone AND date(disposition_date) between '" . $fromdt . "' AND '" . $todt . "') )AS CALL_ID,
                                        a.phone AS AMEYO_PHONE,
                                        b.phone AS CRM_PHONE,                                      
                                        (SELECT disposition FROM survey where id=(SELECT max(id) FROM survey p3
                                        WHERE p3.phone = a.phone AND date(date) between '" . $fromdt . "' AND '" . $todt . "') )AS CRM_disposition, 
                                        a.user_disposition AS AMEYO_USER_disposition,
                                        a.system_disposition AS AMEYO_SYS_disposition,
                                        a.disposition_date AS AMEYO_disp_Date,
                                        (SELECT date FROM survey where id=(SELECT max(id) FROM survey p2
                                        WHERE p2.phone = a.phone AND date(date) between '" . $fromdt . "' AND '" . $todt . "') )AS CRM_DATE
                                        FROM ameyo_dialler a
                                        INNER JOIN survey b ON a.phone=b.phone
                                        WHERE  (a.phone, a.id) IN(SELECT phone, MAX(id) FROM ameyo_dialler WHERE date(disposition_date) between '" . $fromdt . "' AND '" . $todt . "'  GROUP BY phone)
                                        GROUP BY b.phone");
                                    $sel->execute();

                                    $t = 0;
                                    while ($row = $sel->fetch(PDO::FETCH_ASSOC)) {
                                        $t += 1;
                                        ?>
                                        <tr class=<?php echo $cls; ?>>
                                            <td class="tblR"><?php echo $t; ?></td>
                                               <td class="tblR"><?php echo $row['CALL_ID']; ?></td>
                                            <td class="tblR"><?php echo $row['AMEYO_PHONE']; ?></td>
                                                   <td class="tblR"><?php echo $row['CRM_PHONE']; ?></td>
                                               
                                            <td class="tblR"><?php echo $row['AMEYO_SYS_disposition']; ?></td>
                                             <td class="tblR"><?php echo $row['AMEYO_USER_disposition']; ?></td>
                                            <td class="tblR"><?php echo $row['CRM_disposition']; ?></td>
                                             <td class="tblR"><?php echo $row['AMEYO_disp_Date']; ?></td>
                                                  <td class="tblR"><?php echo $row['CRM_DATE']; ?></td>
                                            
                                        </tr>
                                    <?php }
                                    ?>

                                </table>
                            </div>
                            <div class="pagecon">

                            </div>
                            <div class="clear"></div>
                        </div>



                    </div>

                </div>
                </td>
                </tr>
                <?php // echo pagination($statement,$per_page,$page,$url='?');   ?>   
            </div>
            </div>

            </div>

            <!-- end .content -->
        </body>
    </html>

    <?php
} else {
    echo "You are not Authorized to access this page.Please get out of here!";
}
?>
