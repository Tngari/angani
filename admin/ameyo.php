<?php
//Start session
include("../include/config.php");
error_reporting(0);
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../../index.php");
    exit();
}
if ($_SESSION['level'] == "Admin" || $_SESSION['level'] == "Supervisor") {
    $fromdt = $_GET["fromdate"];
    $todt = $_GET["todate"];

    // $month = date('m', strtotime($fromdt));
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>::Angani::</title>
            <link href="../css/style.css" rel="stylesheet" type="text/css" />
            <link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
            <link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
            <script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
            <link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
            <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
            <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
            <script type="text/javascript" src="../js/table2CSV.js" ></script>
            <script type="text/javascript">
                $(function () {
                    $('#fromdt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('#todt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });
                });


            </script>
        </head>

        <body>
            <div class="wrapper">

                <div class="header">



                    <div class="logo">
                        <a href="index.php"><img src="../images/logo.png" alt="" height="67" border="0" />	</a> 
                    </div>

                    <div class="">

                        <?php include('admin_nav.php'); ?>

                    </div>

                </div>
                <div class="midnav" style="width:2590px">



                    <span>Reports</span>
                    <span style="float:right"><a href="../logout.php"> Logout</a></span>
                    <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>

                </div>
                <div class="container-fluid" style="background-color:#FFF;	width:2600px;
                     min-height:800px;
                     margin-left:0px auto 0px auto;
                     padding:0px;
                     -webkit-border-top-left-radius: 3px;
                     -webkit-border-top-right-radius: 3px;
                     -moz-border-radius-topleft: 3px;
                     -moz-border-radius-topright: 3px;
                     border-top-left-radius: 3px;
                     border-top-right-radius: 3px;
                     box-shadow:  0px 1px 1px #000;
                     -moz-box-shadow: 0px 1px 1px #000;
                     -webkit-box-shadow: 0px 1px 1px #000;
                     box-shadow: 0px 8px 18px #1c1c1c;
                     -moz-box-shadow: 0px 8px 18px #1c1c1c;
                     -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
                    <div class="captionWrapper">
                        <!--                        <ul>
                        <?php
                        $later = "2017-01-01";
                        $leo = date('Y-m-d');
                        ?>    
                        
                                                        <li><a href="crm_vs_ameyo.php?fromdate=<?php echo $later; ?>&todate=<?php echo $leo; ?>"><h2 class="curr">CRM VS AMEYO</h2></a></li>
                                                      <li><a href="ameyo.php?fromdate=<?php echo $later; ?>&todate=<?php echo $leo; ?>"><h2 class="curr">AMEYO ONLY</h2></a></li>
                                                          <li><a href="crm_capture.php?fromdate=<?php echo $later; ?>&todate=<?php echo $leo; ?>"><h2 class="curr">CRM DATA CAPTURE</h2></a></li>
                                                          <li><a href="ameyo_uploadcsv.php"<h2 class="curr">AMEYO Upload CSV</h2></a></li>
                        
                        
                                                </ul>-->
                    </div>                           

                    <div class="formCon" style="float:center; width:40%; margin-left:10px;margin-right:10px;padding:10px" >
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                                <form id="form1" name="form1" method="get" action="ameyo.php">




                                    <tr>
                                        <td >Date Loaded:</td>
                                        <td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <td>&nbsp;</td><td ><label>
                                            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
                                                   background-color:#F27F22;
                                                   height:25px;
                                                   -webkit-border-radius: 4px;
                                                   -moz-border-radius: 4px;
                                                   border-radius: 4px;
                                                   border:1px #b58530 solid;
                                                   color:#633c15;
                                                   font-size:15px;
                                                   cursor:pointer;

                                                   font-weight:bold;"/>
                                        </label> </td></form>



                                <td>
                                    <form action="getCSV.php" method ="post" > <label>
                                            <input type="hidden" name="csv_text" id="csv_text">
                                                <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
                                                       background-color:#F27F22;
                                                       height:25px;
                                                       -webkit-border-radius: 4px;
                                                       -moz-border-radius: 4px;
                                                       border-radius: 4px;
                                                       border:1px #b58530 solid;
                                                       color:#633c15;
                                                       font-size:15px;
                                                       cursor:pointer;
                                                       font-weight:bold;"/>
                                        </label> 	
                                    </form>
                                    <td>
                                        <form action="delete_notcontacted.php" method ="post" > <label>
                                                <input type="hidden" name="csv_text" id="csv_text">
                                                    <input type="submit" alt="Submit Form"  value="Delete all Not Contacted" onclick="return confirm('Are you sure you want to delete!!! This action may be irreversible');" style=" padding:0px 20px;
                                                           background-color:#F27F22;
                                                           height:25px;
                                                           -webkit-border-radius: 4px;
                                                           -moz-border-radius: 4px;
                                                           border-radius: 4px;
                                                           border:1px #b58530 solid;
                                                           color:#633c15;
                                                           font-size:15px;
                                                           cursor:pointer;
                                                           font-weight:bold;"/>
                                            </label> 	
                                        </form>
                                    </td>
                                    <script>
                                        function getCSVData() {
                                            var csv_value = $('#csvdownload').table2CSV({delivery: 'value'});
                                            $("#csv_text").val(csv_value);
                                        }
                                    </script>
                                    <script>
                                        function show_alert() {
                                            alert("Are you sure you want to delete. This action may be irreversible");
                                        }

                                        var el = document.getElementById('myCoolForm');

                                        el.addEventListener('submit', function () {
                                            return confirm('Are you sure you want to delete. This action may be irreversible');

                                        }, false);
                                    </script>
                                </td>

                            </tr>
                        </table>

                    </div>

                    <div class="" >

                        <div class="clear"></div>


                        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
                            <div class="pagecon" style="float:center; margin-left:10px;">

                            </div>     
                            <div id="files">									  
                                <table width="41%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
                                    <tr class="tablebx_topbg">
                                        <td width="6%" class="tblRB">#</td>
                                        <td width="9%" class="tblRB">Full Name</td>
                                        <td width="14%" class="tblRB">Name</td>
                                        <td width="11%" class="tblRB">Phone</td>
                                        <td width="17%" class="tblRB">Email</td>
                                        <td width="22%" class="tblRB">First Name</td>
                                        <td width="21%" class="tblRB">Last Name</td>

            <!--                                        <td class="tblRB">Q3_d</td>-->

            <!--                                        <td class="tblRB">Q3_2</td>-->



                                    </tr>

                                    <?php
                                    //$fromdt=STR_TO_DATE(disposition_date, '%m/%d/%Y %H:%i');
                                    //$todt=STR_TO_DATE(disposition_date, '%m/%d/%Y %H:%i');
                                    //AND date(disposition_date) between '" . $fromdt . "' AND '" . $todt . "'


                                    $sel = dbConnect()->prepare("SELECT * FROM leads
                                                            WHERE contacted='N' AND date(datetimestamp) LIKE '" . $fromdt . "%' AND subs !='SUBS' GROUP BY phone"); //AND date(datetimestamp) LIKE '2017-03-21%' 
                                    $sel->execute();

                                    $t = 0;
                                    while ($row = $sel->fetch(PDO::FETCH_ASSOC)) {
                                        $t += 1;
                                        ?>
                                        <tr class=<?php echo $cls; ?>>
                                            <td class="tblR"><?php echo $t; ?></td>

                                            <td class="tblR"><?php echo $row['full name']; ?></td>
                                            <td class="tblR"><?php echo $row['name']; ?></td>
                                            <td class="tblR"><?php echo $row['phone']; ?></td>
                                            <td class="tblR"><?php echo $row['email']; ?></td>
                                            <td class="tblR"><?php echo $row['first name']; ?></td>
                                            <td class="tblR"><?php echo $row['last name']; ?></td>

                                        </tr>
                                    <?php }
                                    ?>

                                </table>
                            </div>
                            <div class="pagecon">

                            </div>
                            <div class="clear"></div>
                        </div>



                    </div>

                </div>
                </td>
                </tr>
                <?php // echo pagination($statement,$per_page,$page,$url='?');    ?>   
            </div>
            </div>

            </div>

            <!-- end .content -->
        </body>
    </html>

    <?php
} else {
    echo "You are not Authorized to access this page.Please get out of here!";
}
?>
