
<?php
//Start session
include("../include/config.php");
error_reporting(0);
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../../index.php");
    exit();
}
if ($_SESSION['level'] == "Admin" || $_SESSION['level'] == "Supervisor") {
    $fromdt = $_GET["fromdate"];
    $todt = $_GET["todate"];

    $month = date('m', strtotime($fromdt));
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>::Chase Bank Dormant Accounts ::</title>
            <link href="../css/style.css" rel="stylesheet" type="text/css" />
            <link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
            <link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
            <script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
            <link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
            <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
            <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
            <script type="text/javascript" src="../js/table2CSV.js" ></script>
            <script type="text/javascript">
                $(function () {
                    $('#fromdt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('#todt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });
                });


            </script>
        </head>

        <body>
            <div class="wrapper">

                <div class="header">



                    <div class="logo">
                        <a href="index.php"><img src="../images/chasebank.png" alt="" height="67" border="0" />	</a> 
                    </div>

                    <div class="">

                        <?php include('admin_nav.php'); ?>

                    </div>

                </div>
                <div class="midnav" style="width:2590px">



                    <span>Reports</span>
                    <span style="float:right"><a href="../logout.php"> Logout</a></span>
                    <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>

                </div>
                <div class="container-fluid" style="background-color:#FFF;	width:2600px;
                     min-height:800px;
                     margin-left:0px auto 0px auto;
                     padding:0px;
                     -webkit-border-top-left-radius: 3px;
                     -webkit-border-top-right-radius: 3px;
                     -moz-border-radius-topleft: 3px;
                     -moz-border-radius-topright: 3px;
                     border-top-left-radius: 3px;
                     border-top-right-radius: 3px;
                     box-shadow:  0px 1px 1px #000;
                     -moz-box-shadow: 0px 1px 1px #000;
                     -webkit-box-shadow: 0px 1px 1px #000;
                     box-shadow: 0px 8px 18px #1c1c1c;
                     -moz-box-shadow: 0px 8px 18px #1c1c1c;
                     -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
                    <div class="captionWrapper">
                        <ul>
                            <li><a href="index.php"><h2 class="curr">CSV Reports</h2></a></li>



                        </ul>
                    </div>
                    <div class="formCon" style="float:center; width:40%; margin-left:10px;margin-right:10px;padding:10px" >
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                                <form id="form1" name="form1" method="get" action="index.php">




                                    <tr>
                                        <td >From:</td>
                                        <td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr><tr><td >To:</td>
                                        <td ><input name='todate' type='text'  id="todt" /></td>
                                    </tr>	<tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <td>&nbsp;</td><td ><label>
                                            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
                                                   background-color:#F27F22;
                                                   height:25px;
                                                   -webkit-border-radius: 4px;
                                                   -moz-border-radius: 4px;
                                                   border-radius: 4px;
                                                   border:1px #b58530 solid;
                                                   color:#633c15;
                                                   font-size:15px;
                                                   cursor:pointer;

                                                   font-weight:bold;"/>
                                        </label> </td></form>



                                <td>
                                    <form action="getCSV.php" method ="post" > <label>
                                            <input type="hidden" name="csv_text" id="csv_text">
                                                <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
                                                       background-color:#F27F22;
                                                       height:25px;
                                                       -webkit-border-radius: 4px;
                                                       -moz-border-radius: 4px;
                                                       border-radius: 4px;
                                                       border:1px #b58530 solid;
                                                       color:#633c15;
                                                       font-size:15px;
                                                       cursor:pointer;
                                                       font-weight:bold;"/>
                                        </label> 	
                                    </form>
                                    <script>
                                        function getCSVData() {
                                            var csv_value = $('#csvdownload').table2CSV({delivery: 'value'});
                                            $("#csv_text").val(csv_value);
                                        }
                                    </script>
                                </td>

                            </tr>
                        </table>

                    </div>

                    <div class="" >

                        <div class="clear"></div>


                        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
                            <div class="pagecon" style="float:center; margin-left:10px;">

                            </div>     
                            <div id="files">									  
                                <table width="100%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
                                    <tr class="tablebx_topbg">
                                        <td class="tblRB">#</td>
                                        <td class="tblRB">Phone No</td>
    <!--                                        <td class="tblRB">Service Class</td>
                                        <td class="tblRB">Tier</td>-->
                                        <td class="tblRB">Enumerator</td>
                                        <td class="tblRB">Call Status</td>
                                        <td class="tblRB"> Good morning / Habari za Asubuhi ! My name is Admin am calling you from Chase Bank Is this a good time to speak to you? </td>
                                        <td class="tblRB">Thank you for choosing to bank with us. May I know if I am speaking to ( ..............)?</td>
                                        <td class="tblRB">Please indicate a convenient date to call you back</td>
                                        <td class="tblRB">Please indicate a convenient  Time to call you back</td>

                                        <td class="tblRB">Are you willing to conduct the interview if we call you back at a date & time of your convenience?</td>
                                        <td class="tblRB">Being our valued customer ( ............) we would like to know if you have any queries or concerns you would like for us to clarify pertaining your ( ............) account?
                                        </td>
                                        <td class="tblRB">Capture the complaints raised by the client </td>
                                        <td class="tblRB">Capture any other comment raised by tye client </td> 
                                        <td class="tblRB">Q3_d</td>

    <!--                                        <td class="tblRB">Q3_2</td>-->
                                        <td class="tblRB">Looking at our records, we noticed that your account is dormant and you have not used your account since 8/29/2012, could there be a reason why?

                                            Kulingana na rekodi zetu zinaonyesha kuwa haujakua ukitumia akaunti yako kutoka 8/29/2012, Tungependa kujua sababu ni zipi? </td>
                                        <td class="tblRB">Moved - Employer requirements
                                            **Name of employer must be captured** </td>
                                        <td class="tblRB">Capture any other comment raised by tye client </td>
                                        <td class="tblRB">Kindly visit your nearest Chase Bank branch and fill the dormant account activation form for the account to be activated. Ensure to carry your national Identification card. You can proceed to do an over the counter deposit for the account to be fully activated. Which branch would be most convenient for you to visit and activate the account?</td>
                                        <td class="tblRB">Please do provide  the date you are planning to visit</td>
                                        <td class="tblRB">Please do provide the branch name  you are planning to visit</td>
                                        <td class="tblRB">Our award winning Mobile Application Mfukoni is available on Play store. It enables you check your balance, transfer money, download your Statement pay your bills among other services. Do you have access to the mobile banking services?</td>
                                        <td class="tblRB">Could you be interested in mobile banking setup? </td>
                                        <td class="tblRB">Capture reason as to why customer is not interested? </td>
                                        <td class="tblRB">(..............) other than this account do you have any other bank account?</td>
                                        <td class="tblRB">What bank would this be?</td>
                                        <td class="tblRB">** Capture other Bank incase Q6.2 above selection is "Other"**</td>
                                        <td class="tblRB">What reason makes you bank with Bank of Africa?</td>
                                        <td class="tblRB">Company partnership MOU **Must state the name of the company** </td>
                                        <td class="tblRB">Company requirement salary account **Must state the name of the company** </td>
                                        <td class="tblRB">Competitive Interest rates Loan **State type of loan** </td>
                                        <td class="tblRB">Vendor - Must have account with bank **Must state the name of the vendor** </td>
                                        <td class="tblRB"> On a scale of 0 to 10, how likely are you to recommend (Bank Name) to your friends or family?</td>
                                        <td class="tblRB"> Capture any other reasons? </td>
                                        <td class="tblRB">What suggestion would you have for the Bank that when implemented you would have all your accounts with us?
                                        </td>

                                        <td class="tblRB">On a scale of 0 to 10, how likely are you to recommend Chase Bank to your friends or family?</td>
                                        <td class="tblRB">What are the reason for the above score?</td>
                                        <td class="tblRB"> What suggestion would you have for the Bank that when implemented you would have all your accounts with us? </td>
                                        <td class="tblRB">On a scale of 0 to 10, how likely are you to recommend Chase Bank to your friends or family? </td>
                                        <td class="tblRB">What are the reason for the above score?</td>
                                        <td class="tblRB">Escalation</td>


                                        <td>Call Back Date & Time</td>
                                        <td>Survey Disposition</td>
                                        <td>Disposition Comments</td>
                                        <td>Date</td>

                                        <td>Group</td>
                                    </tr>

                                    <?php
                                    $sel = dbConnect()->prepare("SELECT survey.*,leads.batch FROM survey INNER JOIN leads ON survey.lid=leads.id WHERE date(date) between '" . $fromdt . "' AND '" . $todt . "' AND disposition!=''");
                                    $sel->execute();

                                    $t = 0;
                                    while ($row = $sel->fetch(PDO::FETCH_ASSOC)) {
                                        $t += 1;
                                        ?>
                                        <tr class=<?php echo $cls; ?>>
                                            <td class="tblR"><?php echo $t; ?></td>

                                            <td class="tblR"><?php echo $row['phone']; ?></td>
        <!--                                            <td class="tblR"><?php echo $row['serviceclass_desc']; ?></td>
                                            <td class="tblR"><?php echo $row['tier']; ?></td>-->
                                            <td class="tblR"><a href=""><?php echo $row['interviewer']; ?></a></td>
                                            <td class="tblR"><?php
                                                $cs = $row['cs'];
                                                if ($cs == 1) {
                                                    echo "Reachable";
                                                }if ($cs == 2) {
                                                    echo "Unreachable";
                                                }
                                                ?></td>

                                            <td class="tblR"><?php echo $row['Q1']; ?></td>
                                            <td class="tblR"><?php echo $row['Q2_1']; ?></td>
                                           
                                            <td class="tblR"><?php echo $row['Q3']; ?></td>
                                            <td class="tblR"><?php echo $row['Q4']; ?></td>



                                             <td class="tblR"><?php echo $row['Q2']; ?></td>
                                            <td class="tblR"><?php echo $row['Q3_a']; ?></td>
                                            <td class="tblR"><?php echo $row['Q3_b']; ?></td>
                                            <td class="tblR"><?php echo $row['Q3_c']; ?></td>

                                            <td class="tblR"><?php echo $row['Q3_d']; ?></td>
        <!--                                            <td class="tblR"><?php echo $row['Q3_2']; ?></td>-->
                                            <td class="tblR"><?php echo $row['Q4_1_a']; ?></td>
                                            <td class="tblR"><?php echo $row['Q4_1_employer']; ?></td>
                                            <td class="tblR"><?php echo $row['Q4_1_b']; ?></td>
                                            <td ><?php echo $row['Q5_1']; ?></td>
                                            <td ><?php echo $row['Q5_2_a']; ?></td>
                                            <td ><?php echo $row['Q5_2_b']; ?></td>
                                            <td ><?php echo $row['Q5_3_a']; ?></td>
                                            <td ><?php echo $row['Q5_3_b']; ?></td>
                                            <td ><?php echo $row['Q5_3_c']; ?></td>
                                            <td ><?php echo $row['Q6_1']; ?></td>
                                            <td ><?php echo $row['Q6_2']; ?></td>
                                            <td ><?php echo $row['Other_Bank']; ?></td>
                                            <td ><?php echo $row['Q6_3_a']; ?></td>
                                            <td ><?php echo $row['Q6_3_a_company']; ?></td>
                                            <td ><?php echo $row['Q6_3_a_company2']; ?></td>
                                            <td ><?php echo $row['Q6_3_a_loan']; ?></td>
                                            <td ><?php echo $row['Q6_3_a_vendor']; ?></td>

                                            <td ><?php echo $row['Q6_3_a_scale']; ?></td>
                                            <td ><?php echo $row['Q6_3_b']; ?></td>
                                            <td  ><?php echo $row['Q6_4']; ?></td>
                                            <td  ><?php echo $row['Q6_5']; ?></td>
                                            <td  ><?php echo $row['Q6_6']; ?></td>
                                            <td  ><?php echo $row['Q7_1']; ?></td>
                                            <td  ><?php echo $row['Q7_2']; ?></td>
                                            <td  ><?php echo $row['Q7_3']; ?></td>

                                            <td  ><?php echo $row['Escalation']; ?></td>


                                            <!--END OF SMS PART REPORT -->
                                            <td><?php echo $row['callbacktime']; ?></td>
                                            <td><?php echo $row['disposition']; ?></td>
                                            <td ><?php echo $row['disptype']; ?></td>
                                            <td><?php echo $row['date']; ?></td>
                                            <!-- <td><?php
                                            $cs = $row['cs'];
                                            if ($cs == 2) {
                                                echo $row['redial_times'];
                                            }
                                            ?></td>-->
                                            <td><?php echo $row['batch']; ?></td>
                                        </tr>
                                    <?php }
                                    ?>

                                </table>
                            </div>
                            <div class="pagecon">

                            </div>
                            <div class="clear"></div>
                        </div>



                    </div>

                </div>
                </td>
                </tr>
                <?php // echo pagination($statement,$per_page,$page,$url='?');   ?>   
            </div>
            </div>

            </div>

            <!-- end .content -->
        </body>
    </html>

    <?php
} else {
    echo "You are not Authorized to access this page.Please get out of here!";
}
?>
