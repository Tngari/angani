<?php
//Start session
require_once('../Connections/db.php'); error_reporting(0); 
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_username']) || (trim($_SESSION['sess_username']) == '')) {
header("location:index.php");
exit();
}
?>
<?php
//$sub = Employees::model()->findAll("is_deleted=:x", array(':x'=>0));

$data = '';

	$empy =mysql_query("SELECT country_id FROM appraisal_data WHERE country_id!='' GROUP BY country_id")or die(mysql_error());
	$vm=mysql_fetch_array($empy);
	//echo $vm['country_id'];
	foreach($vm as $vm_1)
	{
	$query=mysql_query("SELECT * FROM appraisal_data WHERE country_id='$vm'");
	$rows = mysql_num_rows($query);
	$data ='{name:"'.$vm_1['country_id'].'",
			   y:'.count($rows).',
			   sliced: true,
			   selected: true,},';
	}



//echo $data;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>::ZUKU AGENT'S MONITORING SYSTEM::-EVALUATION FORM </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
<script script type="text/javascript" src="../demo.js"></script>
<script type="text/javascript" src="../js/chart/highcharts.js"></script>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript">
var chart;
$(document).ready(function() {
	chart = new Highcharts.Chart({
		chart: {
			renderTo:'container'
		},
		title: {
			text: 'Campus Performance'
		},
				subtitle: {
			text: 'School A, School B, School C'
		},
		xAxis: {
			categories: ['Average Attendance', 'Average Anual Percentage', 'Average Passout', 'Average Dropout', 'Average Tacher Leave']
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Percentage'
			}
		},
		tooltip: {
			formatter: function() {
				var s;
				if (this.point.name) { // the pie chart
					s = ''+
						this.point.name +': '+ this.y +' Percentage';
				} else {
					s = ''+
						this.x  +': '+ this.y;
				}
				return s;
			}
		},
		credits: {
			enabled: false
		},
		labels: {
			items: [{
				html: 'Total No.of Students',
				style: {
					left: '40px',
					top: '8px',
					color: 'black'
				}
			}]
		},
		series: [{
			type: 'column',
			shadow:0,
			color: '#efc25d',
			name: 'School A',
			data: [30, 20, 10, 30, 10]
		}, {
			type: 'column',
			shadow:0,
			color: '#f09447',
			name: 'School B',
			data: [20, 30, 10, 5, 35]
		}, {
			type: 'column',
			shadow:0,
			color: '#8ac8c3',
			name: 'School C',
			data: [40, 10, 15, 25, 10]
		}, {
			type: 'spline',
			name: 'State Average',
			//shadow:0,
			lineColor:'#ea90ab',
			lineWidth :4,
					marker: {
					enabled: true,
					symbol: 'circle',
					radius: 6,
					fillColor :'#ea90ab',
					lineColor : '#fff',
					lineWidth:2,

		},
			data: [35, 26, 17, 70, 20]
		}, {
			type: 'pie',
			shadow:0,
			name: 'Total No.of Students',
			data: [{
				name: 'School A',
				y: 30,
				color: '#efc25d' // Jane's color
			}, {
				name: 'School B',
				y: 60,
				color: '#f09447' // John's color
			}, {
				name: 'School C',
				y: 20,
				color: '#8ac8c3' // Joe's color
			}],
			center: [100, 80],
			size: 100,
			showInLegend: false,
			dataLabels: {
				enabled: false
			}
		}]
	});
});
</script>

<script src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<body>
<div class="wrapper">
 <div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/logo.png" alt=""  border="0" />	</a> 
			</div>
                 <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav">
    
   
        
		 <span> Admin Dashboard</span>
		  <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['sess_name'];?></span>
		 
     </div>

<div class="container-fluid">
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
      <tr>
        <td valign="top" width="75%"><div style="padding-left:20px;" >
<h1><?php echo "Dashboard";?></h1>
<div class="overview"  >
	<div class="overviewbox ovbox1">
    	<h1><?php echo 'Total Agents';?></h1>
        <div class="ovrBtm"><?php
$result = mysql_query("SELECT * FROM agents WHERE agent_name!=''");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;
		?></div>
    </div>
    <div class="overviewbox ovbox2">
    	<h1><?php echo "Total Success calls";?></h1>
        <div class="ovrBtm"><?php
$result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail='0'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;
		?></div>
    </div>
    <div class="overviewbox ovbox3">
    	<h1><?php echo "Automatic Fail Calls";?></h1>
        <div class="ovrBtm"><?php
$result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail>='1'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;
		?></div>
    </div>
  <div class="clear"></div>
    
</div>


<div class="clear"></div>
  <div class="formCon2">
  
   <div class="list_contner">  <div class="clear"></div>
    <div class="tablebx2"> 
	
  <table width="100%" id="csvdownload" border="0" cellspacing="0" cellpadding="0" style="text-align:center; ">
 <tr>
  <form id="form1" name="form1" method="get" action="index.php">
       

				  	
				 
       <td >From:</td>
                	<td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                    

  <td >To:</td>
                    <td ><input name='todate' type='text'   id="todt" /></td>
    	
           <td ><label>
            <input type="submit" name="Submit" value="Filter" style=" padding:0px 20px;
	
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> </td></form>
		   <td> <form action="getCSV.php" method ="post" > <label>
		 <input type="hidden" name="csv_text" id="csv_text">
            <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
	
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> 	</form></td>
		   <script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
 </tr>
  <tr class="tablebx_topbg">
  <td>COUNTRY</td>
   <td >No.of Agents</td> 
   <td>Total Calls</td>
   <td>Successful Calls</td> 
   <td>Automatic failed calls</td>
    <td>Coached calls</td>
    <!--<td>Calls this Month</td> 
	<td>Calls Last Month</td>-->
  </tr>
  <tr >
  <td>Malawi</td>
   <td ><?php 
   $fromdt = $_GET["fromdate"];
            $todt = $_GET["todate"];
   
   $result = mysql_query("SELECT * FROM agents WHERE agent_name!='' AND country_id=2 " );
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE country_id=2 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail='0' AND country_id=2 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail>='1' AND country_id=2 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
	 <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE coached='Yes' AND country_id=2 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
  <!--  <td><?php
$month=date('m');
	$result = mysql_query("SELECT * FROM appraisal_data WHERE date_of_call='$month' AND country_id=2");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
	<td><?php
$month=date('m',-1);
	$result = mysql_query("SELECT * FROM appraisal_data WHERE date_of_call='$month' AND country_id=2");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>-->
  </tr>
 
   <tr >
  <td>Tanzania</td>
   <td ><?php $result = mysql_query("SELECT * FROM agents WHERE agent_name!='' AND country_id=3");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE country_id=3 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail='0' AND country_id=3 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail>='1' AND country_id=3 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
	 <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE coached='Yes' AND country_id=3 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
  <!--  <td><?php
$month=date('Y-m');
	$result = mysql_query("SELECT * FROM appraisal_data WHERE date_of_call='$month' AND country_id=3");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
	<td><?php
$month=date('Y,m',-1);
	$result = mysql_query("SELECT * FROM appraisal_data WHERE date_of_call='$month' AND country_id=3");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>-->
  </tr>

   <tr >
  <td>Uganda</td>
   <td ><?php $result = mysql_query("SELECT * FROM agents WHERE agent_name!='' AND country_id=4");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE country_id=4 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail='0' AND country_id=4 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail>='1' AND country_id=4 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
	 <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE coached='Yes' AND country_id=4 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
   <!-- <td><?php
$month=date('m');
	$result = mysql_query("SELECT * FROM appraisal_data WHERE date_of_call='$month' AND country_id=4");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
	<td><?php
$month=date('m',-1);
	$result = mysql_query("SELECT * FROM appraisal_data WHERE date_of_call='$month' AND country_id=4");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>-->
  </tr>
 
   <tr >
  <td>Zambia</td>
   <td ><?php $result = mysql_query("SELECT * FROM agents WHERE agent_name!='' AND country_id=5");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE country_id=5 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail='0' AND country_id=5 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
   <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE automatic_fail>='1' AND country_id=5 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
	 <td><?php $result = mysql_query("SELECT * FROM appraisal_data WHERE coached='Yes' AND country_id=5 AND date_of_call between '". $fromdt . "' AND '". $todt . "'");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>
   <!-- <td><?php
$month=date('m');

	$result = mysql_query("SELECT * FROM appraisal_data WHERE date_of_call='$month' AND country_id=5");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td> 
	<td><?php
$month=date('m',-1);
	$result = mysql_query("SELECT * FROM appraisal_data WHERE date_of_call='$month' AND country_id=5");
	$numberOfRows = MYSQL_NUM_ROWS($result);	
	echo $numberOfRows;?></td>-->
  </tr>
 </table>
  
  
  
  </div>
  </div>
  
  </div>
 
 	</div></td>
        
      </tr>
    </table>

    </div>



  
  <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
  <script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script></div>
		</body>
		</html>