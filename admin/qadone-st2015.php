<?php
include("../include/tblcms.php");

session_start();
 error_reporting(0);
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../login.php");
exit();

}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>::Ilogix QA Parameters SYSTEM::-EVALUATION FORM </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	
    <link rel="stylesheet" type="text/css" href="../css/formstyle.css" />
    <link rel="stylesheet" type="text/css" href="../css/dashboard.css" />
    <link rel="stylesheet" type="text/css" href="../css/formelements.css" />

<script src="http://code.jquery.com/jquery-latest.js"></script>
<style>
.tblBorder {
		border:#232b81 solid 1px;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	.tblBorder a {
	text-decoration:none;
	}
	.tblBorder a:link {
	text-decoration:none;
	}
	.tblBorder a:visited {
	text-decoration:none;
	}
	.tblBorder a:hover, a:active, a:focus {
	text-decoration:underline;
	}
	
	.tblBorder2 {
		border:#e67817 solid 1px;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblBorder2a {
		border:#e67817 solid 1px;
		text-align:left;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
		background-color:#f4cca8;
	}
	
	.tblBorder3 {
		border:#232b81 solid 1px;
		text-align:left;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblBorder3b {
		border:#232b81 solid 1px;
		background-color:#fdf3cb;
		color:#ff0000;
		text-align:left;
		font-size:14px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
		padding-bottom:5px;
		padding-top:5px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblShowing {
		text-align:center;
		font-size:15px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
	}
	
	.tblQO {
		background-color:#4d485c;
		color:#FFFFFF;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tblQO2 {
		background-color:#e67817;
		color:#FFFFFF;
		text-align:center;
		font-size:13px;
		font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
		font-weight:bold;
		padding-bottom:3px;
		padding-top:3px;
		padding-left:6px;
		padding-right:6px;
	}
	
	.tbl1a {
		background-color:#fac090;
		font-weight:bold;
		text-align:right;
	}
	
	.tbl1aa {
		background-color:#fac090;
		font-weight:bold;
		text-align:center;
		padding:5px;
	}
	
	.tbl1b {
		background-color:#a15b21;
		color:#FFFFFF;
		font-weight:bold;
		text-align:right;
	}
	
	.tbl1bb {
		background-color:#a15b21;
		color:#FFFFFF;
		font-weight:bold;
		text-align:center;
		padding:5px;
	}
	
	.tbl2b {
		background-color:#f05454;
		text-align:left;
		color:#000000;
		font-weight:bold;
	}
	.tbl2a {
		background-color:#c42727;
		color:#FFFFFF;
		font-weight:bold;
		text-align:left;
	}
	
	.tbl3a {
		background-color:#ff9136;
		color:#000000;
		text-align:left;
		font-weight:bold;
	}
	.tbl3b {
		background-color:#cc5c00;
		color:#FFFFFF;
		text-align:left;
	}
	
	.tbl4b {
		background-color:#3e7804;
		color:#FFFFFF;
		text-align:center;
	}
	
	.tbl5b {
		background-color:#ff9136;
		color:#000000;
		text-align:center;
	}
</style>
</head>
<body>
<div class="wrapper">
 <div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/logo-plantwise.png" alt=""  border="0" />		</a> </div>
            
            
              <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav">
    
   
        <a class="first-letter"> Home</a>
		 <span>QA</span>
		   <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
     </div>

<div class="container formCon" style="margin-bottom:0px" >
<?php
//include the connections file

//save the data on the DB

if(isset($_POST['action']) && $_POST['action'] == 'submitform')
{
	//recieve the variables
	$par1 = $_POST['par1'];
	$par2 = $_POST['par2'];
	$par3 = $_POST['par3'];
	$par4 = $_POST['par4'];
	$par5 = $_POST['par5'];
	$par6 = $_POST['par6'];
	$par7 = $_POST['par7'];
	$par8 = $_POST['par8'];
	$par9 = $_POST['par9'];
	$par10 = $_POST['par10'];
	$par11 = $_POST['par11'];
	$par12 = $_POST['par12'];
	$par13 = $_POST['par13'];
	$par14 = $_POST['par14'];
	$par15 = $_POST['par15'];
	$par16 = $_POST['par16'];
	$par17 = $_POST['par17'];
	$par18 = $_POST['par18'];
	$par19 = $_POST['par19'];
	$par20 = $_POST['par20'];
	$par21 = $_POST['par21'];
	$par22 = $_POST['par22'];
	$par23 = $_POST['par23'];
	$processor = $_POST['processor'];
	$customername = $_POST['customername'];
	$phoneno = $_POST['phoneno'];
	$supervisor = $_POST['supervisor'];
	$auditor = $_POST['auditor'];
	$transactiondate = $_POST['transactiondate'];
	$samaid = $_POST['samaid'];
	$qcid = $_POST['qcid'];
	$comments = $_POST['comments'];
	$qadate = $_POST['qadate'];
	$sid= $_POST['sid'];
	$qualitypassed = $_POST['qualitypassed'];
	
	$totalmarks = $par1 + $par2 + $par3 + $par4 + $par5 + $par6 + $par7 + $par8 + $par9 + $par10 + $par11 + $par12 + $par13 + $par14 + $par15 + $par16 + $par17 + $par18 + $par19 + $par20 + $par21 + $par22 + $par23; 
	
	$cont=$qualitypassed;
	if ($cont=='Yes'){ $qamarks = $totalmarks; }
	else if ($cont=='Edit'){ $qamarks = $totalmarks * (50/100); }
	else if ($cont=='No'){ $qamarks = '0'; }
		
	//save the data on the DB
	
	mysql_select_db($database_tblcms, $tblcms);
	
	$insert_query = sprintf("INSERT INTO qcfiles (par1, par2, par3, par4, par5, par6, par7, par8, par9, par10, par11, par12, par13, par14, par15, par16, par17, par18, par19, par20, par21, par22, par23, processor, customername, phoneno, supervisor, auditor, transactiondate, samaid, qcid, comments, qadate, qualitypassed, totalmarks, qamarks) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
							sanitize($par1, "text"),
							sanitize($par2, "text"),
							sanitize($par3, "text"),
							sanitize($par4, "text"),
							sanitize($par5, "text"),
							sanitize($par6, "text"),
							sanitize($par7, "text"),
							sanitize($par8, "text"),
							sanitize($par9, "text"),
							sanitize($par10, "text"),
							sanitize($par11, "text"),
							sanitize($par12, "text"),
							sanitize($par13, "text"),
							sanitize($par14, "text"),
							sanitize($par15, "text"),
							sanitize($par16, "text"),
							sanitize($par17, "text"),
							sanitize($par18, "text"),
							sanitize($par19, "text"),
							sanitize($par20, "text"),
							sanitize($par21, "text"),
							sanitize($par22, "text"),
							sanitize($par23, "text"),
							sanitize($processor, "text"),
							sanitize($customername, "text"),
							sanitize($phoneno, "text"),
							sanitize($supervisor, "text"),
							sanitize($auditor, "text"),
							sanitize($transactiondate, "date"),
							sanitize($samaid, "text"),
							sanitize($qcid, "text"),
							sanitize($comments, "text"),
							sanitize($qadate, "date"),
							sanitize($qualitypassed, "text"),
							sanitize($totalmarks, "text"),
							sanitize($qamarks, "date"));
	
	$result = mysql_query($insert_query, $tblcms) or die(mysql_error());
	$lmt = mysql_insert_id();
	if($result)
	{	
$val="F";
$user=$_SESSION['name'];
$query_f = "UPDATE survey SET quality='".$val."',quality_by='".$user."' WHERE id='".$sid."'";
$finished= mysql_query($query_f, $tblcms) or die(mysql_error());
		echo "<h1 align='center'>QA File Saved Successfully!!</h1>";
	}
}

function sanitize($value, $type) 
{
  $value = (!get_magic_quotes_gpc()) ? addslashes($value) : $value;

  switch ($type) {
    case "text":
      $value = ($value != "") ? "'" . $value . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $value = ($value != "") ? intval($value) : "NULL";
      break;
    case "double":
      $value = ($value != "") ? "'" . doubleval($value) . "'" : "NULL";
      break;
    case "date":
      $value = ($value != "") ? "'" . $value . "'" : "NULL";
      break;
  }
  
  return $value;
}
?>
<?php
$nchi = $_POST['qcid'];
mysql_select_db($database_tblcms, $tblcms);
$query_country = "SELECT * FROM qc_data WHERE id = '".$nchi."'";
$country = mysql_query($query_country, $tblcms) or die(mysql_error());
$row_country = mysql_fetch_assoc($country);
$totalRows_country = mysql_num_rows($country);


			$later=date('Y-m-')."01";
			$leo=date('Y-m-d');
	
?>
<a href="index.php?fromdate=<?php echo $later;?>&todate=<?php echo $leo;?>"><h1 align="center" style="color:red">Continue >>></h1></a>
</div>
 <div class="midfooter">
    
   
        <a class="first-letter"> &copy <?php echo date('Y');?> Developed and Designed by Techno Brain BPO/ITES</a>
		
     </div>
</body>
</html>
