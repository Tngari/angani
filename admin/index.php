<?php
//Start session
include("../include/config.php");
error_reporting(0);
session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if (!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
    header("location:../../index.php");
    exit();
}
if ($_SESSION['level'] == "Admin" || $_SESSION['level'] == "Supervisor") {
    $fromdt = $_GET["fromdate"];
    $todt = $_GET["todate"];

    $month = date('m', strtotime($fromdt));
    
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>::Angani::Main Report</title>
            <link href="../css/style.css" rel="stylesheet" type="text/css" />
            <link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
            <link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
            <script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
            <link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
            <script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
            <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
            <script type="text/javascript" src="../js/table2CSV.js" ></script>
            <script type="text/javascript">
                $(function () {
                    $('#fromdt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('#todt').datepicker({
                        dateFormat: 'yy-mm-dd',
                        firstDay: 1,
                        changeMonth: true,
                        changeYear: true,
                    });
                });


            </script>
        </head>

        <body>
            <div class="wrapper">

                <div class="header">



                    <div class="logo">
                        <a href="index.php"><img src="../images/logo.png" alt="" height="67" border="0" />	</a> 
                    </div>

                    <div class="">

                        <?php include('admin_nav.php'); ?>

                    </div>

                </div>
                <div class="midnav" style="width:2590px">



                    <span>Reports</span>
                    <span style="float:right"><a href="../logout.php"> Logout</a></span>
                    <span style="float:right"> Welcome <?php echo $_SESSION['name']; ?></span>

                </div>
                <div class="container-fluid" style="background-color:#FFF;	width:2600px;
                     min-height:800px;
                     margin-left:0px auto 0px auto;
                     padding:0px;
                     -webkit-border-top-left-radius: 3px;
                     -webkit-border-top-right-radius: 3px;
                     -moz-border-radius-topleft: 3px;
                     -moz-border-radius-topright: 3px;
                     border-top-left-radius: 3px;
                     border-top-right-radius: 3px;
                     box-shadow:  0px 1px 1px #000;
                     -moz-box-shadow: 0px 1px 1px #000;
                     -webkit-box-shadow: 0px 1px 1px #000;
                     box-shadow: 0px 8px 18px #1c1c1c;
                     -moz-box-shadow: 0px 8px 18px #1c1c1c;
                     -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
                    <div class="captionWrapper">
                        <ul>
                            <li><a href="index.php"><h2 class="curr">CSV Reports</h2></a></li>



                        </ul>
                    </div>
                    <div class="formCon" style="float:center; width:40%; margin-left:10px;margin-right:10px;padding:10px" >
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                                <form id="form1" name="form1" method="get" action="index.php">




                                    <tr>
                                        <td >From:</td>
                                        <td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr><tr><td >To:</td>
                                        <td ><input name='todate' type='text'  id="todt" /></td>
                                    </tr>	<tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <td>&nbsp;</td><td ><label>
                                            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
                                                   background-color:#F27F22;
                                                   height:25px;
                                                   -webkit-border-radius: 4px;
                                                   -moz-border-radius: 4px;
                                                   border-radius: 4px;
                                                   border:1px #b58530 solid;
                                                   color:#633c15;
                                                   font-size:15px;
                                                   cursor:pointer;

                                                   font-weight:bold;"/>
                                        </label> </td></form>



                                <td>
                                    <form action="getCSV.php" method ="post" > <label>
                                            <input type="hidden" name="csv_text" id="csv_text">
                                                <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
                                                       background-color:#F27F22;
                                                       height:25px;
                                                       -webkit-border-radius: 4px;
                                                       -moz-border-radius: 4px;
                                                       border-radius: 4px;
                                                       border:1px #b58530 solid;
                                                       color:#633c15;
                                                       font-size:15px;
                                                       cursor:pointer;
                                                       font-weight:bold;"/>
                                        </label> 	
                                    </form>
                                    <script>
                                        function getCSVData() {
                                            var csv_value = $('#csvdownload').table2CSV({delivery: 'value'});
                                            $("#csv_text").val(csv_value);
                                        }
                                    </script>
                                </td>

                            </tr>
                        </table>

                    </div>

                    <div class="" >

                        <div class="clear"></div>


                        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
                            <div class="pagecon" style="float:center; margin-left:10px;">

                            </div>     
                            <div id="files">									  
                                <table width="80%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
                                    <tr class="tablebx_topbg">
                                        <td width="3%" class="tblRB">#</td>
                                        <td width="3%" class="tblRB">Account Name</td>
                                        <td width="3%" class="tblRB">Name</td>
                                        <td width="4%" class="tblRB">Phone Number</td>
                                        <td width="4%" class="tblRB">Call status</td>
                                        <td width="4%" class="tblRB">survey date</td>
                                        <td width="5%" class="tblRB">Q1: Hello,good morning/afternoon/evening? my name is ----- calling from Angani, could i speak to </td>
                                         <td width="5%" class="tblRB">Q2. Can I talk to Mr./Mrs/Ms./Dr./Prof.----- at some other date and time?</td>
                                        
                                        <td width="6%" class="tblRB">Please indicate a tentative date to call you back</td>
                                        <td width="6%" class="tblRB"><p>Thank you&nbsp;---, have you heard  about Angani?</p></td>
                                        <td width="6%" class="tblRB">Q4 
                                        <p>Fantastic…. The reason we called was to inform  you about our disaster recovery solution that will significantly reduce your IT  infrastructure costs while ensuring you have a secure and reliable platform to  store your business data. Shall we continue?</p></td>

                                       <td width="5%" class="tblRB">Q5:  
                                       <p>So I believe securing your business information is extremely critical. Is  this something that your business is doing?</p></td>
                                       
                                       <td width="9%" class="tblRB">Q6 Ok.  And you do agree that ensuring that, it&rsquo;s backed up automatically in a secure,  off site location gives you a piece of mind that your business info is safe?       </td>
                                        <td width="7%" class="tblRB"><p>q7 What&rsquo;s more,  our service is very affordable. For roughly 5/= per GB per month you have the benefit  of ensuring your business data is safe. That&rsquo;s a small cost for securing your  data. Do you agree or disagree?</p></td>
                                        <td width="5%" class="tblRB">Q8 Date of appointment</td>
                                         
                                        <td width="4%" class="tblRB">Email</td>
                                        <td width="4%" class="tblRB">Email Given</td>
                                        <td width="4%" class="tblRB">Q 10. Is there any question you would like me to address with regards to our Services / or any clarification on the information I have given you?</td>
                                         
                                        <td width="5%">Call Back Date & Time</td>
                                        <td width="6%">Survey Disposition</td>
                                        <td class="tblRB">Enumerator</td>
                                          
                                        <td width="4%">Disposition Comments</td>
                                    </tr>

                                    <?php
                                    $sel = dbConnect()->prepare("SELECT * FROM survey INNER JOIN leads ON survey.lid=leads.id WHERE survey.id IN (SELECT MAX(id) FROM survey WHERE date(date) between '" . $fromdt . "' AND '" . $todt . "' GROUP BY phone) ");
									
                                    $sel->execute();

                                    $t = 0;
                                    while ($row = $sel->fetch(PDO::FETCH_ASSOC)) {
                                        $t += 1;
                                        ?>
                                        <tr class=<?php echo $cls; ?>>
                                            <td class="tblR"><?php echo $t; ?></td>

                                            <td class="tblR"><?php echo $row['subs']; ?></td>
                                             <td class="tblR"><?php echo $row['name']; ?></td>
                                             <td class="tblR"><?php echo $row['phone']; ?></td>
                                             <td class="tblR"><?php
                                                $cs = $row['cs'];
                                                if ($cs == 1) {
                                                    echo "Reachable";
                                                }if ($cs == 2) {
                                                    echo "Unreachable";
                                                }
                                                ?></td>
                                             <td class="tblR"><?php echo $row['date']; ?></td>
                                          <td class="tblR"><?php echo $row['Q1']; ?></td>
                                               <td class="tblR"><?php echo $row['Q2']; ?></td>
                                           
                                            <td class="tblR"><?php echo $row['Q3']; ?></td>
                                            <td class="tblR"><?php echo $row['q3a']; ?></td>



                                          <td class="tblR"><?php echo $row['q4']; ?></td>
                                          <td class="tblR"><?php echo $row['q5']; ?></td>
                                         
                                              <td class="tblR"><?php echo $row['q6']; ?></td>
                                            <td class="tblR"><?php echo $row['q7']; ?></td>
                                            <td class="tblR"><?php echo $row['appointment']; ?> <?php echo $row['time']; ?></td>
                                             
                                             
                                            <td class="tblR"><?php echo $row['email']; ?></td>
                                            <td class="tblR"><?php echo $row['q9']; ?></td>
                                            <td class="tblR"><?php echo $row['Q10']; ?></td>

                                            <!--END OF SMS PART REPORT -->
                                            <td><?php echo $row['callbacktime']; ?></td>
                                            <td><?php echo $row['disposition']; ?></td>
                                            <td class="tblR"><a href=""><?php echo $row['interviewer']; ?></a></td>
                                            <td ><?php echo $row['disptype']; ?></td>
                                            <!-- <td><?php
                                            $cs = $row['cs'];
                                            if ($cs == 2) {
                                                echo $red;
                                            }
                                            ?></td>-->
                                           
                                        </tr>
                                    <?php }
                                    ?>

                                </table>
                            </div>
                            <div class="pagecon">

                            </div>
                            <div class="clear"></div>
                        </div>



                    </div>

                </div>
                </td>
                </tr>
                <?php // echo pagination($statement,$per_page,$page,$url='?');   ?>   
            </div>
            </div>

            </div>

            <!-- end .content -->
        </body>
    </html>

    <?php
} else {
    echo "You are not Authorized to access this page.Please get out of here!";
}
?>
