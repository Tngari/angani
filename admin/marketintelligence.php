<?php
//Start session
include("../include/config.php");
include("../include/Dbconn.php");
//error_reporting(0); 
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../../index.php");
exit();
}
if($_SESSION['level']=="Admin" || $_SESSION['level']=="Supervisor" )
			{
$fromdt =$_GET["fromdate"];
            $todt = $_GET["todate"];
			
			
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::ilogix Survey ::</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
	

</script>
<script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="../js/jquery.highchartTable.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart').highchartTable();
});


</script>
</head>

<body>
<div class="wrapper">
	
    	<div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/chasebank.png" alt="" height="67"  border="0" />	</a> 
			</div>
            
           <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav" style="width:1190px">
    
   
        
		 <span>Reports</span>
		  <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
		 
     </div>
	<div class="container-fluid" style="background-color:#FFF;	width:1200px;
	min-height:800px;
	margin-left:0px auto 0px auto;
	padding:0px;
	-webkit-border-top-left-radius: 3px;
-webkit-border-top-right-radius: 3px;
-moz-border-radius-topleft: 3px;
-moz-border-radius-topright: 3px;
border-top-left-radius: 3px;
border-top-right-radius: 3px;
box-shadow:  0px 1px 1px #000;
    -moz-box-shadow: 0px 1px 1px #000;
    -webkit-box-shadow: 0px 1px 1px #000;
box-shadow: 0px 8px 18px #1c1c1c;
    -moz-box-shadow: 0px 8px 18px #1c1c1c;
    -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
	<div class="captionWrapper">
	<ul>
		<?php
			$later="2014-01-01";
			$leo=date('Y-m-d');
			?>
	<li><a href="overalldailysummary.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2  class="curr">Overall Daily Summary</h2></a></li>
	        <li><a href="overalldailysummary_unreachable.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2  class="curr">Overall Daily Summary Unreachable</h2></a></li>

<!--        <li><a href="calldisposition.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>call Disposition</h2></a></li>-->
	<li><a href="pta.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>PTA</h2></a></li>
        <li><a href="outstandingissues.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Outstanding Issues</h2></a></li>
        <li><a href="inactivity.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Inactivity</h2></a></li>
        <li><a href="mfukoni.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Mfukoni</h2></a></li>
        <li><a href="marketintelligence.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Market Intelligence</h2></a></li>
<!--        <li><a href="recommendations.php?fromdate=<?php echo $leo;?>&todate=<?php echo $leo;?>"><h2>Recommendations</h2></a></li>-->
        
		
    </ul>
</div>
 <div class="formCon" style="float:center; width:40%; margin-left:10px;margin-right:10px;padding:10px" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 <tr>
     <form id="form1" name="form1" method="get" action="marketintelligence.php">
       

				  		
		
				  <tr>
       <td >From:</td>
                	<td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                    </tr>
					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr><tr><td >To:</td>
                    <td ><input name='todate' type='text'  id="todt" /></td>
    	</tr>	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
           <td>&nbsp;</td><td ><label>
            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> </td></form>

	 
	
<td>
		  <form action="getCSV.php" method ="post" > <label>
		 <input type="hidden" name="csv_text" id="csv_text">
            <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	font-weight:bold;"/>
          </label> 	
		  </form>
		  <script>
				function getCSVData(){
 				var csv_value=$('#csvdownload,#csvdownload2,#csvdownload3,#csvdownload4').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
		  </td>
	
        </tr>
      </table>
            
			</div>
			
				<div class="" >
    
    <div class="clear"></div>
                                                
    
        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
         <div class="pagecon" style="float:center; margin-left:10px;">
                                                 
                                                  </div>   
            
            <!-- OTHER YES/NO-->
		<div id="files">
                    
                     <label><center><font size="3" color="blue"><b>YES/NO TO OTHER BANKS</b></font></center></label> <br></br>  
    <table width="100%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
    
  <tr class="tablebx_topbg">
  <td class="tblRB">Date</td>
  <?php
  $disp=dbConnect()->prepare("SELECT Q6_1 FROM survey WHERE cs=1 AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_1 !='' || Q6_1 !=NULL)AND disposition!='' GROUP BY Q6_1");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <td class="tblRB"><?php echo $dispo['Q6_1'];?></td>
 <?php
						}
						
						?>
<td class="tblRB">Total</td>	  
  </tr>

			<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
        
          
						$t=0;
                                                $total=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
                                                       
							$total+=$total;
							
           echo '<tr><td class="tblR"><a href="YES_NO_Otherbanks_reports.php?fromdate='.$dt.'">'.$dt.'</a></td>'; 
	$full=dbConnect()->prepare("SELECT Q6_1 FROM survey WHERE cs=1 AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_1 !='' || Q6_1 !=NULL) AND disposition !='' GROUP BY Q6_1");			
	  $full->execute();
	 $tt=0;
         $count=0;
         
          //QUERY TO COUNT THE NUMBER OF ROWS
                   

                    $result = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (Q6_1 !='' || Q6_1 !=NULL) AND cs=1 AND disposition !=''", $link);
                    $num_rows = mysql_num_rows($result);

                
                    
                    //echo "$num_rows Rows\n";
                    
                    
	//$vv="SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 GROUP BY disposition";
	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
	   {
              
                
               
		     $value=$rows['Q6_1'];
		   	$pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q6_1='".$value."' AND cs=1 AND disposition !=''");			
	  $pls->execute();
	$count=$pls->rowCount();
        
	  
					echo '<td class="tblR">'.$count.'</td>';
					
	   }
       
					?>
        <?php echo '<td class="tblR">'.$num_rows.'</td>'; ?>
    </tr>
	<?php 

	} 
        
            echo '<tr bgcolor="#FFFF00"> <td class="tblR">Total</td>';
  $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE cs=1 AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_1 !='' || Q6_1 !=NULL) AND disposition!='' GROUP BY Q6_1");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
					}
                                        
                
                 
                     $result2 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_1 !='' || Q6_1 !=NULL) AND cs=1 AND disposition !=''", $link);
                    $num_rows2 = mysql_num_rows($result2);                        
//                                         
//  $disp2=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE cs=1 AND (Q6_1 !='' || Q6_1 !=NULL) AND disposition!=''");			
//	  $disp2->execute();	
//						
//							
 echo'<td class="tblRB">';
     echo $num_rows2;
//    
//    
//    
//    
echo '</td>';
						
						?>
    </tr>
        
	
</table>

<div  style="margin-top:100px">

<table width="100%" class="highchart"  data-graph-container-before="1" data-graph-type="column" style="display:none;">
  <caption>Graphical Representation</caption>
   <thead>
      <tr>
            <th class="tblRB">Date</th>
  <?php
  $disp=dbConnect()->prepare("SELECT Q6_1 FROM survey WHERE cs=1 AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_1 !='' || Q6_1 !=NULL)AND disposition!='' GROUP BY Q6_1");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <th class="tblRB"><?php echo $dispo['Q6_1'];?></th>
 <?php
						}
						
						?>
	  
      </tr>
   </thead>
  
     <tbody>
	<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
							
							
							echo '<tr>
    <td class="tblR">'.$dt.'</td>';
	$full=dbConnect()->prepare("SELECT Q6_1 FROM survey WHERE cs=1 AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_1 !='' || Q6_1 !=NULL)AND disposition!='' GROUP BY Q6_1");			
	  $full->execute();
	 
	//$vv="SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 GROUP BY disposition";
	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['Q6_1'];
		   	$pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q6_1='".$value."' AND cs=1 AND cs=1 AND disposition!=''");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
					echo '<td class="tblR">'.$count.'</td>';
					
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>
  		
</table>

</div>
</div>
            
            
            <BR><BR>
                   
            <!--OTHER BANKS-->
            	<div id="files">
                    
                            <label><center><font size="3" color="blue"><b>OTHER BANK DETAILS BREAKDOWN</b></font></center></label> <br></br>  
    <table width="100%" id="csvdownload2" border="0" cellspacing="0" cellpadding="5" >
    
  <tr class="tablebx_topbg">
  <td class="tblRB">Date</td>
  
  <?php
  $disp=dbConnect()->prepare("SELECT Q6_2 FROM survey WHERE cs=1 AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_2 !='' || Q6_2 !=NULL) AND disposition!='' GROUP BY Q6_2");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <td class="tblRB"><?php echo $dispo['Q6_2'];?></td>
 <?php
						}
						
						?>
	<td class="tblRB">Total</td>  
  </tr>

			<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
							
							
		echo '<tr><td class="tblR"><a href="YES_NO_Otherbanks_reports.php?fromdate='.$dt.'">'.$dt.'</a></td>';
	$full=dbConnect()->prepare("SELECT Q6_2 FROM survey WHERE cs=1  AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_2 !='' || Q6_2 !=NULL) AND disposition!='' GROUP BY Q6_2");			
	  $full->execute();
	 
           //QUERY TO COUNT THE NUMBER OF ROWS
                   
                    $result2 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (Q6_2 !='' || Q6_2 !=NULL) AND cs=1 AND disposition!=''", $link);
                    $num_rows2 = mysql_num_rows($result2);

                    //echo "$num_rows Rows\n";
                    
          
	//$vv="SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 GROUP BY disposition";
	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['Q6_2'];
		   	$pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q6_2='".$value."' AND cs=1 AND disposition!=''");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
	       
        
        echo '<td class="tblR">'.$count.'</td>';
                                        
					
	   }

					?>
  <?php echo '<td class="tblR">'.$num_rows2.'</td>'; ?>
    </tr>
    
	
    
	
      
      <?php 

	} 
    
     echo '<tr bgcolor="#FFFF00"> <td class="tblR">Total</td>';
  $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND cs=1 AND (Q6_2 !='' || Q6_2 !=NULL) AND disposition!='' GROUP BY Q6_2");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
					}
						
                   
                     $result3 = mysql_query("SELECT * FROM survey WHERE  date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_2 !='' || Q6_2 !=NULL) AND cs=1 AND disposition!=''", $link);
                    $num_rows3 = mysql_num_rows($result3);                        
//                                         
//  $disp2=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE cs=1 AND (Q6_1 !='' || Q6_1 !=NULL) AND disposition!=''");			
//	  $disp2->execute();	
//						
//							
 echo'<td class="tblRB">';
     echo $num_rows3;
//    
//    
//    
//    
echo '</td>';
						
						?>
    </tr>
</table>

<div  style="margin-top:100px">

<table width="100%" class="highchart"  data-graph-container-before="1" data-graph-type="column" style="display:none;">
  <caption>Graphical Representation</caption>
   <thead>
      <tr>
            <th class="tblRB">Date</th>
  <?php
  $disp=dbConnect()->prepare("SELECT Q6_2 FROM survey WHERE cs=1 AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_2 !='' || Q6_2 !=NULL) AND disposition!='' GROUP BY Q6_2");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <th class="tblRB"><?php echo $dispo['Q6_2'];?></th>
 <?php
						}
						
						?>
	
      </tr>
   </thead>
  
     <tbody>
	<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
							
							
							echo '<tr>
    <td class="tblR">'.$dt.'</td>';
	$full=dbConnect()->prepare("SELECT Q6_2 FROM survey WHERE cs=1 AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_2 !='' || Q6_2 !=NULL) AND disposition!='' GROUP BY Q6_2");			
	  $full->execute();
	 
	//$vv="SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 GROUP BY disposition";
	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['Q6_2'];
		   	$pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q6_2='".$value."' AND cs=1 AND disposition!=''");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
					echo '<td class="tblR">'.$count.'</td>';
					
	   }

					?>
  
    </tr>
	<?php 

	} ?>			
   </tbody>
  		
</table>

</div>
</div>
            
            <BR><BR><BR>
            
             <!-- BREAKDOWN OF PROMOTERS, DETRACTORS, AND...FOR EACH BANK/ NPS FOR OTHER BANKS-->
            	<div id="files">
                    
                    <label><center><font size="3" color="blue"><b>NPS SCORE BREAKDOWN FOR EACH BANK</b></font></center></label> <br></br>
    <table width="100%" id="csvdownload3" border="0" cellspacing="0" cellpadding="5" >
    
  <tr class="tablebx_topbg">
  <td class="tblRB">Date</td>
  
  <?php
  $disp=dbConnect()->prepare("SELECT Q6_2 FROM survey WHERE cs=1 AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_2 !='' || Q6_2 !=NULL) AND disposition!='' AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='') GROUP BY Q6_2");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							?>
 <td class="tblRB"><?php echo $dispo['Q6_2'];?></td>
 <?php
						}
						
						?>
	<td class="tblRB">Total</td>  
  </tr>

			<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
							
							
			echo '<tr><td class="tblR"><a href="otherbanks_NPS_reports.php?fromdate='.$dt.'">'.$dt.'</a></td>';
	$full=dbConnect()->prepare("SELECT Q6_2 FROM survey WHERE cs=1  AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_2 !='' || Q6_2 !=NULL) AND disposition!='' AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='') GROUP BY Q6_2");			
	  $full->execute();
	 
           //QUERY TO COUNT THE NUMBER OF ROWS
                   
                    $result2 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (Q6_2 !='' || Q6_2 !=NULL) AND cs=1 AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='') AND disposition!=''", $link);
                    $num_rows2 = mysql_num_rows($result2);

                    //echo "$num_rows Rows\n";
                    
          
	//$vv="SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 GROUP BY disposition";
	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['Q6_2'];
		   	$pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q6_2='".$value."' AND cs=1 AND disposition!='' AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='')");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
					
        //CHECK DETRACTOR, PASSIVE, PROMOTER
        $result2 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q6_2='".$value."' AND cs=1 AND cs=1 AND disposition!='' AND Q6_3_a_Scale >=0 AND Q6_3_a_Scale <=6 AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='')", $link);
        $count_CHECKd = mysql_num_rows($result2);
        
        
         $result3 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q6_2='".$value."' AND cs=1 AND cs=1 AND disposition!='' AND Q6_3_a_Scale >=7 AND Q6_3_a_Scale <=8 AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='')", $link);
        $count_CHECKpa = mysql_num_rows($result3);
        
         $result4 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q6_2='".$value."' AND cs=1 AND cs=1 AND disposition!='' AND Q6_3_a_Scale >=9 AND Q6_3_a_Scale <=10 AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='')", $link);
        $count_CHECKpr = mysql_num_rows($result4);
        
         $resultp = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND Q6_2='".$value."' AND cs=1 AND cs=1 AND disposition!='' AND Q6_3_a_Scale >=0 AND Q6_3_a_Scale <=10 AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='')", $link);
        $count_CHECKp = mysql_num_rows($resultp);
        
        $per_d=($count_CHECKd/$count_CHECKp)*100;
        $per_d=round($per_d, 2);
        $per_pa=($count_CHECKpa/$count_CHECKp)*100;
        $per_pa=round($per_pa, 2);
        $per_pr=($count_CHECKpr/$count_CHECKp)*100;
        $per_pr=round($per_pr, 2);
        
  
         // echo '<td class="tblR">Detractor='.$count_CHECKd.',<br> Passive='.$count_CHECKpa.',<br> Promoter='.$count_CHECKpr.'<br> Total Evaluators='.$count_CHECKp.'<br>%Detractor='.$per_d.',%Passive='.$per_pa.', %Promoter='.$per_pr.'</td>';
          
          
         echo '<td class="tblR">
<table>
<tr>
<td>
Detractor='.$count_CHECKd.'
</td>
<td>
%Detractor='.$per_d.'
</td>
</tr>
<tr>
<td>
Passive='.$count_CHECKpa.'
</td>
<td>
%Passive='.$per_pa.'
</td>
</tr>
<tr>
<td>
Promoter='.$count_CHECKpr.'
</td>
<td>
%Promoter='.$per_pr.'
</td>
</tr>
<tr>
<td>
Total Evaluators='.$count_CHECKp.'
</td>
<td>
</td>
</tr>
</table>
</td>';
           
        ///END OF CHECK DETRACTOR, PASSIVE, PROMOTER 
    
        
        
        
        
        
        //echo '<td class="tblR">'.$count.'</td>';
                                        
					
	   }

					?>
  <?php echo '<td class="tblR">'.$num_rows2.'</td>'; ?>
    </tr>
    
	
    
	
      
      <?php 

	} 
    
     echo '<tr bgcolor="#FFFF00"> <td class="tblR">Total</td>';
  $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND cs=1 AND (Q6_2 !='' || Q6_2 !=NULL) AND disposition!='' AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='') GROUP BY Q6_2");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
					}
						
                   
                     $result3 = mysql_query("SELECT * FROM survey WHERE  date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_2 !='' || Q6_2 !=NULL) AND cs=1 AND disposition!='' AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='')", $link);
                    $num_rows3 = mysql_num_rows($result3);                        
//                                         
//  $disp2=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE cs=1 AND (Q6_1 !='' || Q6_1 !=NULL) AND disposition!=''");			
//	  $disp2->execute();	
//						
//							
 echo'<td class="tblRB">';
     echo $num_rows3;
//    
//    
//    
//    
echo '</td>';
						
						?>
    </tr>
</table>


</div>
            
             
                   <br></br><br>
    <label><center><font size="3" color="blue"><b>OTHER BANKS NPS OVERALL ANALYSIS</b></font></center></label> <br></br>
<table width="100%" id="csvdownload299" border="0" cellspacing="0" cellpadding="5" >
    <?php
    	$full=dbConnect()->prepare("SELECT Q6_2 FROM survey WHERE cs=1  AND date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_2 !='' || Q6_2 !=NULL) AND disposition!='' AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='') GROUP BY Q6_2");			
	  $full->execute();
	 
         
            echo'<tr class="tablebx_topbg">';
	//$vv="SELECT COUNT(*) AS count FROM survey WHERE date(date)='".$dt."' AND disposition!='' AND cs=1 GROUP BY disposition";
	   while($rows=$full->fetch(PDO::FETCH_ASSOC))
	   {
		     $value=$rows['Q6_2'];
	   				
        //CHECK DETRACTOR, PASSIVE, PROMOTER
        $result22 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND cs=1 AND disposition!='' AND Q6_3_a_Scale >=0 AND Q6_3_a_Scale <=6 AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='') AND Q6_2='".$value."'", $link);
        $count_CHECKd2 = mysql_num_rows($result22);
        
        
         $result32 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND cs=1 AND disposition!='' AND Q6_3_a_Scale >=7 AND Q6_3_a_Scale <=8 AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='') AND Q6_2='".$value."'", $link);
        $count_CHECKpa2 = mysql_num_rows($result32);
        
         $result42 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND cs=1 AND disposition!='' AND Q6_3_a_Scale >=9 AND Q6_3_a_Scale <=10 AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='') AND Q6_5 <=10 AND Q6_2='".$value."'", $link);
        $count_CHECKpr2 = mysql_num_rows($result42);
        
         $resultp2 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND cs=1 AND disposition!='' AND Q6_3_a_Scale >=0 AND Q6_3_a_Scale <=10 AND (Q6_3_a_Scale !=NULL || Q6_3_a_Scale !='') AND Q6_2='".$value."'", $link);
        $count_CHECKp2 = mysql_num_rows($resultp2);
        
        $per_d2=($count_CHECKd2/$count_CHECKp2)*100;
        $per_d2=round($per_d2, 2);
        $per_pa2=($count_CHECKpa2/$count_CHECKp2)*100;
        $per_pa2=round($per_pa2, 2);
        $per_pr2=($count_CHECKpr2/$count_CHECKp2)*100;
        $per_pr2=round($per_pr2, 2);
        
        
  
         // echo '<td class="tblR">Detractor='.$count_CHECKd.',<br> Passive='.$count_CHECKpa.',<br> Promoter='.$count_CHECKpr.'<br> Total Evaluators='.$count_CHECKp.'<br>%Detractor='.$per_d.',%Passive='.$per_pa.', %Promoter='.$per_pr.'</td>';
           
        ///END OF CHECK DETRACTOR, PASSIVE, PROMOTER 
          
         echo '<td>'
                 . '<tr><td><font color="blue"><b>'.$value.' </b></font></td><td></td></tr>'
                 . '<tr><td>Detractor='.$count_CHECKd2.' </td><td>%Detractor='.$per_d2.'</td></tr>'
                 . '<tr><td>Passive='.$count_CHECKpa2.'</td><td>%Passive='.$per_pa2.'</td></tr>'
                 . '<tr><td>Promoter='.$count_CHECKpr2.'</td><td>%Promoter='.$per_pr2.'</td></tr>'
                 . '<tr><td>Total Evaluators='.$count_CHECKp2.'</td><td></td></tr>'
                 . '</td>';
         
         
         }
        ?>
           
    </tr>
     
   
</table>      
             
             
             <BR><BR><BR>
              <!-- NPS CHASE BANK-->
            	<div id="files">
                    
                    <label><center><font size="3" color="blue"><b>NPS SCORE CHASE BANK</b></font></center></label> <br></br>
    <table width="100%" id="csvdownload4" border="0" cellspacing="0" cellpadding="5" >
    
  <tr class="tablebx_topbg">
  <td class="tblRB">Date</td>
  

	<td class="tblRB">Chase Bank NPS</td>  
        <td class="tblRB">Total</td> 
  </tr>

			<?php 
			
	$sel=dbConnect()->prepare("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' GROUP BY date(date)");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$dt=date('Y-m-d',strtotime($row['date']));
							
							
		echo '<tr><td class="tblR"><a href="NPS_chase_reports.php?fromdate='.$dt.'">'.$dt.'</a></td>';
	 
           //QUERY TO COUNT THE NUMBER OF ROWS
                   
                    $result2 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND disposition!=''", $link);
                    $num_rows2 = mysql_num_rows($result2);

                    //echo "$num_rows Rows\n";
//                    
       
		     
        $pls=dbConnect()->prepare("SELECT * FROM survey WHERE date(date)='".$dt."' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND cs=1 AND disposition!=''");			
	  $pls->execute();
	$count=$pls->rowCount();
	  
					
        //CHECK DETRACTOR, PASSIVE, PROMOTER
        $result2 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND cs=1 AND disposition!='' AND Q6_5 >=0 AND Q6_5 <=6 ", $link);
        $count_CHECKd = mysql_num_rows($result2);
        
        
         $result3 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND cs=1 AND disposition!='' AND Q6_5 >=7 AND Q6_5 <=8 ", $link);
        $count_CHECKpa = mysql_num_rows($result3);
        
         $result4 = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND cs=1 AND disposition!='' AND Q6_5 >=9 AND Q6_5 <=10 ", $link);
        $count_CHECKpr = mysql_num_rows($result4);
        
         $resultp = mysql_query("SELECT * FROM survey WHERE date(date)='".$dt."' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND cs=1 AND disposition!='' AND Q6_5 >=0 AND Q6_5 <=10 ", $link);
        $count_CHECKp = mysql_num_rows($resultp);
        
        $per_d=($count_CHECKd/$count_CHECKp)*100;
        $per_d=round($per_d, 2);
        $per_pa=($count_CHECKpa/$count_CHECKp)*100;
        $per_pa=round($per_pa, 2);
        $per_pr=($count_CHECKpr/$count_CHECKp)*100;
        $per_pr=round($per_pr, 2);
        
  
         // echo '<td class="tblR">Detractor='.$count_CHECKd.',<br> Passive='.$count_CHECKpa.',<br> Promoter='.$count_CHECKpr.'<br> Total Evaluators='.$count_CHECKp.'<br>%Detractor='.$per_d.',%Passive='.$per_pa.', %Promoter='.$per_pr.'</td>';
           
        ///END OF CHECK DETRACTOR, PASSIVE, PROMOTER 
    
         echo '<td class="tblR">
<table width="100%">
<tr>
<td align="left">
Detractor='.$count_CHECKd.'
</td>
<td>
%Detractor='.$per_d.'
</td>
</tr>
<tr>
<td align="left">
Passive='.$count_CHECKpa.'
</td>
<td align="left">
%Passive='.$per_pa.'
</td>
</tr>
<tr>
<td align="left">
Promoter='.$count_CHECKpr.'
</td>
<td align="left">
%Promoter='.$per_pr.'
</td>
</tr>
<tr>
<td align="left">
Total Evaluators='.$count_CHECKp.'
</td>
<td align="left">
</td>
</tr>
</table>
</td>';
        
        
        
        
        //echo '<td class="tblR">'.$count.'</td>';
                                        
					
	   

					?>
  <?php echo '<td class="tblR">'.$num_rows2.'</td>'; ?>
    </tr>
    
	
    
	
      
      <?php 

	} 
    
     echo '<tr bgcolor="#FFFF00"> <td class="tblR">Total</td>';
  $disp=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND cs=1 AND (Q6_5 !='' || Q6_5 !=NULL ) AND disposition!=''");			
	  $disp->execute();	
						
						$t=0;
						while($dispo=$disp->fetch(PDO::FETCH_ASSOC))
						{
							
 echo'<td class="tblRB">';
         echo $dispo['TOTAL'];
    
    
    
    
   echo '</td>';
					}
						
                   
                     $result3 = mysql_query("SELECT * FROM survey WHERE  date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND disposition!=''", $link);
                    $num_rows3 = mysql_num_rows($result3);                        
//                                         
//  $disp2=dbConnect()->prepare("SELECT COUNT(*) AS TOTAL FROM survey WHERE cs=1 AND (Q6_1 !='' || Q6_1 !=NULL) AND disposition!=''");			
//	  $disp2->execute();	
//						
//							
 echo'<td class="tblRB">';
     echo $num_rows3;
//    
//    
//    
//    
echo '</td>';
						
						?>
    </tr>
</table>
                    <br></br><br>
    <label><center><font size="3" color="blue"><b>CHASE BANKS NPS OVERALL ANALYSIS</b></font></center></label> <br></br>
    <table width="100%" id="csvdownload4" border="0" cellspacing="0" cellpadding="5" >

                    
                        
                        <TR><?php
                         				
        //CHECK DETRACTOR, PASSIVE, PROMOTER
        $result2 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND disposition!='' AND Q6_5 >=0 AND Q6_5 <=6 ", $link);
        $count_CHECKd = mysql_num_rows($result2);
        
        
         $result3 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND disposition!='' AND Q6_5 >=7 AND Q6_5 <=8 ", $link);
        $count_CHECKpa = mysql_num_rows($result3);
        
         $result4 = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND disposition!='' AND Q6_5 >=9 AND Q6_5 <=10 ", $link);
        $count_CHECKpr = mysql_num_rows($result4);
        
         $resultp = mysql_query("SELECT * FROM survey WHERE date(date) between '". $fromdt . "' AND '". $todt . "' AND (Q6_5 !='' || Q6_5 !=NULL ) AND cs=1 AND disposition!='' AND Q6_5 >=0 AND Q6_5 <=10 ", $link);
        $count_CHECKp = mysql_num_rows($resultp);
        
        $per_d=($count_CHECKd/$count_CHECKp)*100;
         $per_d=round($per_d, 2);
        $per_pa=($count_CHECKpa/$count_CHECKp)*100;
         $per_pa=round($per_pa, 2);
        $per_pr=($count_CHECKpr/$count_CHECKp)*100;
         $per_pr=round($per_pr, 2);
        
  
         // echo '<td class="tblR">Detractor='.$count_CHECKd.',<br> Passive='.$count_CHECKpa.',<br> Promoter='.$count_CHECKpr.'<br> Total Evaluators='.$count_CHECKp.'<br>%Detractor='.$per_d.',%Passive='.$per_pa.', %Promoter='.$per_pr.'</td>';
           
        ///END OF CHECK DETRACTOR, PASSIVE, PROMOTER 
    
         echo '<td class="tblR"><table width="100%"><tr><td align="left">Detractor='.$count_CHECKd.'
</td><td>%Detractor='.$per_d.'</td></tr><tr>
<td align="left">Passive='.$count_CHECKpa.'</td><td align="left">
%Passive='.$per_pa.'</td></tr><tr><td align="left">Promoter='.$count_CHECKpr.'
</td><td align="left">%Promoter='.$per_pr.'</td></tr><tr>
<td align="left">Total Evaluators='.$count_CHECKp.'</td>
<td align="left"></td></tr></table></td>';
           ?>
                            
                        </TR> 
                    </table>
    
           
</div>
             
<div class="pagecon">
  
                                              </div>
<div class="clear"></div>
    </div>
    
    
    
    </div>
						
	</div>
	</td>
	</tr>
    <?php // echo pagination($statement,$per_page,$page,$url='?');?>   
    </div>
			</div>
	 
	</div>
	
  <!-- end .content -->
</body>
</html>

<?php
			}
			
			else
			{
			echo "You are not Authorized to access this page.Please get out of here!";	
				
			}
				
?>
