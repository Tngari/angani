<?php
//Start session
include("../../include/config.php");
error_reporting(0); 
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['username']) || (trim($_SESSION['username']) == '')) {
header("location:../../index.php");
exit();
}
?>
<?php
/*
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}*/
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::CASUAL PAYMENT SYSTEM::-REPORTS</title>
<link href="../../css/style.css" rel="stylesheet" type="text/css" />
<link href="../../css/formstyle.css" rel="stylesheet" type="text/css" />
<link href="../../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="wrapper">
	
    	<div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../../images/logo.png" alt=""  border="0" />	</a> 
			</div>
            
           <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav">
    
   
        
		 <span>Reports</span>
		  <span style="float:right"><a href="../logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['name'];?></span>
		 
     </div>
	<div class="container-fluid" style="background-color:#FFF;	width:1200px;
	min-height:800px;
	margin:0px auto 0px auto;
	padding:0px;

	-webkit-border-top-left-radius: 3px;
-webkit-border-top-right-radius: 3px;
-moz-border-radius-topleft: 3px;
-moz-border-radius-topright: 3px;
border-top-left-radius: 3px;
border-top-right-radius: 3px;
box-shadow:  0px 1px 1px #000;
    -moz-box-shadow: 0px 1px 1px #000;
    -webkit-box-shadow: 0px 1px 1px #000;
box-shadow: 0px 8px 18px #1c1c1c;
    -moz-box-shadow: 0px 8px 18px #1c1c1c;
    -webkit-box-shadow: 0px 8px 18px #1c1c1c;"><br/>
	<div class="captionWrapper">
	<ul>
	<li><a href="index.php"><h2>CSV Reports</h2></a></li>
	<li><a href="consolidated.php"><h2>Consolidated Reports</h2></a></li>
    	
		
    </ul>
</div>
 <div class="formCon" style="float:center; width:80%; margin-left:10px;margin-right:10px;padding:10px" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 <tr>
<form id="form1" name="form1" method="get" action="consolidated.php">
       

				  		
		
				  <tr>
       <td >From:</td>
                	<td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                    </tr>
					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr><tr><td >To:</td>
                    <td ><input name='todate' type='text'  id="todt" /></td>
    	</tr>	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
           <td>&nbsp;</td><td ><label>
            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> </td></form>
	 <td>
		  <form action="getCSV.php" method ="post" > <label>
		 <input type="hidden" name="csv_text" id="csv_text">
            <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
	background-color:#F27F22;
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	font-weight:bold;"/>
          </label> 	</form>
		  <script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
		  </td>
        </tr>
      </table>
            
			</div>
			
				<div class="">
    
    <div class="clear"></div>
                                                
    
        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
         <div class="pagecon" style="float:center; margin-left:10px;">
                                                 
                                                  </div>     
											  
    <table width="100%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
    
  <tr class="tablebx_topbg">
 <td class="tblRB">#</td>
 <td class="tblRB">Date uploaded </td>
	  <td class="tblRB">Login/Uploaded by</td>
	  <td class="tblRB">Project</td>
	  <td class="tblRB">No.of casuals</td>
	   <td class="tblRB">Total casual Amount</td>
	    
  </tr>

			<?php 	
			//include("../../include/config.php");
			$fromdt =mysql_real_escape_string($_GET["fromdate"]);
            $todt = mysql_real_escape_string($_GET["todate"]);
     	
		$sel=dbConnect()->prepare("SELECT SUM(casual_pay) AS Total,COUNT(casual_name) AS No,date_uploaded,uploaded_by,project FROM cps WHERE date_uploaded between '". $fromdt . "' AND '". $todt . "' GROUP BY uploaded_by,date_uploaded");			
	  $sel->execute();	
						
						$t=0;
						while($row=$sel->fetch(PDO::FETCH_ASSOC))
						{
							$t+=1;
						?>
  
 <tr class=<?php echo $cls;?>>
    <td class="tblR"><?php echo $t;?></td>
    <td class="tblR"><a href="view.php?id=<?php echo $row['id'];?>"><?php echo $row['date_uploaded']; ?></a></td>
   <td><?php echo $row['uploaded_by']; ?></td>
    <td class="tblR"><?php
echo $doc=$row['project'];
	//echo $doc;	?></td>
 <td><?php echo $row['No']; ?></td>
 <td><?php echo $row['Total']; ?></td>
  
  
  
    </tr>
	<?php 
	  if($cls=="even")
  {
	 $cls="odd" ;
  }
  else
  {
	  $cls="even"; 
  }
	} ?>
	
</table>
<div class="pagecon">
  
                                              </div>
<div class="clear"></div>
    </div>
    
    
    
    </div>
						
	</div>
	</td>
	</tr>
    <?php // echo pagination($statement,$per_page,$page,$url='?');?>   
    </div>
			</div>
	 
	</div>
  <!-- end .content -->
</body>
</html>
<?php
//mysql_free_result($userDets);
//mysql_free_result($air2013_reports);
?>
