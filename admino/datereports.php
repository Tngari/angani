<?php
//Start session
require_once('../Connections/db.php'); error_reporting(0); 
session_start();
 
//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_username']) || (trim($_SESSION['sess_username']) == '')) {
header("location:index.php");
exit();
}
?>
<?php
/*
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}*/
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";

//mysql_select_db($database_air2013);
$query_userDets = sprintf("SELECT * FROM agents WHERE agent_name = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

//$fromdt = $_GET["fromdate"];

//$todt = $_GET["todate"];
//$agent_name=mysql_real_escape_string($_GET['agent_name']);

///mysql_select_db($database_air2013);
//$query_airtel_reports = "SELECT * FROM appraisal_data WHERE  date_of_call between '". $fromdt . "' AND '". $todt . "' OR agent_name='$agent_name' ";
//$air2013_reports = mysql_query($query_airtel_reports) or die(mysql_error());
//$row_airtel_reports = mysql_fetch_assoc($air2013_reports);
//$totalRows_airtel_reports = mysql_num_rows($air2013_reports);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::ZUKU AGENT'S MONITORING SYSTEM::-EVALUATION FORM</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/formstyle.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="wrapper">
	
    	<div class="header">
     
   
     
        	<div class="logo">
            <a href="index.php"><img src="../images/logo.png" alt=""  border="0" />	</a> 
			</div>
            
           <div class="">
            
<?php include('admin_nav.php');?>
               
            </div>
    
      </div>
 <div class="midnav2">
    
   
        
		 <span> Evaluation Form</span>
		  <span style="float:right"><a href="logout.php"> Logout</a></span>
		 <span style="float:right"> Welcome <?php echo $_SESSION['sess_name'];?></span>
		 
     </div>
	<div class="container-fluid2" ><br/>
	<div class="captionWrapper">
	<ul>
	<li><a href="team.php"><h2>Overall Reports</h2></a></li>
	<li><a href="datereports.php"><h2 class="cur">Reports by Dates</h2></a></li>
    	<li><a href="reports.php"><h2>Successful Calls Reports</h2></a></li>
        <li><a href="failed.php"><h2>Failed Calls Report</h2></a></li>
		
    </ul>
</div>
 <div class="formCon2"  >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
 
<form id="form1" name="form1" method="get" action="datereports.php">
       

				  		
		
				  <tr>
       <td >From:</td>
                	<td ><input name='fromdate' type='text'  id="fromdt"  /></td>
                    </tr>
					<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr><tr><td >To:</td>
                    <td ><input name='todate' type='text'  id="todt" /></td>
    	</tr>	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
           <td>&nbsp;</td><td ><label>
            <input type="submit" name="Submit" value="Get Reports" style=" padding:0px 20px;
	
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> </td></form>
	 <td>
		  <form action="getCSV.php" method ="post" > <label>
		 <input type="hidden" name="csv_text" id="csv_text">
            <input type="submit" alt="Submit Form"  value="Download 2 Excel" onclick="getCSVData()" style=" padding:0px 20px;
	
	height:25px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border:1px #b58530 solid;
	color:#633c15;
	font-size:15px;
	cursor:pointer;
	
	font-weight:bold;"/>
          </label> 	</form>
		  <script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
		  </td>
        </tr>
      </table>
            
			</div>
			
				<div class="">
    
    <div class="clear"></div>
                                                
    
        <div class="tablebx" style="float:center; margin-left:10px;margin-right:10px;">  
         <div class="pagecon" style="float:center; margin-left:10px;">
                                                 
                                                  </div>     
											  
    <table width="100%" id="csvdownload" border="0" cellspacing="0" cellpadding="5" >
    
  <tr class="tablebx_topbg">
 <td class="tblRB">#</td>
	  <td class="tblRB">ID</td>
	  <td class="tblRB">Agent Name </td>
	  <td class="tblRB">Date of Monitoring </td>
	  <td class="tblRB">Date of Call </td>
	  <td class="tblRB">QA/Supervisor </td>
	  <td class="tblRB">call No </td>
	  <td class="tblRB">Account No </td>
	  <td class="tblRB">Country</td>
    <td class="tblRB">Total % </td>
	<td class="tblRB">Call Status </td>
	<td >Welcome(10%) </td>
	<td  > Data Protection(5%)</td>
	<td >Troubleshooting(30%) </td>
	<td >Issue Resolution(30%)</td>
	<td >Soft Skills(10%) </td>
	<td >Value Add(5%) </td>
	<td >Equipment Use(5%) </td>
	<td >Closing Skills(5%) </td>
	<td >Coached </td>
	<td >Coaching File</td>
    
  </tr>

			<?php 	
			 $country=$_SESSION['sess_country'];
			$fromdt = $_GET["fromdate"];
            $todt = $_GET["todate"];
            $agent_name=mysql_real_escape_string($_GET['agent_name']);
			 $state=mysql_real_escape_string($_GET['country']);
			$qa=mysql_real_escape_string($_GET['qa']);

						if($country==1)
						{
						$sel="SELECT * FROM appraisal_data WHERE date_of_call between '". $fromdt . "' AND '". $todt . "' ";
						}
						else 
						{
							$sel="SELECT * FROM appraisal_data WHERE  date_of_call between '". $fromdt . "' AND '". $todt . "'  AND country_id='$country' ";
						}
						
						
						$selQ=mysql_query($sel) or die(mysql_error());
						$t=0;
						while($row=mysql_fetch_array($selQ))
						{
							$t+=1;
						?>
  
 <tr class=<?php echo $cls;?>>
    <td class="tblR"><?php echo $t;?></td>
    <td class="tblR"><?php echo $row['id']; ?></td>
    <td class="tblR"><a href="view.php?id=<?php echo $row['id'];?>"><?php echo $row['agent_name']; ?></a></td>
     <td class="tblR"><?php 
	$doa=date('Y-m-d',strtotime($row['date_of_appraisal']));
	echo $doa; ?></td>
    <td class="tblR"><?php
$doc=date('Y-m-d',strtotime($row['date_of_call']));
	echo $doc;	?></td>
    <td class="tblR"><?php echo $row['qc_person_id']; ?></td>
   	<td class="tblR"><?php echo $row['call_no']; ?></td>
	 <td class="tblR"><?php echo $row['account_no']; ?></td>
	   <td class="tblR"><?php $c=$row['country_id'];
if($c==2)
{
echo "Malawi";
}
elseif ($c==3)
{
echo "Tanzania";
}
elseif ($c==4)
{
echo "Uganda";
}
elseif ($c==5) 
{
echo "Zambia";
}
else{
echo "";
}
	 ?></td>
	 <td class="tblR"><?php echo $row['total_score']; ?></td>
 <td class="tblR"><?php 
	  if ($row['total_score']!='0')
{echo '<div style="color:green;">Successful</div>';}
else
{
echo '<div style="color:red;">Automatic Fail</div>';
}	  ?></td>
	 <td ><?php echo ($row['q1']+$row['q2']); ?></td>
<td><?php  echo ($row['q3']+$row['q4']); ?></td>
  <td ><?php echo $row['q5']; ?></td>
<td ><?php echo ($row['q6']+$row['q7']+$row['q8']);?></td>
  <td  ><?php echo ($row['q9']+$row['q10']+$row['q11']+$row['q12']+$row['q13']+$row['q14']+$row['q15']+$row['q16']+$row['q17']+$row['q18']); ?></td>
<td ><?php echo ($row['q19']+$row['q20']); ?></td>
  <td  ><?php echo ($row['q21']+$row['q22']+$row['q23']); ?></td>
<td  ><?php echo $row['q24']; ?></td>
<td><?php echo $row['coached']; ?></td>
<td  ><?php 
if($row['coached']=='No')
{
echo "No file";
}
else
{
echo '<a href="../../uploads/'.$row['coachfile'].'" target="_blank">'.$row['coachfile'].'</a>' ; 
}
?></td>

  
    </tr>
	<?php 
	  if($cls=="even")
  {
	 $cls="odd" ;
  }
  else
  {
	  $cls="even"; 
  }
	} ?>
	
</table>
<div class="pagecon">
  
                                              </div>
<div class="clear"></div>
    </div>
    
    
    
    </div>
						
	</div>
	</td>
	</tr>
    <?php // echo pagination($statement,$per_page,$page,$url='?');?>   
    </div>
			</div>
	 
	</div>
  <!-- end .content -->
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($air2013_reports);
?>
