<?php 
include "managed/session.php";
include "managed/dbconfig.php";
?>
<?php 
if(!isset($_SESSION['username'])){
	header("location:login.php?status=1");
}

?>
<?php 
if(!isset($_SESSION['username'])){
	header("location:login.php?status=1");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>TBBL Ticketing System</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<script type="text/javascript" src="js/jquery.livequery.js"></script>
<script type="text/javascript" src="js/jquery-1.3.2.js"></script>

<script>
function showResult(str)
{
if (str.length==0)
  {
  document.getElementById("livesearch").innerHTML="";
  document.getElementById("livesearch").style.border="0px";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("livesearch").innerHTML=xmlhttp.responseText;
    document.getElementById("livesearch").style.border="1px solid #3399FF";
	document.getElementById("livesearch").style.padding="5px 10px 5px 5px";
    }
  }
xmlhttp.open("GET","livesearch.php?q="+str,true);
xmlhttp.send();
}
</script>

<script>
function showRSS(str)
{
if (str.length==0)
  {
  document.getElementById("rssOutput").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("rssOutput").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","getrss.php?q="+str,true);
xmlhttp.send();
}
</script>
<style media="all" type="text/css">
@import "css/all.css";
#top_div {
	position:relative;
	left:540px;
	width:428px;
	height:45px;
	z-index:1;
	border:1px solid #FFFFFF;
	border-radius:4px;
}
#content_div {
	position:relative;
	left:8px;
	top:0px;
	width:950px;
	height:auto;
	z-index:2;
	background-color: #FFFFFF;
	border:1px solid #3399FF;
	border-radius:4px;
	margin-bottom:10px;
}
.content_div {
	position:relative;
	left:150px;
	top:0px;
	width:600px;
	height:auto;
	z-index:2;
	background-color: #FFFFFF;
	margin-bottom:10px;
	font-size:15px;
	text-align:justify;
	font-family:calibri;
}
.style1 {
	font-size: 14px;
	font-weight: bold;
}
.style2 {font-size: 12px}
</style>
</head>
<body>

<div id="main">
  <div id="header"><p style="float:right; margin-right:20px"> Logged in as :  <?php echo $_SESSION['username']; ?>&nbsp;&nbsp;&nbsp;<a href="logout.php"><img src="img/logout.png" height="21" /><b>Logout</b></a></p></div>
  <div id="middle">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p></p>
    <table width="609" border="0" align="center">
      <tr>
        <td width="290" height="85"><a href="managed/index.php"><img src="img/managedservices.png" width="230" height="70" alt="managedserv" /></a></td>
        <td width="245">&nbsp;</td>
        <td width="230"><a href="helpdesk/index.php"><img src="img/helpdesk.png" width="230" height="70" alt="helpdesk" /></a></td>
        <td width="9">&nbsp;</td>
      </tr>
    </table>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
  </div>
  <div id="footer"></div>
</div>
</body>
</html>
