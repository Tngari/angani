<style type="text/css">
#othleft-sidebar ul li table {
	font-size: 10px;
}
</style>


<div id="othleft-sidebar">
               
				 <ul >
				
					
				 
				
				 	
				 <li style="margin-top:50px;text-align:center;font-weight:bold;">
				<?php echo 'ANGANI';?>  
				   </li>
				 <li>
				   <table width="255" height="134" cellpadding="0" cellspacing="0">
				     <col width="32" />
				     <col width="288" />
				     <col width="142" />
				     <tr>
				       <td width="32" height="21"><strong>No</strong>.</td>
				       <td width="125"><strong>Item</strong></td>
				       <td width="305"><strong>Retail Price    (VAT Incl)</strong></td>
			         </tr>
				     <tr>
				       <td height="24">1</td>
				       <td>Licence Cost</td>
				       <td>KES 5,000 </td>
			         </tr>
				     <tr>
				       <td height="20">2</td>
				       <td>Back up 100 GB</td>
				       <td>KES 11,999 </td>
			         </tr>
				     <tr>
				       <td height="22">3</td>
				       <td>Back up 250 GB</td>
				       <td>KES 20,999 </td>
			         </tr>
				     <tr>
				       <td height="22">4</td>
				       <td>Back up 500 GB</td>
				       <td>KES 35,999 </td>
			         </tr>
				     <tr>
				       <td height="23">5</td>
				       <td>Back up 1024 GB</td>
				       <td>KES 53,999 </td>
			         </tr>
			       </table>
				 </li>
				 <li>&nbsp; </li>
				 <li>
				   <table cellspacing="0" cellpadding="0">
				     <col width="288" />
				     <col width="142" />
				     <tr>
				       <td colspan="2" width="221">*    Price inclusive of one (1) license</td>
			         </tr>
				     <tr>
				       <td colspan="2" width="221">*    Where customer has more than 1 server / desktop that requires backup, they'll    purchase additional license</td>
			         </tr>
				     <tr>
				       <td colspan="2" width="221">*    Backups are two (2) in nature - Image Backups or File Backups</td>
			         </tr>
				     <tr>
				       <td colspan="2" width="221">*    Image Backups applicable to only Windows Based Servers</td>
			         </tr>
				     <tr>
				       <td colspan="2" width="221">*    Tier 3 &amp; 4 are paid bi annually. Tier 1 &amp; 2 are annual payments</td>
			         </tr>
			       </table>
				   </li>

  </ul>
				
      	
</div>
	
        <script type="text/javascript">
		

	$(document).ready(function () {
            //Hide the second level menu
            $('#othleft-sidebar da').hide();            
            //Show the second level menu if an item inside it active
            $('li.list_active').parent("ul").show();
            
            $('#othleft-sidebar').children('ul').children('li').children('a').click(function () {                    
                
                 if($(this).parent().children('ul').length>0){                  
                    $(this).parent().children('ul').toggle();    
                 }
                 
            });
          
            
        });

    </script>