-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2017 at 08:53 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zuku_payment`
--
CREATE DATABASE IF NOT EXISTS `zuku_payment` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `zuku_payment`;

-- --------------------------------------------------------

--
-- Table structure for table `attempts`
--

DROP TABLE IF EXISTS `attempts`;
CREATE TABLE `attempts` (
  `id` int(11) NOT NULL,
  `lid` varchar(250) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `disposation` varchar(100) NOT NULL,
  `date_done` varchar(20) NOT NULL,
  `attempt` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attempts_list`
--

DROP TABLE IF EXISTS `attempts_list`;
CREATE TABLE `attempts_list` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `code` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attempts_list`
--

INSERT INTO `attempts_list` (`id`, `name`, `code`) VALUES
(1, 'Attempt 1', '1'),
(2, 'Attempt 2', '2'),
(3, 'Attempt 3', '3'),
(4, 'Attempt 4', '4'),
(5, 'Attempt 5', '5'),
(6, 'Attempt 6', '6');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

DROP TABLE IF EXISTS `leads`;
CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `subs` varchar(250) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `phone3` varchar(255) DEFAULT NULL,
  `previous_package` varchar(250) DEFAULT NULL,
  `previous_package_speed` varchar(255) DEFAULT NULL,
  `current_package` varchar(255) DEFAULT NULL,
  `current_package_speed` text,
  `payment_date` varchar(255) DEFAULT NULL,
  `cycle` varchar(255) DEFAULT NULL,
  `amount_due` varchar(50) DEFAULT NULL,
  `amount_in` varchar(50) DEFAULT NULL,
  `amount_to_pay` varchar(50) DEFAULT NULL,
  `contacted` varchar(12) NOT NULL DEFAULT 'N',
  `batch` varchar(250) NOT NULL DEFAULT 'Batch 1',
  `status` text NOT NULL,
  `source` varchar(250) DEFAULT NULL,
  `datetimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qcfiles`
--

DROP TABLE IF EXISTS `qcfiles`;
CREATE TABLE `qcfiles` (
  `id` int(11) NOT NULL,
  `qcid` int(11) DEFAULT NULL,
  `par1` int(11) DEFAULT NULL,
  `par2` int(11) DEFAULT NULL,
  `par3` int(11) DEFAULT NULL,
  `par4` int(11) DEFAULT NULL,
  `par5` int(11) DEFAULT NULL,
  `par6` int(11) DEFAULT NULL,
  `par7` int(11) DEFAULT NULL,
  `par8` int(11) DEFAULT NULL,
  `par9` int(11) DEFAULT NULL,
  `par10` int(11) DEFAULT NULL,
  `par11` int(11) DEFAULT NULL,
  `par12` int(11) DEFAULT NULL,
  `par13` int(11) DEFAULT NULL,
  `par14` int(11) DEFAULT NULL,
  `par15` int(11) DEFAULT NULL,
  `par16` int(11) DEFAULT NULL,
  `par17` int(11) DEFAULT NULL,
  `par18` int(11) DEFAULT NULL,
  `par19` int(11) DEFAULT NULL,
  `par20` int(11) DEFAULT NULL,
  `par21` int(11) DEFAULT NULL,
  `par22` int(11) DEFAULT NULL,
  `par23` int(11) DEFAULT NULL,
  `processor` varchar(100) DEFAULT NULL,
  `customername` varchar(100) DEFAULT NULL,
  `supervisor` varchar(100) DEFAULT NULL,
  `auditor` varchar(100) DEFAULT NULL,
  `transactiondate` date DEFAULT NULL,
  `samaid` varchar(10) DEFAULT NULL,
  `phoneno` varchar(100) DEFAULT NULL,
  `qadate` date DEFAULT NULL,
  `comments` varchar(2000) DEFAULT NULL,
  `qualitypassed` varchar(20) DEFAULT NULL,
  `totalmarks` int(11) DEFAULT NULL,
  `qamarks` int(11) DEFAULT NULL,
  `asof` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qc_data`
--

DROP TABLE IF EXISTS `qc_data`;
CREATE TABLE `qc_data` (
  `id` int(11) NOT NULL,
  `lid` int(50) NOT NULL,
  `interviewer` varchar(200) NOT NULL,
  `cs` varchar(10) NOT NULL,
  `Q1` text NOT NULL,
  `Q2` varchar(50) NOT NULL,
  `Q3` varchar(50) NOT NULL,
  `Q4` varchar(50) NOT NULL,
  `Q5` text NOT NULL,
  `Q5a` varchar(250) DEFAULT NULL,
  `Q5o` varchar(250) DEFAULT NULL,
  `Q6` text NOT NULL,
  `Q7` text NOT NULL,
  `Q7_text` varchar(250) DEFAULT NULL,
  `Q8` text NOT NULL,
  `Q9` text NOT NULL,
  `Q9a` varchar(250) DEFAULT NULL,
  `other1` varchar(250) DEFAULT NULL,
  `other2` varchar(250) DEFAULT NULL,
  `Q10` text NOT NULL,
  `Q11` text NOT NULL,
  `Q12` text NOT NULL,
  `Q13` text NOT NULL,
  `Q14` text NOT NULL,
  `Q15` text NOT NULL,
  `Q16` text NOT NULL,
  `Q17` text NOT NULL,
  `Q18` text NOT NULL,
  `Q18a` varchar(250) DEFAULT NULL,
  `other_1` varchar(250) DEFAULT NULL,
  `other_2` varchar(250) DEFAULT NULL,
  `Q19` text NOT NULL,
  `Q20` text NOT NULL,
  `Q21` text NOT NULL,
  `Q22` text NOT NULL,
  `Q22a` varchar(250) DEFAULT NULL,
  `Q22b` varchar(250) DEFAULT NULL,
  `Q22c` varchar(250) DEFAULT NULL,
  `Q23` text NOT NULL,
  `Q24` text NOT NULL,
  `Q25` text NOT NULL,
  `Q26` text NOT NULL,
  `Q27` text NOT NULL,
  `Q28` text NOT NULL,
  `Q29` text NOT NULL,
  `Q29a` varchar(250) DEFAULT NULL,
  `Q29_other` varchar(250) DEFAULT NULL,
  `Q29_other2` varchar(250) DEFAULT NULL,
  `Q30` text NOT NULL,
  `Q31` text NOT NULL,
  `Q32` text NOT NULL,
  `Q33` text NOT NULL,
  `Q34` text NOT NULL,
  `Q35` text NOT NULL,
  `Q36` text NOT NULL,
  `Q37` text NOT NULL,
  `Q38` text NOT NULL,
  `Q39` text NOT NULL,
  `Q40` text NOT NULL,
  `Q41` text NOT NULL,
  `Q42` text NOT NULL,
  `Q43` text NOT NULL,
  `Q44` text NOT NULL,
  `Q45` text NOT NULL,
  `Q46` text NOT NULL,
  `Q47` text NOT NULL,
  `Q48` text NOT NULL,
  `Q49` text NOT NULL,
  `Q50` text NOT NULL,
  `Q51` text NOT NULL,
  `Q52` text NOT NULL,
  `Q53` text NOT NULL,
  `Q54` text NOT NULL,
  `Q55` text NOT NULL,
  `Q56` text NOT NULL,
  `Q57` text NOT NULL,
  `Q58` text NOT NULL,
  `Q59` text NOT NULL,
  `Q60` text NOT NULL,
  `Q61` text NOT NULL,
  `Q62` text NOT NULL,
  `Q63` text NOT NULL,
  `Q64` text NOT NULL,
  `Q65` text NOT NULL,
  `Q66` text NOT NULL,
  `Q67` text NOT NULL,
  `Q68` text NOT NULL,
  `Q69` text NOT NULL,
  `Q70` text NOT NULL,
  `Q71` text NOT NULL,
  `Q72` text NOT NULL,
  `Q73` text NOT NULL,
  `Q74` text NOT NULL,
  `Q75` text NOT NULL,
  `Q76` text NOT NULL,
  `Q77` text NOT NULL,
  `Q78` text NOT NULL,
  `Q79` text NOT NULL,
  `Q80` text NOT NULL,
  `Q81` text NOT NULL,
  `lang` varchar(50) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `farmer` varchar(255) DEFAULT NULL,
  `id_number` varchar(200) NOT NULL,
  `gender` varchar(200) DEFAULT NULL,
  `clinic_visited` varchar(255) DEFAULT NULL,
  `date_visited` varchar(50) DEFAULT NULL,
  `crop` varchar(200) DEFAULT NULL,
  `callbacktime` varchar(200) NOT NULL,
  `disposition` text NOT NULL,
  `disptype` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `start_time` varchar(200) NOT NULL,
  `stop_time` varchar(50) NOT NULL,
  `last_page` varchar(250) DEFAULT NULL,
  `redial_times` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qc_data`
--

INSERT INTO `qc_data` (`id`, `lid`, `interviewer`, `cs`, `Q1`, `Q2`, `Q3`, `Q4`, `Q5`, `Q5a`, `Q5o`, `Q6`, `Q7`, `Q7_text`, `Q8`, `Q9`, `Q9a`, `other1`, `other2`, `Q10`, `Q11`, `Q12`, `Q13`, `Q14`, `Q15`, `Q16`, `Q17`, `Q18`, `Q18a`, `other_1`, `other_2`, `Q19`, `Q20`, `Q21`, `Q22`, `Q22a`, `Q22b`, `Q22c`, `Q23`, `Q24`, `Q25`, `Q26`, `Q27`, `Q28`, `Q29`, `Q29a`, `Q29_other`, `Q29_other2`, `Q30`, `Q31`, `Q32`, `Q33`, `Q34`, `Q35`, `Q36`, `Q37`, `Q38`, `Q39`, `Q40`, `Q41`, `Q42`, `Q43`, `Q44`, `Q45`, `Q46`, `Q47`, `Q48`, `Q49`, `Q50`, `Q51`, `Q52`, `Q53`, `Q54`, `Q55`, `Q56`, `Q57`, `Q58`, `Q59`, `Q60`, `Q61`, `Q62`, `Q63`, `Q64`, `Q65`, `Q66`, `Q67`, `Q68`, `Q69`, `Q70`, `Q71`, `Q72`, `Q73`, `Q74`, `Q75`, `Q76`, `Q77`, `Q78`, `Q79`, `Q80`, `Q81`, `lang`, `phone`, `farmer`, `id_number`, `gender`, `clinic_visited`, `date_visited`, `crop`, `callbacktime`, `disposition`, `disptype`, `date`, `start_time`, `stop_time`, `last_page`, `redial_times`) VALUES
(1, 1711, 'admin', '1', '', '', '', '', '', NULL, NULL, '', '', NULL, '', '', NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '255754384056', 'Meck Amon uyole mbeya', '', '', '', '', '', '', '', '', '2015-11-30 14:25:57', '2015-11-30 15:22:57', '', NULL, 0),
(2, 8129, 'dorcas', '1', '', '', '', '', '', NULL, NULL, '', '', NULL, '', '', NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '255785773915', '255785773915', '', '', '', '12.6.2015 3:07:53 PM', '', '', '', '', '2015-12-09 13:47:33', '2015-12-09 14:46:03', '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `state_tbl`
--

DROP TABLE IF EXISTS `state_tbl`;
CREATE TABLE `state_tbl` (
  `id` int(11) NOT NULL,
  `s_id` varchar(20) NOT NULL,
  `match_done` varchar(100) NOT NULL,
  `s_date` varchar(20) NOT NULL,
  `date_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state_tbl`
--

INSERT INTO `state_tbl` (`id`, `s_id`, `match_done`, `s_date`, `date_timestamp`) VALUES
(2, '3473', 'Yes', '2015-11-02 08:08:10', '2015-12-09 12:33:20'),
(3, '3475', 'Yes', '2015-11-02 08:10:00', '2015-12-09 12:33:20'),
(4, '3479', 'Yes', '2015-11-02 08:21:56', '2015-12-09 12:33:20'),
(5, '3482', 'Yes', '2015-11-02 08:25:03', '2015-12-09 12:33:20'),
(6, '3497', 'Yes', '2015-11-02 08:35:37', '2015-12-09 12:33:20'),
(7, '3499', 'Yes', '2015-11-02 08:36:40', '2015-12-09 12:33:20'),
(8, '3502', 'Yes', '2015-11-02 08:58:30', '2015-12-09 12:33:20'),
(9, '3512', 'Yes', '2015-12-09 14:46:48', '2015-12-09 12:33:20'),
(10, '3517', 'Yes', '2015-12-09 14:48:10', '2015-12-09 12:33:20'),
(11, '3518', 'Yes', '2015-12-09 14:50:41', '2015-12-09 12:33:20'),
(12, '3808', 'Yes', '2015-11-04 08:38:37', '2015-12-09 12:33:20'),
(13, '3812', 'Yes', '2015-12-09 14:31:30', '2015-12-09 12:33:20'),
(14, '3824', 'Yes', '2015-11-04 08:52:14', '2015-12-09 12:33:20'),
(15, '3830', 'Yes', '2015-11-04 08:54:10', '2015-12-09 12:33:20'),
(16, '3836', 'Yes', '2015-11-04 08:58:45', '2015-12-09 12:33:20'),
(17, '3839', 'Yes', '2015-11-04 09:07:24', '2015-12-09 12:33:20'),
(18, '3853', 'Yes', '2015-11-04 09:16:00', '2015-12-09 12:33:20'),
(19, '3857', 'Yes', '2015-11-04 09:17:39', '2015-12-09 12:33:20'),
(20, '3877', 'Yes', '2015-12-09 10:36:38', '2015-12-09 12:33:20'),
(21, '3881', 'Yes', '2015-12-09 10:38:23', '2015-12-09 12:33:20'),
(22, '3884', 'Yes', '2015-12-09 10:41:34', '2015-12-09 12:33:20'),
(23, '3885', 'Yes', '2015-12-09 10:43:08', '2015-12-09 12:33:20'),
(24, '3887', 'Yes', '2015-12-09 10:54:19', '2015-12-09 12:33:20'),
(25, '3890', 'Yes', '2015-12-09 10:55:57', '2015-12-09 12:33:20'),
(26, '3900', 'Yes', '2015-12-09 11:08:14', '2015-12-09 12:33:20'),
(27, '3904', 'Yes', '2015-12-09 11:35:50', '2015-12-09 12:33:20'),
(28, '3908', 'Yes', '2015-12-09 11:52:09', '2015-12-09 12:33:20'),
(29, '3912', 'Yes', '2015-12-09 11:56:06', '2015-12-09 12:33:20'),
(30, '3921', 'Yes', '2015-12-09 12:00:01', '2015-12-09 12:33:20'),
(31, '3923', 'Yes', '2015-12-09 12:00:42', '2015-12-09 12:33:20'),
(32, '3930', 'Yes', '2015-12-09 12:01:32', '2015-12-09 12:33:20'),
(33, '3932', 'Yes', '2015-12-09 12:02:29', '2015-12-09 12:33:20'),
(34, '3952', 'Yes', '2015-12-09 12:03:37', '2015-12-09 12:33:20'),
(35, '3955', 'Yes', '2015-12-09 12:05:25', '2015-12-09 12:33:20'),
(36, '3960', 'Yes', '2015-12-09 12:05:59', '2015-12-09 12:33:20'),
(37, '3965', 'Yes', '2015-12-09 12:06:51', '2015-12-09 12:33:20'),
(38, '3970', 'Yes', '2015-12-09 12:23:27', '2015-12-09 12:33:20'),
(39, '3972', 'Yes', '2015-12-09 12:30:30', '2015-12-09 12:33:20'),
(40, '3979', 'Yes', '2015-12-09 12:43:23', '2015-12-09 12:33:20'),
(41, '3980', 'Yes', '2015-12-09 12:59:16', '2015-12-09 12:33:20'),
(42, '3982', 'Yes', '2015-12-09 13:01:12', '2015-12-09 12:33:20'),
(43, '3986', 'Yes', '2015-12-09 13:56:07', '2015-12-09 12:33:20'),
(44, '3995', 'Yes', '2015-12-09 13:56:42', '2015-12-09 12:33:20'),
(45, '4003', 'Yes', '2015-12-09 14:00:39', '2015-12-09 12:33:20'),
(46, '4015', 'Yes', '2015-12-09 14:03:27', '2015-12-09 12:33:20'),
(47, '4017', 'Yes', '2015-12-09 14:04:50', '2015-12-09 12:33:20'),
(48, '4025', 'Yes', '2015-12-09 14:28:13', '2015-12-09 12:33:20'),
(49, '4040', 'Yes', '2015-12-09 14:43:40', '2015-12-09 12:33:20'),
(50, '4055', 'Yes', '2015-12-09 14:45:28', '2015-12-09 12:33:20'),
(51, '4057', 'Yes', '2015-12-09 14:30:31', '2015-12-09 12:33:20'),
(52, '4058', 'Yes', '2015-12-09 14:30:00', '2015-12-09 12:33:20'),
(53, '4059', 'Yes', '2015-12-09 14:29:17', '2015-12-09 12:33:20'),
(54, '4075', 'Yes', '2015-12-09 14:28:46', '2015-12-09 12:33:20'),
(55, '4076', 'Yes', '2015-12-09 14:27:37', '2015-12-09 12:33:20'),
(56, '4079', 'Yes', '2015-12-09 14:24:33', '2015-12-09 12:33:20'),
(57, '4080', 'Yes', '2015-12-09 14:19:25', '2015-12-09 12:33:20'),
(58, '4088', 'Yes', '2015-12-09 14:18:17', '2015-12-09 12:33:20'),
(59, '4098', 'Yes', '2015-12-09 14:17:30', '2015-12-09 12:33:20'),
(60, '4112', 'Yes', '2015-12-09 14:06:52', '2015-12-09 12:33:20'),
(65, '3522', 'Yes', '2015-12-09', '2015-12-09 13:35:07'),
(66, '3523', 'Yes', '2015-12-09', '2015-12-09 13:36:55'),
(67, '3527', 'Yes', '2015-12-09', '2015-12-09 13:37:28'),
(68, '3528', 'Yes', '2015-12-09', '2015-12-09 13:38:14'),
(69, '3531', 'Yes', '2015-12-09', '2015-12-09 13:39:09'),
(70, '3532', 'Yes', '2015-12-09', '2015-12-09 13:39:51'),
(71, '3533', 'Yes', '2015-12-09', '2015-12-09 13:40:42'),
(72, '3542', 'Yes', '2015-12-09', '2015-12-09 13:41:27'),
(73, '3543', 'Yes', '2015-12-09', '2015-12-09 13:45:56'),
(74, '3545', 'Yes', '2015-12-09', '2015-12-09 13:49:28'),
(75, '3546', 'Yes', '2015-12-09', '2015-12-09 13:52:35'),
(76, '3549', 'Yes', '2015-12-09', '2015-12-09 13:54:20'),
(77, '3551', 'Yes', '2015-12-09', '2015-12-09 13:55:07'),
(78, '3554', 'Yes', '2015-12-09', '2015-12-09 13:56:51'),
(79, '3555', 'Yes', '2015-12-09', '2015-12-09 13:57:35'),
(80, '3559', 'Yes', '2015-12-09', '2015-12-09 13:58:12'),
(81, '3570', 'Yes', '2015-12-09', '2015-12-09 13:59:03'),
(82, '3575', 'Yes', '2015-12-09', '2015-12-09 13:59:53'),
(83, '3577', 'Yes', '2015-12-09', '2015-12-09 14:01:12'),
(84, '3578', 'Yes', '2015-12-09', '2015-12-09 14:18:51'),
(85, '3579', 'Yes', '2015-12-09', '2015-12-09 14:19:31'),
(86, '3580', 'Yes', '2015-12-09', '2015-12-09 14:20:22'),
(87, '3582', 'Yes', '2015-12-09', '2015-12-09 14:21:38'),
(88, '3583', 'Yes', '2015-12-09', '2015-12-09 14:24:02'),
(89, '3593', 'Yes', '2015-12-09', '2015-12-09 14:25:55'),
(90, '3599', 'Yes', '2015-12-09', '2015-12-09 14:26:33'),
(91, '3601', 'Yes', '2015-12-09', '2015-12-09 14:27:16'),
(92, '3603', 'Yes', '2015-12-09', '2015-12-09 14:28:25'),
(93, '3604', 'Yes', '2015-12-09', '2015-12-09 14:29:12'),
(94, '3605', 'Yes', '2015-12-09', '2015-12-09 14:30:03'),
(95, '3612', 'Yes', '2015-12-09', '2015-12-09 14:30:53'),
(96, '3614', 'Yes', '2015-12-09', '2015-12-09 14:31:31'),
(97, '3615', 'Yes', '2015-12-09', '2015-12-09 14:32:39'),
(98, '3617', 'Yes', '2015-12-09', '2015-12-09 14:33:45'),
(99, '3618', 'Yes', '2015-12-09', '2015-12-09 14:35:12'),
(100, '3619', 'Yes', '2015-12-09', '2015-12-09 14:35:52'),
(101, '3620', 'Yes', '2015-12-09', '2015-12-09 14:36:34'),
(102, '3621', 'Yes', '2015-12-09', '2015-12-09 14:37:45'),
(103, '3622', 'Yes', '2015-12-09', '2015-12-09 14:38:23'),
(104, '3629', 'Yes', '2015-12-09', '2015-12-09 14:39:02'),
(105, '3636', 'Yes', '2015-12-09', '2015-12-09 14:39:50'),
(106, '3798', 'Yes', '2015-12-09', '2015-12-09 14:40:45'),
(107, '3792', 'Yes', '2015-12-09', '2015-12-09 14:42:10'),
(108, '3638', 'Yes', '2015-12-09', '2015-12-09 14:44:08'),
(109, '3650', 'Yes', '2015-12-09', '2015-12-09 14:45:26'),
(110, '3652', 'Yes', '2015-12-09', '2015-12-09 14:46:23'),
(111, '3655', 'Yes', '2015-12-09', '2015-12-09 14:47:23'),
(112, '3659', 'Yes', '2015-12-09', '2015-12-09 14:48:27'),
(113, '3661', 'Yes', '2015-12-09', '2015-12-09 14:49:49'),
(114, '3662', 'Yes', '2015-12-09', '2015-12-09 14:50:41'),
(115, '3664', 'Yes', '2015-12-09', '2015-12-09 14:52:03'),
(116, '3669', 'Yes', '2015-12-09', '2015-12-09 14:53:15'),
(117, '3673', 'Yes', '2015-12-09', '2015-12-09 14:54:59'),
(118, '3674', 'Yes', '2015-12-09', '2015-12-09 14:56:58'),
(119, '3675', 'Yes', '2015-12-09', '2015-12-09 14:58:07'),
(120, '3681', 'Yes', '2015-12-09', '2015-12-09 14:59:24'),
(121, '3685', 'Yes', '2015-12-09', '2015-12-09 15:00:43'),
(122, '3696', 'Yes', '2015-12-09', '2015-12-09 15:01:27'),
(123, '3703', 'Yes', '2015-12-09', '2015-12-09 15:29:42'),
(124, '3704', 'Yes', '2015-12-09', '2015-12-09 15:30:56'),
(125, '3706', 'Yes', '2015-12-09', '2015-12-09 15:31:51'),
(126, '3709', 'Yes', '2015-12-09', '2015-12-09 15:32:39'),
(127, '3710', 'Yes', '2015-12-09', '2015-12-09 15:33:27'),
(128, '3712', 'Yes', '2015-12-09', '2015-12-09 15:35:51'),
(129, '3713', 'Yes', '2015-12-09', '2015-12-09 15:38:45'),
(130, '3714', 'Yes', '2015-12-09', '2015-12-09 15:39:37'),
(131, '3716', 'Yes', '2015-12-09', '2015-12-09 15:40:23'),
(132, '3719', 'Yes', '2015-12-09', '2015-12-09 15:41:07'),
(133, '3728', 'Yes', '2015-12-09', '2015-12-09 15:42:10'),
(134, '3735', 'Yes', '2015-12-09', '2015-12-09 15:43:09'),
(135, '3737', 'Yes', '2015-12-09', '2015-12-09 15:43:59'),
(136, '3739', 'Yes', '2015-12-09', '2015-12-09 15:44:44'),
(137, '3744', 'Yes', '2015-12-09', '2015-12-09 15:45:41'),
(138, '3751', 'Yes', '2015-12-09', '2015-12-09 15:46:27'),
(139, '3753', 'Yes', '2015-12-09', '2015-12-09 15:47:34'),
(140, '3769', 'Yes', '2015-12-09', '2015-12-09 15:48:32'),
(141, '3786', 'Yes', '2015-12-09', '2015-12-09 15:49:20'),
(142, '4115', 'Yes', '2015-12-09', '2015-12-09 15:52:08'),
(143, '4119', 'Yes', '2015-12-09', '2015-12-09 15:53:13'),
(144, '4123', 'Yes', '2015-12-09', '2015-12-09 15:57:08'),
(145, '4124', 'Yes', '2015-12-09', '2015-12-09 15:57:42'),
(146, '4125', 'Yes', '2015-12-09', '2015-12-09 15:59:39'),
(147, '4126', 'Yes', '2015-12-10', '2015-12-10 06:20:16'),
(148, '4131', 'Yes', '2015-12-10', '2015-12-10 06:20:52'),
(149, '4140', 'Yes', '2015-12-10', '2015-12-10 06:21:44'),
(150, '4145', 'Yes', '2015-12-10', '2015-12-10 06:25:44'),
(151, '4146', 'Yes', '2015-12-10', '2015-12-10 06:26:16'),
(152, '4147', 'Yes', '2015-12-10', '2015-12-10 06:26:54'),
(153, '4164', 'Yes', '2015-12-10', '2015-12-10 06:27:27'),
(154, '4165', 'Yes', '2015-12-10', '2015-12-10 06:28:18'),
(155, '4166', 'Yes', '2015-12-10', '2015-12-10 06:28:46'),
(156, '4168', 'Yes', '2015-12-10', '2015-12-10 06:29:24'),
(157, '4169', 'Yes', '2015-12-10', '2015-12-10 06:30:06'),
(158, '4171', 'Yes', '2015-12-10', '2015-12-10 06:30:41'),
(159, '4173', 'Yes', '2015-12-10', '2015-12-10 06:31:18'),
(160, '4180', 'Yes', '2015-12-10', '2015-12-10 06:31:56'),
(161, '4181', 'Yes', '2015-12-10', '2015-12-10 06:32:30'),
(162, '4186', 'Yes', '2015-12-10', '2015-12-10 06:33:20'),
(163, '4193', 'Yes', '2015-12-10', '2015-12-10 06:34:03'),
(164, '4197', 'Yes', '2015-12-10', '2015-12-10 06:42:38'),
(165, '4211', 'Yes', '2015-12-10', '2015-12-10 06:43:22'),
(166, '4218', 'Yes', '2015-12-10', '2015-12-10 06:47:08'),
(167, '4219', 'Yes', '2015-12-10', '2015-12-10 06:51:42'),
(168, '4221', 'Yes', '2015-12-10', '2015-12-10 06:52:59'),
(169, '4226', 'Yes', '2015-12-10', '2015-12-10 06:54:04'),
(170, '4230', 'Yes', '2015-12-10', '2015-12-10 07:03:48'),
(171, '4231', 'Yes', '2015-12-10', '2015-12-10 07:04:26'),
(172, '4232', 'Yes', '2015-12-10', '2015-12-10 07:05:05'),
(173, '4234', 'Yes', '2015-12-10', '2015-12-10 07:07:21'),
(174, '4243', 'Yes', '2015-12-10', '2015-12-10 07:08:54'),
(175, '4244', 'Yes', '2015-12-10', '2015-12-10 07:10:07'),
(176, '4252', 'Yes', '2015-12-10', '2015-12-10 07:22:44'),
(177, '4255', 'Yes', '2015-12-10', '2015-12-10 07:24:28'),
(178, '4257', 'Yes', '2015-12-10', '2015-12-10 07:25:07'),
(179, '4258', 'Yes', '2015-12-10', '2015-12-10 07:25:53'),
(180, '4259', 'Yes', '2015-12-10', '2015-12-10 07:27:22'),
(181, '4272', 'Yes', '2015-12-10', '2015-12-10 07:28:11'),
(182, '4274', 'Yes', '2015-12-10', '2015-12-10 07:28:47'),
(183, '4277', 'Yes', '2015-12-10', '2015-12-10 07:29:36'),
(184, '4296', 'Yes', '2015-12-10', '2015-12-10 07:41:39'),
(185, '4302', 'Yes', '2015-12-10', '2015-12-10 07:53:16'),
(186, '4303', 'Yes', '2015-12-10', '2015-12-10 07:54:49'),
(187, '4308', 'Yes', '2015-12-10', '2015-12-10 07:56:09'),
(188, '4310', 'Yes', '2015-12-10', '2015-12-10 07:56:56'),
(189, '4317', 'Yes', '2015-12-10', '2015-12-10 07:57:43'),
(190, '4325', 'Yes', '2015-12-10', '2015-12-10 08:04:44'),
(191, '4326', 'Yes', '2015-12-10', '2015-12-10 08:05:29'),
(192, '4331', 'Yes', '2015-12-10', '2015-12-10 08:07:28'),
(193, '4340', 'Yes', '2015-12-10', '2015-12-10 08:08:43'),
(194, '4344', 'Yes', '2015-12-10', '2015-12-10 08:10:27'),
(195, '4349', 'Yes', '2015-12-10', '2015-12-10 08:11:08'),
(196, '4366', 'Yes', '2015-12-10', '2015-12-10 08:11:47'),
(197, '4373', 'No', '2015-12-10', '2015-12-10 08:12:37'),
(198, '4380', 'Yes', '2015-12-10', '2015-12-10 08:15:51'),
(199, '4396', 'Yes', '2015-12-10', '2015-12-10 08:25:20'),
(200, '4412', 'Yes', '2015-12-10', '2015-12-10 08:26:16'),
(201, '4416', 'Yes', '2015-12-10', '2015-12-10 08:27:31'),
(202, '4418', 'Yes', '2015-12-10', '2015-12-10 08:28:10'),
(203, '4429', 'Yes', '2015-12-10', '2015-12-10 08:29:07'),
(204, '4434', 'Yes', '2015-12-10', '2015-12-10 08:29:55'),
(205, '4489', 'Yes', '2015-12-10', '2015-12-10 08:30:37'),
(206, '4479', 'Yes', '2015-12-10', '2015-12-10 08:31:35'),
(207, '4456', 'Yes', '2015-12-10', '2015-12-10 08:32:12'),
(208, '4446', 'Yes', '2015-12-10', '2015-12-10 08:32:42'),
(209, '4469', 'Yes', '2015-12-10', '2015-12-10 08:33:25'),
(210, '4537', 'Yes', '2015-12-10', '2015-12-10 08:34:28'),
(211, '4539', 'Yes', '2015-12-10', '2015-12-10 08:35:12'),
(212, '4544', 'Yes', '2015-12-10', '2015-12-10 08:36:27'),
(213, '4553', 'Yes', '2015-12-10', '2015-12-10 08:38:28'),
(214, '4559', 'Yes', '2015-12-10', '2015-12-10 08:40:23'),
(215, '4561', 'Yes', '2015-12-10', '2015-12-10 08:41:23'),
(216, '4568', 'Yes', '2015-12-10', '2015-12-10 08:42:54'),
(217, '4575', 'Yes', '2015-12-10', '2015-12-10 08:43:40'),
(218, '4580', 'Yes', '2015-12-10', '2015-12-10 08:44:15'),
(219, '4587', 'Yes', '2015-12-10', '2015-12-10 08:45:27'),
(220, '4592', 'Yes', '2015-12-10', '2015-12-10 08:48:37'),
(221, '4594', 'Yes', '2015-12-10', '2015-12-10 08:49:48'),
(222, '4595', 'Yes', '2015-12-10', '2015-12-10 08:51:42'),
(223, '4596', 'Yes', '2015-12-10', '2015-12-10 08:54:11'),
(224, '4605', 'Yes', '2015-12-10', '2015-12-10 08:57:51'),
(225, '4607', 'Yes', '2015-12-10', '2015-12-10 09:04:17'),
(226, '4626', 'Yes', '2015-12-10', '2015-12-10 09:04:52'),
(227, '4628', 'Yes', '2015-12-10', '2015-12-10 09:05:59'),
(228, '4629', 'Yes', '2015-12-10', '2015-12-10 09:06:46'),
(229, '4631', 'Yes', '2015-12-10', '2015-12-10 09:08:05'),
(230, '4635', 'Yes', '2015-12-10', '2015-12-10 09:09:06'),
(231, '4655', 'Yes', '2015-12-10', '2015-12-10 09:09:39'),
(232, '4659', 'Yes', '2015-12-10', '2015-12-10 09:10:50'),
(233, '4672', 'Yes', '2015-12-10', '2015-12-10 09:11:54'),
(234, '4683', 'Yes', '2015-12-10', '2015-12-10 09:13:44'),
(235, '4684', 'Yes', '2015-12-10', '2015-12-10 09:15:00'),
(236, '4697', 'Yes', '2015-12-10', '2015-12-10 09:15:53'),
(237, '4699', 'Yes', '2015-12-10', '2015-12-10 09:17:02'),
(238, '4796', 'Yes', '2015-12-10', '2015-12-10 09:19:14'),
(239, '4791', 'Yes', '2015-12-10', '2015-12-10 09:20:43'),
(240, '4779', 'Yes', '2015-12-10', '2015-12-10 09:22:04'),
(241, '4765', 'Yes', '2015-12-10', '2015-12-10 09:23:20'),
(242, '4762', 'Yes', '2015-12-10', '2015-12-10 09:24:07'),
(243, '4751', 'Yes', '2015-12-10', '2015-12-10 09:28:29'),
(244, '4746', 'Yes', '2015-12-10', '2015-12-10 09:29:10'),
(245, '4741', 'Yes', '2015-12-10', '2015-12-10 09:29:45'),
(246, '4738', 'Yes', '2015-12-10', '2015-12-10 09:30:17'),
(247, '4729', 'Yes', '2015-12-10', '2015-12-10 09:31:02'),
(248, '4724', 'Yes', '2015-12-10', '2015-12-10 09:32:55'),
(249, '4723', 'Yes', '2015-12-10', '2015-12-10 09:33:54'),
(250, '4705', 'Yes', '2015-12-10', '2015-12-10 09:34:23'),
(251, '4704', 'Yes', '2015-12-10', '2015-12-10 09:35:05'),
(252, '1841', 'Yes', '2015-12-10', '2015-12-10 09:38:15'),
(253, '1871', 'Yes', '2015-12-10', '2015-12-10 09:39:50'),
(254, '3367', 'Yes', '2015-12-10', '2015-12-10 09:42:44'),
(255, '3816', 'Yes', '2015-12-10', '2015-12-10 09:44:16'),
(256, '4092', 'Yes', '2015-12-10', '2015-12-10 09:45:37'),
(257, '4834', 'Yes', '2015-12-10', '2015-12-10 09:46:10'),
(258, '4835', 'Yes', '2015-12-10', '2015-12-10 09:47:31'),
(259, '4850', 'Yes', '2015-12-10', '2015-12-10 09:48:23'),
(260, '4853', 'Yes', '2015-12-10', '2015-12-10 09:52:22'),
(261, '4856', 'Yes', '2015-12-10', '2015-12-10 09:53:31'),
(262, '4893', 'Yes', '2015-12-10', '2015-12-10 09:54:37'),
(263, '4895', 'Yes', '2015-12-10', '2015-12-10 09:56:01'),
(264, '4896', 'Yes', '2015-12-10', '2015-12-10 09:57:32'),
(265, '4903', 'Yes', '2015-12-10', '2015-12-10 09:58:50'),
(266, '4919', 'Yes', '2015-12-10', '2015-12-10 11:12:55'),
(267, '4933', 'Yes', '2015-12-10', '2015-12-10 11:13:42'),
(268, '5004', 'Yes', '2015-12-10', '2015-12-10 11:14:15'),
(269, '5140', 'Yes', '2015-12-10', '2015-12-10 11:16:27'),
(270, '5144', 'Yes', '2015-12-10', '2015-12-10 11:17:32'),
(271, '5153', 'Yes', '2015-12-10', '2015-12-10 11:18:26'),
(272, '5158', 'Yes', '2015-12-10', '2015-12-10 11:19:30'),
(273, '5161', 'Yes', '2015-12-10', '2015-12-10 11:20:29'),
(274, '5171', 'Yes', '2015-12-10', '2015-12-10 11:21:13'),
(275, '315', 'Yes', '2015-12-17', '2015-12-17 08:32:04'),
(276, '338', 'Yes', '2015-12-17', '2015-12-17 08:32:37'),
(277, '424', 'Yes', '2015-12-17', '2015-12-17 08:33:09'),
(278, '504', 'Yes', '2015-12-17', '2015-12-17 08:33:51'),
(279, '519', 'Yes', '2015-12-17', '2015-12-17 08:34:38'),
(280, '550', 'Yes', '2015-12-17', '2015-12-17 08:35:06'),
(281, '628', 'Yes', '2015-12-17', '2015-12-17 08:37:08'),
(282, '802', 'Yes', '2015-12-17', '2015-12-17 08:37:41'),
(283, '818', 'Yes', '2015-12-17', '2015-12-17 08:38:24'),
(284, '922', 'Yes', '2015-12-17', '2015-12-17 08:40:15'),
(285, '949', 'Yes', '2015-12-17', '2015-12-17 08:42:16'),
(286, '962', 'Yes', '2015-12-17', '2015-12-17 08:43:02'),
(287, '972', 'Yes', '2015-12-17', '2015-12-17 08:43:53'),
(288, '1051', 'Yes', '2015-12-17', '2015-12-17 08:44:27'),
(289, '1165', 'Yes', '2015-12-17', '2015-12-17 08:45:07'),
(290, '1277', 'Yes', '2015-12-17', '2015-12-17 08:46:46'),
(291, '1337', 'Yes', '2015-12-17', '2015-12-17 08:47:14'),
(292, '1389', 'Yes', '2015-12-17', '2015-12-17 08:47:46'),
(293, '1418', 'Yes', '2015-12-17', '2015-12-17 08:52:15'),
(294, '1666', 'Yes', '2015-12-17', '2015-12-17 08:53:16'),
(295, '1782', 'Yes', '2015-12-17', '2015-12-17 08:54:13'),
(296, '1862', 'Yes', '2015-12-17', '2015-12-17 08:57:20'),
(297, '1896', 'Yes', '2015-12-17', '2015-12-17 08:57:54'),
(298, '1925', 'Yes', '2015-12-17', '2015-12-17 08:58:30'),
(299, '2104', 'Yes', '2015-12-17', '2015-12-17 08:59:08'),
(300, '2134', 'Yes', '2015-12-17', '2015-12-17 09:01:39'),
(301, '2425', 'Yes', '2015-12-17', '2015-12-17 09:02:46'),
(302, '2602', 'Yes', '2015-12-17', '2015-12-17 09:03:19'),
(303, '2607', 'Yes', '2015-12-17', '2015-12-17 09:30:51'),
(304, '2659', 'Yes', '2015-12-17', '2015-12-17 10:03:45'),
(305, '5163', 'Yes', '2015-12-17', '2015-12-17 10:06:18'),
(306, '5010', 'Yes', '2015-12-17', '2015-12-17 10:07:15'),
(307, '4649', 'Yes', '2015-12-17', '2015-12-17 10:09:33'),
(308, '4647', 'Yes', '2015-12-17', '2015-12-17 10:10:22'),
(309, '4636', 'Yes', '2015-12-17', '2015-12-17 10:11:58'),
(310, '4503', 'Yes', '2015-12-17', '2015-12-17 10:13:07'),
(311, '4494', 'Yes', '2015-12-17', '2015-12-17 12:17:09');

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

DROP TABLE IF EXISTS `survey`;
CREATE TABLE `survey` (
  `id` int(11) NOT NULL,
  `lid` int(50) NOT NULL,
  `reference` varchar(250) NOT NULL DEFAULT '0',
  `stage` varchar(250) NOT NULL DEFAULT '0',
  `interviewer` varchar(200) NOT NULL,
  `cs` varchar(10) DEFAULT NULL,
  `Q1` varchar(100) DEFAULT NULL,
  `dialledno` varchar(255) DEFAULT NULL,
  `Q2` varchar(100) DEFAULT NULL,
  `Q3` varchar(100) DEFAULT NULL,
  `Q4` varchar(100) DEFAULT NULL,
  `Q4_comments` varchar(100) DEFAULT NULL,
  `Q5_a` varchar(255) DEFAULT NULL,
  `Q5_b` varchar(255) DEFAULT NULL,
  `Q5_c` varchar(255) DEFAULT NULL,
  `Q5_d` varchar(255) DEFAULT NULL,
  `Q5_d_1` varchar(255) DEFAULT NULL,
  `service_issue_other_reason` varchar(255) DEFAULT NULL,
  `Q5_d_2` varchar(255) DEFAULT NULL,
  `content_issue_other_reason` varchar(255) DEFAULT NULL,
  `Q5_d_3` varchar(255) DEFAULT NULL,
  `competitor_other_reason` varchar(255) DEFAULT NULL,
  `Q5_e` varchar(255) DEFAULT NULL,
  `subs` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `phone1` varchar(100) DEFAULT NULL,
  `phone2` varchar(30) DEFAULT NULL,
  `phone3` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `previous_package` varchar(50) DEFAULT NULL,
  `previous_package_speed` varchar(100) DEFAULT NULL,
  `current_package` varchar(100) DEFAULT NULL,
  `current_package_speed` varchar(100) DEFAULT NULL,
  `payment_date` varchar(255) DEFAULT NULL,
  `cycle` varchar(255) DEFAULT NULL,
  `amount_due` varchar(50) DEFAULT NULL,
  `amount_in` varchar(50) DEFAULT NULL,
  `amount_to_pay` varchar(50) DEFAULT NULL,
  `callbacktime` varchar(200) DEFAULT NULL,
  `disposition` text,
  `disptype` text,
  `datemail` date DEFAULT NULL,
  `escalation` varchar(255) DEFAULT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `duplicate_date` varchar(250) DEFAULT NULL,
  `start_time` varchar(200) DEFAULT NULL,
  `stop_time` varchar(50) DEFAULT NULL,
  `last_page` varchar(250) DEFAULT NULL,
  `redial_times` int(11) DEFAULT NULL,
  `audio_match` varchar(250) DEFAULT NULL,
  `audio_match_by` varchar(250) DEFAULT NULL,
  `quality` varchar(250) DEFAULT NULL,
  `quality_by` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `int_id` int(11) NOT NULL,
  `user` varchar(150) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`int_id`, `user`, `username`, `password`, `level`) VALUES
(1, 'Administrator', 'admin', '12345', 'Admin'),
(2, 'Balaji Padmanabhan', 'balaji.padmanabhan@technobraingroup.com', 'balaji', 'Admin'),
(3, 'Agent Test', 'test', 'test', 'Agent');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attempts`
--
ALTER TABLE `attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attempts_list`
--
ALTER TABLE `attempts_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qcfiles`
--
ALTER TABLE `qcfiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qc_data`
--
ALTER TABLE `qc_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state_tbl`
--
ALTER TABLE `state_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`int_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attempts`
--
ALTER TABLE `attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attempts_list`
--
ALTER TABLE `attempts_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qcfiles`
--
ALTER TABLE `qcfiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qc_data`
--
ALTER TABLE `qc_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `state_tbl`
--
ALTER TABLE `state_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;
--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `int_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
